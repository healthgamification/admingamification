<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;

class QuestionSetMapping extends Model
{
    //
    use SoftDeletes;
    protected $guarded = ['id'];
    
    public function question()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }

}
