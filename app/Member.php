<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class Member extends Model
{
	use SoftDeletes;
	
protected $fillable= [
	'member_name',
	'member_email',
	'organization_id',
	'member_department',
	'member_designation',
	'password',
	'organization_name'
];

public function department()
{
	return $this->hasOne(DepartmentModel::class,'id','member_department');
}
public function designation()
{
	return $this->hasOne(DesignationModel::class,'id','member_designation');
}
   
}
