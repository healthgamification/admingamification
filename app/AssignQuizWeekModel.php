<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignQuizWeekModel extends Model
{
    //
    protected $table ='org_week_quiz_qssigns';
     protected $guarded = ['id'];

public function week()
{
	return $this->hasOne(AssignWeekModel::class,'id','week_id');
}

public function set()
{
	return $this->hasOne(QuestionSet::class,'id','quiz_set_id');
}
}
