<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignChallengeWeekModel extends Model
{
    //
    protected $table ='org_week_challenge_assigns';
     protected $guarded = ['id'];

public function week()
{
	return $this->hasOne(AssignWeekModel::class,'id','week_id');
}

public function set()
{
	return $this->hasOne(ChallengeSets::class,'id','challenge_set_id');
}
}
