<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class DepartmentModel extends Model
{
    //
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $table='department';

    protected $appends = ['organization_name'];

    public function getOrganizationNameAttribute()
    {
       $org = Organization::where("id",$this->organization_id);
       return $org->count()>0?$org->first()->name:'';
    }
}
