<?php

namespace App;

use App\Http\Helpers\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //
    use SoftDeletes;

    public function player()
    {
    	return $this->hasMany(TeamPlayer::class,'team_id','id')->where("status","Active");
    }
}
