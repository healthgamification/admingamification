<?php

namespace App;

use App\Http\Helpers\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class TeamPlayer extends Model
{
    //
    use SoftDeletes;
    protected $table ='team_players';
    public function team()
    {
        return $this->hasOne(Team::class,'id','team_id');
    }

    public function player()
    {
        return $this->hasOne(Member::class,'id','memebr_id');
    }
}
