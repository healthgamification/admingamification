<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DigitValue implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    private $min,$max; 
    public function __construct($min, $max)
    {
        //
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
     public function passes($attribute, $value)
    {
        //
        if($value >= $this->min && $value <= $this->max){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This field should be between '.$this->min.' to '.$this->max.'.';
    }
}
