<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class Organization extends Model
{
    //
    use SoftDeletes;

    public function teams()
    {
    	return $this->hasMany(Team::class);
    }

    public function users()
    {
    	return $this->hasMany(User::class,'org_id','id');
    }

    public function members()
    {
    	return $this->hasMany(Member::class);
    }
}
