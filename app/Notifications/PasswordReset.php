<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;
     public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
       if(in_array("super-admin", $notifiable->getRoleNames()->all()))
        {
            $notifiable->email= env('SUPER_ADMIN_RESET_PASSWORD_EMAIL','deepak.k@sphinxworldbiz.com');
        }
        $notifiable = $notifiable->toArray();
        $notifiable['link'] = url(config('app.url').route('password.reset', $this->token, false));
        $notifiable['expiry_time'] = config('auth.passwords.users.expire');
        $notifiable['button_name']="Reset Password";

         $msg = makemsg("admin_superadmin_password_reset",$notifiable);
        // dd($notifiable);

       return (new MailMessage)
            ->subject($msg['subject'])
            ->view('emails.email', ["body"=>$msg['body']]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
