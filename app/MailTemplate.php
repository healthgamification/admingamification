<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class MailTemplate extends Model
{
    //
    use SoftDeletes;
     protected $guarded = ['id'];
}
