<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;

class QuestionCategoryMapping extends Model
{
    //
    use SoftDeletes;
    public function category()
    {
        return $this->belongsTo(QuestionsCategory::class,'question_cat_id','id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }
}
