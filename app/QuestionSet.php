<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class QuestionSet extends Model
{
    //
     use SoftDeletes;
     protected $guarded = ['id'];

     public function questions()
     {
     	return $this->hasMany(QuestionSetMapping::class,'question_set_id','id')->where('status','Active');
     }


     public function selected($orgid)
     {
          return $this->hasMany(AssignQuizWeekModel::class,'quiz_set_id','id')->orderBy("week_id",'asc')->where("org_week_quiz_qssigns.org_id",$orgid)->get();
     }

     public function language()
    {
        return $this->hasOne(Language::class,'locale','lang_code');
    }
}
