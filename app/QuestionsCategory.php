<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;

class QuestionsCategory extends Model
{
	use SoftDeletes;
    protected $table = 'questions_category';

     public function questions()
    {
    	return $this->hasMany(QuestionCategoryMapping::class,'question_cat_id','id');
    }
}
