<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class Question extends Model
{
    //
    use SoftDeletes;
    protected $guarded = ['id'];
    public function category()
    {
    	return $this->belongsto(QuestionsCategory::class);
    }

    public function categories()
    {
    	return $this->hasMany(QuestionCategoryMapping::class,'question_id','id')->where('status','Active');
    }

    public function selected()
    {
    	return $this->hasMany(QuestionSetMapping::class,'question_id','id');
    }

    public function language()
    {
        return $this->hasOne(Language::class,'locale','lang');
    }
}
