<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\Http\Helpers\Upload;
use Avatar;

use App\User;
use Lang;

class ProfileController extends Controller
{

    public function profile()
    {
        $user = Auth::user();
        $max_file_size = ini_get('post_max_size');
        return view('profile/profile',compact('user','max_file_size'))->with('userRole', DashboardController::getCurrentUserRole());
    }

    public function updateprofile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email'
        ]);

        if(!empty($request->profilrimage)){
             $this->uploadAvatarAuthUser($request);
        }

        $user = User::find(Auth::id());

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        $msg = __(Lang::locale().'.PROFILE_EDIT_MSG');
         return redirect('/profile')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    public function resetimage(Request $request)
    {
        $user = $this->removeAvatarAuthUser();
       return response($user->avatar_url, 200);
    }

    public function getpassword()
    {
       return view('profile/password')->with('userRole', DashboardController::getCurrentUserRole());
    } 

    public function postpassword(Request $request)
    {
        $this->validate($request, [
            'current' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string'
        ]);

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return redirect('/password')->withInput()->withErrors(['current'=> 'Current password does not match'])->with('userRole', DashboardController::getCurrentUserRole());
        }

        $user->password = Hash::make($request->password);
        $user->save();
        $msg = __(Lang::locale().'.PROFILE_PASSWORD_EDIT_MSG');
       return redirect('/password')->with('message',$msg)->with('userRole', DashboardController::getCurrentUserRole());
    }
    
    public function getAuthUser ()
    {
        return Auth::user();
    }

    public function updateAuthUser (Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.Auth::id()
        ]);

        $user = User::find(Auth::id());

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);

        return $user;
    }

    public function updatePasswordAuthUser(Request $request)
    {
        $this->validate($request, [
            'current' => 'required|string',
            'password' => 'required|string|confirmed',
            'password_confirmation' => 'required|string'
        ]);

        $user = User::find(Auth::id());

        if (!Hash::check($request->current, $user->password)) {
            return response()->json(['errors' => ['current'=> ['Current password does not match']]], 422);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        return $user;
    }

    public function uploadAvatarAuthUser(Request $request)
    {
        $upload = new Upload();
        $avatar = $upload->upload($request->profilrimage, 'avatars/'.Auth::id())->resize(200, 200)->getData();

        $user = User::find(Auth::id());
        $user->avatar = $avatar['basename'];
        $user->save();

        return $user;
    }

    public function removeAvatarAuthUser()
    {
        $user = User::find(Auth::id());
        $user->avatar = 'avatar.png';
        $user->save();

        return $user;
    }
}
