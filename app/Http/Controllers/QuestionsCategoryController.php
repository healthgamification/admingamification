<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Input;
use Lang;
use DB;
use Validator;
use App\Answers;
use App\QuestionsCategory;
use App\Question;
use App\QuestionSetMapping;
use App\QuestionCategoryMapping;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers\Log;
use App\QueCatLog;

/*
Questions CAtegory Constroller handles all the operations reqarding question bank Categories
*/
class QuestionsCategoryController extends Controller
{
    /*
    Method Name: index
	Operation: Display All Categories
	Date: 13th Sep 2019
	By: ASK
    */
    protected $perpage=10;

    public function index()
    {  
        $currentUserRole= DashboardController::getCurrentUserRole();
        $currentUserId= Auth::user()->id;
        $perpage = $this->perpage;
        if ($currentUserRole=='admin') {
        	redirect ('dashboard');
        } else {
            $data = QuestionsCategory::select('*')->orderBy('created_at', 'desc')->paginate($perpage);
        }
        return view('qcategory/index',compact('data','perpage'))->with('userRole', DashboardController::getCurrentUserRole());
    }

    /*
	 Method Name: create new category
	Operation: Display category form
	Date: 13th Sep 2019
	By: ASK
    */
    public function create()
    {
        return view('qcategory/create',compact(0))->with('userRole', DashboardController::getCurrentUserRole());
    }

    /*
	 Method Name: store new category
	Operation: Display category form
	Date: 13th Sep 2019
	By: ASK
    */

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:questions_category,category_name',
            'status' => 'required'
           
        ], [
        'name.required' => ' The name field is required.',
        'category_name.unique' => 'The Category Name has been already taken.',
    	]);
     
        $questionCategory = new QuestionsCategory();
        $questionCategory->category_name = $request->name;
        $questionCategory->status = $request->status;
        $questionCategory->save();
        $msg=__(Lang::locale().'.QUESTION_CATEGORY_CREATE_MSG');
        Log::create_log(new QueCatLog(),'create',["userid"=>Auth::id(),"msg"=>"New Category Added", "createdid"=>$questionCategory->id]);
        return redirect('/qcategory')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    /*
	 Method Name: Ajax Search in List Category Data
	Operation: Display category form
	Date: 13th Sep 2019
	By: ASK
    */

    public function fetch_data($perpage,Request $request)
    {
        if ($request->ajax()) {
            echo $sort_by = $request->get('sortby');
            echo $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $data = QuestionsCategory::where('category_name', 'like', '%'.$query.'%')
                    ->orWhere('status', 'like', '%'.$query.'%')
                    ->orderBy($sort_by, $sort_type)
                    ->paginate($perpage);
          
            return view('qcategory/search_data', compact('data','perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
        }
    }

     /*
	 Method Name: Edit 
	Operation: Display Edit Category form
	Date: 13th Sep 2019
	By: ASK
    */
    public function edit($id)
    {
        $questionCategoryData = QuestionsCategory::find($id);
        return view('qcategory/edit')->with('questionCategoryData', $questionCategoryData)->with('userRole', DashboardController::getCurrentUserRole());
    }

    /*
	Method Name: update 
	Operation: Save Edit Category form
	Date: 13th Sep 2019
	By: ASK
    */
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:questions_category,category_name,'.$request->id,
            'status' => 'required'
        ], [
            'name.required' => ' The name field is required.',
            'category_name.unique' => 'The Category Name has been already taken.',
        ]);
        $qcat = QuestionsCategory::find($id);
        QuestionsCategory::where('id', $id)
        ->update(['category_name' => $request->input('name'),'status' => $request->input('status')]);
        $old_status = $qcat->status;
        if($old_status != $request->status){
            $que_cat_map_all_data = QuestionCategoryMapping::where('question_cat_id',$id)->get();
            $que_cat_map_data = QuestionCategoryMapping::select('question_id')->whereIn('question_id',$que_cat_map_all_data->pluck('question_id'))->groupBy('question_id')->havingRaw('count(question_id) = 1')->get();
            
            QuestionCategoryMapping::where('question_cat_id',$id)->where('status',"!=",'Deleted')->update(['status'=>$request->input('status')]);
            $question_data = Question::whereIn('id',$que_cat_map_data->pluck("question_id"))->get();
            Question::whereIn('id',$question_data->pluck("id"))->where('status',"!=",'Deleted')->update(['status'=>$request->input('status')]);
            $ques_set_map_data = QuestionSetMapping::whereIn('question_id',$question_data->pluck("id"))->get();
            QuestionSetMapping::whereIn('question_id',$question_data->pluck("id"))->where('status',"!=",'Deleted')->update(['status'=>$request->input('status')]);
        }
        $msg=__(Lang::locale().'.QUESTION_CATEGORY_EDIT_MSG');
        Log::create_log(new QueCatLog(),'update',["userid"=>Auth::id(),"msg"=>"update category","catid"=>$id,"updatedata"=>$request->all()]);
        return redirect('/qcategory')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    /*
	Method Name: destroy 
	Operation: Delete Category
	Date: 13th Sep 2019
	By: ASK
    */
    public function destroy($id)
    {
        $data = QuestionsCategory::find($id);
        QuestionsCategory::destroy($id);
        $que_cat_map_all_data = QuestionCategoryMapping::where('question_cat_id',$id)->get();
        $que_cat_map_data = QuestionCategoryMapping::select('question_id')->whereIn('question_id',$que_cat_map_all_data->pluck('question_id'))->groupBy('question_id')->havingRaw('count(question_id) = 1')->get();         
        QuestionCategoryMapping::destroy(QuestionCategoryMapping::where('question_cat_id',$id)->get()->pluck("id"));
        $question_data = Question::whereIn('id',$que_cat_map_data->pluck("question_id"))->get();
        Question::destroy($question_data->pluck("id"));
        $ques_set_map_data = QuestionSetMapping::whereIn('question_id',$question_data->pluck("id"))->get();
        QuestionSetMapping::destroy($ques_set_map_data->pluck("id"));  
        
        $msg=__(Lang::locale().'.QUESTION_CATEGORY_DELETE_MSG');
        Log::create_log(new QueCatLog(),'delete',["userid"=>Auth::id(),"msg"=>"Delete category","catid"=>$id,"data"=>$data,"que_cat_map_data"=>$que_cat_map_data,"question_data"=>$question_data,"que_set_map_data"=>$ques_set_map_data]);
        return redirect('/qcategory')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Display full detail of Question Category.
     *
     * @param  \App\QuestionsCategory  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $questionCategoryData = QuestionsCategory::find($id);
            if(!empty($questionCategoryData)){
                return view('qcategory/view',compact('questionCategoryData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/qcategory')->with('message',$msg);
            }
        }
        else{
            return redirect('/dashboard');
        }
        
    }
}
