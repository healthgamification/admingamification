<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Lang;
use App\Organization;
use App\ChallengeCategory;
use App\Http\Controllers\DashboardController;
class ExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view('export/index')->with('userRole',DashboardController::getCurrentUserRole());
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'type'=>'required'
        ]);      

       // dd($request);

        $type = $request->type;
        if($type=="1")
        {
              $file = 'downloadfiles/members.csv';
            if (file_exists($file)) {
                $this->downloadSampleFile($file);
            }else{
                $msg=__(Lang::locale().'.EXPORT_ERROR_MSG');
                return redirect('/export')->with('message', $msg);                
            }
           // $this->downloadSampleFile();
        } 
        else if($type=="2"){
            $this->downloadOgranization();
        }else if($type=="3")
        {
             $file = 'downloadfiles/Challenge.csv';
            if (file_exists($file)) {
                $this->downloadSampleFile($file);
            }else{
                $msg=__(Lang::locale().'.EXPORT_ERROR_MSG');
                return redirect('/export')->with('message', $msg);                
            }
        }else if($type=='4')
        {
              $this->downloadCategories();
        }
        else if($type=='5'){
            $file = 'downloadfiles/Question.csv';
            if (file_exists($file)) {
                $this->downloadSampleFile($file);
            }else{
                $msg=__(Lang::locale().'.EXPORT_ERROR_MSG');
                return redirect('/export')->with('message', $msg);                
            }
        }

    }


    private function downloadSampleFile($filename)
    {
       $filedata = file_get_contents($filename);

    // SUCCESS
       if ($filedata)
       {
        // GET A NAME FOR THE FILE
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename='.basename($filename));
            $fp = fopen("php://output", "w");
            
            if (($handle = fopen($filename, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                    fputcsv($fp, $data);
                }
                fclose($handle);
            }
           
            fclose($fp);
            header('Set-Cookie: filedownloaded=true'); 
            header("Pragma: no-cache");
            $msg=__(Lang::locale().'.EXPORT_MSG');
            return redirect('/export')->with('message', $msg);   
        }

    // FAILURE
        else
    {
     $msg=__(Lang::locale().'.EXPORT_ERROR_MSG');
     return redirect('/export')->with('message', $msg); 
 }
}

private function downloadOgranization()
{
 $data = Organization::orderBy("name",'asc')->get(['id','name']);
 $html ='<table> <thead> <tr> <th>Organization ID</th> <th> Organization Name</th> </tr> </thead> <tbody>';
 foreach($data as $org){
    $html.='<tr><td>'.$org->id.'</td><td>'.$org->name.'</td></tr>';
}
$html .='</tbody> </table>';
header('Content-Type: application/ms-excel');
header('Content-Disposition: attachment; filename=organization.xls');
header('Pragma: no-cache');
echo $html;
header('Set-Cookie: filedownloaded=true'); 
header("Pragma: no-cache");
flush();
$msg=__(Lang::locale().'.EXPORT_MSG');
        return redirect('/export')->with('message', $msg);   
}

private function downloadCategories()
{
 $data = ChallengeCategory::orderBy("name",'asc')->get(['id','name']);
 $html ='<table> <thead> <tr> <th>Challenge Category ID</th> <th>Name</th> </tr> </thead> <tbody>';
 foreach($data as $cat){
    $html.='<tr><td>'.$cat->id.'</td><td>'.$cat->name.'</td></tr>';
}
$html .='</tbody> </table>';
header('Content-Type: application/ms-excel');
header('Content-Disposition: attachment; filename=challenge_category.xls');
header('Pragma: no-cache');
echo $html;
header('Set-Cookie: filedownloaded=true'); 
header("Pragma: no-cache");
flush();
$msg=__(Lang::locale().'.EXPORT_MSG');
        return redirect('/export')->with('message', $msg);   
}

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


}
