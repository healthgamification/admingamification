<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\Organization;
use App\Member;
use App\User;
use Avatar;
use Illuminate\Validation\Rule;
use App\Http\Controllers\DashboardController;
use App\Memberlog;
use App\TeamPlayer;
use App\Http\Helpers\Log;
use Session;

use App\DepartmentModel;
use App\DesignationModel;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $perpage=25;
    public function index($organizationId=NULL)
    {   $perpage= $this->perpage;
        $members = array();
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $organization_data = Organization::where('status','Active')->orderBy('name','asc')->get();
        } else {
            $organizationId=DashboardController::getCurrentAdminUserOrganizationId();
            $organization_data = Organization::where('id',$organizationId)->orderBy('name','asc')->get();
        }
        if($organizationId != NULL)
        {
            $members = Member::where('organization_id',$organizationId)->orderBy('member_name', 'asc')->paginate($perpage);
        }
        else{
            $members = Member::orderBy('member_name', 'asc')->paginate($perpage);
        }
       
        return view('member/index', compact('members','perpage','organization_data','organizationId'))->with('currentpage', 0);
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create($organizationId=NULL)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $organizations =  Organization::select('name', 'id')->orderBy("name","asc")->where('status', 'Active')->get();
            $departments =array(); //DepartmentModel::orderBy("name","asc")->get();
            $designations =array(); //DesignationModel::orderBy("name","asc")->get();
            if(!empty($organizationId))
            {
                $departments =DepartmentModel::orderBy("name","asc")->where("organization_id",$organizationId)->get();
                $designations =DesignationModel::orderBy("name","asc")->where("organization_id",$organizationId)->get();
            }
        } else {
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
         
            $organizations = Organization::select('name', 'id')->orderBy("name","asc")->where('status', 'Active')->where('id', $orgId->org_id)->get();
            $departments =DepartmentModel::orderBy("name","asc")->where("organization_id",$orgId->org_id)->get();
            $designations =DesignationModel::orderBy("name","asc")->where("organization_id",$orgId->org_id)->get();
        }
        return view('member/create',compact('organizationId','designations','departments'))->with('organizations', $organizations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'member_name' => 'required',
            'member_email' => 'required|email|unique:members,member_email',
            'organization_id' => 'required',
            'status' => 'required'
           
        ], [
            'member_name.required' => ' The name field is required.',
            'organization_id.required' => 'Select organization field is required.',
            'member_email.required' => 'The member email id field is required.',
            'member_email.unique' => 'The member email id has been already taken.',
        ]);
        $member = new Member();
        $org_data = DB::table('organizations')->select('name','no_of_quizzes','no_of_challenge')->where('id', request('organization_id'))->first();
        $no_of_challenge = (isset($org_data->no_of_challenge))?$org_data->no_of_challenge:0;
        $no_of_quizzes = (isset($org_data->no_of_quizzes))?$org_data->no_of_quizzes:0;
        $member->member_name = request('member_name');
        $member->member_email = request('member_email');
        $member->organization_id = request('organization_id');
        $member->password =  Hash::make('123');
        $member->member_department = request('member_department');
        $member->member_designation = request('member_designation');
        $member->status = request('status');
        /* Update No of Quizzes and No of Challenges on 23rd Sep 2019 by Atul */
        $member->no_of_quizzes = (request('no_of_quizzes') != '')?request('no_of_quizzes'):$no_of_quizzes;
        $member->no_of_challenge = (request('no_of_challenge') != '')?request('no_of_challenge'):$no_of_challenge;        
        $member->organization_name = $org_data->name;

        // $member->avatar = Avatar::create($member->member_name)->getImageObject()->encode('png');
        $member->save();
        send_mail($member->member_email,'create_member',$member->toArray());
        Log::create_log(new Memberlog(), 'create', ["userid"=>Auth::id(),"msg"=>"Create New Member","memberid"=>$member->id]);
        // Storage::put('avatars/'.$member->id.'/avatar.png', (string) $member->avatar);
        $msg=__(Lang::locale().'.MEMBER_CREATE_MSG');
        //return redirect('/members')->with('message', $msg);
         // print_r($organizations); //exit;
        if($request->btnvalue =="new"){                
            return redirect('member/create/'.$member->organization_id)->with('message', $msg)->with('message_type','success');
        }else{            
            return redirect('members/'.$member->organization_id)->with('message', $msg)->with('message_type','success');
        }
    }

    
    /**
     * Display full detail of single member.
     *
     * @param  \App\MemberModel  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $memberData = Member::find($id);
        } else {
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $organizations =  Organization::select('name', 'id')->where('id', $orgId->org_id)->get();
            $memberData = Member::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!$memberData)
        {
            return redirect("/members")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        else{
            $teamPlayerData = TeamPlayer::where('memebr_id',$id)->where('organization_id',$memberData->organization_id)->orderBy('id','DESC')->first();
                return view('member/view',compact('memberData','teamPlayerData'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $organizations =  Organization::select('name', 'id')->where('status', 'Active')->get();
            $memberData = Member::find($id);
        }
        else
        {
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $organizations =  Organization::select('name', 'id')->where('id', $orgId->org_id)->get();       
            $memberData = Member::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!$memberData)
        {
            return redirect("/members")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        $departments =DepartmentModel::orderBy("name","asc")->where("organization_id",$memberData->organization_id)->get();
        $designations =DesignationModel::orderBy("name","asc")->where("organization_id",$memberData->organization_id)->get();
        $images = scandir('avatars',0);
        unset($images[0]);
        unset($images[1]);
        return view('member/edit',compact('images','departments','designations'))->with('memberData', $memberData)->with('organizations', $organizations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'member_name' => 'required',
            'member_email' => 'required|unique:members,member_email,'.$request->id,
            'status' => 'required',
            'profile_image'=>'required'
           
        ], [
            'member_name.required' => ' The name field is required.',
            'member_email.unique' => 'The member email id has been already taken.',
        ]);
       $old=Member::find($id);     
         
        /* Update No of Quizzes and No of Challenges on 23rd Sep 2019 by Atul */  
        $member = Member::find($id);
        $member->member_name =$request->input('member_name');
        $member->avatar =$request->input('profile_image');
        $member->member_email =$request->input('member_email');
        $member->member_designation =$request->input('member_designation');
        $member->member_department =$request->input('member_department');
        $member->status =$request->input('status');
         if (DashboardController::getCurrentUserRole()!='admin') {
        $no_of_challenge = ($request->input('no_of_challenge') !='')?$request->input('no_of_challenge'):0;
        $no_of_quizzes = ($request->input('no_of_quizzes') !='')?$request->input('no_of_quizzes'):0; 
        $member->no_of_quizzes = $no_of_quizzes;
        $member->no_of_challenge = $no_of_challenge;
        }  
        $member->save();    
        
            if($old->status!=$request->status)
            {
            TeamPlayer::where("memebr_id",$id)->update(['status'=>$request->status]);
        }
        $msg=__(Lang::locale().'.MEMBER_EDIT_MSG');
      Log::create_log(new Memberlog(),'update',["userid"=>Auth::id(),"msg"=>"Update Member","memberid"=>$id,"memberold"=>$old]);
       return redirect('/members/'.request('organization_id'))->with('message',$msg)->with('message_type','success');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (DashboardController::getCurrentUserRole()=='super-admin') {
            $member = Member::find($id);
        } else {            
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $member = Member::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!$member)
        {
            return redirect("/members")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        if($member->is_admin == 1){
            User::where("org_id", $member->organization_id)->where('email',$member->member_email)->forceDelete();
        }
        $member->delete();

        TeamPlayer::where("memebr_id",$member->id)->update(['status'=>"Deleted"]);
        $msg=__(Lang::locale().'.MEMBER_DELETE_MSG');
        Log::create_log(new Memberlog(), 'delete', ["userid"=>Auth::id(),"msg"=>"Delete Member","memberid"=>$id,"memberold"=>$member]);
        return redirect('/members/'.$member->organization_id)->with('message', $msg)->with('message_type','success');
    }


    public function fetch_data($organizationId=null,Request $request)
    {

        if (DashboardController::getCurrentUserRole()=='admin' && $organizationId != 0) {
           $organizationId =  User::select('org_id')->where('id', auth()->user()->id)->first()->org_id;
        }
        
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);

            if (DashboardController::getCurrentUserRole()=='admin') {
                $organizationId=DashboardController::getCurrentAdminUserOrganizationId(); 
            } else {
            }

            $members = Member::query();
            if($organizationId != 0){
                $members->where('organization_id', $organizationId);
            }
            if(!empty($query))
            {
            $members->where(function ($q) use ($query) {
                $q->orWhere('member_name', 'like', '%'.$query.'%')
                ->orWhere('member_email', 'like', '%'.$query.'%')
                ->orWhere('organization_name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $members = $members->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
    
            // echo $request->get('page'); exit;
                  
            return view('member/search_data', compact('members','perpage','organizationId'))->with('currentpage', $request->get('page'));
        }
    }
    public function uploadcsv(Request $request)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);
        $allowExt = array('csv');
        $ext = $request->file->getClientOriginalExtension();
        if (in_array($ext, $allowExt)) {
            $handle = fopen($request->file->getRealPath(), 'r');
            
            $data=array();
            $row=1;
            $uploadedrow=0;
            $wrongemail=0;
            $wrongorg=0;
            $wrongdesig = 0;
            $wrongdepart = 0;
            $column = array('member_name','member_email','organization_id','member_department','member_designation','password');
            $required_column=array('member_name','member_email','organization_id');
            $inc=0;
            $validator = 0;
            $msg = '';
            $error_array = array();
            while (!feof($handle)) {
                $d = fgetcsv($handle, 1024, ',');
                $error_check = '';
                $desig = '';
                $depart = '';
                if ($row > 1 && !empty($d)) {
                    array_push($d, Hash::make('123'));
                    $data = array_combine($column, $d);
                    $r=0;
                    $desig = $data['member_designation'];
                    $depart = $data['member_department'];
                    foreach ($required_column as  $col) {
                        if (empty($data[$col])) {
                            $r++;
                        }
                    }
                    if($r==0){
                    if ($this->valid_email($data['member_email'])) {
                        $orgname = Organization::find($data['organization_id']);
                        if (isset($orgname->name)) { 
                            if($data['member_department'] != ''){
                                $check_valid_department = DepartmentModel::where(['organization_id'=>$data['organization_id'],'name'=>trim($data['member_department'])])->first();
                                if(empty($check_valid_department)){
                                    $error_check = 'Wrong Department Name';
                                    $wrongdepart++;
                                }
                                else{
                                    $data['member_department'] = $check_valid_department->id;
                                }
                            }
                            if($data['member_designation'] != ''){
                                $check_valid_designation = DesignationModel::where(['organization_id'=>$data['organization_id'],'name'=>trim($data['member_designation'])])->first();
                                if(empty($check_valid_designation)){
                                    $error_check = 'Wrong Designation Name';
                                     $wrongdesig++;
                                }
                                else{
                                     $data['member_designation'] = $check_valid_designation->id;
                                }
                            }        
                            if($error_check == ''){                    
                                $data['organization_name']=$orgname->name;
                                $MemberEmail = DB::table("members")->where('member_email', $data['member_email'])->first();
                                if ($MemberEmail) {
                                    $inc++;
                                    $error_check = 'Duplicate Email';
                                }
                                else{
                                    $Member = Member::firstOrCreate(['member_email' => $data['member_email']], $data);
                                        $error_check = 'Uploaded';
                                        send_mail($Member->member_email,'create_member',$Member->toArray());
                                        $uploadedrow++;
                                }
                            }
                                        
                        } else {
                            $wrongorg++;
                            $error_check = 'Organization ID is not mapped';
                        }
                    } else {
                        $wrongemail++;
                        $error_check = 'Wrong mail ID';
                    }
                }
                else{
                    $validator++;
                    $error_check = 'Validation Issue';
                }
                $error_array[] = array("member_name"=>$data['member_name'],"member_email"=>$data['member_email'],"organization_id"=>$data['organization_id'],"member_department"=>$depart,"member_designation"=>$desig,'type'=>$error_check); 
                }
               
                  
               
                $row++;
            }
            fclose($handle);
            Session::put('bulk_error',$error_array);
            $msg .='<a href="/log/bulkerrorlog" target="_blank"> Click Here</a> for upload report. <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>';
            echo Lang::get(Lang::locale().'.MEMBER_BULK_UPLOAD_NO_OF_DUPLICATE',['duplicate' => $inc,"wrongorg"=>$wrongorg,"wrongmail"=>$wrongemail,"wrongdesig"=>$wrongdesig,"wrongdepart"=>$wrongdepart,"validation"=>$validator,"uploaded"=>$uploadedrow]).$msg;
            Log::create_log(new Memberlog(), 'bulkupload', ["userid"=>Auth::id(),"msg"=>"Bulk Member upload","totalrow"=>($row-1),"duplicaterow"=>$inc,"uploadedrow"=>$uploadedrow,"wrongemail"=>$wrongemail,"wrongdesig"=>$wrongdesig,"wrongdepart"=>$wrongdepart,"wrongorg"=>$wrongorg]);
        } else {
            return response(['errors'=>['file'=>[__(Lang::locale().'.MEMBER_BULK_UPLOAD_ERROR_MSG')]]], 422)->header('Content-Type', 'application/json');
        }
    }

    private function valid_email($str)
    {
        return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? false : true;
    }

    public function downloadcsv(){
            
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename=bulkerror.csv');
            $fp = fopen("php://output", "w");
            fputcsv($fp, array('member_name', 'member_email', 'organization_id','member_department','member_designation','Status'));
            foreach(Session::get('bulk_error') as $row) {
                fputcsv($fp, array($row['member_name'], $row['member_email'], $row['organization_id'], $row['member_department'], $row['member_designation'], $row['type']));
            
            }
            fclose($fp);
            header('Set-Cookie: filedownloaded=true'); 
            header("Pragma: no-cache");

         
    }

    public function getDepartmentAndDesignation($orgid)
    {
        $departments =DepartmentModel::orderBy("name","asc")->where("organization_id",$orgid)->get();
        $designations =DesignationModel::orderBy("name","asc")->where("organization_id",$orgid)->get();
        $depart[] = '<option value="">Select Department</option>';
        foreach ($departments as $department) {
            $depart[]='<option value="'.$department->id.'">'.$department->name.'</option>';
        }

        $desi[] = '<option value="">Select Designation</option>';
        foreach ($designations as $designation) {
            $desi[]='<option value="'.$designation->id.'">'.$designation->name.'</option>';
        }
       return response(['department'=>$depart,"designation"=>$desi], 200)
                  ->header('Content-Type', 'application/json'); 
    }

}
