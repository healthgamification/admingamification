<?php
namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;


class SidebarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getCurrentUserRole()
    {
        $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
        return $currentUserRole[0]->name;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        return view('layouts/sidebar')->with('userRole', $this->getCurrentUserRole());
    }
   
}
