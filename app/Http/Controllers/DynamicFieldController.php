<?php

namespace App\Http\Controllers;

use App\DynamicField;
use Illuminate\Http\Request;
use Lang;
use Auth;

class DynamicFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage=50;
    private $field=array(
        "text"=>"Text",
        "select"=>"Select",
        "dbselect"=>"DB Select",
        "radio"=>"Radio",
        "checkbox"=>"Checkbox",
        "textarea"=>"Textarea",
        "datepicker"=>"Datepicker"
    );
    public function index()
    {
        //
        $dynamicfield = DynamicField::orderBy("priority","asc")->paginate($this->perpage);
        return view("dynamicfield.index",compact('dynamicfield'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view("dynamicfield.create",["fields"=>$this->field]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $this->validate($request,[
            'name'=>'required',
            'status'=>'required'
        ],
        ['name.required'=>"The field id required."]);
        $priority=DynamicField::all()->count();
        DynamicField::create([
            "name"=>$request->name,
            "label"=>$request->label,
            "label_attr"=>$request->label_attr,
            "type"=>$request->type,
            "options"=>json_encode($request->input('options',[])),
            "attr"=>$request->attr,
            "created_by"=>Auth::id(),
            "status"=>$request->status,
            "priority"=>$priority
        ]);
        if($request->btnvalue=="new")
        {
            return redirect()->back()->with("message",__(Lang::locale().'.DYNAMICFIELD_CREATE_MSG'));
        }        
        return redirect("/dynamicfields")->with("message",__(Lang::locale().'.DYNAMICFIELD_CREATE_MSG'));
    }

    /**
     * Display full detail of Language.
     *
     * @param  \App\Language  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $dynamicfield = DynamicField::find($id);
            if(!empty($dynamicfield)){
                return view('dynamicfield.view',compact('dynamicfield'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                 return redirect("/dynamicfields")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'));
            }
        }
        else{
            return redirect('/dynamicfields');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $dynamicfield = DynamicField::find($id);
         $fields = $this->field;
        return view("dynamicfield.edit",compact('dynamicfield','fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
         $this->validate($request,[
            'name'=>'required',
            'status'=>'required'
        ]);
        DynamicField::where('id',$id)->update([
            "name"=>$request->name,
            "label"=>$request->label,
            "label_attr"=>$request->label_attr,
            "type"=>$request->type,
            "options"=>json_encode($request->input('options',[])),
            "attr"=>$request->attr,
            "status"=>$request->status
        ]);   
       //$this->updateIndex($request->priority,$id);     
        return redirect("/dynamicfields")->with("message",__(Lang::locale().'.DYNAMICFIELD_UPDATE_MSG'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $dynamicfield = DynamicField::find($id);
       $dynamicfield->delete();
        return redirect()->back()->with('message',__(Lang::locale().'.DYNAMICFIELD_DELETE_MSG'));
    }

      public function fetch_data(Request $request)
    {
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
            $dynamicfield = DynamicField::orderBy("priority","asc");
            if(!empty($query))
            {
            $dynamicfield->where(function ($q) use ($query) {
                $q->orWhere('name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $dynamicfield = $dynamicfield->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
            return view('dynamicfield.search_data', compact('dynamicfield','perpage'))->with('currentpage', $request->get('page'));
        }
    }

public function updateIndex($index,$id)
{
    $index = (int) $index;
    $id = (int) $id;
    DynamicField::where("id",$id)->update(["priority"=>$index]);
    $data = DynamicField::where("priority",">=",$index)->where("id","<>",$id)->get();
    foreach ($data as $up) {
        $up->priority= ++$index;
        $up->save();
    }
}
}
