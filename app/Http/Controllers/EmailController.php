<?php

namespace App\Http\Controllers;

use App\MailConfig;
use Illuminate\Http\Request;
use Lang;
use App\MailConfigLog;
use App\Http\Helpers\Log;
use Auth;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $email = MailConfig::orderBy("updated_at","desc")->first();
        return view("email.index",compact('email'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "driver"=>'required',
            "host"=>'required',
            "port"=>'required',
            "from"=>'required',
            "encryption"=>'required',
            "username"=>'required',
            "password"=>'required',
        ]);
        $olddata = MailConfig::where("host", $request->host)->first();
        $mail = MailConfig::updateOrCreate(["host"=>$request->host],[
        "driver" => $request->driver??'smtp',
        "host" => $request->host,
        "port" => $request->port,
        "from" => json_encode(explode(":",$request->from)),
        "encryption" => $request->encryption,
        "username" => $request->username,
        "password" => $request->password,
        "sendmail" => $request->sendmail??'/usr/sbin/sendmail -bs',
        "pretend" => $request->pretend=='1'?true:false]);
        Log::create_log(new MailConfigLog(), 'update', ["userid"=>Auth::id(),"msg"=>"Update Mail Config","olddata"=>$olddata,"data"=>$request->input()]);
        $msg = __(Lang::locale().'.EMAIL_CREATE_MSG');
        return redirect()->back()->with("message",$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailConfig  $mailConfig
     * @return \Illuminate\Http\Response
     */
    public function show(MailConfig $mailConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MailConfig  $mailConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(MailConfig $mailConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailConfig  $mailConfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MailConfig $mailConfig)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailConfig  $mailConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(MailConfig $mailConfig)
    {
        //
    }

    public function testmail(Request $request)
    {
        $this->validate($request,[
            "testmail"=>'required'
        ]);

        $to = $request->testmail;
        send_mail($to,'test_mail',["date"=>date("d-m-Y h:i:s a")]);
        $msg = __(Lang::locale().'.EMAIL_TEST_SEND_MSG');
        return redirect()->back()->with("message",$msg);
    }
}
