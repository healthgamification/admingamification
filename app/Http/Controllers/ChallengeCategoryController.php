<?php
namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\ChallengeCategory;
use App\User;
use App\Challenge;
use Avatar;
use Illuminate\Validation\Rule;
use App\Http\Controllers\DashboardController;
use App\ChallengeSetMappings;
use App\Http\Helpers\Log;
use App\ChallengeLog;
class ChallengeCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // 
        $perpage=25;
        $data = ChallengeCategory::orderBy('name')->paginate($perpage);
        return view("challenge_category/index",compact('perpage','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('challenge_category/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
           
        ], [
            'name.required' => ' The Title field is required.',
            'status.required' => 'The Status is required.',

        ]);
        
            $ChallengeCategory = new ChallengeCategory();       
            $ChallengeCategory->name = request('name'); 
            $ChallengeCategory->status = request('status');
            $ChallengeCategory->description = request('description')??'';
            $ChallengeCategory->save();
            $msg=__(Lang::locale().'.CHALLENGE_CATEGORY_CREATE_MSG');
            Log::create_log(new ChallengeLog(), 'challenge_category_create', ["userid"=>Auth::id(),"msg"=>"New Challenge Category created.","id"=>$ChallengeCategory->id,"data"=>json_encode($ChallengeCategory)]);
            return redirect('/challenge-category')->with('message', $msg)->with('message_type','success');
    }
   
    /* Ajax search.*/
   
    function fetch_data(Request $request)
    { 
     if($request->ajax())
     {  $perpage=25;
        $sort_by = $request->get('sortby');
        $sort_type = $request->get('sorttype');
        $query = $request->get('query'); 
        $query = str_replace(" ", "%", $query);
        $data = ChallengeCategory::where('name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%')
                ->orderBy($sort_by, $sort_type)
                ->paginate($perpage);
                
      return view('challenge_category/search_data', compact('perpage','data'))->render();
     }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChallengeCategory  $challengeCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ChallengeCategory::find($id);
        if(!empty($data)){
            return view('challenge_category/edit',compact('data'));
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-category')->with('message',$msg)->with('message_type','danger');
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChallengeCategory  $challengeCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
           
        ], [
            'name.required' => ' The Title field is required.',
            'status.required' => 'The Status is required.',

        ]);

        $old= ChallengeCategory::find($id);
        $description = !empty($request->input('description'))?$request->input('description'):'';
       
        ChallengeCategory::where('id', $id)
        ->update(['name' => $request->input('name'),'description' => $description,'status' => $request->input('status')]);
        if($old->status != $request->input('status')){
            Challenge::where('challenge_category_id',$id)->where('challenge_status',"!=",'Deleted')->update(['challenge_status'=>$request->input('status')]);
            ChallengeSetMappings::whereIn('challenge_id',Challenge::where('challenge_category_id',$id)->where('challenge_status',"!=",'Deleted')->get()->pluck('id'))->where('status',"!=",'Deleted')->update(['status'=>$request->input('status')]);
        }
          
        Log::create_log(new ChallengeLog(), 'challenge_category_update', ["userid"=>Auth::id(),"msg"=>"Challenge Category updated.","id"=>$id,"olddata"=>json_encode($old),"newdata"=>json_encode($request->input())]);

        $msg=__(Lang::locale().'.CHALLENGE_CATEGORY_EDIT_MSG');
        return redirect('/challenge-category')->with('message',  $msg)->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChallengeCategory  $challengeCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {  
        $challenge = ChallengeCategory::find($id);
        if(!empty($challenge)){
            ChallengeCategory::destroy($id);
            $challenge_data = Challenge::where('challenge_category_id',$id)->get();
            Challenge::destroy($challenge_data->pluck('id'));
            $challenge_set_mapping_data = ChallengeSetMappings::whereIn('challenge_id',$challenge_data->pluck('id'))->get();
            ChallengeSetMappings::destroy($challenge_set_mapping_data->pluck('id'));
            Log::create_log(new ChallengeLog(), 'challenge_category_deleted', ["userid"=>Auth::id(),"msg"=>"Challenge Category deleted.","id"=>$id,"data"=>json_encode($challenge),"challenge_data"=>$challenge_data,"challenge_set_mapping_data"=>$challenge_set_mapping_data]);
            $msg=__(Lang::locale().'.CHALLENGE_CATEGORY_DELETE_MSG');
            return redirect('/challenge-category')->with('message',$msg)->with('message_type','success');
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-category')->with('message',$msg)->with('message_type','danger');
        }
    }

    /**
     * Display full detail of Challenge Category.
     *
     * @param  \App\ChallengeCategory  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $challengeCategoryData = ChallengeCategory::find($id);
        if(!empty($challengeCategoryData)){
            return view('challenge_category/view',compact('challengeCategoryData'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-category')->with('message',$msg)->with('message_type','danger');
        }   
    }
}
