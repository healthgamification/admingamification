<?php

namespace App\Http\Controllers;

use App\AssignWeekModel;
use Illuminate\Http\Request;
use App\Organization;
use App\QuestionSet;
use App\ChallengeSets;
use App\AssignQuizWeekModel;
use App\AssignChallengeWeekModel;
use App\AssignModel;
use Lang;
use App\Http\Helpers\Log;
use Auth;
class AssignQuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage=10;
    public function index($orgid)
    {
        //
        $org = Organization::where("id",$orgid)->first();
        if(!empty($org)){
            $quizdata=QuestionSet::where(["status"=>"Active","lang_code"=>$org->lang_code])->orderBy("title",'asc')->paginate($this->perpage);
            $challengedata=ChallengeSets::where(["status"=>"Active","lang_code"=>$org->lang_code])->orderBy("title",'asc')->paginate($this->perpage);
            $weeks = AssignWeekModel::where("org_id",$orgid)->orderBy("week_no",'asc')->paginate($this->perpage);
            // $selected = QuestionSet::where(["status"=>"Active","lang_code"=>$org->lang_code])->whereHas('selected',function($q){
            //     $q->where("status","Active");
            // })->orderBy("title",'asc')->paginate($this->perpage);
            return view("organization.assign_quiz",compact('org','quizdata','orgid','weeks','challengedata'));
        }
        $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
        return redirect("/organization")->with("message",$msg);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($orgid,Request $request)
    {
        //
        //dd($request);
        $this->validate($request,[
            "weekno.*"=>'required',
            "startdate.*"=>'required|array',
            "enddate.*"=>'required|array',
           // "quiz_id.*"=>'required',
        ],
        [
            //"quiz_id.*"=>'Please assign at least one quizz set.',
        ]
    );
        //dd($request);
        foreach ($request->weekno as $weekno) {
            $indata=array(
                "week_no"=>$weekno,
                "startdate"=>$request->weekstartdate[$weekno],
                "enddate"=>$request->weekenddate[$weekno],
                "org_id"=>$orgid,
            );

            //$week =  AssignWeekModel::create($indata);
            $week =  AssignWeekModel::updateOrCreate(["org_id"=>$orgid,"week_no"=>$weekno],$indata);
            $qids = !empty($request->quiz_id[$weekno])?explode(",", $request->quiz_id[$weekno]):[];
            $challengeids = !empty($request->challenge_id[$weekno])?explode(",", $request->challenge_id[$weekno]):[];
             AssignQuizWeekModel::where("week_id",$week->id)->delete();
            if(!empty($qids))
            {
               
                foreach ($qids as $qid) {
                    $quiz_data = array(
                        "org_id"=>$orgid,
                        "week_id"=>$week->id,
                        "quiz_set_id"=>$qid
                    );
                    AssignQuizWeekModel::create($quiz_data);
                }

            }
            AssignChallengeWeekModel::where("week_id",$week->id)->delete();
            if(!empty($challengeids))
            {
                
                foreach ($challengeids as $cid) {
                    $quiz_data = array(
                        "org_id"=>$orgid,
                        "week_id"=>$week->id,
                        "challenge_set_id"=>$cid
                    );
                    AssignChallengeWeekModel::create($quiz_data);
                }

            }

        }
        //send_mail($user->email,'assign_quizz_and_challenge',$user->toArray());
         Log::create_log(new AssignModel(), 'assign', ["userid"=>Auth::id(),"msg"=>"Assign Quizzess and Challenges in organization.","orgid"=>$orgid,"quizids"=>$request->quiz_id,"challengeids"=>$request->challenge_id,"week_no"=>json_encode($request->weekno)]);
        $msg = __(Lang::locale().'.ASSIGN_CREATE_MSG');
        return redirect(route("assign.quiz",$orgid))->with("message",$msg);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssignQuizModel  $assignQuizModel
     * @return \Illuminate\Http\Response
     */
    public function show(AssignWeekModel $assignQuizModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssignQuizModel  $assignQuizModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AssignWeekModel $assignQuizModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssignQuizModel  $assignQuizModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssignWeekModel $assignQuizModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssignQuizModel  $assignQuizModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssignWeekModel $assignQuizModel)
    {
        //
    }

    
     public function fetch_data($lang,$org_id, Request $request)
    {
            $perpage = $this->perpage;
        if ($request->ajax()) {
            $org = Organization::where("id",$org_id)->first();
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
        $quizdata =QuestionSet::where(["status"=>"Active","lang_code"=>$lang]);
        if(!empty($query))
            {
            $quizdata->where(function ($q) use ($query) {
                $q->orWhere('title', 'like', '%'.$query.'%')
                ->orWhere('description', 'like', '%'.$query.'%');
            }); 
            }
             $quizdata = $quizdata->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
        return view("organization.search_quiz_set_data",compact('lang','quizdata','org','perpage'))->with('currentpage', $request->get('page'));

        }
    }
    public function challengefetch_data($lang,$org_id,Request $request)
    {
            $perpage = $this->perpage;
        if ($request->ajax()) {
             $org = Organization::where("id",$org_id)->first();
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
        $challengedata =ChallengeSets::where(["status"=>"Active",'lang_code'=>$lang]);
        if(!empty($query))
            {
            $challengedata->where(function ($q) use ($query) {
                $q->orWhere('title', 'like', '%'.$query.'%')
                ->orWhere('description', 'like', '%'.$query.'%');
            }); 
            }
             $challengedata = $challengedata->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
        return view("organization.search_challenge_set_data",compact('lang','challengedata','org','perpage'))->with('currentpage', $request->get('page'));

        }
    }
}
