<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChallengeSets;
use App\ChallengeCategory;
use App\Challenge;
use App\ChallengeSetMappings;
use Lang;
use DB;
use App\Http\Controllers\DashboardController;

use App\Http\Helpers\Log;
use App\ChallengeLog;
use App\AssignChallengeWeekModel;
use Auth;
class ChallengeSetsController extends Controller
{
	private $perpage = 10;
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang='en'){
    	$perpage = $this->perpage;
    	$data = ChallengeSets::where('lang_code',$lang)->orderBy('title')->paginate($perpage);
    	$userRole=DashboardController::getCurrentUserRole();
        $languages = getLanguages();
    	return view("challenge/challengeset",compact('data','userRole','perpage','lang','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang='en'){
    	$perpage = $this->perpage;
        $data = Challenge::where("challenge_status","Active")->where('lang_code',$lang)
        ->whereHas('challenges',function($query){
                    $query->where("status","Active");
                })->orderBy('challenge_name','asc')->paginate($perpage);
        $languages = getLanguages();
    	$category_data = ChallengeCategory::where('status','Active')->orderBy('name')->get();
    	return view("challenge/challengeset_create",compact('category_data','data','perpage','lang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            "challenge_set_name"=>'required',
            "status"=>'required',
            "challenge_ids" =>'required|string'         
        ],
        [
            "challenge_ids.required"=>"Please select at least one question from below list.",
        ]);
        $set = ChallengeSets::create(
            [
                "title"=>$request->challenge_set_name,
                "description"=>isset($request->challenge_set_desc)?$request->challenge_set_desc:'',
                "status"=>$request->status,
                "lang_code" => $request->lang
            ]);
        $indata=array();
        foreach (explode(',',$request->challenge_ids) as $qid) {
            $indata[]=array(
                "challenge_set_id"=>$set->id,
                "challenge_id"=>$qid,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')
            );
        }
        ChallengeSetMappings::insert($indata);
        Log::create_log(new ChallengeLog(), 'challenge_set_create', ["userid"=>Auth::id(),"msg"=>"New Challenge Set created.","id"=>$set->id,"challenges"=>$request->challenge_ids]);
        $msg = __(Lang::locale().'.CHALLENGE_SET_CREATE_MSG');
        return redirect("/challenge-sets/".$request->lang)->with("message",$msg)->with('message_type','success');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChallengeSets $id  
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perpage = $this->perpage;
        $challenge_set = ChallengeSets::find($id);
        if(!empty($challenge_set))
        {
            $lang = $challenge_set->lang_code;
            $data = Challenge::where('lang_code',$lang)->where("challenge_status","Active")
            ->whereHas('challenges',function($query){
                        $query->where("status","Active");
                    })
            ->whereHas('selected',function($query) use ($id){
                        $query->where(["challenge_set_id"=>$id,"status"=>"Active"]);
                    })->orderBy('challenge_name','asc')->paginate($perpage);
            $category_data = ChallengeCategory::where('status','Active')->orderBy('name')->get();
            return view("challenge/challenge_set_edit",compact('challenge_set','category_data','lang','data'));
        }   
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-sets')->with('message',$msg)->with('message_type',"danger");
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $this->validate($request,[
            "challenge_set_name"=>'required',
            "challenge_ids" =>'required|string',
            "status"=>'required',         
        ],
        [
            "challenge_ids.required"=>"Please select at least one question from below list.",
        ]);
        $cset = ChallengeSets::find($request->set_id);
        $old_status = $cset->status;
        $cset->title=$request->challenge_set_name;
        $cset->description =isset($request->challenge_set_desc)?$request->challenge_set_desc:'';
        $cset->status=$request->status;
        $cset->lang_code=$request->lang;
        $cset->save();
        $indata=array();
        $oldchallenge = ChallengeSetMappings::where("challenge_set_id",$cset->id)->get()->pluck("challenge_id")->all();
         ChallengeSetMappings::where("challenge_set_id",$cset->id)->forceDelete();
        foreach (explode(',',$request->challenge_ids) as $cid) {
            $indata[]=array(
                "challenge_set_id"=>$cset->id,
                "challenge_id"=>$cid,
                "created_at" => date('Y-m-d H:i:s'),
                "updated_at" => date('Y-m-d H:i:s')

            );
        }
        ChallengeSetMappings::insert($indata);
        if($old_status!=$request->status)
        {
            AssignChallengeWeekModel::where("challenge_set_id",$cset->id)->update(['status'=>$request->status]);
        }
        Log::create_log(new ChallengeLog(), 'challenge_set_update', ["userid"=>Auth::id(),"msg"=>"Challenge Set updated.","id"=>$request->set_id,"olddata"=>json_encode($cset),"newdata"=>json_encode($request->input()),"oldchallenges"=>json_encode($oldchallenge),"challenges"=>$request->challenge_ids]);
        $msg = __(Lang::locale().'.CHALLENGE_SET_EDIT_MSG');
        return redirect("/challenge-sets/".$cset->lang_code)->with("message",$msg)->with('message_type',"success");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cset = ChallengeSets::find($id);
        if(!empty($cset)){
            $challenge_ids = ChallengeSetMappings::where("challenge_set_id",$id)->get();
            
            $cset->delete();
            ChallengeSetMappings::destroy($challenge_ids->pluck('id'));
             AssignChallengeWeekModel::where("challenge_set_id",$cset->id)->update(['status'=>"Deleted"]);
            Log::create_log(new ChallengeLog(), 'challenge_set_deleted', ["userid"=>Auth::id(),"msg"=>"Challenge Set deleted.","id"=>$id,"data"=>json_encode($cset),"challenges"=>$challenge_ids->pluck('challenge_id')]);
            $msg = __(Lang::locale().'.CHALLENGE_SET_DELETE_MSG');
            return redirect()->back()->with("message",$msg)->with('message_type',"success");
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-sets')->with('message',$msg)->with('message_type',"danger");
        }

    }


    /* Fetch data for challenge data */
     /**
     *  Display a listing of the challenge resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function fetch_data($lang='en',$type,$cat,$set, Request $request)
    {
       // $perpage = $this->perpage;
        if ($request->ajax()) {
            $perpage = $this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $q = trim($request->get('query'));
            $q = str_replace(" ", "%", $q);
            $data = Challenge::where('challenge_status','Active')->where('lang_code',$lang);
            
            if($cat!=0)
            {
               $data = $data->whereHas("challenges",function($query) use($cat) {
                    $query->where(["challenge_category_id"=>$cat,"status"=>"Active"]);
                });
            }else{
                $data = $data->whereHas("challenges",function($query) {
                    $query->where("status","Active");
                });
            }
            if($type==1){
                $data = $data->whereHas('selected',function($query) use($set){
                    $query->where(["challenge_set_id"=>$set,"status"=>"Active"]);
                });
            }
            else if($type==2){
                $data = $data->whereDoesntHave('selected',function($query) use($set){
                     $query->where(["challenge_set_id"=>$set,"status"=>"Active"]);
                });
            }

             if($q!='')
                    {
                    $data->where(function($query1) use($q){
                        $query1->where("challenge_name" ,'like', '%'.$q.'%');
                    });   
                    }                
             $data = $data->orderBy($sort_by, $sort_type);

            $data = $data->paginate($perpage);
              return view("challenge/search_challenge_data",compact('data','perpage'));            
        }

    }

    /* Fetch data for challenge set */
     /**
     *  Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_fetch_data($lang,Request $request){
         $perpage = $this->perpage;
      if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
            $data = ChallengeSets::where('lang_code',$lang);
            if($query!='')
            {
             $data->where('title', 'like', '%'.$query.'%')->orderBy($sort_by, $sort_type);
            }
            else{
              $data->orderBy($sort_by, $sort_type);
            }
             $data = $data->paginate($perpage);
        $userRole=$userRole=DashboardController::getCurrentUserRole();

        return view("challenge/search_challenge_set_data",compact('data','userRole','perpage','lang'))->with('currentpage', $request->get('page'));

        }
    }

    /**
     * Display full detail of Challenge Set.
     *
     * @param  \App\ChallengeSets  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $challengeSetData = ChallengeSets::find($id);
        if(!empty($challengeSetData)){
            $challengeData = ChallengeSetMappings::where("challenge_set_id",$id)->get();
            return view('challenge/challengeset_view',compact('challengeSetData','challengeData'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenge-sets')->with('message',$msg)->with('message_type',"danger");
        }
       
    }

}
