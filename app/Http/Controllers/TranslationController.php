<?php

namespace App\Http\Controllers;

use App\Translation;
use App\Language;
use Illuminate\Http\Request;
use Lang;

class TranslationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage=10;
    public function index()
    {
        //
        $langs = Language::orderBy("priority","desc")->orderBy("name",'asc')->paginate($this->perpage);
        return view("translation.index",compact('langs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("translation.create-language");
    }

    /**
     * Show the form for creating a new Translation.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTranslationCreate($id)
    {
        //
        $lang = Language::where('locale','en')->first();
        $currentlang = Language::find($id);
        $currentlanggroup=array();
        foreach ($currentlang->translations as $ln) {
           $currentlanggroup[$ln->group][$ln->item]=$ln;
        }
        ksort($currentlanggroup);
        $english = array();
        foreach ($lang->translations as $ln) {
           $english[$ln->group][$ln->item]=$ln;
        }
        ksort($english);
        return view("translation.create-tranlation",compact('lang','currentlang','currentlanggroup','english'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postTranslationCreate(Request $request)
    {
        //
        $this->validate($request,[
            "locale.*"=>'required',
            "namespace.*"=>'required',
            "group.*"=>'required',
            "item.*"=>'required',
        ]);
        foreach ($request->translation as $key => $item) {
            //Translation::where(["locale"=>$request->locale,"namespace"=>$request->namespace[$key],"group"=>$request->group[$key],"item"=> $key])->update(["text"=>$item??'']);                   
            Translation::updateOrCreate(["locale"=>$request->locale,"namespace"=>$request->namespace[$key],"group"=>$request->group[$key],"item"=> $key],["text"=>$item??'']);                   
        }
        return redirect("/translations")->with("message",__(Lang::locale().'.TRANSLATION_CREATE_MSG'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'locale'=>'required|unique:languages',
            'name'=>'required'
        ],
        ['locale.unique'=>"The language code has already taken."]);
        Language::create(["locale"=>$request->locale,"name"=>$request->name,"status"=>"Inactive"]);
        \DB::insert("insert into translations (locale,namespace,`group`,item,unstable,locked,status,text) SELECT '".$request->locale."' as de, namespace, `group`, item, unstable, locked, status,'' as txt from translations WHERE locale='en'");
        return redirect("/translations")->with("message",__(Lang::locale().'.LANGUAGE_CREATE_MSG'));
    }

    /**
     * Display full detail of Language.
     *
     * @param  \App\Language  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $languageData = Language::find($id);
            if(!empty($languageData)){
                return view('translation/view',compact('languageData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/translations')->with('message',$msg);
            }
        }
        else{
            return redirect('/dashboard');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $lang = Language::find($id);
        return view("translation.edit-language",compact('lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
         $this->validate($request,[
            'locale'=>'required',
            'name'=>'required',
            'status'=>'required'
        ]);
        Language::where('id',$id)->update(["locale"=>$request->locale,"name"=>$request->name,"status"=>$request->status]);        
        return redirect("/translations")->with("message",__(Lang::locale().'.LANGUAGE_UPDATE_MSG'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Translation  $translation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
       $lng = Language::find($id);
       $lng->forceDelete();
       Translation::where("locale",$lng->locale)->forceDelete();
        return redirect()->back()->with('message',__(Lang::locale().'.LANGUAGE_DELETE_MSG'));
    }

      public function fetch_data($organizationId=null,Request $request)
    {
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
            $langs = Language::query();
            if(!empty($query))
            {
            $langs->where(function ($q) use ($query) {
                $q->orWhere('locale', 'like', '%'.$query.'%')
                ->orWhere('name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $perpage=$this->perpage;
            $langs = $langs->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
            return view('translation.search_data', compact('langs','perpage'))->with('currentpage', $request->get('page'));
        }
    }
}
