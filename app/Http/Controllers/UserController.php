<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\Organization;
use App\User;
use App\Member;
use Illuminate\Support\Facades\Storage;
use Avatar;
use App\Http\Controllers\DashboardController;
use App\Userlog;
use App\Http\Helpers\Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage=25;
    public function index()
    {  
        $perpage=$this->perpage;
        $currentUserRole= DashboardController::getCurrentUserRole();
        $currentUserId= Auth::user()->id;
        if ($currentUserRole=='admin') {
         $data = User::select('*')->where('org_id', json_decode(Auth::user()->org_id))->orderBy('name', 'asc')->paginate($perpage);
        } else {
            $data = User::select('*')->orderBy('name', 'asc')->paginate($perpage);
        }
       
        return view('users/index',compact('data','perpage','currentUserId'))->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Display full detail of single organization.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $ogData = Organization::find($id);
        if(!empty($ogData)){
            return view('organization/view')->with('ogData', $ogData)->with('adminUsers', $this->getAdminUsers())->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/users')->with('message',$msg)->with('message_type','danger');
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = DB::table('roles')->get(['id','display_name']);
        $organizations = Organization::orderBy('name')->get(['id','name']);
        return view('users/create',compact('roles','organizations'))->with('userRole', DashboardController::getCurrentUserRole());
    }
    public function getAdminUsers()
    {
        $adminUsers = DB::select('SELECT * FROM users join model_has_roles on users.id=model_has_roles.model_id WHERE model_has_roles.role_id=2');
        

        return $adminUsers;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
            'role' => 'required'
           
        ]);
       
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'org_id' => $request->org_id,
            'password' => Hash::make($request->password)
        ]);
        if ($request->org_id!='') {
            User::where('id', $user->id)->update(['org_id' => $request->org_id,'org_name' => $this->getOrganizationNamebyId($request->org_id)]);
        }
        $rolesNames = $request->role;
        $user->assignRole($rolesNames);

        //$avatar = Avatar::create($user->name)->getImageObject()->encode('png');
       // $this->makeprofilephoto($user->id,$avatar);
        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        $this->makeprofilephoto($user->id,$avatar);
        send_mail($request->email,'create_user',$user->toArray());
        $msg=__(Lang::locale().'.USER_CREATE_MSG');
        Log::create_log(new Userlog(),'create',["userid"=>Auth::id(),"msg"=>"New User Create", "createduserid"=>$user->id]);
        return redirect('/users')->with('message', $msg)->with('message_type','success')->with('userRole', DashboardController::getCurrentUserRole());
    }

    private function makeprofilephoto($id,$avatar)
    {
        $f = ['storage','avatars',$id];
        $path=array();
        $rpath='';
        foreach($f as $d){
            array_push($path,$d);
            $rpath = implode("/", $path);
            if(!file_exists($rpath))
            {
                mkdir($rpath,777);
            }
        }       
        file_put_contents($rpath.'/avatar.png', (string) $avatar);
    }
    /**
     * all
     *
     * @return all organization
     */
    public function all()
    {
        return User::all();
    }
    /**
     * Display full detail of user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userData = User::find($id);
        if(!empty($userData)){
            $userrole = $this->getUserRoles($id)->roles;
            return view('users/view',compact('userData','userrole'))->with('currentUserRole', DashboardController::getCurrentUserRole());
        }
        else{
             $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/users')->with('message',$msg)->with('message_type','danger');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $user = User::find($id);
        } else {            
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $user = User::where(["org_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!$user)
        {
            return redirect("/users")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
         $roles = DB::table('roles')->get(['id','display_name']);
        $organizations = Organization::orderBy('name')->get(['id','name']);
        $userrole = $this->getUserRoles($id)->roles;
        return view('users/edit',compact('roles','organizations','userrole'))->with('user', $user)->with('userRole', DashboardController::getCurrentUserRole());
    }

     public function getOrganizationNamebyId($org_id)
    {
        $organizationName = Organization::select('name')->where('id', $org_id)->first();
        return $organizationName->name;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'password' => 'string|nullable',
            'role' => 'required'
        ]);
         
        $user = User::find($request->id);
        $userolddata=$user;
        if ($request->roletype=='admin' && $request->org_id!='') {
            $user->org_id= $request->org_id;
            $user->org_name= $this->getOrganizationNamebyId($request->org_id);
        } elseif ($request->roletype=='super-admin' && $request->org_id!='') {
            $user->org_id= '';
            $user->org_name= '';
        }

        if ($user->name != $request->name) {
            $avatar = Avatar::create($request->name)->getImageObject()->encode('png');
            $this->makeprofilephoto($user->id,$avatar);
            $user->name = $request->name;
        }
        if ($user->email != $request->email) {
            $user->email = $request->email;
        }
        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }
       
      
        $user->save();
       
         
        $rolesNames = $request->role;
        $user->syncRoles($rolesNames);

        $user2= User::findOrFail($request->id);
        echo $user2->getRoleNames();
        if (strpos($user2->getRoleNames(), '"admin"') !== false) {
            User::where('id', $request->id)->update(['org_id' => $request->org_id,'org_name' => $this->getOrganizationNamebyId($request->org_id)]);
        } else {
            User::where('id', $request->id)->update(['org_id' => '','org_name' => '']);
        }
        $msg=__(Lang::locale().'.USER_EDIT_MSG');
       Log::create_log(new Userlog(),'update',["userid"=>Auth::id(),"msg"=>"User Update", "updateduserid"=>$id,"userolddata"=>$userolddata]);
        return redirect('/users')->with('message', $msg)->with('message_type','success')->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!empty($user))
        {
            $user->delete();       
            $msg=__(Lang::locale().'.USER_DELETE_MSG');
            Log::create_log(new Userlog(),'delete',["userid"=>Auth::id(),"msg"=>"User Deleted", "deleteduserid"=>$id, "deleteduserdata"=>$user]);
            return redirect('/users')->with('message', $msg)->with('message_type','success')->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
             $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/users')->with('message',$msg)->with('message_type','danger');
        }
    }

   

    /**
     * search organization by address,name
     *
     * @param  mixed $request
     *
     * @return match search organization data
     */
    
    function fetch_data(Request $request)
    { 
     if($request->ajax())
     {
        $perpage = $this->perpage;
        $currentUserId= Auth::user()->id;
        $sort_by = $request->get('sortby');
        $sort_type = $request->get('sorttype');
        $query = $request->get('query'); 
        $query = str_replace(" ", "%", $query);
        $data = User::where('name', 'like', '%'.$query.'%')
                    ->orWhere('email', 'like', '%'.$query.'%')
                    ->orWhere('org_name', 'like', '%'.$query.'%')
                    ->orderBy($sort_by, $sort_type)
                    ->paginate($perpage);
          
        return view('users/search_data', compact('data','perpage','currentUserId'))->with('userRole', DashboardController::getCurrentUserRole())->render();
     }
    }

    private function getUserRoles($user)
    {
        $user = User::findOrFail($user);
        if (strpos($user->getRoleNames(), '"admin"') !== false) {
           
            $user->roletype='admin';
        } else {
            $user->roletype='super-admin';
        }
        $user->getRoleNames();

        return $user;
    }
}
