<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\Organization;
use App\Member;
use App\User;
use App\Team;
use App\TeamPlayer;
use Avatar;
use Illuminate\Validation\Rule;
use App\Http\Controllers\DashboardController;
use App\TeamLog;
use App\Http\Helpers\Log;
use App\DepartmentModel;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage =25;
   public function index($organizationId=NULL)
    {
        $perpage=$this->perpage;
        $team_data = array();
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $organization_data = Organization::where('status','Active')->orderBy('name','asc')->get();
        } else {
            $organizationId=DashboardController::getCurrentAdminUserOrganizationId();
            $organization_data = Organization::where('id',$organizationId)->orderBy('name','asc')->get();
        }
        if($organizationId != NULL)
        {
          $team_data = Team::where('organization_id',$organizationId)->orderBy('name', 'asc')->paginate($perpage);
        }else{
           $team_data = Team::orderBy('name', 'asc')->paginate($perpage);
        }
        return view('team/index', compact('team_data','perpage','organization_data','organizationId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($organizationId=NULL)
    {
        if (DashboardController::getCurrentUserRole()=='admin') {
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $organizations =  Organization::select('name', 'id')->where('id', $orgId->org_id)->orderBy('name','asc')->get();
            $organizationId = $orgId->org_id;
        } else {
            $organizations = Organization::select('name', 'id')->where('status', 'Active')->orderBy('name','asc')->get();
        }   
        return view('team/create',compact('organizationId'))->with('organizations', $organizations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'organization_id' => 'required',
            'status' => 'required'
        ], [
            'team_name.required' => ' The name field is required.',
            'organization_id.required' => 'Select organization field is required.',
        ]);
        $team = new Team();
        $team->name = request('name');
        $team->organization_id = request('organization_id');
        $team->description = request('description');
        $team->status = request('status');
        $team->organization_name=  DB::table('organizations')->select('name')->where('id', $team->organization_id)->value('name');
        // $team->avatar = Avatar::create($team->team_name)->getImageObject()->encode('png');
        $team->save();
        Log::create_log(new TeamLog(), 'create', ["userid"=>Auth::id(),"msg"=>"Create New Team",'orgid'=>$team->organization_id,"teamid"=>$team->id]);
        // Storage::put('avatars/'.$team->id.'/avatar.png', (string) $team->avatar);
        $msg=__(Lang::locale().'.TEAM_CREATE_MSG');
        if($request->btnvalue =="new"){                
            return redirect('team/create/'. $team->organization_id)->with('message', $msg)->with('message_type','success');
        }else{            
            return redirect('teams/'. $team->organization_id)->with('message', $msg)->with('message_type','success');
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $team
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $organizations =  Organization::select('name', 'id')->where('status', 'Active')->orderBy('name','asc')->get();
            $teamData = Team::find($id);
        } else {
            
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $organizations = Organization::select('name', 'id')->where('id', $orgId->org_id)->orderBy('name','asc')->get();
            $teamData = Team::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!$teamData)
        {
            return redirect("/teams")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        return view('team/edit')->with('teamData', $teamData)->with('organizations', $organizations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'organization_id' => 'required',
            'status' => 'required'
           
        ], [
            'team_name.required' => ' The name field is required.',
            'organization_id.required' => 'Select organization field is required.',
        ]);
        $old=Team::find($id);
        $team = new Team();
        $team->name = request('name');
        $team->organization_id = request('organization_id');
        $team->description = request('description');
        $team->status = request('status');
        $team->organization_name=  DB::table('organizations')->select('name')->where('id', $team->organization_id)->value('name');
        DB::table('teams')
            ->where('id', $id)
            ->update(['name' => $request->input('name'),'description' => $request->input('description'),'organization_id' => $request->input('organization_id'),'status' => $request->input('status'),'organization_name' => $team->organization_name,'status' => $request->input('status')]);
        Log::create_log(new TeamLog(), 'update', ["userid"=>Auth::id(),"msg"=>"Update Team",'orgid'=>$team->organization_id,"teamid"=>$id,'olddata'=>$old]);
        $msg=__(Lang::locale().'.TEAM_EDIT_MSG');
        return redirect('/teams/'.request('organization_id'))->with('message',  $msg)->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         if (DashboardController::getCurrentUserRole()=='super-admin') {
            $team = Team::find($id);
         }else{
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $team = Team::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
         }
        if(!$team)
        {
            return redirect("/teams")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        $player_data = DB::table('team_players')->select('*')->where('team_id',$id)->get();
        $team->destroy($id);
        $msg=__(Lang::locale().'.TEAM_DELETE_MSG');
        Log::create_log(new TeamLog(), 'delete', ["userid"=>Auth::id(),"msg"=>"Delete Team",'orgid'=>$team->organization_id,"teamid"=>$id,'olddata'=>$team,'playerdata' => $player_data]);
        return redirect('/teams/'.$team->organization_id)->with('message', $msg)->with('message_type','success');
    }

    public function fetch_data($organizationId=null,Request $request)
    {
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            if (DashboardController::getCurrentUserRole()=='admin') {
                $organizationId=DashboardController::getCurrentAdminUserOrganizationId();
            }
           $team_data = Team::query();           
            if($organizationId != 0){
               $team_data = Team::where('organization_id', $organizationId);
            }
            if(!empty($query))
            {
            $team_data->where(function ($q) use ($query) {
                $q->where('name', 'like', '%'.$query.'%')
                ->orWhere('organization_name', 'like', '%'.$query.'%');
            }); 
            }
            $team_data = $team_data->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
            return view('team/search_data', compact('team_data','perpage','organizationId'))->render();
        }
    }
    // Manage Team Player
    public function addplayer($team_id, $org_id)
    {
        $team = Team::find($team_id);
        $org = Organization::find($org_id);
        if(empty($team) || empty($org)){
            return redirect("/teams")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        } else if($team->organization_id != $org_id){
            return redirect("/teams")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        $perpage=25;
        if (DashboardController::getCurrentUserRole()=='super-admin') {
           $members = Member::where('members.organization_id', $org_id)
                ->where('members.status', '=', 'Active')
                ->join('team_players as tp', function ($join) use ($org_id,$team_id) {
                    $join->on('members.id', '=', 'tp.memebr_id')->where(["tp.organization_id"=>$org_id,"tp.team_id"=>$team_id]);
                })
                ->orderBy('member_name', 'ASC')
                ->select(['tp.status','members.id','member_name','member_department','member_email'])
                ->paginate($perpage);
        } else {
         $organizationId=DashboardController::getCurrentAdminUserOrganizationId();
            $members = Member::where('members.organization_id', $org_id)
             ->where('members.status', '=', 'Active')
             ->join('team_players as tp', function ($join) use ($org_id,$team_id) {
                 $join->on('members.id', '=', 'tp.memebr_id')->where(["tp.organization_id"=>$org_id,"tp.team_id"=>$team_id]);
             })
            ->orderBy('member_name', 'ASC')
            ->select(['tp.status','members.id','member_name','member_department','member_email'])
            ->where('tp.status', '!=', 'Delete')
            ->paginate($perpage);
        }
        $unselectedmembers =  Member::where('members.organization_id', $org_id)
        ->where('members.status', '=', 'Active')
        ->leftjoin('team_players as tp', 'members.id', '=', 'tp.memebr_id')
        ->whereRaw("tp.memebr_id IS NULL")
        ->orderBy('member_name', 'ASC')
        ->select(['members.id','member_name','member_department','member_email'])
        ->paginate($perpage);
        $msg=__(Lang::locale().'.PLAYER_ADD_MSG');
        return view('team/manage_player', compact('team', 'org', 'members', 'unselectedmembers', 'perpage'))->with('message', $msg)->with('message_type','success');
    }

    public function getPlayerByOrgid($is_seleted, $orgid, $team_id, $perpage=25, Request $request)
    {
        // if($request->)
        // $members = Member::where('organization_id',$orgid)->paginate($perpage);
        // return view('team/search_member_data', compact('members','perpage'))->render();
    
        if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);

            if (DashboardController::getCurrentUserRole()=='admin') {
                $organizationId=DashboardController::getCurrentAdminUserOrganizationId();
           
           

                if ($is_seleted==1) {
                    $members = Member::where('members.organization_id', $orgid)
                    ->where('tp.status', '!=', 'Delete')
                    ->where('members.status', '=', 'Active')
                    ->join('team_players as tp', function ($join) use ($orgid,$team_id) {
                        $join->on('members.id', '=', 'tp.memebr_id')
                                ->on('members.organization_id', '=', 'tp.organization_id')
                                ->where(["tp.organization_id"=>$orgid,"tp.team_id"=>$team_id]);
                    })->where(function ($q) use ($query) {
                        $q->where('member_name', 'like', '%'.$query.'%')
                        ->orWhere('member_department', 'like', '%'.$query.'%')
                        ->orWhere('member_email', 'like', '%'.$query.'%');
                    })->orderBy('member_name', 'ASC')
                    ->select(['tp.status','members.id','member_name','member_department','member_email'])
                     ->paginate($perpage);
                } else {
                    $members=Member::where('members.organization_id', $orgid)
                 ->where('members.status', '=', 'Active')
                    ->leftjoin('team_players as tp', 'members.id', '=', 'tp.memebr_id')
                    ->whereRaw("tp.memebr_id IS NULL")
                 ->where(function ($q) use ($query) {
                     $q->where('member_name', 'like', '%'.$query.'%')
                     ->orWhere('member_department', 'like', '%'.$query.'%')
                     ->orWhere('member_email', 'like', '%'.$query.'%');
                 })->orderBy('member_name', 'ASC')
                    ->select(['members.id','member_name','member_department','member_email']);
                    $members = $members->orderBy($sort_by, $sort_type)
                    ->select(['tp.status','members.id','member_name','member_department','member_email'])
                    
                     ->paginate($perpage);
                }
            } else {
                $members = Member::where('member_name', 'like', '%'.$query.'%')
                ->orWhere('member_department', 'like', '%'.$query.'%')
                ->orWhere('member_email', 'like', '%'.$query.'%');
                if ($is_seleted==1) {
                    $members= $members->join('team_players as tp', function ($join) use ($orgid,$team_id) {
                        $join->on('members.id', '=', 'tp.memebr_id')
                                ->on('members.organization_id', '=', 'tp.organization_id')
                                ->where(["tp.organization_id"=>$orgid,"tp.team_id"=>$team_id]);
                    });
                } else {
                    $members=Member::where('members.organization_id', $orgid)
                    ->leftjoin('team_players as tp', 'members.id', '=', 'tp.memebr_id')
                    ->whereRaw("tp.memebr_id IS NULL")
                 ->where(function ($q) use ($query) {
                     $q->where('member_name', 'like', '%'.$query.'%')
                     ->orWhere('member_department', 'like', '%'.$query.'%')
                     ->orWhere('member_email', 'like', '%'.$query.'%');
                 })->orderBy('member_name', 'ASC')
                    ->select(['members.id','member_name','member_department','member_email']);
                }
                $members = $members->orderBy($sort_by, $sort_type)
               ->select(['tp.status','members.id','member_name','member_department','member_email'])
               ->where('members.status', '=', 'Active')
                ->paginate($perpage);
            }
            $view = 'search_selected_member_data';
            $unselectedmembers=array();
            if ($is_seleted==0) {
                $view ='search_unselected_member_data';
                $unselectedmembers = $members;
            }

            return view('team/'.$view, compact('members', 'perpage', 'unselectedmembers'))->render();
        }
    }

    /**
         * save players for a team.
         *
         * @param  \App\Team  $team
         * @return \Illuminate\Http\Response
         */

    public function saveplayer(Request $request)
    {
        $this->validate($request, [
            'org_id' => 'required',
            'team_id' => 'required',
            'members' => 'required'
        ]);

        $members = explode(',', $request->members);
        $insertarray = array();
        foreach ($members as $member) {
            $data=array();
            $data['organization_id']=$request->org_id;
            $data['team_id']=$request->team_id;
            $data['memebr_id']=$member;
            $data['status']='Active';
            array_push($insertarray, $data);
            $memberv = Member::find($member)->toArray();
            $team = Team::find($request->team_id)->toArray();
            $emaildata = array_merge($memberv,$team);
            send_mail($memberv['member_email'],'assign_employee_to_team',$emaildata);
        }
        Log::create_log(new TeamLog(), 'addplayer', ["userid"=>Auth::id(),"msg"=>"Add Player in the team",'orgid'=>$request->org_id,"teamid"=>$request->team_id,'totalplayer'=>count($insertarray)]);
        DB::table("team_players")->insert($insertarray);
        DB::table('teams')->where('id', $request->team_id)->update(array('total_player'=> DB::raw('total_player+'.count($insertarray))));
        DB::table('organizations')->where('id', $request->org_id)->update(array('consume_license'=> DB::raw('consume_license+'.count($insertarray))));
    }
    /**
         * update players for a team.
         *
         * @param  \App\Team  $team
         * @return \Illuminate\Http\Response
         */

    public function updateplayer(Request $request)
    {
        $jj=0;
        foreach ($request->member_id as $member) {
            $data['organization_id']=$request->org_id;
            $data['team_id']=$request->team_id;
            $data['memebr_id']=$member;
            $data['status']=$request->status[$member];
            DB::table('team_players')->where('memebr_id', $data['memebr_id'])->update(array('status' => $data['status']));
        }
        //DB::table('teams')->where('id', $request->team_id)->update(array('total_player'=> DB::raw('total_player+'.$jj)));
        $msg=__(Lang::locale().'.PLAYER_UPDATE_MSG');
        return redirect('/players/'.$request->team_id.'/'.$request->org_id)->with('message', $msg)->with('message_type','success');
    }

    /**
     * Display full detail of single team.
     *
     * @param  \App\MemberModel  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $teamData = Team::find($id);
        }
        else{
            $orgId =  User::select('org_id')->where('id', auth()->user()->id)->first();
            $teamData = Team::where(["organization_id"=>$orgId->org_id,"id"=>$id])->first();
        }
        if(!empty($teamData)){
                $teamPlayerData = TeamPlayer::where('team_id',$id)->where('status','Active')->get();
                return view('team/view',compact('teamData','teamPlayerData'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
                return redirect("/teams")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
    }
}
