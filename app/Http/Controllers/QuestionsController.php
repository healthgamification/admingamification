<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Input;
use Lang;
use DB;
use Validator;
use App\Answers;
use App\QuestionsCategory;
use App\Question;
use App\QuestionCategoryMapping;
use App\QuestionSetMapping;
use App\QuestionBulkUploadLog;
use App\Http\Controllers\DashboardController;
use App\QueLog;
use App\Http\Helpers\Log;
use Illuminate\Support\Facades\Storage;
use Session;
use App\Language;

/*
Questions Constroller handles all the operations reqarding question bank questions
*/
class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $perpage=10;
    public function index($category_id=NULL,$lang='en')
    {  
        $currentUserRole= DashboardController::getCurrentUserRole();
        $currentUserId= Auth::user()->id;
        $data = array();
        $perpage = $this->perpage;
        if ($currentUserRole=='admin') {
        	redirect ('dashboard');
        } else {
            // $data = Question::whereHas('categories',function($query) use($lang){
            //     $query->where('lang', $lang);
            // })->orderBy('created_at', 'desc')->paginate($perpage);

            $data = Question::where("lang",$lang)
          ->whereHas('categories',function($q){
           $q->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
          })->orderBy('question', 'asc')->paginate($perpage);

            if($category_id != NULL && $category_id != 0){
                $data = Question::whereHas('categories',function($query) use ($lang,$category_id){
                $query->where('lang', $lang)->where('question_cat_id', $category_id)->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
            })->orderBy('created_at', 'desc')->paginate($perpage);
            }
             $category_data =  DB::table('questions_category')->select('category_name', 'id')->where('status', 'Active')->get();
            
        }
        $languages = getLanguages();
        return view('questions/index',compact('data','category_id','category_data','lang','languages','perpage'))->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id=NULL,$language='en')
    {	
    	$qcategorydata = DB::table('questions_category')->select('id','category_name')->where('status','Active')->get();
        return view('questions/create',compact('qcategorydata','category_id','language'))->with('userRole', DashboardController::getCurrentUserRole());
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
    	$this->validate($request, [
            'lang' => 'required',
            'question' => 'required',
            'status' => 'required',
            'fact' => 'required|max:150'
        ]);
        $category = $request->category;
        $question = new Question();
        $question->lang = $request->lang;
        //$question->category_id = 0;
        $question->question = $request->question;
        $question->fact = $request->fact;
        $question->weightage = isset($request->weightage)?$request->weightage:1;
        $question->status = $request->status;
        $question->options_count = count(array_filter($request->challenge_options));
        $question->save();
        if($question->options_count>0){
            $this->storeOptions($request->challenge_options,$question->id,$request->challange_answer);
        }
        $redirected_category = 0;
        $total_category = count(array_filter($request->category));
        if($total_category > 0){
            foreach($category as $cat){
            	$questioncategory = new QuestionCategoryMapping();
                $questioncategory->question_cat_id = $cat;
                $questioncategory->question_id = $question->id;
                $questioncategory->status = 'Active';
                $questioncategory->save();
                $redirected_category = $cat;
            }
        }
        Log::create_log(new QueLog(), 'create', ["userid"=>Auth::id(),"msg"=>"Create New Question","id"=>$question->id,"options"=>json_encode($request->challenge_options),'data'=>$question]);
        $msg=__(Lang::locale().'.QUESTION_CREATE_MSG');
        // return redirect('/questions')->with('message', $msg);
        if($request->btnvalue =="new"){                
            return redirect('question/create/'.$redirected_category.'/'.$question->lang)->with('message', $msg);
        }else{            
            return redirect('questions/'.$redirected_category.'/'.$question->lang)->with('message', $msg);
        }

    }

	 /**
     * Store options.
     *
     * @param  array of options
     * @return void
     */

    public function storeOptions($options,$questionid,$answer)
    {    
    	
        DB::table('answers')->where('question_id', '=', $questionid)->delete();
        foreach($options as $key => $option){
            if(!empty($option))
            {
        	$is_correct = 0;
        	if($key == $answer){
        		$is_correct = 1;
        	}
            DB::table('answers')->insert(
                ['option' => $option, 'question_id' => $questionid,'is_correct' => $is_correct,'created_at' => date('Y-m-d H:i:s')]
            );
        }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  categoryid, perpage
     * @return \Illuminate\Http\Response
     */
    function fetch_data($perpage,$category_id=NULL,$lang='en',Request $request)
    { 
	    if($request->ajax())
	    { 
            $paginate = $perpage;
		    $sort_by = $request->get('sortby');
		    $sort_type = $request->get('sorttype');
		    $query = $request->get('query'); 
		    $query = str_replace(" ", "%", $query);
		    
             $data = Question::where(["lang"=>$lang])
          ->whereHas('categories',function($q){
           $q->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
          });
            if($category_id != 0 && $category_id != NULL){
                $data = Question::whereHas('categories',function($query) use ($lang,$category_id){
                $query->where('lang', $lang)->where('question_cat_id', $category_id)->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
                });
            }
            if(!empty($query))
            {
                $data->where(function ($q) use ($query) {
                    $q->orWhere('question', 'like', '%'.$query.'%')
                    ->orWhere('status', 'like', '%'.$query.'%');
                }); 
            }
            $data = $data->orderBy($sort_by, $sort_type)
            ->paginate($paginate);
		    return view('questions/search_data', compact('data','category_id','lang'))->with('perpage', $paginate)->with('userRole', DashboardController::getCurrentUserRole())->render();
	    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  question id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questions = Question::find($id);
        $questions->delete();   
        $options = Answers::where('question_id',$id)->get();  
        $question_cat_mapping = QuestionCategoryMapping::where('question_id',$id)->get();
        QuestionCategoryMapping::destroy($question_cat_mapping->pluck('id'));
        $question_set_mapping = QuestionSetMapping::where('question_id',$id)->get();
        QuestionSetMapping::destroy($question_set_mapping->pluck('id'));
        $msg=__(Lang::locale().'.QUESTION_DELETE_MSG');
        Log::create_log(new QueLog(),'delete',["userid"=>Auth::id(),"msg"=>"Question Deleted", "id"=>$id, "data"=>$questions,  "answerdata"=>$options,'question_cat_mapping'=>$question_cat_mapping->pluck('question_cat_id'),'question_set_mapping'=>$question_set_mapping->pluck('question_set_id')]);
        return redirect('/questions/')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  question id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $qcategorydata = array();
        $question = Question::find($id);
        $category_data = $question->categories;

        foreach ($category_data as $cd) {
            $qcategorydata[] = $cd->category->id;
        }
        
        $allcategorydata = DB::table('questions_category')->select('id','category_name')->where('status','Active')->orderBy('id','ASC')->get();
    	$options = Answers::where('question_id',$id)->get();  
        return view('questions/edit',compact('qcategorydata','question','options','allcategorydata'))->with('userRole', DashboardController::getCurrentUserRole());
    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  question id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'lang' => 'required',
            'category' => 'required',
            'question' => 'required',
            'status' => 'required',
            'fact' => 'required|max:150'
        ]);
         
        $questions = Question::find($request->id);
        $old_status = $questions->status;
        $questionolddata= clone $questions;
        $questions->lang = $request->lang;
        $questions->question = $request->question;
        $questions->fact = $request->fact;
        $questions->weightage = isset($request->weightage)?$request->weightage:1;
        $questions->status = $request->status;
       	$questions->options_count = count(array_filter($request->challenge_options));
        $questions->save();
        $total_category = count(array_filter($request->category));
        $old_category_data = QuestionCategoryMapping::where('question_id',$id)->forceDelete(); 
        if($total_category > 0){
            foreach($request->category as $cat){
                $questioncategory = new QuestionCategoryMapping();
                $questioncategory->question_cat_id = $cat;
                $questioncategory->question_id = $questions->id;
                $questioncategory->status = 'Active';
                $questioncategory->save();
                $redirected_category = $cat;
            }
        }
        if($questions->options_count>0){
            $this->storeOptions($request->challenge_options,$questions->id,$request->challange_answer);
        }
      	if($old_status != $request->status){
            //QuestionCategoryMapping::where('question_id',$id)->update(['status'=>$request->status]);
            QuestionSetMapping::where('question_id',$id)->update(['status'=>$request->status]);
        }
        
        $msg=__(Lang::locale().'.QUESTION_EDIT_MSG');
       	Log::create_log(new QueLog(),'update',["userid"=>Auth::id(),"msg"=>"Question Update", "id"=>$id,"data"=>$questions,"olddata"=>$questionolddata,"old_category_data"=>$old_category_data]);
        // return redirect('/questions')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole());
        return redirect('/questions/')->with('message',  $msg)->with('userRole', DashboardController::getCurrentUserRole());
    }

    public function bulkUpload(Request $request)
    {
        $this->validate($request, [
                'file' => 'required',
        ]);
        $allowExt = array('csv');
        $ext = $request->file->getClientOriginalExtension();
        if(in_array($ext, $allowExt)) {
            $linecount = 0;
            $handle = fopen($request->file->getRealPath(), 'r');
            while(!feof($handle)){
                $line = fgets($handle, 4096);
                $linecount = $linecount + substr_count($line, PHP_EOL);
            }
            fclose($handle);
            if($linecount < 501 && $linecount > 1){
                $handle = fopen($request->file->getRealPath(), 'r');
                $data=array();
                $row=1;
                $uploadedrow=0;
                $wrongcat=0;
                $wrongvalidation=0;
                $msg='';
                $column = array('lang','question_category_id','questions','options_count','answer','fact');
                $required_column=array('lang','question_category_id','questions','options_count','answer','fact');
                $inc=0;
                $wronglang=0;

                $totaldata=array();
                while (!feof($handle)) {
                    $d = fgetcsv($handle, 1024, ',');  
                    if ($row > 1 && !empty($d)) {
                        $r=0;
                        $d = array_combine($column, $d);
                        foreach ($required_column as  $col) {
                            if (empty($d[$col])) {
                                $r++;
                            }
                        }
                        if($r==0){
                            $language_check = Language::where('locale',trim($d['lang']))->first();
                            if(!empty($language_check)){
                                $question_cat_data = explode(":",trim($d['question_category_id']));
                                $cat_flag = 1;
                                foreach($question_cat_data as $qcd){
                                    $question_cat = QuestionsCategory::where('category_name',trim($qcd))->first();
                                    if(isset($question_cat->category_name)) {
                                        $category_id = $question_cat->id;
                                        $duplicate = Question::where(["question"=>trim($d['questions']),"lang"=>$d['lang']])->whereHas('categories',function($query) use ($category_id){
                                            $query->where('question_cat_id', $category_id)->whereHas("category",function($qq){
                                            $qq->where("status","Active");
                                            });
                                        })->count();
                                        if ($duplicate>0) {
                                            $inc++;
                                            $totaldata['Duplicate'][$row]=$d;
                                        }else{
                                            if($cat_flag){
                                                $options = explode(":",$d['options_count']);
                                                $data['question'] = $d['questions'];
                                                $data['lang'] = $d['lang'];
                                                $data['fact'] = $d['fact'];
                                                $data['status'] = 'Active';
                                                $data['weightage'] = 1;
                                                $data['options_count'] = count($options);
                                                $question = Question::create($data);
                                                Log::create_log(new QueLog(), 'create', ["userid"=>Auth::id(),"msg"=>"Create New Question","id"=>$question->id,"options"=>json_encode($options),'data'=>$question]);
                                                if(count($options) > 0){
                                                    $inoptions= array(); 
                                                    foreach ($options as $val) {
                                                        $is_correct = 0;
                                                        if(trim($val) == trim($d['answer'])){
                                                            $is_correct = 1;
                                                        }
                                                        $inoptions[]=array("option"=>trim($val),"question_id"=>$question->id,'is_correct'=>$is_correct,'status'=>'Active');
                                                    }                                
                                                    Answers::insert($inoptions);
                                                }
                                                $cat_flag = 0;
                                                $totaldata['uploaded'][$row]=$d;
                                                $uploadedrow++;
                                            }
                                            QuestionCategoryMapping::insert(array('question_cat_id'=>$category_id,'question_id'=>$question->id,'status'=>'Active'));
                                            
                                        }
                                    }
                                    else {
                                        $wrongcat++;
                                        $totaldata['Wrong Category'][$row]=$d;
                                    }
                                }
                            }
                            else{
                                $wronglang++;
                                $totaldata['Wrong Language'][$row]=$d;
                            }
                        } else {
                            $wrongvalidation++;
                            $totaldata['Required Field Not Found'][$row]=$d;
                        }
                    }
                    $row++;
                }
                fclose($handle);
                $ind = array(
                    "user_id"=>\Auth::id(),
                    "filename"=>$request->file->getClientOriginalName(),
                    "data"=>json_encode($totaldata),
                    "type"=>"Quiz",
                );
                
                Session::put('question_bulk_error',$totaldata);
                $msg .='<a href="/questionlog/bulkerrorlog" target="_blank"> Click Here</a> for uploaded report.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                
                $loguploded = QuestionBulkUploadLog::create($ind);
                echo Lang::get(Lang::locale().'.QUESTION_BULK_UPLOAD_NO_OF_DUPLICATE',['duplicate' => $inc,"wrongcat"=>$wrongcat,"wronglang"=>$wronglang,"validation"=>$wrongvalidation,"uploaded"=>$uploadedrow]).$msg;
                       // }
                Log::create_log(new QueLog(), 'bulkupload', ["userid"=>Auth::id(),"msg"=>"Bulk Question upload.","totalrow"=>($row-1),'duplicate' => $inc,"wrongcat"=>$wrongcat,"wronglang"=>$wronglang,"validation"=>$wrongvalidation,"uploaded"=>$uploadedrow]);
            }
            else{
                 return response(['errors'=>['file'=>[__(Lang::locale().'.QUESTION_BULK_UPLOAD_LENGTH_ERROR_MSG')]]], 422)->header('Content-Type', 'application/json');
            }
        } else {
            return response(['errors'=>['file'=>[__(Lang::locale().'.CHALLENGE_BULK_UPLOAD_ERROR_MSG')]]], 422)->header('Content-Type', 'application/json');
        }
    }

    public function downloadcsv(){
            
            header('Content-type: application/ms-excel');
            header('Content-Disposition: attachment; filename=bulkerror.csv');
            $fp = fopen("php://output", "w");
            fputcsv($fp, array('Language', 'Category Name', 'Questions', 'Options (add option separate by colon)','Correct Answer','Fact about Question','Type'));
            foreach(Session::get('question_bulk_error') as $key => $row) {
                $k = $key;
                foreach ($row as $value) {
                    array_push($value, $k);
                    fputcsv($fp, $value);
                }
            }
            fclose($fp);
            header('Set-Cookie: filedownloaded=true'); 
            header("Pragma: no-cache");

         
    }

    /**
     * Display full detail of single question.
     *
     * @param  \App\Question  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $questionData = Question::find($id);
            if(!empty($questionData)){
                $questionOptionData = DB::table('answers')->where('question_id',$id)->orderBy('option')->get();
                return view('questions/view',compact('questionData','questionOptionData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/questions')->with('message',$msg);
            }
        }
        else{
            return redirect('/dashboard');
        }
        
    }


}
