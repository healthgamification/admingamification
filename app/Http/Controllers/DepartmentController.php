<?php

namespace App\Http\Controllers;

use App\DepartmentModel;
use App\Organization;
use App\DepardesignLog;
use Illuminate\Http\Request;

use App\Http\Helpers\Log;
use Auth;
use Lang;
class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $perpage=25;
    public function index($orgid=null)
    {
        //
        $departments = DepartmentModel::orderBy("name",'asc');
        if(!empty($orgid)){
            $departments->where("organization_id",$orgid);
        }
        $departments = $departments->paginate($this->perpage);
        $organization_data = Organization::orderBy("name",'asc')->get();
        return view("department.index",compact('orgid','departments','organization_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($orgid=null)
    {
        //
        $organizations  = Organization::orderBy("name",'asc')->get();
        return view("department.create",compact('organizations','orgid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'status'=>"required"
        ]);
       DepartmentModel::create([
        "name"=>$request->name,
        "organization_id"=>$request->organization_id,
        "status"=>$request->status,
        "description"=>$request->description??''
    ]);
      Log::create_log(new DepardesignLog(), 'departmentcreate', ["userid"=>Auth::id(),"msg"=>"Create New Department","data"=>$request->input()]);
        $msg=__(Lang::locale().'.DEPARTMENT_CREATE_MSG');
        if($request->btnvalue =="new"){                
            return redirect('department/create/'.$request->organization_id)->with('message', $msg);
        }else{            
            return redirect('departments/'.$request->organization_id)->with('message', $msg);
        }
    }

    /**
     * Display full detail of single department.
     *
     * @param  \App\DepartmentModel  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $departmentData = DepartmentModel::find($id);
            if(!empty($departmentData)){
                $organizationData = Organization::find($departmentData->organization_id);
                return view('department/view',compact('departmentData','organizationData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                 $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/departments')->with('message',$msg);
            }
        }else{
            return redirect('/dashboard');
        }
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $department = DepartmentModel::find($id);
         if($department==null)
         {
           return redirect("/departments")->with("message",__(Lang::locale().'.DEPARTMENT_NOT_FOUND_MSG'));
         }
         $organizations  = Organization::orderBy("name",'asc')->get();
        return view("department.edit",compact('organizations','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        //
        //
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'status'=>"required"
        ]);
        $olddata = DepartmentModel::find($id);
       DepartmentModel::where("id",$id)->update([
        "name"=>$request->name,
        "organization_id"=>$request->organization_id,
        "status"=>$request->status,
        "description"=>$request->description??''
    ]);
      Log::create_log(new DepardesignLog(), 'departmentupdate', ["userid"=>Auth::id(),"msg"=>"Update Department","olddata"=>$olddata,"data"=>$request->input()]);
        $msg=__(Lang::locale().'.DEPARTMENT_UPDATE_MSG');                  
        return redirect('departments/'.$request->organization_id)->with('message', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DepartmentModel  $departmentModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $department = DepartmentModel::find($id);
         if($department==null)
         {
           return redirect("/departments")->with("message",__(Lang::locale().'.DEPARTMENT_NOT_FOUND_MSG'));
         }
         $department->delete();
         Log::create_log(new DepardesignLog(), 'departmentdelete', ["userid"=>Auth::id(),"msg"=>"Delete Department","data"=>$department]);
        $msg=__(Lang::locale().'.DEPARTMENT_DELETE_MSG');                  
        return redirect('departments/')->with('message', $msg);
    }

    public function fetch_data($orgid=null,Request $request)
    {
        $perpage=$this->perpage;
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);

            $departments = DepartmentModel::query();
            if($orgid != 0){
                $departments->where('organization_id', $orgid);
            }
            if(!empty($query))
            {
            $departments->where(function ($q) use ($query) {
                $q->orWhere('name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $departments = $departments->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
            return view('department.search_data', compact('departments','perpage','orgid'))->with('currentpage', $request->get('page'));
        }
    }
}
