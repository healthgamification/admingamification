<?php
namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function getCurrentUserRole()
    {
        $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
        return $currentUserRole[0]->name;
    }

    public static function getCurrentAdminUserOrganizationId()
    {
        $organizationId =  DB::table('users')->select('org_id')->where('id', auth()->user()->id)->first();

        return $organizationId->org_id;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        return view('dashboard')->with('userRole', $this->getCurrentUserRole());
    }
   
}
