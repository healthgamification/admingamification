<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\Organization;
use App\Challenge;
use App\User;
use Avatar;
use Illuminate\Validation\Rule;
use App\Http\Controllers\DashboardController;
use App\ChallengeCategory;
use App\ChallengeBulkUploadLog;
use App\ChallengeSetMappings;
use App\Http\Helpers\Log;
use App\ChallengeLog;
use Session;
use App\Rules\DigitValue;
use App\Language;

class ChallengeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage =25;
    public function index($category_id=null,$lang='en')
    {  
        $perpage=$this->perpage;
        $challenge_sets = DB::table('challenge_categories')->orderBy('name', 'asc')->where('status', '=', 'Active')->get();
        $data = Challenge::where("lang_code",$lang)
                ->whereHas('challenges',function($query){
                    $query->where("status","Active");
                })->orderBy('challenge_name', 'asc')->paginate($perpage);
        if($category_id !=null && $category_id !=0)
        {
            $data = Challenge::where(["challenge_category_id"=>$category_id,"lang_code"=>$lang])->orderBy('challenge_name', 'asc')->paginate($perpage);
        }

        $languages = getLanguages();
        return view('challenge/index', compact('challenge_sets','data','perpage','category_id','lang','languages'));
    }

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getChallengebySetid($setId)
    {  
       $challenge_sets = DB::table('challenge_categories')->orderBy('name', 'asc')->where('status', '=', 'Active')->get();
       
       $data = DB::table('challenges')->orderBy('challenge_name', 'asc')->where('challenge_category_id', '=', $setId)->paginate(25);
       return view('challenge/index', compact('challenge_sets','data','setId'));
    } 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoty_id=null,$lang='en')
    { 
        $challenge_sets = DB::table('challenge_categories')->orderBy('name', 'asc')->where('status', '=', 'Active')->get();
        $languages = getLanguages();
        return view('challenge/create', compact('challenge_sets','categoty_id','lang','languages'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {// print_r(request('challenge_name'));
       // exit;
      
        $this->validate($request, [
            'challenge_name' => 'required',
            'challenge_description' => 'required',
            'challenge_status' => 'required',
            'challenge_category_id' => 'required',
            'lang_code' => 'required',
            'challenge_complexity' => ['required', new DigitValue(1,7)],
            'no_of_days_to_complete' => ['required', new DigitValue(1,7)]
           
        ], [
            'challenge_name.required' => ' The Title field is required.',
            'challenge_description.required' => 'The Description field is required.',
            'challenge_status.required' => 'The Status is required.',
            'lang_code.required' => 'The Language field is required.'
        ]);
        
            $challenge = new Challenge();       
            $challenge->challenge_name = request('challenge_name');
            $challenge->challenge_description = request('challenge_description');
            $challenge->options_count = count(array_filter($request->challenge_options));
            $challenge->challenge_status = request('challenge_status');
            $challenge->challenge_category_id = request('challenge_category_id');
            $challenge->complexity_of_challenge = request('challenge_complexity');
            $challenge->no_of_days_to_complete = request('no_of_days_to_complete');
            $challenge->lang_code = request('lang_code');
            $challenge->save();
            
            if($challenge->options_count>0){
            $this->storeOptions($request->challenge_options,$request->challenge_point,$challenge->id);
            }
            $msg=__(Lang::locale().'.CHALLENGE_CREATE_MSG');

             Log::create_log(new ChallengeLog(), 'create', ["userid"=>Auth::id(),"msg"=>"New Challenge created.","id"=>$challenge->id,"options"=>json_encode($request->challenge_options),"data"=>json_encode($challenge)]);

            if($request->btnvalue =="new"){                
            return redirect('challenge/create/'.$challenge->challenge_category_id.'/'.$challenge->lang_code)->with('message', $msg)->with('message_type','success');
        }else{            
            return redirect('challenges/'.$challenge->challenge_category_id.'/'.$challenge->lang_code)->with('message', $msg)->with('message_type','success');
        }
    }

    /**
     * Store options.
     *
     * @param  array of options
     * @return void
     */
    public function storeOptions($options,$point,$challengeid)
    {    
        DB::table('challenge_options')->where('challenge_id', '=', $challengeid)->delete();
        foreach($options as $key => $option){
            if(!empty($option))
            {
            DB::table('challenge_options')->insert(
                ['option_name' => $option, 'challenge_id' => $challengeid, 'point'=>$point[$key]]
            );
        }
        }
    }
    
    /**
     * Display full detail of single challange.
     *
     * @param  \App\MemberModel  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $challengeData = Challenge::find($id);
        if(!empty($challengeData)){
            $challengeOptionData = DB::table('challenge_options')->where('challenge_id',$id)->orderBy('option_name')->get();
            return view('challenge/view',compact('challengeData','challengeOptionData'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenges')->with('message',$msg)->with('message_type','danger');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $challengeData = Challenge::find($id);
        if(!empty($challengeData)){
            $challenge_options =  DB::table('challenge_options')->where('challenge_id', $id)->get();
            $challenge_sets = DB::table('challenge_categories')->orderBy('name', 'asc')->where('status', '=', 'Active')->get();
            $languages = getLanguages();
            return view('challenge/edit',compact('languages'))->with('setId', '')->with('challenge_sets', $challenge_sets)->with('challengeData', $challengeData)->with('challenge_options',$challenge_options);
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenges')->with('message',$msg)->with('message_type','danger');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'challenge_name' => 'required',
            'challenge_description' => 'required',
            'challenge_status' => 'required',
            'challenge_category_id' => 'required',
            'lang_code' => 'required',
            'challenge_complexity' => ['required', new DigitValue(1,7)],
            'no_of_days_to_complete' => ['required', new DigitValue(1,7)]
           
        ], [
            'challenge_name.required' => ' The Title field is required.',
            'challenge_description.required' => 'The Description field is required.',
            'challenge_status.required' => 'The Status is required.',
            'lang_code.required' => 'The language field is required.',
          
        ]);
        $challenge = Challenge::find($id);
        $old_status = $challenge->challenge_status;       
        $oldoption = DB::table('challenge_options')->where('challenge_id', '=', $challenge->id)->get()->pluck("option_name");
            DB::table('challenges')
            ->where('id', $id)
            ->update(['challenge_name' => $request->input('challenge_name'),'challenge_category_id' => $request->input('challenge_category_id'),'challenge_description' => $request->input('challenge_description'),'lang_code' => $request->input('lang_code'),'options_count' => count(array_filter($request->challenge_options)),'challenge_status' => $request->input('challenge_status'),'complexity_of_challenge' => $request->input('challenge_complexity'),'no_of_days_to_complete' => $request->input('no_of_days_to_complete')]);
            if($challenge->options_count>0){
            $this->storeOptions($request->challenge_options,$request->challenge_point,$id);
            }
            if($old_status != $request->input('challenge_status')){
                ChallengeSetMappings::where('challenge_id',$id)->update(['status'=>$request->input('challenge_status')]);
            }

             Log::create_log(new ChallengeLog(), 'update', ["userid"=>Auth::id(),"msg"=>"Challenge updated.","id"=>$challenge->id,"options"=>json_encode($request->challenge_options),"oldoption"=>json_encode($oldoption),"olddata"=>json_encode($challenge),"newdata"=>json_encode($request->input())]);

            $msg=__(Lang::locale().'.CHALLENGE_EDIT_MSG');
            return redirect('/challenges/'.$request->input('challenge_category_id').'/'.$request->input('lang_code'))->with('message',$msg)->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Challenge  $challenge
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $challenge=Challenge::find($id);
        if(!empty($challenge)){
            $option = DB::table('challenge_options')->where('challenge_id', '=', $id)->get()->pluck("option_name");
            Challenge::destroy($id);
            ChallengeSetMappings::destroy(ChallengeSetMappings::where('challenge_id',$id)->get()->pluck('id'));
            Log::create_log(new ChallengeLog(), 'deleted', ["userid"=>Auth::id(),"msg"=>"Challenge deleted.","id"=>$challenge->id,"options"=>json_encode($option),"data"=>json_encode($challenge)]);

            $msg=__(Lang::locale().'.CHALLENGE_DELETE_MSG');
            return redirect('/challenges/'.$challenge->challenge_category_id)->with('message',$msg);
        }
        else{
            $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/challenges')->with('message',$msg)->with('message_type','danger');
        }
    }
    function fetch_data($category_id=null,$lang='en',Request $request)
    { 
        $perpage= $this->perpage;
        if($request->ajax())
        { 
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            if($category_id==0){
                $data = Challenge::where("lang_code",$lang)
                    ->whereHas('challenges',function($query){
                        $query->where("status","Active");
                    });
            }else{
                $data = Challenge::where(['challenge_category_id'=>$category_id,'lang_code'=>$lang])->whereHas('challenges',function($query){
                        $query->where("status","Active");
                    });
            }
            if(!empty($query))
            {
                $data->where(function($query1) use($query){
                $query1->where("challenge_name" ,'like', '%'.$query.'%');
                });   
            }                
            $data = $data->orderBy($sort_by, $sort_type)
                ->paginate($perpage);
            $challenge_sets = DB::table('challenge_categories')->orderBy('name', 'asc')->where('status', '=', 'Active')->get();
            return view('challenge/search_data', compact('challenge_sets','data','perpage','category_id','lang'))->render();
        }
    }

public function bulkUpload(Request $request)
{
     $this->validate($request, [
            'file' => 'required',
        ]);
        $allowExt = array('csv');
        $ext = $request->file->getClientOriginalExtension();
        if (in_array($ext, $allowExt)) {
            $linecount = 0;
            $handle = fopen($request->file->getRealPath(), 'r');
            while(!feof($handle)){
                $line = fgets($handle, 4096);
                $linecount = $linecount + substr_count($line, PHP_EOL);
            }
            fclose($handle);
            if($linecount < 501 && $linecount > 1){
                $handle = fopen($request->file->getRealPath(), 'r');
                $data=array();
                $row=1;
                $uploadedrow=0;
                $wrongcat=0;
                $wrongvalidation=0;
                $wronglang=0;
                $wrongcomplexity=0;
                $msg='';
                $column = array('lang_code','challenge_category_id','challenge_name','challenge_description','options_count','complexity_of_challenge','challenge_status');
                $required_column=array('lang_code','challenge_category_id','challenge_name','challenge_description','complexity_of_challenge');
                $inc=0;
                $totaldata=array();
                while(!feof($handle))
                {
                    $d = fgetcsv($handle, 1024,',');
                    if($row > 1 && !empty($d)) {
                        array_push($d, "Active");
                        $data = array_combine($column, $d);
                        $r=0;
                        foreach ($required_column as  $col) {
                            if (empty($data[$col])) {
                                $r++;
                            }
                        }
                        if(trim($data['complexity_of_challenge']) > 0 &&  trim($data['complexity_of_challenge'])<=7){
                            if ($r==0) {
                                $language_check = Language::where('locale',trim($data['lang_code']))->first();
                                if(!empty($language_check)){
                                    $challenge_set = ChallengeCategory::where('name',trim($data['challenge_category_id']))->first();
                                    if (isset($challenge_set->name)) {
                                        $data['challenge_category_id'] = $challenge_set->id;
                                         $duplicate = Challenge::where(["challenge_name"=>$data['challenge_name'],"challenge_category_id"=>$data['challenge_category_id'],"lang_code"=>$data['lang_code']])->count();
                                        if ($duplicate>0) {
                                            $inc++;
                                             $totaldata['Duplicate'][$row]=$d;
                                        }
                                        else{
                                            $options = explode(":",$data['options_count']);
                                            $data['options_count'] = count($options);
                                            $challenge = Challenge::create($data);
                                            Log::create_log(new ChallengeLog(), 'create', ["userid"=>Auth::id(),"msg"=>"New Challenge created.","id"=>$challenge->id,"options"=>json_encode($options),"data"=>json_encode($challenge)]);
                                            $inoptions= array(); 
                                            if(count($options) > 0 ){
                                                foreach ($options as $val) {
                                                    $option = explode("#",$val);
                                                    $opt = $option[0];
                                                    $point =  ($option[1] !='')?$option[1]:0;
                                                    $inoptions[]=array("option_name"=>$opt,'point'=>$point,"challenge_id"=>$challenge->id);
                                                }                                
                                                DB::table('challenge_options')->insert($inoptions);
                                            }
                                            $totaldata['uploaded'][$row]=$d;
                                            $uploadedrow++;
                                        }
                                    } else {
                                        $wrongcat++;
                                        $totaldata['Wrong Category'][$row]=$d;
                                    }
                                }else{
                                    $wronglang++;
                                    $totaldata['Wrong Language'][$row]=$d;
                                }
                            } 
                            else {
                                $wrongvalidation++;
                                $totaldata['Required Field Not Found'][$row]=$d;
                            }
                        }
                        else{
                            $wrongcomplexity++;
                            $totaldata['Wrong Complexity Range'][$row]=$d;
                        }
                       
                    }
                    $row++;
                }
                fclose($handle);
                $ind = array(
                    "user_id"=>\Auth::id(),
                    "filename"=>$request->file->getClientOriginalName(),
                    "data"=>json_encode($totaldata),
                    "type"=>"Challenge",
                );
                
                Session::put('challenge_bulk_error',$totaldata);
                $msg .='<a href="/challengelog/bulkerrorlog" target="_blank"> Click Here</a> for bulk upload report.<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>';
                
                $loguploded = ChallengeBulkUploadLog::create($ind);
                // if ($inc>0) {
                //     echo $totalDuplicate=$inc .__(Lang::locale().'.CHALLENGE_BULK_UPLOAD_NO_OF_DUPLICATE').'<a href="/bulkuploadlog/'.$loguploded->id.'" target="_blank"> Click Here</a> for detail.';
                // } else {
                  echo Lang::get(Lang::locale().'.CHALLENGE_BULK_UPLOAD_NO_OF_DUPLICATE',['duplicate' => $inc,"wrongcat"=>$wrongcat,"wronglang"=>$wronglang,'wrongcomplexity'=>$wrongcomplexity,"validation"=>$wrongvalidation,"uploaded"=>$uploadedrow]).$msg;
               // }
                 Log::create_log(new ChallengeLog(), 'bulkupload', ["userid"=>Auth::id(),"msg"=>"Bulk Challenge upload.","totalrow"=>($row-1),'duplicate' => $inc,"wrongcat"=>$wrongcat,"validation"=>$wrongvalidation,"wronglang"=>$wronglang,'wrongcomplexity'=>$wrongcomplexity,"uploaded"=>$uploadedrow]);
            }
            else{
                 return response(['errors'=>['file'=>[__(Lang::locale().'.CHALLENGE_BULK_UPLOAD_LENGTH_ERROR_MSG')]]], 422)->header('Content-Type', 'application/json');
            }
        } else {
            return response(['errors'=>['file'=>[__(Lang::locale().'.CHALLENGE_BULK_UPLOAD_ERROR_MSG')]]], 422)->header('Content-Type', 'application/json');
        }
}

    public function bulkUploadLog($logid)
    {
        $updata = ChallengeBulkUploadLog::find($logid);
        return view("challenge.uploadlog",compact('updata'));
    }

    public function downloadcsv(){
                
                header('Content-type: application/ms-excel');
                header('Content-Disposition: attachment; filename=bulkerror.csv');
                $fp = fopen("php://output", "w");
                fputcsv($fp, array('Language', 'Category Name', 'Title', 'Description ','Options (add option separate by colon) (hash separate the marks)','Complexity (1-7)','Status','Challenge Status'));
                foreach(Session::get('challenge_bulk_error') as $key => $row) {
                    $k = $key;
                    foreach ($row as $value) {
                        array_push($value, $k);
                        fputcsv($fp, $value);
                    }
                }
                fclose($fp);
                header('Set-Cookie: filedownloaded=true'); 
                header("Pragma: no-cache");

             
    }

}
