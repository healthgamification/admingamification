<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use Avatar;
use App\User;
use DB;

class UserController extends Controller
{
    public function filter(Request $request)
    {
        $query = User::query();
        if ($request->search) {
            $query->where('name', 'LIKE', '%'.$request->search.'%');
        }
        

        $users = $query->orderBy($request->input('orderBy.column'), $request->input('orderBy.direction'))
                    ->paginate($request->input('pagination.per_page'));
        

        $users->load('roles');
      
        return $users;
    }
    public function getOrganizationNamebyId($org_id)
    {
        $organizationName = DB::table('organizations')->select('name')->where('id', $org_id)->first();
        return $organizationName->name;
    }
    public function show($user)
    {
        return User::findOrFail($user);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
            'roles' => 'required|array'
           
        ]);
       
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'org_id' => $request->org_id,
            'password' => Hash::make($request->password)
        ]);
        if ($request->org_id!='') {
            User::where('id', $user->id)->update(['org_id' => $request->org_id,'org_name' => $this->getOrganizationNamebyId($request->org_id)]);
        }
        $rolesNames = array_pluck($request->roles, ['name']);
        $user->assignRole($rolesNames);

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);
        
        return $user;
    }

    public function update(Request $request)
    {    //echo $this->getUserRoles($request->id); exit;
        // echo $request->roletype; exit;
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email,'.$request->id,
            'password' => 'string|nullable',
            'roles' => 'required|array'
        ]);
         
        $user = User::find($request->id);
        if ($request->roletype=='admin' && $request->org_id!='') {
            $user->org_id= $request->org_id;
            $user->org_name= $this->getOrganizationNamebyId($request->org_id);
        } elseif ($request->roletype=='super-admin' && $request->org_id!='') {
            $user->org_id= '';
            $user->org_name= '';
        }

        if ($user->name != $request->name) {
            $avatar = Avatar::create($request->name)->getImageObject()->encode('png');
            Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);
            $user->name = $request->name;
        }
        if ($user->email != $request->email) {
            $user->email = $request->email;
        }
        if ($request->password != '') {
            $user->password = Hash::make($request->password);
        }
       
      
        $user->save();
       
         
        $rolesNames = array_pluck($request->roles, ['name']);
        $user->syncRoles($rolesNames);

        $user2= User::findOrFail($request->id);
        echo $user2->getRoleNames();
        if (strpos($user2->getRoleNames(), '"admin"') !== false) {
            User::where('id', $request->id)->update(['org_id' => $request->org_id,'org_name' => $this->getOrganizationNamebyId($request->org_id)]);
        } else {
            User::where('id', $request->id)->update(['org_id' => '','org_name' => '']);
        }

        return $user;
    }

    public function destroy($user)
    {
        return User::destroy($user);
    }

    public function count()
    {
        return User::count();
    }

    public function getUserRoles($user)
    {
        $user = User::findOrFail($user);
        if (strpos($user->getRoleNames(), '"admin"') !== false) {
           
            $user->roletype='admin';
        } else {
            $user->roletype='super-admin';
        }
        $user->getRoleNames();

        return $user;
    }
}
