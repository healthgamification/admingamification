<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DateTime;
use DateTimeZone;
use App;
use Redirect;
use Hash;
use Input;
use Lang;
use DB;
use Validator;
use App\Organization;
use App\User;
use App\Member;
use App\Team;
use Illuminate\Support\Facades\Storage;
use Avatar;
use App\Http\Controllers\DashboardController;
use App\Orglog;
use App\Http\Helpers\Log;
use App\QuestionSet;
use App\AssignQuizWeekModel;
use App\AssignChallengeWeekModel;
use App\TeamPlayer;
use App\Memberlog;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   private $perpage=25;
    public function index()
    {
        $currentUserRole= DashboardController::getCurrentUserRole();
        $perpage=$this->perpage;
        $currentUserId= Auth::user()->id;
        if ($currentUserRole=='super-admin') {
            $data = Organization::select('*')->orderBy('name', 'asc')->paginate($perpage);
        } else {
            $data = Organization::select('*')->where('id', json_decode(Auth::user()->org_id))->orderBy('name', 'asc')->paginate($perpage);
        }
       
        return view('organization/index', compact('data'))->with('userRole', DashboardController::getCurrentUserRole());
    }

    /**
     * Display full detail of single organization.
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($id);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){
        return view('organization/view')->with('ogData', $ogData)->with('adminUsers', $this->getAdminUsers())->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
    }

    /**
     * Display a listing of the users of the organization.
     *
     * @return \Illuminate\Http\Response
     */
    public function users($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($id);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){
            $orgid =$ogData->id;
            $perpage=$this->perpage;
            $adminUsers = User::where('org_id', $ogData->id)->orderBy('name', 'asc')->paginate($perpage);
            $users = Member::where('organization_id', $ogData->id)->orderBy('member_name', 'asc')->paginate($perpage);
               
            return view('organization/users', compact('perpage', 'orgid'))->with('ogData', $ogData)->with('adminUsers', $adminUsers)->with('users', $users)->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
    }
   

    public function makeAdminfromUsers($id, $userId)
    {
        $usersData = Member::find($userId);
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($id);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){   
            $user = User::create([
                'name' => $usersData['member_name'],
                'email' => $usersData['member_email'],
                'password' => Hash::make('123')
            ]);
       
            User::where('id', $user->id)->update(['org_id' => $ogData['id'],'org_name' => $ogData['name']]);
            $user->assignRole('admin');
            $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
            Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);
            $usersData->is_admin=1;
            $usersData->save();
            send_mail($user->email,'make_admin_from_user',$user->toArray());
            Log::create_log(new Orglog(),'makeadmin',["userid"=>Auth::id(),"msg"=>"Convert member to admin","memberid"=>$userId,"orgid"=>$ogData['id']]);
            $msg = __(Lang::locale().'.USER_MAKE_ADMIN_MSG');
            return redirect()->back()->with("message", $msg)->with('message_type','success');
        }
        else{
             $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect("/organization")->with('message',$msg)->with('message_type','danger');
        }
    }
    public function removeAdminfromUsers($id, $userId)
    {
        $usersData = Member::find($userId);
        if(DashboardController::getCurrentUserRole()=='admin'){
            $ogData = Organization::find(Auth::user()->org_id);
        }else{
            $ogData = Organization::find($id);
        }
        if(!empty($ogData)){
            $user = User::where('email', $usersData['member_email'])->first();
            $user->removeRole('admin');
            $user->forceDelete();
            $usersData->is_admin=0;
            $usersData->save();
            send_mail($user->email,'remove_admin_from_user',$user->toArray());
             Log::create_log(new Orglog(),'removeadmin',["userid"=>Auth::id(),"msg"=>"Convert admin to Member","memberid"=>$userId,"orgid"=>$ogData['id']]);
            $msg = __(Lang::locale().'.USER_REMOVE_ADMIN_MSG');
            return redirect()->back()->with("message",$msg)->with('message_type','success');
        }
        else{
             $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect("/organization")->with('message',$msg)->with('message_type','danger');
        }
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('organization/create')->with('userRole', DashboardController::getCurrentUserRole());
    }
    public function getAdminUsers()
    {
        $adminUsers = DB::select('SELECT * FROM users join model_has_roles on users.id=model_has_roles.model_id WHERE model_has_roles.role_id=2');
        

        return $adminUsers;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'name' => 'required',
        'address' => 'required',
        'email' => 'required|email',
        'status' => 'required',
        'lang' => 'required'
       
    ], [
        'name.required' => ' The name field is required.',
    ]);
        $organization = new Organization();
 
        $organization->name = request('name');
        $organization->address = request('address');
        $organization->email = request('email');
        $organization->countryCode = request('countryCode');
        $organization->phone = request('phone');
        $organization->mobile = request('mobile');
        $organization->no_of_quizzes = request('no_of_quizzes');
        $organization->no_of_challenge = request('no_of_challenge');
        $organization->no_of_license = $request->no_of_license??0;
        $organization->about = request('about');
        $organization->lang_code = request('lang');
        $organization->status = request('status');
        $organization->save();
         Log::create_log(new Orglog(),'create',["userid"=>Auth::id(),"msg"=>"Create neworganization","orgid"=>$organization->id]);
         send_mail($organization->email,'create_organization',$organization->toArray());
        $msg=__(Lang::locale().'.ORGANIZATION_CREATE_MSG');
        return redirect('/organization')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole())->with('message_type','success');;
    }
    /**
     * all
     *
     * @return all organization
     */
    public function all()
    {
        return Organization::all();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizationData = Organization::find($id);
        if(!empty($organizationData)){
        return view('organization/edit')->with('organizationData', $organizationData)->with('adminUsers', $this->getAdminUsers())->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'lang' => 'required',
            'status' => 'required'
        ], [
            'name.required' => ' The name field is required.',
        ]);
        DB::table('organizations')
        ->where('id', $id)
        ->update(['name' => $request->input('name'),'address' => $request->input('address'),'email' => $request->input('email'),'phone' => $request->input('phone'),'countryCode' => $request->input('countryCode'),'mobile' => $request->input('mobile'),'about' => $request->input('about'),'no_of_challenge' => $request->input('no_of_challenge'),'no_of_quizzes' => $request->input('no_of_quizzes'),'no_of_license' => $request->no_of_license??0,'lang_code' => $request->input('lang'),'status' => $request->input('status')]);
        $msg=__(Lang::locale().'.ORGANIZATION_EDIT_MSG');
        Log::create_log(new Orglog(),'update',["userid"=>Auth::id(),"msg"=>"update organization","orgid"=>$id,"updatedata"=>$request->all()]);
        return redirect('/organization')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole())->with('message_type','success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($id); 
        }else{
            //$ogData = Organization::find(Auth::user()->org_id);
            $ogData = array();
        }
        if(!empty($ogData)){
            User::destroy(User::where('org_id', $ogData->id)->get()->pluck("id")->all());
            Member::destroy(Member::where('organization_id', $ogData->id)->get()->pluck("id")->all());
            Team::destroy(Team::where('organization_id', $ogData->id)->get()->pluck("id")->all());
            Organization::destroy($ogData->id);
           
            $msg=__(Lang::locale().'.ORGANIZATION_DELETE_MSG');
            Log::create_log(new Orglog(),'delete',["userid"=>Auth::id(),"msg"=>"Delete organization","orgid"=>$ogData->id]);
            return redirect('/organization')->with('message', $msg)->with('userRole', DashboardController::getCurrentUserRole())->with('message_type','success');
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }

    }

    public function destroyAdminUser($id, $userId)
    {
        $user = User::find($userId);
        
        $member = Member::where("member_email", $user->email);
        if ($member->count()>0) {
            $member->update(['is_admin'=>'0']);
        }
        $user->removeRole('admin');
        $user->forceDelete();
        $msg = __(Lang::locale().'.ADMIN_UASER_DELETE_MSG');
        Log::create_log(new Orglog(),'deleteorgusers',["userid"=>Auth::id(),"msg"=>"Delete users assign to organization","orgid"=>$id]);
        return redirect()->back()->with('message', $msg)->with('message_type','success');
    }

    public function destroyUser($id, $userId)
    {
        if (DashboardController::getCurrentUserRole()=='super-admin') {
            $member = Member::find($userId);
            $ogData = Organization::find($id);
        } else {            
            $ogData = Organization::find(Auth::user()->org_id);
            $member = Member::where(["organization_id"=>$ogData->org_id,"id"=>$userId])->first();
        }
        if(!$member)
        {
            return redirect("/members")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        if(empty($ogData))
        {
            return redirect("/organization")->with("messgae",__(Lang::locale().'.NOT_FOUND_TITLE'))->with('message_type','danger');
        }
        if($member->is_admin == 1){
            User::where("org_id", $member->organization_id)->where('email',$member->member_email)->forceDelete();
        }
        $member->delete();
        $adminUsers = User::where('org_id', $ogData->id)->paginate($this->perpage);
        $users = Member::where('organization_id', $ogData->id)->paginate($this->perpage);
        TeamPlayer::where("memebr_id",$member->id)->update(['status'=>"Deleted"]);
        $msg=__(Lang::locale().'.MEMBER_DELETE_MSG');
        Log::create_log(new Memberlog(), 'delete', ["userid"=>Auth::id(),"msg"=>"Delete Member","memberid"=>$id,"memberold"=>$member]);
        return redirect('organization/'.$ogData->id.'/view/users')->with('ogData', $ogData)->with('adminUsers', $adminUsers)->with('users', $users)->with('userRole', DashboardController::getCurrentUserRole())->with('message', 'Admin user deleted!')->with('message_type','success');
    }

    /**
     * search organization by address,name
     *
     * @param  mixed $request
     *
     * @return match search organization data
     */
  

    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $data = Organization::where('name', 'like', '%'.$query.'%')
                    ->orWhere('email', 'like', '%'.$query.'%')
                    ->orWhere('phone', 'like', '%'.$query.'%')
                    ->orWhere('status', 'like', '%'.$query.'%')
                    ->orWhere('mobile', 'like', '%'.$query.'%')
                    ->orderBy($sort_by, $sort_type)
                    ->paginate($perpage);
          
            return view('organization/search_data', compact('data','perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
        }
    }

     // User adn admin fetch table Data
     public function fetch_admin_users($type, $orgid, $perpage, Request $request)
     {
         if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($orgid);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
         if ($request->ajax()) {
             $sort_by = $request->get('sortby');
             $sort_type = $request->get('sorttype');
             $query = $request->get('query');
             $query = str_replace(" ", "%", $query);
             $view ='search_data_user';
             $adminUsers=array();
             if ($type==0) {
                 $users = Member::where('organization_id', $ogData->id)
                     ->where(function ($dquery) use ($query) {
                         $dquery->where('member_name', 'like', '%'.$query.'%')
                     ->orWhere('member_email', 'like', '%'.$query.'%')
                     ->orWhere('member_department', 'like', '%'.$query.'%')
                     ->orWhere('member_designation', 'like', '%'.$query.'%');
                     })
                     ->orderBy($sort_by, $sort_type)
                     ->paginate($perpage);
             } else {
                 $adminUsers = User::where('org_id', $ogData->id)
                     ->where(function ($dquery) use ($query) {
                         $dquery->where('name', 'like', '%'.$query.'%')
                     ->orWhere('email', 'like', '%'.$query.'%');
                     })
                     ->orderBy($sort_by, $sort_type)
                     ->paginate($perpage);
                 $view ='search_data_admin';
             }
           
             return view('organization/'.$view, compact('adminUsers', 'type', 'users', 'orgid', 'perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
         }
     }
 
 
     // Ogranization team data
 
     public function getTeams($id)
     {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($id);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){
         $orgid =$ogData->id;
         $perpage=$this->perpage;
             
         $team_data =Team::where('organization_id', $orgid)->orderBy('name', 'asc')->paginate($perpage);
         return view('organization/teams', compact('perpage', 'orgid', 'ogData', 'team_data'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
     }
 
     public function fetch_teams($orgid, $perpage, Request $request)
     {
         if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($orgid);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
         if ($request->ajax()) {
             $sort_by = $request->get('sortby');
             $sort_type = $request->get('sorttype');
             $query = $request->get('query');
             $query = str_replace(" ", "%", $query);
             $view ='search_data_user';
             $team_data = Team::where('organization_id', $ogData->id)
                     ->where(function ($dquery) use ($query) {
                         $dquery->where('name', 'like', '%'.$query.'%')
                     ->orWhere('description', 'like', '%'.$query.'%');
                     })
                     ->orderBy($sort_by, $sort_type)
                     ->paginate($perpage);
                 
             return view('organization/search_data_team', compact('adminUsers', 'type', 'team_data', 'orgid', 'perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
         }
     }


      // Ogranization quizz data
 
     public function getQuizzes($orgid)
     {
       if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($orgid);
        }else{
             $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){
            $orgid =$ogData->id;
            $perpage=$this->perpage;
            $data = AssignQuizWeekModel::where(["org_id"=>$orgid,"status"=>"Active"])->groupBy("quiz_set_id")->paginate($perpage);
            return view('organization/quizzes', compact('perpage', 'orgid', 'ogData', 'data'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
    }
 
     public function fetch_quizzes($orgid, Request $request)
     {
         if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($orgid); 
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        $orgid =$ogData->id;
        $perpage= $this->perpage;
         if ($request->ajax()) {
             $sort_by = $request->get('sortby');
             $sort_type = $request->get('sorttype');
             $query = $request->get('query');
             $query = str_replace(" ", "%", $query);
             $data = AssignQuizWeekModel::where(["org_id"=>$orgid,"status"=>"Active"])->groupBy("quiz_set_id")
                     ->whereHas('set',function ($dquery) use ($query,$sort_by,$sort_type) {
                        if(!empty($query))
                        {
                         $dquery->where('title', 'like', '%'.$query.'%')
                         ->orWhere('status', 'like', '%'.$query.'%');                     
                        }
                     //$dquery->orderBy($sort_by, $sort_type);
                     })
                     ->orderBy("quiz_set_id", $sort_type)
                     ->paginate($perpage);
                 
             return view('organization/quizz_data', compact('data', 'orgid', 'perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
         }
     }

      // Ogranization challenge data
 
     public function getChallenges($orgid)
     {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $ogData = Organization::find($orgid);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        if(!empty($ogData)){
            $orgid =$ogData->id;
            $perpage=$this->perpage;
            $data = AssignChallengeWeekModel::where(["org_id"=>$orgid,"status"=>"Active"])->groupBy("challenge_set_id")->paginate($perpage);
            return view('organization/challenges', compact('perpage', 'orgid', 'ogData', 'data'))->with('userRole', DashboardController::getCurrentUserRole());
        }
        else{
            $msg = __(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/dashboard')->with('message',$msg)->with('message_type','danger');
        }
    }
 
     public function fetch_challenges($orgid, Request $request)
     {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
             $ogData = Organization::find($orgid);
        }else{
            $ogData = Organization::find(Auth::user()->org_id);
        }
        $orgid =$ogData->id;
        $perpage= $this->perpage;
         if ($request->ajax()) {
             $sort_by = $request->get('sortby');
             $sort_type = $request->get('sorttype');
             $query = $request->get('query');
             $query = str_replace(" ", "%", $query);
             $data = AssignChallengeWeekModel::where(["org_id"=>$orgid,"status"=>"Active"])->groupBy("challenge_set_id")
                     ->whereHas('set',function ($dquery) use ($query,$sort_by,$sort_type) {
                        if(!empty($query))
                        {
                         $dquery->where('title', 'like', '%'.$query.'%')
                         ->orWhere('status', 'like', '%'.$query.'%');                     
                        }
                     //$dquery->orderBy($sort_by, $sort_type);
                     })
                     ->orderBy("challenge_set_id", $sort_type)
                     ->paginate($perpage);
                 
             return view('organization/challenge_set_data', compact('data', 'orgid', 'perpage'))->with('userRole', DashboardController::getCurrentUserRole())->render();
         }
     }
}
