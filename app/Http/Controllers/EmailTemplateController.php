<?php

namespace App\Http\Controllers;

use App\MailTemplate;
use Illuminate\Http\Request;
use Lang;
use App\MailTemplateLog;
use App\Http\Helpers\Log;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage=10;
    public function index()
    {
        //
        $templates=MailTemplate::orderBy("subject","asc")->paginate($this->perpage);
        return view("template.index",compact('templates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("template.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            "subject"=>"required",
            "body"=>"required",
            "type"=>"required",
            "lang_code"=>"required",
            "status"=>"required",
        ]);

        MailTemplate::updateOrcreate(["type"=>$request->type],[
            "type"=>$request->type,
            "subject"=>$request->subject,
            "body"=>$request->body,
            "lang_code"=>$request->lang_code,
            "status"=>$request->status,
            "created_by"=>\Auth::id(),
        ]);
        Log::create_log(new MailTemplateLog(), 'create', ["userid"=>\Auth::id(),"msg"=>"Create New Template","data"=>$request->input()]);
        $msg = __(\Lang::locale().'.TEMPLATE_CREATE_MSG');
        return redirect("/templates")->with("message",$msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function show($id,MailTemplate $mailTemplate)
    {
        //
        $template= $mailTemplate->find($id);
        if(!$template)
        {
             return redirect("/templates")->with("message",__(Lang::locale().'.NOT_FOUND_TITLE')); 
        }
         return view("template.view",compact("template"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit($id,MailTemplate $mailTemplate)
    {
        //
        $templete= $mailTemplate->find($id);
        $variables = implode(" ",getTableInfo($templete->type));
         return view("template.edit",compact("templete",'variables'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, MailTemplate $mailTemplate)
    {
        //
        $this->validate($request,[
            "subject"=>"required",
            "body"=>"required",
            "type"=>"required",
            "lang_code"=>"required",
            "status"=>"required",
        ]);
        $olddata = MailTemplate::find($id);
        MailTemplate::where("id",$id)->update([
            "type"=>$request->type,
            "subject"=>$request->subject,
            "body"=>$request->body,
            "lang_code"=>$request->lang_code,
            "status"=>$request->status,
            "updated_by"=>\Auth::id(),
        ]);
        Log::create_log(new MailTemplateLog(), 'update', ["userid"=>\Auth::id(),"msg"=>"Update Template","olddata" => $olddata,"data"=>$request->input()]);
        $msg = __(\Lang::locale().'.TEMPLATE_UPDATE_MSG');
        return redirect("/templates")->with("message",$msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,MailTemplate $mailTemplate)
    {
        //
        $deletedata = $mailTemplate->find($id);
        $mailTemplate->find($id)->delete();
        Log::create_log(new MailTemplateLog(), 'delete', ["userid"=>\Auth::id(),"msg"=>"Delete Template","data"=>$deletedata]);
        $msg = __(\Lang::locale().'.TEMPLATE_DELETE_MSG');
        return redirect("/templates")->with("message",$msg);
    }

     public function fetch_data($organizationId=null,Request $request)
    {
        
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);            

            $templates = MailTemplate::query();
            if(!empty($query))
            {
            $templates->where(function ($q) use ($query) {
                $q->orWhere('subject', 'like', '%'.$query.'%')
                ->orWhere('type', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $templates = $templates->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
                  
            return view('template/search_data', compact('templates','perpage'));
        }
    }
}
