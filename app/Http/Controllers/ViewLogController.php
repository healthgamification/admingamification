<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\Log;

class ViewLogController extends Controller
{
    //

    public function index($model,$id)
    {
    	$modalname = encrypedecrypt($model,false);
    	$model = "App\\".$modalname;
    	$logs=Log::view_log(new $model());
    	$logtype = Log::getType(new $model());
    	return view("viewlog.index",compact("modalname",'logtype','logs'));
    }

    /**
     * view a newly created log.
     *
     * @param  ModelName  $model
     * @param  id  $id
     * @return View LogView
     */
    public function view($encmodel,$id)
    {
    	$modalname = encrypedecrypt($encmodel,false);
    	$model = "App\\".$modalname;
    	$logs = Log::viewLogsById(new $model(),$id);
    	return view("viewlog.view.$modalname",compact('logs'));
    }
}
