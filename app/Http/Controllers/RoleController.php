<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Modules\Module;
use Lang;
use DB;

class RoleController extends Controller
{
    private $perpage = 10;

    /**
     * List resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $totalPermission = count(Permission::all());
        $data = Role::paginate($this->perpage);
        return view('roles.index',compact('data','totalPermission'));
    }

    /**
     * List resources.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    function fetch_data(Request $request)
    { 
        $perpage= $this->perpage;
        if($request->ajax())
        { 
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            
            $data = Role::query();
            if(!empty($query))
            {
                $data->where(function($query1) use($query){
                    $query1->where("name" ,'like', '%'.$query.'%');
                    $query1->whereOr("display_name" ,'like', '%'.$query.'%');
                });   
            }  
            $totalPermission = count(Permission::all());              
            $data = $data->orderBy($sort_by, $sort_type)->paginate($perpage); 
          return view('roles.search_data', compact('data','perpage','totalPermission'))->render();
        }
    }

    /**
     * Display create page.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $moduleData = Module::all();
        return view('roles.create',compact('moduleData'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:roles',
            'display_name' => 'required|string|unique:roles'
        ]);

        $role = Role::create([
            'name' => $request->name,
            'display_name' => $request->display_name
        ]);

        if(isset($request->permission)){
            $permissions = Permission::whereIn('id',$request->permission)->get();
            $role->givePermissionTo($permissions);
        }
        $msg =__(Lang::locale().'.ROLE_CREATE_MSG');
        return redirect('roles')->with('message', $msg);
    }

    public function show ($id)
    {
        $roleData = Role::find($id);
        if(!empty($roleData)){
            $moduleData = Module::all();
            return view("roles.view",compact('roleData','moduleData','id'));
        }
        else{
             $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
            return redirect('/roles')->with('message',$msg);
        }
    }

    /**
     * Display Edit form.
     *
     * @param  Spatie\Permission\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $roleData = Role::find($id);
        if(!empty($roleData))
        {
            if($roleData->name != 'super-admin'){
                $moduleData = Module::all();
                return view("roles.edit",compact('roleData','moduleData','id'));
            }
            $msg =__(Lang::locale().'.NOT_FOUND_TITLE');
        }
        else{
            $msg =__(Lang::locale().'.NOT_FOUND_TITLE');
        }
        return redirect('roles')->with('message', $msg);
    }

    public function update (Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:roles,name,'.$request->id,
            'display_name' => 'required|string|unique:roles,display_name,'.$request->id
        ]);

        $role = Role::find($request->id);

        if ($role->name != $request->name) {
            $role->name = $request->name;
        }

        if ($role->display_name != $request->display_name) {
            $role->display_name = $request->display_name;
        }

        if(isset($request->permission)){
            $permissions = Permission::whereIn('id',$request->permission)->get();
            $role->syncPermissions($permissions);
        }

        $role->save();
        $msg =__(Lang::locale().'.ROLE_UPDATE_MSG');
        return redirect('roles')->with('message', $msg);
    }

    public function destroy ($id,Role $role)
    {
        $role->destroy($id);
        $role->bootHasPermissions($id);
        $msg =__(Lang::locale().'.ROLE_DELETE_MSG');
        return redirect('roles')->with('message', $msg);
    }

}
