<?php

namespace App\Http\Controllers;

use App\DesignationModel;
use App\Organization;
use App\DepardesignLog;
use Illuminate\Http\Request;

use App\Http\Helpers\Log;
use Auth;
use Lang;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   protected $perpage=25;
    public function index($orgid=null)
    {
        //
        $designations = DesignationModel::orderBy("name",'asc');
        if(!empty($orgid)){
            $designations->where("organization_id",$orgid);
        }
        $designations = $designations->paginate($this->perpage);
        $organization_data = Organization::orderBy("name",'asc')->get();
        return view("designation.index",compact('orgid','designations','organization_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($orgid=null)
    {
        //
        $organizations  = Organization::orderBy("name",'asc')->get();
        return view("designation.create",compact('organizations','orgid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'status'=>"required"
        ]);
       DesignationModel::create([
        "name"=>$request->name,
        "organization_id"=>$request->organization_id,
        "status"=>$request->status,
        "description"=>$request->description??''
    ]);
      Log::create_log(new DepardesignLog(), 'designationcreate', ["userid"=>Auth::id(),"msg"=>"Create New Designation","data"=>$request->input()]);
        $msg=__(Lang::locale().'.DESIGNATION_CREATE_MSG');
        if($request->btnvalue =="new"){                
            return redirect('designation/create/'.$request->organization_id)->with('message', $msg);
        }else{            
            return redirect('designations/'.$request->organization_id)->with('message', $msg);
        }
    }

    /**
     * Display full detail of single designation.
     *
     * @param  \App\DesignationModel  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $designationData = DesignationModel::find($id);
            if(!empty($designationData)){
                $organizationData = Organization::find($designationData->organization_id);
                return view('designation/view',compact('designationData','organizationData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                 $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/designations')->with('message',$msg);
            }
        }else{
            return redirect('/dashboard');
        }
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DesignationModel  $designationModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $designation = DesignationModel::find($id);
         if($designation==null)
         {
           return redirect("/designations")->with("message",__(Lang::locale().'.DESIGNATION_NOT_FOUND_MSG'));
         }
         $organizations  = Organization::orderBy("name",'asc')->get();
        return view("designation.edit",compact('organizations','designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DesignationModel  $designationModel
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        //
        //
        $this->validate($request,[
            'name'=>'required',
            'organization_id'=>'required',
            'status'=>"required"
        ]);
        $olddata = DesignationModel::find($id);
       DesignationModel::where("id",$id)->update([
        "name"=>$request->name,
        "organization_id"=>$request->organization_id,
        "status"=>$request->status,
        "description"=>$request->description??''
    ]);
      Log::create_log(new DepardesignLog(), 'designationupdate', ["userid"=>Auth::id(),"msg"=>"Update Designation","olddata"=>$olddata,"data"=>$request->input()]);
        $msg=__(Lang::locale().'.DESIGNATION_UPDATE_MSG');                  
        return redirect('designations/'.$request->organization_id)->with('message', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DesignationModel  $designationModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $designation = DesignationModel::find($id);
         if($designation==null)
         {
           return redirect("/designations")->with("message",__(Lang::locale().'.DESIGNATION_NOT_FOUND_MSG'));
         }
         $designation->delete();
         Log::create_log(new DepardesignLog(), 'designationdelete', ["userid"=>Auth::id(),"msg"=>"Delete Designation","data"=>$designation]);
        $msg=__(Lang::locale().'.DESIGNATION_DELETE_MSG');                  
        return redirect('designations/')->with('message', $msg);
    }

    public function fetch_data($orgid=null,Request $request)
    {
        $perpage=$this->perpage;
        if ($request->ajax()) {
            $perpage=$this->perpage;
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);

            $designations = DesignationModel::query();
            if($orgid != 0){
                $designations->where('organization_id', $orgid);
            }
            if(!empty($query))
            {
            $designations->where(function ($q) use ($query) {
                $q->orWhere('name', 'like', '%'.$query.'%')
                ->orWhere('status', 'like', '%'.$query.'%');
            }); 
            }
            $designations = $designations->orderBy($sort_by, $sort_type)
            ->paginate($perpage);
            return view('designation.search_data', compact('designations','perpage','orgid'))->with('currentpage', $request->get('page'));
        }
    }
}
