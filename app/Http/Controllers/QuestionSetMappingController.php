<?php

namespace App\Http\Controllers;

use App\QuestionSetMapping;
use App\QuestionSet;
use Illuminate\Http\Request;
use App\QuestionsCategory;
use App\Question;
use Illuminate\Support\Facades\Auth;
use App\QuestionCategoryMapping;
use App\Http\Controllers\DashboardController;
use Lang;
use App\QueLog;
use App\Http\Helpers\Log;
use Illuminate\Support\Facades\Storage;
use App\AssignQuizWeekModel;
class QuestionSetMappingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $perpage=10;
   
    public function index($lang='en')
    {
        //
        $perpage = $this->perpage;
        $data =QuestionSet::where("lang_code",$lang)->orderBy('title','asc')->paginate($perpage);
        $userRole=DashboardController::getCurrentUserRole();
        return view("questions.questionset",compact('lang','data','userRole','perpage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang='en')
    {
        //
        $perpage = $this->perpage;
          $data = Question::where(["status"=>"Active","lang"=>$lang])
          ->whereHas('categories',function($q){
           $q->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
          })->orderBy('question','asc')->paginate($perpage);
        $category_data = QuestionsCategory::where("status","Active")->orderBy("category_name",'asc')->get();
        return view("questions.questionset_create",compact('lang','data','category_data','perpage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "question_set_name"=>'required',
            "question_ids" =>'required|string',
            "status"=>'required'          
        ],
        [
            "question_ids.required"=>"Please select at least one question from below list.",
        ]);
        $set = QuestionSet::create(
            [
                "title"=>$request->question_set_name,
                "description"=>isset($request->question_set_desc)?$request->question_set_desc:'',
                "lang_code"=>$request->lang,
                "status"=>$request->status
            ]);
        $indata=array();
        foreach (explode(',',$request->question_ids) as $qid) {
            $indata[]=array(
                "question_set_id"=>$set->id,
                "question_id"=>$qid
            );
        }
        QuestionSetMapping::insert($indata);
        $msg = __(Lang::locale().'.QUESTION_SET_CREATE_MSG');
        return redirect("/question-sets/".$request->lang)->with("message",$msg);
    }

    /**
     * Display full detail of single question set.
     *
     * @param  \App\QuestionSet  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(DashboardController::getCurrentUserRole()=='super-admin'){
            $questionSetData = QuestionSet::find($id);
            if(!empty($questionSetData)){
                 $questionData = QuestionSetMapping::where("question_set_id",$id)->get();
                return view('questions/questionset_view',compact('questionSetData','questionData'))->with('userRole', DashboardController::getCurrentUserRole());
            }
            else{
                $msg=__(Lang::locale().'.NOT_FOUND_TITLE');
                return redirect('/question-sets')->with('message',$msg);
            }
        }
        else{
            return redirect('/dashboard');
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionSetMapping  $questionSetMapping
     * @return \Illuminate\Http\Response
     */
    public function edit($lang,$id,QuestionSet $questionSet)
    {
        //
        $perpage = $this->perpage;
        $question_set = $questionSet->find($id);
        $data = Question::where(["status"=>"Active","lang"=>$lang])
          ->whereHas('categories',function($q){
           $q->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
          })
        ->whereHas('selected',function($query) use ($id){
                    $query->where(["question_set_id"=>$id,"status"=>"Active"]);
                })->orderBy("question",'asc')->paginate($perpage);

        $category_data = QuestionsCategory::where("status","Active")->orderBy("category_name",'asc')->get();
        return view("questions.question_set_edit",compact('question_set','lang','perpage','category_data','data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionSetMapping  $questionSetMapping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionSet $questionSet)
    {
        //
        $this->validate($request,[
            "question_set_name"=>'required',
            "question_ids" =>'required|string',
            "status"=>'required'          
        ],
        [
            "question_ids.required"=>"Please select at least one question from below list.",
        ]);
        $qset = $questionSet->find($request->set_id);
        $oldstatus = $qset->status;
        $qset->title=$request->question_set_name;
        $qset->status=$request->status;
        $qset->lang_code=$request->lang;
        $qset->description =isset($request->question_set_desc)?$request->question_set_desc:'';
        $qset->save();

        if($oldstatus!=$request->status)
        {
            AssignQuizWeekModel::where('quiz_set_id',$qset->id)->update(['status'=>$request->status]);
        }
                
        $indata=array();
        QuestionSetMapping::where("question_set_id",$qset->id)->forceDelete();
        foreach (explode(',',$request->question_ids) as $qid) {
            $indata[]=array(
                "question_set_id"=>$qset->id,
                "question_id"=>$qid
            );
        }
        QuestionSetMapping::insert($indata);
        $msg = __(Lang::locale().'.QUESTION_SET_EDIT_MSG');
        return redirect("/question-sets/".$qset->lang_code)->with("message",$msg);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionSetMapping  $questionSetMapping
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,QuestionSetMapping $questionSetMapping)
    {
        //
        $question_ids = $questionSetMapping->where("question_set_id",$id)->get();
        $qset = QuestionSet::find($id);
        $qset->delete();
        QuestionSetMapping::destroy($question_ids->pluck('id'));
        AssignQuizWeekModel::where('quiz_set_id',$qset->id)->update(['status'=>"Deleted"]);
         Log::create_log(new QueLog(), 'quiz_set_deleted', ["userid"=>Auth::id(),"msg"=>"Quiz Set deleted.","id"=>$id,"data"=>json_encode($qset),"questions"=>$question_ids->pluck('question_id')]);
        $msg = __(Lang::locale().'.QUESTION_SET_DELETE_MSG');
        return redirect()->back()->with("message",$msg);
    }


    public function fetch_data($lang='en',$type,$cat,$set, Request $request)
    {
        $perpage=$this->perpage;
        if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $q = trim($request->get('query'));
            $q = str_replace(" ", "%", $q);
            // \DB::enableQueryLog();
            $data = Question::where(["status"=>"Active","lang"=>$lang])
          ->whereHas('categories',function($q){
           $q->whereHas("category",function($qq){
            $qq->where("status","Active");
           });
          });
            if($cat!=0)
            {
               $data = $data->whereHas("categories",function($query) use($cat) {
                    $query->where("question_cat_id",$cat);
                });
            }
            if($type==1){
                $data = $data->whereHas('selected',function($query) use($set){
                     $query->where(["question_set_id"=>$set,"status"=>"Active"]);
                });
            }
            if($type==2){
                $data = $data->whereDoesntHave('selected',function($query) use($set){
                     $query->where(["question_set_id"=>$set,"status"=>"Active"]);
                });
            }

             if($q!='')
                    {
                    $data->where(function($query1) use($q){
                        $query1->where("question" ,'like', '%'.$q.'%');
                    });   
                    }                
             $data = $data->orderBy($sort_by, $sort_type);

            $data = $data->paginate($perpage);
            //dd(\DB::getQueryLog());
              return view("questions.search_question_data",compact('data','perpage'));            
        }

    }

    public function set_fetch_data($perpage,$lang, Request $request)
    {
        if ($request->ajax()) {
            $sort_by = $request->get('sortby');
            $sort_type = $request->get('sorttype');
            $query = trim($request->get('query'));
            $query = str_replace(" ", "%", $query);
        $data =QuestionSet::where("lang_code",$lang);
        if($query!='')
        {
        $data->where('title', 'like', '%'.$query.'%');
        }
        $data = $data->orderBy($sort_by, $sort_type)->paginate($perpage);
        $userRole=$userRole=DashboardController::getCurrentUserRole();

        return view("questions.search_question_set_data",compact('lang','data','userRole','perpage'))->with('currentpage', $request->get('page'));

        }
    }
}
