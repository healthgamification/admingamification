<?php
if(!function_exists("getLanguages")){
	function getLanguages()
	{
		$lang =array();
		$data= \DB::table("languages")->where("status","Active")->orderBy("name","ASC")->get();
		foreach ($data as $value) {
			$lang[]=(object) array("code"=>$value->locale,"name"=>$value->name);
		}
		return $lang;
	}
}

if(!function_exists("custom_date_format")){
	function custom_date_format($date)
	{
		return date(env("DATE_FORMAT","d-m-Y"),strtotime($date));
	}
}

if(!function_exists("send_mail")){
	function send_mail($to,$type,$data)
	{
		$template = App\MailTemplate::where(["status"=>"Active","type"=>$type])->first();
		if($template)
		{
			$body = makeText($template->body,$data);
			$subject = makeText($template->subject,$data);

			\Mail::send('emails.email', compact('body'), function ($message) use($to, $subject)
			{
				$message->to($to)->subject($subject);

			});
		}
	}

	function mapfn()
	{
		return null;
	}

	function makeText($srt,$data){
			preg_match_all("/(\[\[\S+\]\])/im", $srt, $matches);
			$amatches = array_map("mapfn",array_flip(str_replace(["[[","]]"],["",""],$matches[0])));
			$values = array_merge($amatches,$data);
			return str_replace($matches[0], array_values($values), $srt);
	}

	function makemsg($type,$data)
	{
		$template = App\MailTemplate::where(["status"=>"Active","type"=>$type])->first();
		if($template)
		{
			$body = makeText($template->body,$data);
			$subject = makeText($template->subject,$data);

			return ["subject"=>$subject,"body"=>$body];
		}
	}
}

if(!function_exists("getTemplateType"))
{
	function getTemplateType($key=null)
	{

		$template_type = array(
            "create_user"=>'Create Admin And Super Admin',
            "create_organization"=>'Create Organization',
            "make_admin_from_user"=>'Make admin from employee',
            "remove_admin_from_user"=>'Remove employee from admin',
            "assign_quizz_and_challenge"=>'Assign Quizz and Challenges to the organization',
            "create_member"=>'Create Employee',
            "assign_employee_to_team"=>'Assign Employee to Team',
            "admin_superadmin_password_reset"=>'Admin And Super Admin Password Reset',
            "player_password_reset"=>'Player Password Reset',
            "test_mail"=>'Test Email Template'
        );
        if($key!=null){
        	return $template_type[$key];
        }else{
        	return $template_type;
        }
	}
}

if(!function_exists("getTableInfo"))
{
	function getTableInfo($key=null)
	{
		$extra_param = array("[[link]]","[[expiry_time]]","[[button_name]]");
		$template_type = array(
            "create_user"=>'users',
            "create_organization"=>'organizations',
            "make_admin_from_user"=>'members',
            "remove_admin_from_user"=>'members',
            "assign_quizz_and_challenge"=>'organizations',
            "create_member"=>'members',
            "assign_employee_to_team"=>'members,teams',
            "admin_superadmin_password_reset"=>'users',
            "player_password_reset"=>'members',
            "test_mail"=>null
        );
        $table = $template_type[$key];
        $data =array();
        if($table)
        {
        	foreach (explode(",", $table) as $tab1) {
	        	$dtable = \DB::select('DESCRIBE '.$tab1);
	        	foreach ($dtable as $tab) {
	        		array_push($data, "[[".$tab->Field."]]");
	        	}
        }
        	/*  Whenever key contains password_reset word it will add extra params */
        	if(strpos($key, 'password_reset') !== false){

        		$data = array_merge($data, $extra_param);
        	}
        }

        return $data;
	}
}
if(!function_exists("encrypedecrypt"))
{
	function encrypedecrypt($txt,$is_encrypt=true)
	{
		if($is_encrypt)
		{
			return Illuminate\Support\Facades\Crypt::encryptString($txt);
		}else{
			return Illuminate\Support\Facades\Crypt::decryptString($txt);
		}
	}
}