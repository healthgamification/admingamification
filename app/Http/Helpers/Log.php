<?php
namespace  App\Http\Helpers;
use Illuminate\Database\Eloquent\Model;
class Log{

	/**
     * Store a newly created log in database.
     *
     * @param  Illuminate\Database\Eloquent\Model  $model
     * @param  String  $type
     * @param  Array  $data
     */
	public static function create_log(Model $model,$type,Array $data)
	{
		$model->uri=$_SERVER['REQUEST_URI'];
		$model->type=$type;
		$model->description=json_encode($data);
		$model->save();
	}

	/**
     * View all created log from database.
     *
     * @param  Illuminate\Database\Eloquent\Model  $model
     * @param  Bool  $is_all default true (Show all log witout any condition)
     * @param  Number  $perpage default 10 (Show perpage records)
     * @param  String  $from default null (Log created from date)
     * @param  String  $to default null (Log created to date)
     * @return Illuminate\Contracts\Pagination\LengthAwarePaginator $log
     */

	public static function view_log(Model $model,$is_all=true,$perpage=10,$from=null,$to=null)
	{
		$from = $from??date("Y-m-d",strtotime(date('Y-m-d').' -1 day'));
		$to = $to??date('Y-m-d');
		$log = $model->orderBy("created_at","desc");
		if(!$is_all)
			{
				$log->whereRaw("DATE(created_at) >='".$from."' AND DATE(created_at) <='".$to."'");
			}
		$log = $log->paginate($perpage);
		return $log;
	}

	/**
     * View all created log from database.
     *
     * @param  Illuminate\Database\Eloquent\Model  $model    
     * @return Array $logtype
     */

	public static function getType(Model $model)
	{
		
		return $model->select("type")->orderBy("type","asc")->groupBy("type")->get()->pluck("type");
	}

	/**
     * View log by id from database.
     *
     * @param  Illuminate\Database\Eloquent\Model  $model
     * @param  int  $id
     * @return data $log
     */

	public static function viewLogsById(Model $model,$id)
	{
		$log = $model->whereRaw("JSON_EXTRACT(description, '$.id') = $id")->whereIN('type',['create','update','delete'])->orderBy("created_at","desc")->get();
		return $log;
		
	}
}