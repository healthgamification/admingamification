<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;

class ChallengeCategory extends Model
{
    //
    use SoftDeletes;
    public function challenges()
    {
    	return $this->hasMany(Challenge::class,'challenge_category_id','id');
    }
}
