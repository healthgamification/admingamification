<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Schema;

class deletemodule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:delete-module {modulename : Module name you want to delete} {--permission : If you delete permissions}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $modulename='';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->modulename = $this->argument('modulename');
        $permission = $this->option('permission');
        if ($this->confirm('Are you sure to delete this module?')) {
        if($this->isModuleExits()===true)
        {
            if($permission==1)
            {
                $this->deleteWithPermission();
            }else{

                if ($this->confirm('Are you sure to delete permissions?')) {
                 $this->deleteWithPermission();
             }else{
                $this->deleteWithoutPermission();
            }
        }
    }else{
        echo $this->modulename." Module is not exist.";
    }
    echo $this->modulename." Module is deleted.";
}else{
    echo $this->modulename." Module is not deleted.";
}



}

protected function deleteWithoutPermission()
{
 $mname = $this->modulename;
 $vname = strtolower($this->modulename);
 unlink(app_path("/Http/Controllers/{$mname}Controller.php"));
 unlink(app_path("/{$mname}.php"));
 foreach (glob(resource_path("/views/".$vname).'/*') as $file) {
  unlink($file);
}
 rmdir(resource_path("/views/".$vname));

 $this->routes();
 $this->migration();
}
protected function deleteWithPermission()
{
    $this->deleteWithoutPermission();
    $this->deletePermissionAndModule();
}

protected function migration()
{
 $vname = strtolower($this->modulename);
 $tablename = Str::plural(Str::snake($this->modulename));       
 Schema::dropIfExists($tablename);       
 $filename = "*_create_".$vname."_table.php";
 foreach (glob(base_path("/database/migrations/".$filename)) as $file) {
  unlink($file);
}

}

protected function routes($per=0)
{
    $mname = $this->modulename;
    $vname = strtolower($this->modulename);   
    unlink(base_path("/routes/".$vname)."/".$vname.".php");
    rmdir(base_path("/routes/".$vname));

    $routefile = file_get_contents(base_path("/routes/web.php"));
    $line = "require __DIR__ . '/".$vname."/".$vname.".php';";
    $routefile = str_replace($line, '', $routefile);
    file_put_contents(base_path("/routes/web.php"), $routefile);
}

public function deletePermissionAndModule()
{
 $mname = $this->modulename;
 $vname = strtolower($this->modulename);

 $module = \DB::table("modules")->where(['name'=>$vname,'display_name'=>Str::title($mname)]);
 $migration = \DB::table("migrations")->where("migration","like","%$vname%")->delete();
 if($module->count()>0)
 {
     $user = Role::findByName('super-admin');
     $user->revokePermissionTo(Permission::where("module_id",$module->first()->id)->get());
    \DB::table('permissions')->where("module_id",$module->first()->id)->delete();
    $module->delete();    

 }

}

protected function isModuleExits()
{
   $mname = $this->modulename;
   $vname = strtolower($this->modulename);
   $controller = app_path("/Http/Controllers/{$mname}Controller.php");
   $model = app_path("/{$mname}.php");
   $view = resource_path("/views/".$vname);
   if(file_exists($controller) || file_exists($model) || file_exists($view))
   {
    return true;
}else{
    return false;
}
}

}
