<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class createmodule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'game:create-module {modulename : Module name you want to create} {--permission : If you use Spatie laravel permission package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Master module for superadmin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    private $modulename='';
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $this->modulename = $this->argument('modulename');
        $permission = $this->option('permission');
        if($this->isModuleExits()===false)
        {
        if($permission==1)
        {
            $this->createWithPermission();
        }else{

            if ($this->confirm('Are you use Spatie Laravel Permission Package?')) {
               $this->createWithPermission();
            }else{
                $this->createWithoutPermission();
            }
        }
        echo $this->modulename." has been created. Please run this command after changes in migration file. (php artisan migrate)";
    }else{
        echo $this->modulename." Module is already exist.";
    }

        

    }

    protected function createWithoutPermission()
    {
        $this->controller();
        $this->model();
        $this->views();
        $this->routes();
        $this->migration();
    }
    protected function createWithPermission()
    {
        $this->controller();
        $this->model();
        $this->views(1);
        $this->routes(1);
        $this->migration();
        $this->makePermissionAndModule();
    }

    protected function getStub($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }

    protected function controller()
    {
        $mname = $this->modulename;
        $vname = strtolower($this->modulename);
        $rnameplural = Str::plural(strtolower($this->modulename));
        $languagename = Str::upper(strtolower($this->modulename));
        $controllerTemplate = str_replace(
            ['{moduleName}','{viewFolder}','{routeName}','{languageName}'],
            [$mname,$vname,$rnameplural,$languagename],
            $this->getStub('controller')
        );
        file_put_contents(app_path("/Http/Controllers/{$mname}Controller.php"), $controllerTemplate);
    }

    protected function model()
    {
        $mname = $this->modulename;
        $modelTemplate = str_replace(
            ['{moduleName}'],
            [$mname],
            $this->getStub('model')
        );
        file_put_contents(app_path("/{$mname}.php"), $modelTemplate);
    }

    protected function migration()
    {
       $mname = $this->modulename;
       $vname = strtolower($this->modulename);
       $tablename = Str::plural(Str::snake($this->modulename));
       $migrationTemplate = str_replace(
        ['{moduleName}','{tableName}'],
        [$mname,$tablename],
        $this->getStub('migration')
    );
       $filename = date("Y_m_d_His")."_create_".$vname."_table.php";
       file_put_contents(base_path("/database/migrations/".$filename), $migrationTemplate);
   }

   protected function views($per=0)
   {
    $mname = $this->modulename;
    $vname = strtolower($this->modulename);
    $rnameplural = Str::plural(strtolower($this->modulename));
    $languagename = Str::upper(strtolower($this->modulename));
    if(!file_exists(resource_path("/views/".$vname)))
    {
        mkdir(resource_path("/views/".$vname),777);
    }
    foreach (["index","search_data","create","edit","view"] as $file) {
        $filename= $file;   
        $file = $per==0?'p'.$file:$file;    
        $viewTemplate = str_replace(
            ['{moduleName}','{viewFolder}','{routeName}','{languageName}'],
            [$mname,$vname,$rnameplural,$languagename],
            $this->getStub($file)
        );
        
        file_put_contents(resource_path("/views/".$vname."/".$filename.".blade.php"), $viewTemplate);
    }

}

protected function routes($per=0)
{
    $mname = $this->modulename;
    $vname = strtolower($this->modulename);
    $rnameplural = Str::plural(strtolower($this->modulename));
    $languagename = Str::upper(strtolower($this->modulename));
    if(!file_exists(base_path("/routes/".$vname)))
    {
        mkdir(base_path("/routes/".$vname),777);
    }
    $file = $per==0?'proute':"route";
    $routeTemplate = str_replace(
        ['{moduleName}','{routeNamePlural}','{viewFolder}'],
        [$mname,$rnameplural,$vname],
        $this->getStub($file)
    );
    file_put_contents(base_path("/routes/".$vname)."/".$vname.".php", $routeTemplate);
    $routefile = file_get_contents(base_path("/routes/web.php"));
    $routefile .= "\r\nrequire __DIR__ . '/".$vname."/".$vname.".php';";
    file_put_contents(base_path("/routes/web.php"), $routefile);
}

public function makePermissionAndModule()
{
 $mname = $this->modulename;
 $vname = strtolower($this->modulename);

 $moduleId = \DB::table("modules")->insertGetId([
    'name'=>$vname,
    'display_name'=>Str::title($mname),
    'icon'=>'icon-key',
    'active'=>1,
    'created_at'=>date("Y-m-d H:i:s")
]);
 \DB::table('permissions')->insert([
    [
        'name' => 'create-'.$vname,
        'display_name' => 'Create '.Str::title($mname),
        'guard_name' => 'web',
        'module_id' => $moduleId,
        'created_at' => Date('Y-m-d H:i:s')
    ],
    [
        'name' => 'read-'.$vname,
        'display_name' => 'Read '.Str::title($mname),
        'guard_name' => 'web',
        'module_id' => $moduleId,
        'created_at' => Date('Y-m-d H:i:s')
    ],
    [
        'name' => 'update-'.$vname,
        'display_name' => 'Update '.Str::title($mname),
        'guard_name' => 'web',
        'module_id' => $moduleId,
        'created_at' => Date('Y-m-d H:i:s')
    ],
    [
        'name' => 'delete-'.$vname,
        'display_name' => 'Delete '.Str::title($mname),
        'guard_name' => 'web',
        'module_id' => $moduleId,
        'created_at' => Date('Y-m-d H:i:s')
    ]
]);


 $user = Role::findByName('super-admin');
 $user->givePermissionTo(Permission::all());

}

protected function isModuleExits()
{
   $mname = $this->modulename;
   $vname = strtolower($this->modulename);
   $controller = app_path("/Http/Controllers/{$mname}Controller.php");
   $model = app_path("/{$mname}.php");
   $view = resource_path("/views/".$vname);
   if(file_exists($controller) || file_exists($model) || file_exists($view))
   {
    return true;
   }else{
    return false;
   }
}
}
