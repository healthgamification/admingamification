<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;

class ChallengeSetMappings extends Model
{
    //
    use SoftDeletes;
    protected $guarded = ['id'];
    public function challenge()
    {
        return $this->belongsTo(Challenge::class,'challenge_id','id');
    }
}
