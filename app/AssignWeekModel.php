<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignWeekModel extends Model
{
    //
    protected $table ='organization_weeks';
     protected $guarded = ['id'];

    public function quiz()
    {
    	return $this->hasMany(AssignQuizWeekModel::class,"week_id","id")->where("status","Active");
    }

    public function challenge()
    {
    	return $this->hasMany(AssignChallengeWeekModel::class,"week_id","id")->where("status","Active")->where("org_week_challenge_assigns.org_id",$this->org_id);
    }

}
