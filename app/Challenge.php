<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class Challenge extends Model
{
	 use SoftDeletes;

    protected $guarded = ['id'];
    public function challenges()
    {
    	return $this->hasOne(ChallengeCategory::class,'id','challenge_category_id');
    }

    public function selected()
    {
    	return $this->hasMany(ChallengeSetMappings::class,'challenge_id','id');
    }



     /**
     * Get the name of the "deleted at" column.
     *
     * @return string
     */
    public function getDeletedAtColumn()
    {
        return defined('static::challenge_status') ? static::challenge_status : 'challenge_status';
    }

    public function language()
    {
        return $this->hasOne(Language::class,'locale','lang_code');
    }

    public function category()
    {
        return $this->hasOne(ChallengeCategory::class,'id','challenge_category_id');
    }


}
