<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBulkUploadLog extends Model
{
    //
    protected $table ='question_bulk_upload_log';
    protected $guarded = ['id'];
}
