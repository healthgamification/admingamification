<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class ChallengeSets extends Model
{
    //
    use SoftDeletes;
     protected $guarded = ['id'];

     public function challenges()
     {
     	return $this->hasMany(ChallengeSetMappings::class,'challenge_set_id','id')->where('status', 'Active');
     }

    public function selected($orgid)
     {
     	return $this->hasMany(AssignChallengeWeekModel::class,'challenge_set_id','id')->orderBy("week_id",'asc')->where("org_week_challenge_assigns.org_id",$orgid)->get();
     }

     public function language()
    {
        return $this->hasOne(Language::class,'locale','lang_code');
    }
}
