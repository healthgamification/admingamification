<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class DynamicField extends Model
{
	use SoftDeletes;
    protected $guarded = ['id'];
}