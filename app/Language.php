<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers\SoftDeletes;
class Language extends Model
{
    //
    use SoftDeletes;
     protected $guarded = ['id'];

    public function translations()
    {
    	return $this->hasMany(Translation::class,'locale','locale')->orderBy("item",'asc');
    }
}
