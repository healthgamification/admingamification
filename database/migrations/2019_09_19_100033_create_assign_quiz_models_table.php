<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignQuizModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('organization_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("week_no")->unsigned()->nullable();
            $table->date("startdate");
            $table->date("enddate");

            $table->integer("org_id")->unsigned()->nullable();
            $table->foreign('org_id')->references('id')->on('organizations'); 

           $table->string("status")->default("Active");
            $table->timestamps();
        });

        Schema::create('org_week_quiz_qssigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("org_id")->unsigned()->nullable();
            $table->foreign('org_id')->references('id')->on('organizations'); 

            $table->integer("week_id")->unsigned()->nullable();
            $table->foreign('week_id')->references('id')->on('organization_weeks');

            $table->integer("quiz_set_id")->unsigned()->nullable();
            $table->foreign('quiz_set_id')->references('id')->on('question_sets');
             $table->string("status")->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_week_quiz_qssigns');
        Schema::dropIfExists('organization_weeks');
    }
}
