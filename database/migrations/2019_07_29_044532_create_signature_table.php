<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignatureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signature', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name');
            $table->string('designation');
            $table->string('linkedinUrl');
            $table->string('websiteName');
            $table->string('websiteUrl');
            $table->string('mobileNo');
            $table->string('landlineNo');
            $table->string('emailAddress');
            $table->string('skypeId');
            $table->text('officeAddress');
            $table->longText('image');
            $table->string('signatureType');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signature');
    }
}
