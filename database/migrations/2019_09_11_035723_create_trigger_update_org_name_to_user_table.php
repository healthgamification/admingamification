<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateOrgNameToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `update_og_name_to_user_table`;CREATE TRIGGER `update_og_name_to_user_table` AFTER UPDATE ON `organizations`
        FOR EACH ROW UPDATE `users` SET `org_name`=(select name from `organizations` where `id` = new.id) WHERE org_id=new.id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_og_name_to_user_table');
    }
}
