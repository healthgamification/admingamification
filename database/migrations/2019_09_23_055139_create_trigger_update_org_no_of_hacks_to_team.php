<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateOrgNoOfHacksToTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         DB::unprepared('DROP TRIGGER IF EXISTS `update_org_no_of_hacks_to_member_table`; CREATE TRIGGER `update_org_no_of_hacks_to_member_table` AFTER UPDATE ON `organizations` FOR EACH ROW UPDATE `members` SET `no_of_challenge`=(select no_of_challenge from `organizations` where `id` = new.id) WHERE organization_id=new.id and old.no_of_challenge!=new.no_of_challenge');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                    Schema::dropIfExists('update_org_no_of_hacks_to_member_table');
    }
}
