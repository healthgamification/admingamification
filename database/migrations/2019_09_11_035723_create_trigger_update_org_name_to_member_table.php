<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateOrgNameToMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `update_og_name_to_member_table`;CREATE TRIGGER `update_og_name_to_member_table` AFTER UPDATE ON `organizations`
        FOR EACH ROW UPDATE `members` SET `organization_name`=(select name from `organizations` where `id` = new.id) WHERE organization_id=new.id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_og_name_to_member_table');
    }
}
