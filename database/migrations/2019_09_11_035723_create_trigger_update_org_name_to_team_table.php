<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateOrgNameToTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `update_organization_name_to_team_table`;CREATE TRIGGER `update_organization_name_to_team_table` AFTER UPDATE ON `organizations`
        FOR EACH ROW UPDATE `teams` SET `organization_name`=(select name from `organizations` where `id` = new.id) WHERE organization_id=new.id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_organization_name_to_team_table');
    }
}
