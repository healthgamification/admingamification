<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name",120);
            $table->string("label",120);
            $table->longtext("label_attr")->nullable();
            $table->string("type",20);
            $table->longtext("options")->nullable();
            $table->longtext("attr")->nullable();
            $table->integer("created_by")->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('users');
            $table->smallInteger("priority",5)->default(0);
            $table->string("status",20)->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_fields');
    }
}
