<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSelectedOptionInPlayerChallenges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_challenges', function (Blueprint $table) {
            //
           
            $table->integer("selected_option")->unsined()->nullable()->after("obtained_mark");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_challenges', function (Blueprint $table) {
            //
        });
    }
}
