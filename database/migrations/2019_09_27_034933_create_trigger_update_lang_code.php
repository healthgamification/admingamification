<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerUpdateLangCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TRIGGER IF EXISTS `update_lang_code`;
DELIMITER $$
CREATE TRIGGER `update_lang_code` AFTER UPDATE ON `languages` FOR EACH ROW 
        BEGIN
        IF (old.locale <> new.locale) THEN
        UPDATE `challenges` SET `lang_code`=new.locale WHERE challenges.lang_code=old.locale;
        UPDATE `challenge_sets` SET `lang_code`=new.locale WHERE challenge_sets.lang_code=old.locale;
        UPDATE `organizations` SET `lang_code`=new.locale WHERE organizations.lang_code=old.locale;
        UPDATE `questions` SET `lang`=new.locale WHERE questions.lang=old.locale;
        UPDATE `question_sets` SET `lang_code`=new.locale WHERE question_sets.lang_code=old.locale;
        END IF ;
        END $$
        DELIMITER ;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_lang_code');
    }
}
