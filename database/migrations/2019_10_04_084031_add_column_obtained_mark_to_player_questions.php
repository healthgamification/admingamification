<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnObtainedMarkToPlayerQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_questions', function (Blueprint $table) {
            //
            $table->float("obtained_mark",6,2)->default(0.00)->after("is_correct");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_questions', function (Blueprint $table) {
            //
        });
    }
}
