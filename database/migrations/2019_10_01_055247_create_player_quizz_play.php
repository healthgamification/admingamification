<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerQuizzPlay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('category_name');
            $table->tinyInteger("qsetid");
            $table->tinyInteger("question_id");
            $table->tinyInteger("week_no");
            $table->string("qsettitle",100);
            $table->text("qsetdescription");
            $table->text('question')->collate('utf8_general_ci');
            $table->string('lang',20)->default('en');
            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->integer('team_id')->unsigned()->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->integer('memebr_id')->unsigned()->nullable();
            $table->foreign('memebr_id')->references('id')->on('members');
            $table->tinyInteger('weightage')->default(1);
            $table->string("correct_answer");
            $table->integer('user_answer')->unsigned()->nullable();
            $table->tinyInteger('is_correct')->default(0);
            $table->tinyInteger('response_time')->default(0);
            $table->string('status')->default('Active');
            $table->timestamps();
        });
          Schema::create('player_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('question_id');
            $table->tinyInteger('answer_id');
            $table->string('option',255)->collate('utf8_general_ci');
            $table->tinyInteger('is_correct')->default(0);
            $table->string('status')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('play_questions');
        Schema::dropIfExists('play_answers');
    }
}
