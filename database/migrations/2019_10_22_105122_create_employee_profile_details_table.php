<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeProfileDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_profile_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("field_id")->unsigned()->nullable();
            $table->foreign('field_id')->references('id')->on('dynamic_fields');
            $table->string("title",200);
            $table->longtext("value");
            $table->integer("member_id")->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('members');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_profile_details');
    }
}
