<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChallengeOptionPointColumnInPlayerChallengeOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('player_challenge_options', function (Blueprint $table) {
            //
            $table->float("point",6,2)->default(100)->after("challenge_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('player_challenge_options', function (Blueprint $table) {
            //
        });
    }
}
