<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengeSetMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_set_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("challenge_set_id")->unsigned()->nullable();
            $table->foreign('challenge_set_id')->references('id')->on('challenge_sets');
            $table->integer("challenge_id")->unsigned()->nullable();
            $table->foreign('challenge_id')->references('id')->on('challenges');
            $table->string("status",15)->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_set_mappings');
    }
}
