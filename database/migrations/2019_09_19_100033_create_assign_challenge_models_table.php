<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignChallengeModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org_week_challenge_assigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("org_id")->unsigned()->nullable();
            $table->foreign('org_id')->references('id')->on('organizations'); 

            $table->integer("week_id")->unsigned()->nullable();
            $table->foreign('week_id')->references('id')->on('organization_weeks');

            $table->integer("challenge_set_id")->unsigned()->nullable();
            $table->foreign('challenge_set_id')->references('id')->on('challenge_sets');
             $table->string("status")->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_week_challenge_assigns');
    }
}
