<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_configs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('driver',20);
            $table->string('host',50);
            $table->string('port',6);
            $table->string('from',255);
            $table->string('encryption',4);
            $table->string('username',50);
            $table->string('password',100);
            $table->string('sendmail',255);
            $table->boolean('pretend')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_configs');
    }
}
