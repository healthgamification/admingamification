<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerForPlayercount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        DROP TRIGGER IF EXISTS `playerCount`;CREATE DEFINER=`root`@`localhost` TRIGGER `playerCount` AFTER UPDATE ON `team_players` FOR EACH ROW UPDATE `teams` SET `total_player`=(select count(*) from `team_players` where `team_id` = new.team_id and status!="Delete" ) WHERE id=new.team_id
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playerCount');
    }
}
