<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayerChallengePlay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('player_challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('category_name');
            $table->tinyInteger("csetid");
            $table->integer("challenge_id")->unsigned();
            $table->integer("week_no")->unsigned()->nullable();
            $table->foreign('week_no')->references('id')->on('organization_weeks');
            $table->string("csettitle",100);
            $table->text("csetdescription");
            $table->text('challenge')->collate('utf8_general_ci');
            $table->text('challenge_desc')->collate('utf8_general_ci');
            $table->string('lang',20)->default('en');
            $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->integer('team_id')->unsigned()->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->integer('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('members');
            $table->tinyInteger('complexity_of_challenge')->default(5);
            $table->float("obtained_mark",5,2);
            $table->string('status')->default('Active');
            $table->index("challenge_id");
            $table->timestamps();
        });
          Schema::create('player_challenge_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("challenge_id")->unsigned();
            $table->foreign('challenge_id')->references('challenge_id')->on('player_challenges');
             $table->integer('option_id')->unsigned()->nullable();
            $table->string('option',255)->collate('utf8_general_ci');
            $table->string('status')->default('Active');
            $table->index("option_id");
            $table->timestamps();
        });

        Schema::create('player_challenge_start', function (Blueprint $table) {
            $table->increments('id');
           $table->integer('organization_id')->unsigned()->nullable();
            $table->foreign('organization_id')->references('id')->on('organizations');
            $table->integer('team_id')->unsigned()->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->integer('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('members');            
            $table->integer("week_no")->unsigned()->nullable();
            $table->foreign('week_no')->references('id')->on('organization_weeks');
            $table->integer("challenge_id")->unsigned();
            $table->foreign('challenge_id')->references('challenge_id')->on('player_challenges');
            $table->date("start_date");
            $table->date("end_date");
            $table->string('status')->default('Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
