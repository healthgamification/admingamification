<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionSetMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_set_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("question_set_id")->unsigned()->nullable();
            $table->foreign('question_set_id')->references('id')->on('question_sets');
            $table->integer("question_id")->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('questions');
            $table->string("status",15)->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_set_mappings');
    }
}
