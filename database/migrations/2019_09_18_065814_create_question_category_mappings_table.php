<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionCategoryMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_category_mappings', function (Blueprint $table) {
            $table->increments('id');
              $table->integer("question_cat_id")->unsigned()->nullable();
            $table->foreign('question_cat_id')->references('id')->on('questions_category');
            $table->integer("question_id")->unsigned()->nullable();
            $table->foreign('question_id')->references('id')->on('questions');
            $table->string("status",15)->default("Active");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_category_mappings');
    }
}
