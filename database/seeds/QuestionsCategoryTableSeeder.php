<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class QuestionsCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'quiz-category',
            'display_name' => 'Quiz Categories',
            'icon' => 'icon-notebook',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'create-quiz-category',
                'display_name' => 'Create Quiz Category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-quiz-category',
                'display_name' => 'Read Quiz Category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-quiz-category',
                'display_name' => 'Update Quiz Category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-quiz-category',
                'display_name' => 'Delete Quiz Category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
    }
}
