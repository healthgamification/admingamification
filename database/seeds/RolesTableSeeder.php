<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'roles',
            'display_name' => 'Roles',
            'icon' => 'icon-key',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'read-roles',
                'display_name' => 'Read',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'create-roles',
                'display_name' => 'Create',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-roles',
                'display_name' => 'Update',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-roles',
                'display_name' => 'Delete',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        // Create default roles
        $superadmin = Role::create([
            'name' => 'super-admin',
            'display_name' => 'Super Admin'
        ]);
        // Assign permissions to admin role
        $superadmin->givePermissionTo(Permission::all());
        
        $admin = Role::create([
            'name' => 'admin',
            'display_name' => 'Admin'
        ]);

    }
}
