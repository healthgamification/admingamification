<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class MailConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $moduleId = DB::table('modules')->insertGetId([
            'name' => 'mail_config',
            'display_name' => 'Mail Config',
            'icon' => 'icon-envelope-open',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'update-mail-config',
                'display_name' => 'Update Mail Config',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
    }
}
