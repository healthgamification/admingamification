<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $moduleId = DB::table('modules')->insertGetId([
            'name' => 'language',
            'display_name' => 'Languages',
            'icon' => 'icon-note',
            'created_at' => date('Y-m-d H:i:s')
        ]);

         // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-language',
                'display_name' => 'Create Language',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-language',
                'display_name' => 'Read Language',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-language',
                'display_name' => 'Update Language',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-language',
                'display_name' => 'Delete Language',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-translations',
                'display_name' => 'Update Translation Data',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);

         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
    }
}
