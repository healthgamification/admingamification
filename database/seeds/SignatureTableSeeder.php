<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SignatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'signature',
            'display_name' => 'Signature',
            'icon' => 'icon-key'
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-signature',
                'display_name' => 'Create Signature',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'read-signature',
                'display_name' => 'Read Signature',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'update-signature',
                'display_name' => 'Update Signature',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ],
            [
                'name' => 'delete-signature',
                'display_name' => 'Delete Signature',
                'guard_name' => 'web',
                'module_id' => $moduleId
            ]
        ]);
        
       
    }
}
