<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'organization',
            'display_name' => 'Organization',
            'icon' => 'icon-key',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-organization',
                'display_name' => 'Create Organization',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-organization',
                'display_name' => 'Read Organization',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-organization',
                'display_name' => 'Update Organization',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-organization',
                'display_name' => 'Delete Organization',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
             [
                'name' => 'create-organization-admin',
                'display_name' => 'Create Organization Admin',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
             [
                'name' => 'delete-organization-admin',
                'display_name' => 'Delete Organization Admin',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);
        
        $admin = Role::findByName('super-admin');
        $admin->givePermissionTo(Permission::all());

        // $user = Role::findByName('admin');
        // $user->givePermissionTo('read-organization');
    }
}
