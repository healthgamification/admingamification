<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class ChallengeCategoryTableDataSeeder extends Seeder
{   
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'challenge-category',
            'display_name' => 'Challenge Category',
            'icon' => 'icon-key',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-challenge-category',
                'display_name' => 'Create challenge category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-challenge-category',
                'display_name' => 'Read challenge category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'view-challenge-category',
                'display_name' => 'View challenge category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-challenge-category',
                'display_name' => 'Update challenge category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-challenge-category',
                'display_name' => 'Delete challenge category',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
         
         
    }
}
