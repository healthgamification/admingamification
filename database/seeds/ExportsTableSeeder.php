<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class ExportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'export',
            'display_name' => 'Exports',
            'icon' => 'icon-home',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'read-export',
                'display_name' => 'Read Exports',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());

         // $user = Role::findByName('admin');
         // $user->givePermissionTo(Permission::all());
    }
}
