<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'member',
            'display_name' => 'Member',
            'icon' => 'icon-key',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-member',
                'display_name' => 'Create member',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-member',
                'display_name' => 'Read member',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-member',
                'display_name' => 'Update member',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-member',
                'display_name' => 'Delete member',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

         // Assign permissions to admin role
         // $admin = Role::findByName('admin');
         // $admin->givePermissionTo(Permission::all());
 
         // Assign permissions to super-admin role
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
        
       
    }
}
