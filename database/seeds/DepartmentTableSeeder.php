<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'department',
            'display_name' => 'Departments',
            'icon' => 'icon-home',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'create-department',
                'display_name' => 'Create Department',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-department',
                'display_name' => 'Read Department',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-department',
                'display_name' => 'Update Department',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-department',
                'display_name' => 'Delete Department',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
    }
}
