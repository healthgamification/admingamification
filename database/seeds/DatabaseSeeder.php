<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(DashboardTableSeeder::class);
        $this->call(OrganizationTableSeeder::class);
        $this->call(MemberTableSeeder::class);
        $this->call(ChallengeTableSeeder::class);
        $this->call(TeamTableSeeder::class);
        $this->call(ChallengeCategoryTableDataSeeder::class);
        $this->call(AssignQuizChallengeTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        $this->call(ExportsTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(DesignationTableSeeder::class);
        $this->call(ChallengeSetTableSeeder::class);
        $this->call(QuestionSetTableSeeder::class);
        $this->call(QuestionsCategoryTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(MailConfigsTableSeeder::class);
        $this->call(MailTemplateTableSeeder::class);
        $this->call(LogTableSeeder::class);
        
    }
}
