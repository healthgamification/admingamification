<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class DesignationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'designation',
            'display_name' => 'Designations',
            'icon' => 'icon-people',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        DB::table('permissions')->insert([
            [
                'name' => 'create-designation',
                'display_name' => 'Create Designation',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-designation',
                'display_name' => 'Read Designation',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-designation',
                'display_name' => 'Update Designation',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-designation',
                'display_name' => 'Delete Designation',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

        
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
    }
}
