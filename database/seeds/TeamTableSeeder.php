<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Module
        $moduleId = DB::table('modules')->insertGetId([
            'name' => 'team',
            'display_name' => 'Team',
            'icon' => 'icon-key',
            'created_at' => Date('Y-m-d H:i:s')
        ]);

        // Permissions
        DB::table('permissions')->insert([
            [
                'name' => 'create-team',
                'display_name' => 'Create team',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-team',
                'display_name' => 'Read team',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'view-team',
                'display_name' => 'View team',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'update-team',
                'display_name' => 'Update team',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-team',
                'display_name' => 'Delete team',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'read-player',
                'display_name' => 'Read Player',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'add_player',
                'display_name' => 'Add Player',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ],
            [
                'name' => 'delete-player',
                'display_name' => 'Delete Player',
                'guard_name' => 'web',
                'module_id' => $moduleId,
                'created_at' => Date('Y-m-d H:i:s')
            ]
        ]);

         // Assign permissions to admin role
         // $admin = Role::findByName('admin');
         // $admin->givePermissionTo(Permission::all());
 
         // Assign permissions to user role
         $user = Role::findByName('super-admin');
         $user->givePermissionTo(Permission::all());
        
       
    }
}
