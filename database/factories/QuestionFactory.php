<?php

use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        'category_id' => "1",
        'question' => $faker->text,
        'weightage' => "1",
        'lang' => (rand(0,1)==1?'en':'de'),
        'options_count' => '5'
    ];
});
