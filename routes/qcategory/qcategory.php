<?php
use Illuminate\Support\Facades\Input;

Route::middleware('auth')->group(function () {
    Route::group(['prefix' => 'qcategory'], function () {
        Route::get('/', 'QuestionsCategoryController@index')->middleware('permission:read-quiz-category');
        Route::get('/create', 'QuestionsCategoryController@create')->middleware('permission:create-quiz-category');
        Route::post('/store', 'QuestionsCategoryController@store')->middleware('permission:create-quiz-category');
        Route::get('/{id}/edit', 'QuestionsCategoryController@edit')->middleware('permission:update-quiz-category');
        Route::post('/update/{id}', 'QuestionsCategoryController@update')->middleware('permission:update-quiz-category');
        Route::get('/{id}/delete', 'QuestionsCategoryController@destroy')->middleware('permission:delete-quiz-category');
        Route::get('/{perpage?}/ajax/fetch_data/', 'QuestionsCategoryController@fetch_data');
        Route::get('/{id}/view', 'QuestionsCategoryController@show')->middleware('permission:read-quiz-category');
    });
});
