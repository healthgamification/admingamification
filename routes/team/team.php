<?php
use Illuminate\Support\Facades\Input;
use App\Organization;
use App\User;

Route::middleware('auth')->group(function () {
    
    Route::group(['prefix' => 'teams'], function () {
        Route::get('/{org_id?}', 'TeamController@index')->middleware('permission:read-team');
       
        Route::get('/{id}/view', 'TeamController@view')->middleware('permission:read-team');
        Route::get('/{org_id?}/ajax/fetch_data/', 'TeamController@fetch_data')->middleware('permission:read-team');
    });
    Route::get('team/create/{org_id?}', 'TeamController@create')->middleware('permission:create-team');
    Route::post('team/store', 'TeamController@store')->middleware('permission:create-team');
    Route::get('team/{id}/edit', 'TeamController@edit')->middleware('permission:update-team');
    Route::get('team/{id}/view', 'TeamController@show')->middleware('permission:read-team');
    Route::post('team/update/{id}', 'TeamController@update')->middleware('permission:update-team');
    Route::get('team/{id}/delete', 'TeamController@destroy')->middleware('permission:delete-team');
    // api
    Route::group(['prefix' => 'api/team'], function() {
      Route::get('/all', 'TeamController@all');
  });

//Players Route
  Route::group(['prefix' => 'players'], function () {
    Route::get('/{team_id}/{org_id}', 'TeamController@addplayer')->middleware('permission:read-player');
    Route::post('/save', 'TeamController@saveplayer')->middleware('permission:read-player');
    Route::get('/{is_seleted}/{orgid}/{team_id}/{perpage?}/ajax/fetch_data/', 'TeamController@getPlayerByOrgid')->middleware('permission:read-player');
    Route::post('/update', 'TeamController@updateplayer')->middleware('permission:read-player');
    
});
});
