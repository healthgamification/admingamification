<?php
Route::middleware('auth')->group(function () {
        Route::group(['prefix' => 'users'], function() {
            // Route::view('/', 'users.index')->middleware('permission:read-users');
            // Route::view('/create', 'users.create')->middleware('permission:create-users');
            // Route::view('/{user}/edit', 'users.edit')->middleware('permission:update-users');
             Route::get('/', 'UserController@index')->middleware('permission:read-users');
             Route::get('/ajax/fetch_data/', 'UserController@fetch_data');

             //create user             
             Route::get('/create', 'UserController@create')->middleware('permission:create-users');
             Route::post("/store",'UserController@store')->middleware("permission:create-users");
             //update user             
             Route::get('/{id}/edit', 'UserController@edit')->middleware('permission:update-users');
             Route::post("/update/{id}",'UserController@update')->middleware("permission:update-users");

             //delete user
             Route::get('/{id}/delete', 'UserController@destroy')->middleware('permission:delete-users');
              Route::get('/{id}/view', 'UserController@show')->middleware('permission:read-users');
        });



        // api
        Route::group(['prefix' => 'api/users'], function() {
            Route::get('/getUserRoles/{user}', 'UserController@getUserRoles');
            Route::get('/count', 'UserController@count');
            Route::post('/filter', 'UserController@filter')->middleware('permission:read-users');

            Route::get('/{user}', 'UserController@show')->middleware('permission:read-users');
            Route::post('/store', 'UserController@store')->middleware('permission:create-users');
            Route::put('/update/{user}', 'UserController@update')->middleware('permission:update-users');
            Route::delete('/{user}', 'UserController@destroy')->middleware('permission:delete-users');
        });
});
