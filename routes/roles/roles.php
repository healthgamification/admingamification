<?php
Route::middleware('auth')->group(function () {
    
        // views
        Route::group(['prefix' => 'roles'], function() {
            Route::get('/', 'RoleController@index')->middleware('permission:read-roles');
            ;
            Route::get('/ajax/fetch_data/', 'RoleController@fetch_data')->middleware('permission:read-roles');
            ;
        });

        Route::group(['prefix' => 'role'],function() {
            Route::get('/create','RoleController@create')->middleware('permission:create-roles');
            Route::post('/store','RoleController@store')->middleware('permission:create-roles');
            Route::get('{id}/edit','RoleController@edit')->middleware('permission:update-roles');
            Route::post('/update','RoleController@update')->middleware('permission:update-roles');
            Route::get('{id}/delete','RoleController@destroy')->middleware('permission:delete-roles');
            Route::get('{id}/view','RoleController@show')->middleware('permission:read-roles');
        });
    
});
