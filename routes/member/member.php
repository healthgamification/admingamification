<?php
use Illuminate\Support\Facades\Input;
use App\Organization;
use App\User;

Route::middleware('auth')->group(function () {
    // Route::get('member/create','MemberController@create')->middleware('permission:read-member');
    
    Route::group(['prefix' => 'members'], function () {
        Route::get('/{org_id?}', 'MemberController@index')->middleware('permission:read-member');
       
        Route::get('/{id}/view', 'MemberController@view')->middleware('permission:read-member');
        Route::get('/{org_id?}/ajax/fetch_data/', 'MemberController@fetch_data');
        Route::post('/bulk_upload','MemberController@uploadcsv')->middleware('permission:create-member');

    });
     Route::get('log/bulkerrorlog','MemberController@downloadcsv')->middleware('permission:create-member');
    Route::get('member/create/{org_id?}', 'MemberController@create')->middleware('permission:create-member');
    Route::post('member/store', 'MemberController@store')->middleware('permission:create-member');
    Route::get('member/{id}/edit', 'MemberController@edit')->middleware('permission:update-member');
    Route::post('member/update/{id}', 'MemberController@update')->middleware('permission:update-member');
    Route::get('member/{id}/delete', 'MemberController@destroy')->middleware('permission:delete-member');
    Route::get('member/{id}/view', 'MemberController@show');
    Route::get('member/getdepartmentanddesignation/{orgid}', 'MemberController@getDepartmentAndDesignation');
   

    Route::any('/members/search', function () {
        $q = Input::get('q');
        if ($q != "") {
            $adminId = DB::select('SELECT org_id FROM users WHERE name LIKE \'%'.$q.'%\'');
            //print_r($adminId); exit;
            if ($adminId) {
                $members = Organization::where('id', $adminId)->paginate(1)->setPath('');
            } else {
                $members = Organization::where('name', 'LIKE', '%' . $q . '%')->orWhere('address', 'LIKE', '%'.$q.'%')->orWhere('email', 'LIKE', '%'.$q.'%')->orWhere('phone', 'LIKE', '%'.$q.'%')->orWhere('id', 'LIKE', '%'.$q.'%')->paginate(1)->setPath('');
            }
            $pagination = $members->appends(array(
         'q' => Input::get('q')
       ));
            if (count($members) > 0) {
                return view('member/index')->with('members', $members)->withQuery($q)->with('q',$q);
            }
        }
        return view('member/index')->with('members', $members)->withQuery($q)->with('q',$q);
    });

    // api
    Route::group(['prefix' => 'api/member'], function() {
      Route::get('/all', 'MemberController@all');
  });
});
