<?php
use Illuminate\Support\Facades\Input;

Route::middleware('auth')->group(function () {
    Route::group(['prefix' => 'questions'], function () {
        Route::get('/{category_id?}/{lang?}', 'QuestionsController@index')->middleware('permission:read-question');
        Route::get('/{perpage?}/{category_id?}/{lang?}/ajax/fetch_data/', 'QuestionsController@fetch_data')->middleware('permission:read-question');
        Route::post("/bulk_upload",'QuestionsController@bulkUpload')->middleware('permission:read-question');
    });
    Route::get('questionlog/bulkerrorlog','QuestionsController@downloadcsv')->middleware('permission:read-question');
     Route::get('question/{id}/edit', 'QuestionsController@edit')->middleware('permission:update-question');
      Route::get('question/create/{category_id?}/{lang?}', 'QuestionsController@create')->middleware('permission:create-question');
     Route::post('question/store', 'QuestionsController@store')->middleware('permission:create-question');
     Route::post('question/update/{id}', 'QuestionsController@update')->middleware('permission:update-question');
        Route::get('question/{id}/delete', 'QuestionsController@destroy')->middleware('permission:delete-question');
        Route::get('question/{id}/view', 'QuestionsController@show')->middleware('permission:read-question');

      Route::group(['prefix' => 'question-sets'], function () {
      	Route::get("/{lang?}","QuestionSetMappingController@index")->middleware('permission:read-quiz-set');
        Route::get('/{perpage}/{lang}/ajax/fetch_data/', 'QuestionSetMappingController@set_fetch_data')->middleware('permission:read-quiz-set');
        Route::get("/{id}/delete","QuestionSetMappingController@destroy")->middleware('permission:delete-quiz-set')->name("qset.delete");
      });
       Route::group(['prefix' => 'question-set'], function () {
      	Route::get("/create/{lang?}","QuestionSetMappingController@create")->middleware('permission:create-quiz-set')->name("question.create");
      	  Route::get('/{lang}/{type}/{cat}/{set}/ajax/fetch_data/', 'QuestionSetMappingController@fetch_data');
      	  Route::post("/save","QuestionSetMappingController@store")->middleware('permission:create-quiz-set')->name("setsave");
      	  Route::get("/{lang?}/{id}/edit",'QuestionSetMappingController@edit')->middleware('permission:update-quiz-set')->name("qset.edit");
          Route::get("/{id}/view",'QuestionSetMappingController@show')->middleware('permission:read-quiz-set')->name("qset.view");
      	  Route::post("/setupdate",'QuestionSetMappingController@update')->middleware('permission:update-quiz-set')->name("setupdate");
      });
});
