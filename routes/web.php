<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
    return view('auth/login');
});

Auth::routes();
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/home', function () {
    return redirect('dashboard');
});

Route::get('/dashboard', 'DashboardController@index')->name('home');

require __DIR__ . '/profile/profile.php';
require __DIR__ . '/users/users.php';
require __DIR__ . '/signature/signature.php';
require __DIR__ . '/roles/roles.php';
require __DIR__ . '/roles/permissions.php';
require __DIR__ . '/modules/modules.php';
require __DIR__ . '/organization/organization.php';
require __DIR__ . '/member/member.php';
require __DIR__ . '/challenge/challenge.php';
require __DIR__ . '/challenge/challenge_category.php';
require __DIR__ . '/team/team.php';
require __DIR__ . '/export/export.php';
require __DIR__ . '/qcategory/qcategory.php';
require __DIR__ . '/questions/questions.php';
require __DIR__ . '/translation/translation.php';
require __DIR__ . '/department/department.php';
require __DIR__ . '/designation/designation.php';
require __DIR__ . '/email/email.php';
require __DIR__ . '/template/template.php';
require __DIR__ . '/viewlog/viewlog.php';


require __DIR__ . '/dynamicfield/dynamicfield.php';