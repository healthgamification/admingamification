<?php
Route::middleware('auth')->group(function () {
	Route::group(['prefix' => 'departments'], function() {
		Route::get('/{orgid?}','DepartmentController@index')->middleware('permission:read-department')->name("department.index");
		Route::get('/{orgid?}/ajax/fetch_data/', 'DepartmentController@fetch_data')->middleware('permission:read-department');
	});  

	Route::group(['prefix' => 'department'], function() {
		Route::get('/create/{orgid?}','DepartmentController@create')->middleware('permission:create-department')->name("department.create");
		Route::get('/edit/{id}','DepartmentController@edit')->middleware('permission:update-department')->name("department.edit");
		Route::post('/store','DepartmentController@store')->middleware('permission:create-department')->name("department.store");
		Route::post('/update/{id}','DepartmentController@update')->middleware('permission:update-department')->name("department.update");
		Route::get('/delete/{id}','DepartmentController@destroy')->middleware('permission:delete-department')->name("department.destroy");
		Route::get('/{id}/view','DepartmentController@show')->middleware('permission:read-department')->name("department.view");
	});   
});

?>