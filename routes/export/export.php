<?php
Route::middleware('auth')->group(function () {
  Route::group(['prefix' => 'export'], function() {
    Route::get('/','ExportController@index')->middleware('permission:read-export')->name("export"); 
    Route::post("/",'ExportController@store')->middleware('permission:read-export')->name("pexport");
});   
});

?>