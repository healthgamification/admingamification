<?php
use Illuminate\Support\Facades\Input;
use App\Organization;
use App\User;

Route::middleware('auth')->group(function () {
   
    Route::group(['prefix' => 'challenges'], function () {
        Route::get('/{categoty_id?}/{lang?}', 'ChallengeController@index')->middleware('permission:read-challenge');
        Route::get('/{category_id?}/{lang?}/ajax/fetch_data/', 'ChallengeController@fetch_data')->middleware('permission:read-challenge');
        Route::post("/bulk_upload",'ChallengeController@bulkUpload')->middleware('permission:read-challenge');
    });
    Route::get('challengelog/bulkerrorlog','ChallengeController@downloadcsv')->middleware('permission:read-challenge');
    Route::get('challenge/create/{categoty_id?}/{lang?}', 'ChallengeController@create')->middleware('permission:create-challenge');
    Route::post('challenge/store', 'ChallengeController@store')->middleware('permission:create-challenge');
    Route::get('challenge/{id}/edit', 'ChallengeController@edit')->middleware('permission:update-challenge');
    Route::post('challenge/update/{id}', 'ChallengeController@update')->middleware('permission:update-challenge');
    Route::get('challenge/{id}/delete', 'ChallengeController@destroy')->middleware('permission:delete-challenge');
    Route::get('challenge/{id}/view', 'ChallengeController@show')->middleware('permission:read-challenge');

    Route::get("/bulkuploadlog/{logid}",'ChallengeController@bulkUploadLog');

    /**** Route for Challenge sets starts from here ****/
    Route::group(['prefix' => 'challenge-sets'],function() {
        Route::get('/{lang?}','ChallengeSetsController@index')->middleware('permission:read-challenge-set');
        Route::get("/{lang?}/ajax/fetch_data","ChallengeSetsController@get_fetch_data")->middleware('permission:read-challenge-set');
    });
    Route::group(['prefix' => 'challenge-set'],function() {
        Route::get('/{lang}/{type}/{cat}/{set}/ajax/fetch_data/', 'ChallengeSetsController@fetch_data')->middleware('permission:read-challenge-set');
        Route::get('/create/{lang?}','ChallengeSetsController@create')->middleware('permission:create-challenge-set');
        Route::post('/store','ChallengeSetsController@store')->middleware('permission:create-challenge-set');
        Route::get('/{id}/edit', 'ChallengeSetsController@edit')->middleware('permission:update-challenge-set');
        Route::get('/{id}/view', 'ChallengeSetsController@show')->middleware('permission:read-challenge-set');
        Route::post('/update', 'ChallengeSetsController@update')->middleware('permission:update-challenge-set');
        Route::get('/{id}/delete', 'ChallengeSetsController@destroy')->middleware('permission:delete-challenge-set');
    });
    /**** Route for Challenge sets ends here ****/

    // api
    Route::group(['prefix' => 'api/challenge'], function() {
      Route::get('/all', 'ChallengeController@all');
  });
});
