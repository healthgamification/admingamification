<?php

Route::middleware('auth')->group(function () {
   
    Route::group(['prefix' => 'challenge-category'], function () {
        Route::get("/","ChallengeCategoryController@index")->middleware('permission:read-challenge-category');
        Route::get('/ajax/fetch_data/', 'ChallengeCategoryController@fetch_data');
    });
    Route::get('challenge-category/create', 'ChallengeCategoryController@create')->middleware('permission:create-challenge-category');
    Route::post('challenge-category/store', 'ChallengeCategoryController@store')->middleware('permission:create-challenge-category');
    Route::get('challenge-category/{id}/edit', 'ChallengeCategoryController@edit')->middleware('permission:update-challenge-category');
    Route::post('challenge-category/update/{id}', 'ChallengeCategoryController@update')->middleware('permission:update-challenge-category');
    Route::get('challenge-category/{id}/delete', 'ChallengeCategoryController@destroy')->middleware('permission:delete-challenge-category');
    Route::get('challenge-category/{id?}/view', 'ChallengeCategoryController@show');

 
});
