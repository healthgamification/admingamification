<?php
Route::middleware('auth')->group(function () {
  Route::group(['prefix' => 'templates'], function() {
    Route::get('/','EmailTemplateController@index')->middleware('permission:read-mail-template')->name("template.index"); 
     Route::get('/ajax/fetch_data/', 'EmailTemplateController@fetch_data')->middleware('permission:read-mail-template');
    Route::post("/save",'EmailController@store')->middleware('permission:read-mail-template')->name("email.save");
    Route::post("/testmail",'EmailController@testmail')->middleware('permission:read-mail-template')->name("email.testmail");
});  
Route::group(['prefix' => 'template'], function() {
     Route::get("/create",'EmailTemplateController@create')->middleware('permission:create-mail-template');
     Route::get("/getdynamicdata/{type}",function($type){
     	return getTableInfo($type);
     })->middleware('permission:read-mail-template');
     Route::post("/store","EmailTemplateController@store")->middleware('permission:create-mail-template');
     Route::get("/{id}/edit","EmailTemplateController@edit")->middleware('permission:update-mail-template');
     Route::post("/update/{id}","EmailTemplateController@update")->middleware('permission:update-mail-template');
     Route::get("/{id}/delete","EmailTemplateController@destroy")->middleware('permission:delete-mail-template');
     Route::get("/{id}/view","EmailTemplateController@show")->middleware('permission:read-mail-template');
}); 
});

?>