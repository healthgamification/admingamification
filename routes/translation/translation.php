<?php
Route::middleware('auth')->group(function () {
    
    Route::group(['prefix' => 'translations'], function () {
    	Route::get("/","TranslationController@index")->middleware('permission:read-language');
        Route::get("/delete/{id}","TranslationController@destroy")->middleware('permission:delete-language');
    	Route::get('/ajax/fetch_data/', 'TranslationController@fetch_data')->middleware('permission:read-language');
    	});

    Route::group(['prefix' => 'translation'], function () {
    	Route::get("/create","TranslationController@create")->middleware('permission:create-language')->name('language.create');
    	Route::post("/create","TranslationController@store")->middleware('permission:create-language')->name('language.post');
    	Route::get("/create/{id}","TranslationController@getTranslationCreate")->middleware('permission:update-translations');
    	Route::post("/create/{id}","TranslationController@postTranslationCreate")->middleware('permission:update-translations')->name('translation.post');
        Route::get("/edit/{id}","TranslationController@edit")->middleware('permission:update-language')->name('language.edit');
        Route::post("/edit/{id}","TranslationController@update")->middleware('permission:update-language')->name('language.pedit');
        Route::get("/{id}/view","TranslationController@show")->middleware('permission:read-language')->name('language.view');
        
    	});
});