<?php
Route::middleware('auth')->group(function () {
  Route::group(['prefix' => 'email'], function() {
    Route::get('/','EmailController@index')->middleware('permission:update-mail-config')->name("export"); 
    Route::post("/save",'EmailController@store')->middleware('permission:update-mail-config')->name("email.save");
    Route::post("/testmail",'EmailController@testmail')->middleware('permission:	update-mail-config')->name("email.testmail");
});   
});

?>