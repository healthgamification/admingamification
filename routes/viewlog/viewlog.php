<?php
Route::middleware('auth')->group(function () {
  Route::group(['prefix' => 'view-log'], function() {
    Route::get('/{model}/{id}','ViewLogController@index')->name("log.view");
    Route::get('view/{model}/{id}','ViewLogController@view')->name("log.getview");  
});   
});

?>