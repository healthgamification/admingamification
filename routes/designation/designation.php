<?php
Route::middleware('auth')->group(function () {
	Route::group(['prefix' => 'designations'], function() {
		Route::get('/{orgid?}','DesignationController@index')->middleware('permission:read-designation')->name("designation.index");
		Route::get('/{orgid?}/ajax/fetch_data/', 'DesignationController@fetch_data')->middleware('permission:read-designation');
	});  

	Route::group(['prefix' => 'designation'], function() {
		Route::get('/create/{orgid?}','DesignationController@create')->middleware('permission:create-designation')->name("designation.create");
		Route::get('/edit/{id}','DesignationController@edit')->middleware('permission:update-designation')->name("designation.edit");
		Route::post('/store','DesignationController@store')->middleware('permission:create-designation')->name("designation.store");
		Route::post('/update/{id}','DesignationController@update')->middleware('permission:update-designation')->name("designation.update");
		Route::get('/delete/{id}','DesignationController@destroy')->middleware('permission:delete-designation')->name("designation.destroy");
		Route::get('/{id}/view','DesignationController@show')->middleware('permission:read-designation')->name("designation.view");
	});   
});

?>