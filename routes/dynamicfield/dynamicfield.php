<?php
Route::middleware('auth')->group(function () {
  Route::group(['prefix' => 'dynamicfields'], function() {
    Route::get('/','DynamicFieldController@index')->middleware('permission:read-dynamicfield')->name("dynamicfield.index"); 
     Route::get('/ajax/fetch_data/', 'DynamicFieldController@fetch_data')->middleware('permission:read-dynamicfield');
   
});  
Route::group(['prefix' => 'dynamicfield'], function() {
     Route::get("/create",'DynamicFieldController@create')->middleware('permission:create-dynamicfield');     
     Route::post("/store","DynamicFieldController@store")->middleware('permission:create-dynamicfield');
     Route::get("/{id}/edit","DynamicFieldController@edit")->middleware('permission:update-dynamicfield');
     Route::post("/update/{id}","DynamicFieldController@update")->middleware('permission:update-dynamicfield');
     Route::get("/{id}/delete","DynamicFieldController@destroy")->middleware('permission:delete-dynamicfield');
     Route::get("/{id}/view","DynamicFieldController@show")->middleware('permission:read-dynamicfield');
     Route::get("/{index}/{id}/updateindex","DynamicFieldController@updateIndex")->middleware('permission:read-dynamicfield');
}); 
});

?>