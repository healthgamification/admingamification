 <?php
use Illuminate\Support\Facades\Input;
use App\Organization;
use App\User;
use App\Http\Controllers\DashboardController;


Route::middleware('auth')->group(function () {
    // Route::get('organization/create','OrganizationController@create')->middleware('permission:read-organization');
    
    Route::group(['prefix' => 'organization'], function () {
        Route::get('/', 'OrganizationController@index')->middleware('permission:read-organization');
        Route::get('/create', 'OrganizationController@create')->middleware('permission:create-organization');
        Route::post('/store', 'OrganizationController@store')->middleware('permission:create-organization');
        Route::get('/{id}/edit', 'OrganizationController@edit')->middleware('permission:update-organization');
        Route::post('/update/{id}', 'OrganizationController@update')->middleware('permission:update-organization');
        Route::get('/{id}/delete', 'OrganizationController@destroy')->middleware('permission:delete-organization');
        Route::get('/{id}/view', 'OrganizationController@view')->middleware('permission:read-organization');
        Route::get('/{id}/view/users', 'OrganizationController@users')->middleware('permission:read-organization');
        Route::get('/{id}/view/user/{userId}/delete', 'OrganizationController@destroyUser')->middleware('permission:read-organization');
        Route::get('/{id}/view/admin/{userId}/delete', 'OrganizationController@destroyAdminUser')->middleware('permission:read-organization');
        //Route::get('/{id}/view/admin/{userId}', 'OrganizationController@makeAdminfromUsers')->middleware('permission:read-organization');
        Route::get('/{id}/view/admin/{userId}/add', 'OrganizationController@makeAdminfromUsers')->middleware('permission:read-organization');
        Route::get('/{id}/view/admin/{userId}/remove', 'OrganizationController@removeAdminfromUsers')->middleware('permission:read-organization');
        Route::get('/ajax/fetch_data/', 'OrganizationController@fetch_data')->middleware('permission:read-organization');
         // Users and Admins
         Route::get('{type}/{orgid}/{perpage}/ajax/fetch_data/', 'OrganizationController@fetch_admin_users')->middleware('permission:read-organization');

         //Teams
         Route::get('/{id}/view/teams', 'OrganizationController@getTeams')->middleware('permission:read-organization');
          Route::get('/view/team/{orgid}/{perpage}/ajax/fetch_data/', 'OrganizationController@fetch_teams')->middleware('permission:read-organization');

          Route::get("/{orgid}/view/quizzes","OrganizationController@getQuizzes")->middleware('permission:read-organization');
          Route::get('/search/view/quizzes/{orgid}/ajax/fetch_data/', 'OrganizationController@fetch_quizzes')->middleware('permission:read-organization');
          Route::get("/{orgid}/view/challenges","OrganizationController@getChallenges")->middleware('permission:read-organization');
          Route::get('/search/view/challenges/{orgid}/ajax/fetch_data/', 'OrganizationController@fetch_challenges')->middleware('permission:read-organization');
    });

     Route::group(['prefix' => 'assign'], function() {
      Route::get('/quiz/{orgid}', 'AssignQuizController@index')->middleware('permission:create-week')->name('assign.quiz');
      Route::post('/quiz/save/{orgid}', 'AssignQuizController@store')->middleware('permission:create-week')->name('assign.quiz.save');
      Route::get('/{lang}/{org_id}/ajax/fetch_data/', 'AssignQuizController@fetch_data')->middleware('permission:assign-quiz');
       Route::get('/challenge/{lang}/{org_id}/ajax/fetch_data/', 'AssignQuizController@challengefetch_data')->middleware('permission:assign-challenge')->name("challenge.search");
  });

    // api
    Route::group(['prefix' => 'api/organization'], function() {
      Route::get('/all', 'OrganizationController@all');
  });
});
