@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card-header px-0 bg-transparent clearfix">
      <h4 class="float-left">@lang(Lang::locale().'.MY_DASHBOARD')</h4>
    </div>

    

    <div class="row mt-5">
        <div class="card-body pt-0">
            @if(session()->has('message'))
                <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
                {{ session()->get('message') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            @endif 
            <div class="row justify-content-md-left">
                @if ($userRole=='admin')
                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-primary custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-3">
                                @lang(Lang::locale().'.MEMBER_PLURAL') 
                            </div>
                            <!-- <div class="h6 text-center mb-3">
                                <a href="#" class="link-button">View Challenges</a>
                            </div> -->
                            <div class="h6 text-center mb-3">
                                <a href="/member/create" class="link-button"> @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/members" class="link-button">@lang(Lang::locale().'.VIEW_EDIT') @lang(Lang::locale().'.MEMBER_SINGULAR')</a>
                            </div>
                            <div class="text-value"></div>
                        </div>
                    </div>
                    
                </div>

                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-success custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                @lang(Lang::locale().'.TEAM_PLURAL')
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="team/create" class="link-button">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.TEAM_SINGULAR')</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="teams" class="link-button">@lang(Lang::locale().'.VIEW_EDIT') @lang(Lang::locale().'.TEAM_SINGULAR')</a>
                            </div>
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-warning custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                Settings
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/profile" class="link-button">  @lang(Lang::locale().'.PROFILE')</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/password" class="link-button"> @lang(Lang::locale().'.PASSWORD')</a>
                            </div>
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>
                
                @else
                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-success custom-card">
                    <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                @lang(Lang::locale().'.ORGANIZATION_PLURAL')
                            </div>
                            <div class="h6   text-center mb-3">
                                <a href="/organization/create" class="link-button">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</a>
                            </div>
                            <div class="h6   text-center mb-3">
                                <a href="/organization" class="link-button">@lang(Lang::locale().'.VIEW_EDIT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</a>
                            </div>
                            <!-- <div class="h6   text-center mb-3">
                                <a href="/organization" class="link-button">Edit Organization</a>
                            </div> -->
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>
                    
                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-danger custom-card">
                    <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                @lang(Lang::locale().'.CHALLENGE_PLURAL') 
                            </div>
                            <!-- <div class="h6 text-center mb-3">
                                <a href="#" class="link-button">View Challenges</a>
                            </div> -->
                            <div class="h6 text-center mb-3">
                                <a href="/challenge/create" class="link-button"> @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/challenges" class="link-button">@lang(Lang::locale().'.VIEW_EDIT') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</a>
                            </div>
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-info custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                Quizzes
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/question-set/create" class="link-button">Create New Quiz</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/question-sets" class="link-button"> View / Edit Quizzes</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-primary custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                Questions
                            </div>
                            <!-- <div class="h6 text-center mb-3">
                                <a href="#" class="link-button"> View Question</a>
                            </div> -->
                            <div class="h6 text-center mb-3">
                                <a href="/question/create" class="link-button">Create New Question</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/questions" class="link-button">View / Edit Questions</a>
                            </div>
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>
                    
                <div class="col-sm-6 col-xl-4">
                    <div class="card text-white bg-warning custom-card">
                        <div class="card-mask"></div>
                        <div class="card-body">
                            <div class="h2 text-center mb-4">
                                @lang(Lang::locale().'.SETTINGS')
                            </div>
                            <!-- <div class="h6 text-center mb-3">
                                <a href="#" class="link-button"> Permissions</a>
                            </div> -->
                            <div class="h6 text-center mb-3">
                                <a href="/users" class="link-button">  @lang(Lang::locale().'.USER_MANAGEMENT')</a>
                            </div>
                            <div class="h6 text-center mb-3">
                                <a href="/profile" class="link-button mr-3">  @lang(Lang::locale().'.PROFILE')</a>
                                <a href="/password" class="link-button"> @lang(Lang::locale().'.PASSWORD')</a>
                            </div>
                            
                            <div class="text-value"></div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
