@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.USER_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        <form method="POST" action="/users/store" autocomplete="off" data-title="@lang(Lang::locale().'.USER_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.USER_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
              <label for="name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
              <input type="text" id="name" name="name" class="form-control" placeholder="@lang(Lang::locale().'.USER_NAME_PLACEHOLDER')" value="{{ old('name') }}" data-validation="required custom length" data-validation-length="max50">
              <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
              <label for="email">@lang(Lang::locale().'.EMAIL'): <span class="text-danger">*</span></label>
              <input type="text" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="@lang(Lang::locale().'.USER_EMAIL_PLACEHOLDER')" data-validation="required email length" data-validation-length="max50">
              <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('password') ? 'has-error' : '' }}">
              <label>@lang(Lang::locale().'.PASSWORD'): <span class="text-danger">*</span></label>
              <input type="password" class="form-control" placeholder="@lang(Lang::locale().'.PASSWORD_PLACEHOLDER')" name="password" data-validation="required length" data-validation-length="max50">
              <span class="text-danger">{{ $errors->first('password') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('role') ? 'has-error' : '' }}" id="role">
              <label>@lang(Lang::locale().'.ROLE'): <span class="text-danger">*</span></label>
              <select class="form-control" name="role" onchange="show_next_element(this,'2','organization');" data-validation="required">
                <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ROLE')</option>
                @forelse($roles as $role)
                <option value="{{$role->id}}">{{$role->display_name}}</option>
                @empty
                @endforelse
              </select>
              <span class="text-danger">{{ $errors->first('role') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('organization') ? 'has-error' : '' }}" id="organization" style="display: none;">
              <label>@lang(Lang::locale().'.ORGANIZATION_SINGULAR'): <span class="text-danger">*</span></label>
              <select class="form-control" name="org_id" data-validation="required">
                <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</option>
                @forelse($organizations as $organization)
                <option value="{{$organization->id}}">{{$organization->name}}</option>
                @empty
                @endforelse
              </select>
              <span class="text-danger">{{ $errors->first('organization') }}</span>
            </div>

            <!--  <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
					<label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
                    <select name="status" class="form-control" name="status" data-validation="required" data-validation-error-msg="@lang(Lang::locale().'.STATUS_ERROR')">
                    <option value="" >Select</option>
                   <option value="Active" >Active</option>
                   <option value="Inactive">Inactive</option>
   
  </select>					<span class="text-danger">{{ $errors->first('status') }}</span>
				</div> -->

            <div class="col-md-12">
              <button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button>
              <button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>

        </form>

      </div>
    </div>
  </div>
</div>
@endsection
