@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $user)

<tr>
               <td>{{$ii}}</td>
               <td>
                <div class="media">
                <div class="avatar float-left mr-3">
                  <img class="img-avatar" src="{{$user->avatar_url}}">
                  <span class="avatar-status badge-success"></span>
                </div>
                <div class="media-body">
                  <div>{{$user->name}}</div>
                  <div class="small text-muted">
                    {{$user->email}}
                  </div>
                </div>
              </div>
            </td>
              <td>{{$user->roles[0]->display_name}}</td>
               <td>{{$user->org_name}}</td>
                <td>
              {{date('d-m-Y',strtotime($user->created_at))}}
            </td>
               
               <td>
              <!--  <a onclick="confirmation('/users/{{$user->id}}/view','@lang(Lang::locale().'.USER_VIEW_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_VIEW_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') "  href="/users/{{$user->id}}/view" class="view-button" title="view"><i class="fas fa-eye"></i></a> -->
              @can('read-users')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
               <a href="/users/{{$user->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
               @endcan
               @can('update-users')
               @if ($userRole!='admin')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/users/{{$user->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
              @endif
               @endcan
               @can('delete-users')
               @if($currentUserId != $user->id)
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/users/{{$user->id}}/delete','@lang(Lang::locale().'.USER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/users/{{$user->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
               @endif
               @endcan
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if($data->hasPages())
              <tr>
                <td colspan="7" id="pagination">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
            @if(count($data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-users')
        <a class="btn btn-primary createbtn" href="/users/create" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.USER_SINGULAR')
        </a>
        @endcan
      </div>
</tr></td>
@endif
            