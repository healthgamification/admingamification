@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{ $userData->name }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.USER_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $userData->name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $userData->org_name!=NULL?$userData->org_name:'' }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.EMAIL')</strong></td>
        <td class="width-30 wordbreak">{{ $userData->email }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ROLE')</strong></td>
        <td class="width-35 wordbreak">{{ $userrole[0]->display_name }}</td>

      </tr>

      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($userData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-users')
      <a href="/users/{{$userData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.USER_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection
