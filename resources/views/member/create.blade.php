@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header px-0 bg-transparent clearfix">
          <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')</h4>
         
        </div>
        @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
        @endif
        <form method="POST" action="/member/store" id="from1" autocomplete="off"  data-title="@lang(Lang::locale().'.MEMBER_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.MEMBER_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
        @if(count($errors))
		
		@endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
        	<div class="col-md-6">
        		  <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
					<label for="name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
					<input type="text" id="name" name="member_name" class="form-control"  value="{{ old('name') }}" data-validation="required custom length"  data-validation-length="max50" placeholder="@lang(Lang::locale().'.MEMBER_NAME_PLACEHOLDER')">
					<span class="text-danger">{{ $errors->first('member_name') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					<label for="email">@lang(Lang::locale().'.EMAIL'): <span class="text-danger">*</span></label>
					<input type="text" id="email" name="member_email" class="form-control"  value="{{ old('member_email') }}" data-validation="required email length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.MEMBER_EMAIL_PLACEHOLDER')">
					<span class="text-danger">{{ $errors->first('member_email') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('organization_id') ? 'has-error' : '' }}">
					<label for="organization">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR'): <span class="text-danger">*</span></label>
                    <select class="form-control" name="organization_id" data-validation="required" onchange="getDepartmentDesignation(this.value);">
					@php
            $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
        $userRole =  $currentUserRole[0]->name; 
             @endphp
            @if ($userRole=='super-admin')
						<option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</option>
					@endif
						@foreach($organizations as $organization)
						{
							<option value="{{$organization->id}}" {{$organizationId==$organization->id?'selected':''}}>{{$organization->name}}</option>
						}
					@endforeach
                   </select>					
				  <span class="text-danger">{{ $errors->first('organization_id') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
					<label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
                    <select name="status" class="form-control" name="status" data-validation="required">
						<option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
						<option value="Active" >@lang(Lang::locale().'.ACTIVE')</option>
						<option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
                   </select>					
				  <span class="text-danger">{{ $errors->first('status') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('member_department') ? 'has-error' : '' }}">
					<label for="member_department"> @lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.DEPATRMENT'):</label>
					<select class="form-control" name="member_department" id="member_department">
            <option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.FIRST')</option>
            @foreach($departments as $department)
            {
              <option value="{{$department->id}}">{{$department->name}}</option>
            }
          @endforeach
          </select>
					<span class="text-danger">{{ $errors->first('member_department') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('member_designation') ? 'has-error' : '' }}">
					<label for="member_designation">@lang(Lang::locale().'.DESIGNATION'):</label>
					<select class="form-control" name="member_designation" id="member_designation">
            <option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.FIRST')</option>
            @foreach($designations as $designation)
            {
              <option value="{{$designation->id}}">{{$designation->name}}</option>
            }
          @endforeach
          </select>
					<span class="text-danger">{{ $errors->first('member_designation') }}</span>
				</div>
        	</div>
        	@php
			$currentUserRole=json_decode(Auth::user()->load('roles')->roles);
        	$userRole =  $currentUserRole[0]->name; 
            @endphp
            @if ($userRole=='super-admin')
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('no_of_quizzes') ? 'has-error' : '' }}">
					<label for="no_of_quizzes">@lang(Lang::locale().'.MEMBER_NO_OF_QIZZ'):</label>
					<input type="text" id="no_of_quizzes" name="no_of_quizzes" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.MEMBER_NO_OF_QIZZ_PLACEHOLDER')" value="{{ old('no_of_quizzes') }}">
					<span class="text-danger">{{ $errors->first('no_of_quizzes') }}</span>
				</div>
        	</div>
        	<div class="col-md-6">
        		<div class="form-group {{ $errors->has('no_of_challenge') ? 'has-error' : '' }}">
					<label for="no_of_challenge">@lang(Lang::locale().'.MEMBER_NO_OF_CHALLENGE'):</label>
					<input type="text" id="no_of_challenge" name="no_of_challenge" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.MEMBER_NO_OF_CHALLENGE_PLACEHOLDER')" value="{{ old('no_of_challenge') }}">
					<span class="text-danger">{{ $errors->first('no_of_challenge') }}</span>
				</div>
        	</div>
        	@endif
        	<div class="col-md-12">
        		<div class="form-group">
			
				<input type="hidden" id="btnvalue" name="btnvalue" value="">
				<button  onclick="$('#btnvalue').val('new');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_NEW')</button>
      			<button  onclick="$('#btnvalue').val('exit');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_EXIT')</button>
      			<button type="button" onclick="window.location.href='/members/{{ $organizationId }}'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
				</div>
        	</div>
        </div>
      
        
				
				
				
				
				
       			
       
				
				
				
                
                
                

         </form>
         
        </div>
      </div>
    </div>
  </div>
@endsection
