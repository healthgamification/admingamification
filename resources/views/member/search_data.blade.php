@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@php //$ii=$currentpage+$members->perPage()-1; @endphp
@foreach($members as $member)


            <tr>
               <td>{{$ii}}</td>
               <td class="wordbreak">{{$member->member_name}}</td>
               <td class="wordbreak">{{$member->member_email}}</td>
               <td>{{$member->department?$member->department->name:''}}</td>
               <td>{{$member->designation?$member->designation->name:''}}</td>
               <td class="wordbreak">{{$member->organization_name}}</td>
               <td>{{custom_date_format($member->created_at)}}</td>
               <td>{{$member->status}}</td>
               
               <td>
                @can('read-member')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
               <a href="/member/{{$member->id}}/view" class="view-button" title="@lang(Lang::locale().'.VIEW') @lang(Lang::locale().'.MEMBER_SINGULAR')"><i class="fas fa-eye"></i></a>
    </span>
               @endcan
               @can('update-member')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/member/{{$member->id}}/edit" class="edit-button" title="@lang(Lang::locale().'.EDIT') @lang(Lang::locale().'.MEMBER_SINGULAR')"><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-member')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/member/{{$member->id}}/delete','@lang(Lang::locale().'.MEMBER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.MEMBER_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/member/{{$member->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
               @endcan
               </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
             @if(count($members)!=0)
            @if($members->hasPages())
              <tr>
                <td colspan="9" id="mainpagenation">
                  
                  {!! $members->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($members)==0)
            <tr><td colspan="9">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-member')
           <button  data-href="/member/create" @if($organizationId==null || $organizationId==0) onclick="window.location.href='/member/create';" @else onclick="window.location.href='/member/create/{{$organizationId}}';" @endif   class="btn btn-primary createbtn"> @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')
        </button>
        @endcan
      </div>
</tr></td>
@endif
            