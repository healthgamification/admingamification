@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.MEMBER_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        <form method="POST" action="/member/update/{{$memberData['id']}}" autocomplete="off" data-title="@lang(Lang::locale().'.MEMBER_EDIT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.MEMBER_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="organization_id" value="{{ $memberData['organization_id'] }}">

          <div class="row mb-5">
            <!-- <div class="col-md-3">@lang(Lang::locale().'.AVATAR'): </div> -->
            <div class="col-md-12">
              <div class="profile-box">
                <img class="img-responsive img-circle" id="imgpriview" src="/avatars/{{$memberData->avatar}}" alt="Avatar" style="width:150px;">
                <input type="hidden" name="profile_image" id="profile_image_ch" value="{{$memberData->avatar}}">
                <div class="btn-edit-circle">
                  <span data-toggle="tooltip" title="@lang(Lang::locale().'.PROFILE_IMAGE_CHANGE_BTN')">
                    <a href="#;" data-toggle="modal" data-target="#selectProfile"><i class="fa fa-pen"></i></a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-md-12 mt-2 justify-content-md-center text-center">
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#selectProfile">@lang(Lang::locale().'.PROFILE_IMAGE_CHANGE_BTN')</button>-->

          <!-- <label  onclick="reset_image(this,'imgpriview','{{$memberData->id}}','{{ csrf_token() }}');" class="btn btn-info" >@lang(Lang::locale().'.PROFILE_IMAGE_RESET_BTN')</label> -->
          <!--</div> -->
          <!-- <small class="form-text text-muted">@lang(Lang::locale().'.PROFILE_AVATAR_TEXT')</small> -->
          <div class="row">
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('member_name') ? 'has-error' : '' }}">
                <label for="member_name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
                <input type="text" id="member_name" name="member_name" class="form-control" value="{{$memberData['member_name']}}" placeholder="@lang(Lang::locale().'.MEMBER_NAME_PLACEHOLDER')" data-validation="required custom length" data-validation-length="max50">
                <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('member_email') ? 'has-error' : '' }}">
                <label for="member_email">@lang(Lang::locale().'.EMAIL'): <span class="text-danger">*</span></label>
                <input type="text" id="member_email" name="member_email" class="form-control" value="{{$memberData['member_email']}}" placeholder="@lang(Lang::locale().'.MEMBER_EMAIL_PLACEHOLDER')" data-validation="required email length" data-validation-length="max50">
                <span class="text-danger">{{ $errors->first('email') }}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('member_department') ? 'has-error' : '' }}">
                <label for="member_department"> @lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.DEPATRMENT'):</label>
                <select class="form-control" name="member_department" id="member_department">
                  <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.DEPATRMENT')</option>
                  @foreach($departments as $department)
                  {
                  <option value="{{$department->id}}" {{$department->id==$memberData->member_department?'selected':''}}>{{$department->name}}</option>
                  }
                  @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('member_department') }}</span>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('member_designation') ? 'has-error' : '' }}">
                <label for="member_designation">@lang(Lang::locale().'.DESIGNATION'):</label>
                <select class="form-control" name="member_designation" id="member_designation">
                  <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.DESIGNATION') </option>
                  @foreach($designations as $designation)
                  {
                  <option value="{{$designation->id}}" {{$designation->id==$memberData->member_designation?'selected':''}}>{{$designation->name}}</option>
                  }
                  @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('member_designation') }}</span>
              </div>
            </div>
            <!-- <div class="col-md-6">
                <div class="form-group {{ $errors->has('organization_id') ? 'has-error' : '' }}">
                <label for="organization_id">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR'): <span class="text-danger">*</span></label>
                <select class="form-control" name="organization_id" data-validation="required" >
                @php
                $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
                $userRole =  $currentUserRole[0]->name; 
                @endphp
                @if ($userRole=='super-admin')
                <option value="" >--Select--</option>
                @endif
                @foreach($organizations as $organization)
                {
                  <option value="{{$organization->id}}" {{$organization->id==$memberData['organization_id']?'selected':''}} >{{$organization->name}}</option>
                }
                @endforeach
                </select>          
                <span class="text-danger">{{ $errors->first('organization_id') }}</span>
                </div> 
              </div> -->
            @php
            $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
            $userRole = $currentUserRole[0]->name;
            @endphp
            @if ($userRole=='super-admin')
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('no_of_quizzes') ? 'has-error' : '' }}">
                <label for="no_of_quizzes">@lang(Lang::locale().'.MEMBER_NO_OF_QIZZ'):</label>
                <input type="text" id="no_of_quizzes" name="no_of_quizzes" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.MEMBER_NO_OF_QIZZ_PLACEHOLDER')" value="{{$memberData['no_of_quizzes']}}">
                <span class="text-danger">{{ $errors->first('no_of_quizzes') }}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('no_of_challenge') ? 'has-error' : '' }}">
                <label for="no_of_challenge">@lang(Lang::locale().'.MEMBER_NO_OF_CHALLENGE'):</label>
                <input type="text" id="no_of_challenge" name="no_of_challenge" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.MEMBER_NO_OF_CHALLENGE_PLACEHOLDER')" value="{{$memberData['no_of_challenge']}}">
                <span class="text-danger">{{ $errors->first('no_of_challenge') }}</span>
              </div>
            </div>
            @endif
            <div class="clearfix"></div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
                <select name="status" class="form-control" name="status" data-validation="required">
                  <option value="Active" {{$memberData['status']=='Active'?'selected':''}}>@lang(Lang::locale().'.ACTIVE')</option>
                  <option value="Inactive" {{$memberData['status']=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
                </select>
                <span class="text-danger">{{ $errors->first('status') }}</span>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button class="btn btn-primary">@lang(Lang::locale().'.SAVE')</button>
              <button type="button" onclick="window.location.href='/members/{{ $memberData['organization_id'] }}'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
            <div class="clearfix"></div>
          </div>
      </div>
      </form>

    </div>
  </div>
</div>
</div>
<!-- Modal -->
<div id="selectProfile" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Modal Header</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-4">
            <input type="text" class="form-control" onkeyup="search_images(this)" placeholder="@lang(Lang::locale().'.SEARCH')">
          </div>
        </div>
        <div class="row text-center">
          @foreach($images as $image)

          <div class="col-md-2 mt-3 searchimage" onclick="chooseimage(this,'{{$image}}');">
            <div class="text-center rounded border border-primary">
              <img class="img-responsive img-circle" id="imgpriview" src="/avatars/{{$image}}" alt="Avatar" style="width:150px;">
              <h4>{{$image}}</h4>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="addimage()" data-add="1">OK</button>
      </div>
    </div>

  </div>
</div>
@endsection
