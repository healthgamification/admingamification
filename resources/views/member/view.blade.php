@extends('layouts.app')

@section('content')

<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <div class="col-md-12">
      <h4>{{ $memberData->member_name }}</h4>
    </div>
  </div>

  <table class="table table-hover mt-2">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.MEMBER_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $memberData->member_name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $memberData->organization_name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.EMAIL')</strong></td>
        <td class="width-30 wordbreak">{{ $memberData->member_email }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.DEPARTMENT_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $memberData->department?$memberData->department->name:'' }}</td>

      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.DESIGNATION_SINGULAR')</strong></td>
        <td class="width-30 wordbreak">{{ $memberData->designation?$memberData->designation->name:''}}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-35 wordbreak">{{ $memberData->status }}</td>

      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.IS') @lang(Lang::locale().'.ADMIN')</strong></td>
        <td class="width-30 wordbreak">@if($memberData->is_admin == 1)
          @lang(Lang::locale().'.YES')
          @else @lang(Lang::locale().'.NO')
          @endif</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.TEAM_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-35 wordbreak">{{ isset($teamPlayerData)?$teamPlayerData->team->name:'' }}</td>

      </tr>
      @if($userRole == 'super-admin')
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.ORGANIZATION_NO_OF_QIZZ_PLACEHOLDER')</strong></td>
        <td class="width-30 wordbreak">{{ $memberData->no_of_quizzes }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_NO_OF_CHALLENGE_PLACEHOLDER')</strong></td>
        <td class="width-35 wordbreak">{{ $memberData->no_of_challenge }}</td>

      </tr>
      @endif
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($memberData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-member')
      <a href="/member/{{$memberData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.MEMBER_SINGULAR')</a>
      @endcan</div>
  </div>
</div>
</div>


@endsection
