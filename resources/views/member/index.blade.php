@extends('layouts.app')

@section('content')
@if(empty($setId))
@php
$setId='';
$style="display:none";
@endphp
@else
@php
$style="display:block";
@endphp
@endif
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">@lang(Lang::locale().'.MEMBER_PLURAL')</h4>
  </div>
  <div class="card-body px-0">
    <div class="alert alert-success alert-dismissible" style="display: none;" id="mainmsg"></div>
    @if(session()->has('message'))
      <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
      </div>
    @endif
    <div class="row justify-content-between">
      <!-- <div class="col-md-12 text-right">
        <button data-toggle="collapse" data-target="#uploadcollapes" class="btn btn-primary bulk-btn">@lang(Lang::locale().'.MEMBER_BULK_UPLOAD_BTN_TEXT')
          <i class="fa fa-chevron-down upload-down-icon"></i>
        </button>
      </div> -->
      <div class="col-md-12">
        @include('member/uploadcsv')
      </div>
    </div>

    <div class="saperator-line mt-4">
      {{ csrf_field() }}
      <div class="row">
        <div class="col-md-3 mt-3">
          <label for="@lang(Lang::locale().'.SEARCH')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.MEMBER_PLURAL')</label>
          <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.MEMBER_PLURAL')" id="serach" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}/{{$organizationId??0}}')" class="form-control">
        </div>
        <div class="col-md-3 mt-3">
          <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</label>
          <select class="form-control" name="setId" onchange="get_filtered_team_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">
            @php
            $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
            $userRole = $currentUserRole[0]->name;
            @endphp
            @if ($userRole=='super-admin')
            <option value="0">@lang(Lang::locale().'.ALL')</option>
            @endif
            @foreach($organization_data as $org_data)
            <option value="{{ $org_data->id }}" {{$organizationId==$org_data->id?'selected':''}}>{{$org_data->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-6 mt-5 text-right">
          <!-- @can('create-member')
          <div class="card-header-actions mr-1">-->
          <!-- <a href="/member/create" class="btn btn-primary">@lang(Lang::locale().'.NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')</a>-->
          <!--<button data-href="/member/create" @if($organizationId==null || $organizationId==0) onclick="window.location.href='/member/create';" @else onclick="window.location.href='/member/create/{{$organizationId}}';" @endif class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')</button>
          </div>
          @endcan -->
        </div>
      </div>
    </div>

  </div>
  <div class="table-body">
    <table class="table table-hover">
      <thead>
        <tr>
          <th class="sorting width-5"># <span class="icons id"></span></th>

          <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'member_name')" style="cursor: pointer">@lang(Lang::locale().'.NAME') <span class="icons member_name"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
          <th class="sorting width-15" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'member_email')" style="cursor: pointer">@lang(Lang::locale().'.EMAIL') <span class="icons member_email"></span></th>
          <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'member_department')" style="cursor: pointer">@lang(Lang::locale().'.DEPATRMENT') <span class="icons member_department"></span></th>
          <th class="sorting width-15" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'member_designation')" style="cursor: pointer">@lang(Lang::locale().'.DESIGNATION') <span class="icons member_designation"></span></th>
          <th class="sorting width-15" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'organization_name')" style="cursor: pointer">@lang(Lang::locale().'.ORGANIZATION_SINGULAR') <span class="icons organization_name"></span></th>
          <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'created_at')" style="cursor: pointer;">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
          <th class="sorting width-5" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'status')" style="cursor: pointer">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>

          <th class="width-15">@lang(Lang::locale().'.ACTION')</th>

        </tr>
      </thead>
      <tbody id="tbody">
        @include('member/search_data')
      </tbody>
    </table>
  </div>

  <div id="loader" style="display:none;">
    <div class="vue-content-placeholders vue-content-placeholders-is-animated">
      <div class="vue-content-placeholders-text">
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
      </div>
    </div>
  </div>

  <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
  <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="member_name" />
  <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
  <input type="hidden" name="hidden_organizationId" id="hidden_organizationId" value="{{$organizationId??0}}" />
  <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$organizationId??0}}" />




  @can('create-member')
  <span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.MEMBER_SINGULAR')" class="btn-plus">
    <a data-href="/member/create" class="createbtn" @if($organizationId==null || $organizationId==0) onclick="window.location.href='/member/create';" @else onclick="window.location.href='/member/create/{{$organizationId}}';" @endif><i class="fa fa-plus"></i></a>
  </span>
  @endcan

</div>
</div>

@endsection
