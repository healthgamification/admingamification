@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{ $languageData->name }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.LANGUAGE_CODE')</strong></td>
        <td class="width-30 wordbreak">{{ $languageData->locale }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.LANGUAGE_NAME')</strong></td>
        <td class="width-30 wordbreak">{{$languageData->name}}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30 wordbreak">{{$languageData->status}}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td class="width-30 wordbreak">{{ custom_date_format($languageData->created_at) }}</td>

      </tr>

    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-language')
      <a href="/translation/edit/{{$languageData->id}}" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection