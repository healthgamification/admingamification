@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.MANAGE_TRANSLATION')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <form method="POST" action="{{route('translation.post',$currentlang->id)}}" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.TEAM_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.TRANSLATION_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @csrf

          <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
              @php
              $li=0;
              @endphp
              @foreach($english as $key=>$val)
              <li class="nav-item">
                <a class="nav-link {{$li==0?'active':''}}" data-toggle="tab" href="#{{$key}}">{{ucfirst(str_replace('_'," ",$key))}}</a>
              </li>
              @php
              $li++;
              @endphp
              @endforeach
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              @php
              $li=0;
              @endphp
              @foreach($english as $key1=>$val)
              <div id="{{$key1}}" class="container tab-pane {{$li==0?'active':'fade'}}">
              <div class="card-sub-header px-0 mb-3 bg-transparent clearfix"><strong class="float-left">{{ucfirst(str_replace('_'," ",$key1))}}</strong></div>
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 30%;">English</th>
                      <th>{{$currentlang->name}}</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($english[$key1] as $key=>$lng)
                    @php
                    $txt='';
                    if(isset($currentlanggroup[$lng->group]))
                    {
                    if(isset($currentlanggroup[$lng->group][$key]))
                    {
                    $txt = $currentlanggroup[$lng->group][$key]->text;
                    }
                    }
                    @endphp
                    <tr>
                      <td>
                        <p>{{$lng->text}}</p>

                      </td>
                      <td>
                        <input type="hidden" name="locale" value="{{$currentlang->locale}}">
                        <input type="hidden" name="namespace[{{$lng->item}}]" value="{{$lng->namespace}}">
                        <input type="hidden" name="group[{{$lng->item}}]" value="{{$lng->group}}">
                        <input type="text" name="translation[{{$lng->item}}]" class="form-control" value="{{$txt}}">
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              @php
              $li++;
              @endphp
              @endforeach
            </div>
          </div>
          <div class="col-md-12 mt-2">
            <input type="submit" class="btn btn-primary" value="@lang(Lang::locale().'.SAVE')">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection