@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($langs as $lang)

            <tr>
               <td>{{$ii}}</td>
               <td>{{$lang->locale}}</td>
               <td>{{$lang->name}}</td>
               <td>
                @can('update-translations')
                <a href="/translation/create/{{$lang->id}}" title="@lang(Lang::locale().'.MANAGE_TRANSLATION')">@lang(Lang::locale().'.MANAGE_TRANSLATION') ({{count(array_filter($lang->translations->pluck('text')->all()))}}/{{$lang->translations->count()}})</a>
                @endcan
              </td>
               <td>{{$lang->status}}</td>
               <td>          

               @if($lang->locale!='en')
               @can('read-language')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
                <a href="{{route('language.view',[$lang->id])}}" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
                @endcan
               @can('update-language')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/translation/edit/{{$lang->id}}" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-language')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/translations/delete/{{$lang->id}}','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.LANGUAGE_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/translations/delete/{{$lang->id}}" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>

               @endcan
               @else
               <span>@lang(Lang::locale().'.DEFAULT')</span>
               @endif
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($langs)!=0)
            @if($langs->hasPages())
              <tr>
                <td colspan="7" id="mainpagenation">
                  
                  {!! $langs->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($langs)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-language')       
        <a  href="{{route('language.create')}}" class="btn btn-primary createbtn"> <i class="fa fa-plus"></i>&nbsp;  @lang(Lang::locale().'.NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</a>
        @endcan
      </div>
</tr></td>
@endif
            