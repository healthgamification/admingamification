@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
        <div class="alert alert-success">
          {{ session()->get('message') }}
        </div>
        @endif
        <form method="POST" action="{{route('language.pedit',$lang->id)}}" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.TEAM_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.LANGUAGE_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @csrf
          <div class="row">
            <div class="form-group col-md-4 {{ $errors->has('locale') ? 'has-error' : '' }}">
              <label> @lang(Lang::locale().'.LANGUAGE_CODE') <span class="text-danger">*</span></label>
              <input type="text" name="locale" class="form-control" data-validation="required length" data-validation-length="max2" placeholder=" @lang(Lang::locale().'.LANGUAGE_CODE')" value="{{old('locale',$lang->locale)}}">
              <span class="text-danger">{{ $errors->first('locale') }}</span>
            </div>
            <div class="form-group col-md-4 {{ $errors->has('name') ? 'has-error' : '' }}">
              <label> @lang(Lang::locale().'.LANGUAGE_NAME') <span class="text-danger">*</span></label>
              <input type="text" name="name" class="form-control" data-validation="required length" data-validation-length="max10" placeholder=" @lang(Lang::locale().'.LANGUAGE_NAME')" value="{{old('name',$lang->name)}}">
              <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>

            <div class="form-group col-md-4 {{ $errors->has('status') ? 'has-error' : '' }}">
              <label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
              <select name="status" class="form-control" name="status" data-validation="required">
                <option value="">@lang(Lang::locale().'.STATUS_ERROR')</option>
                <option value="Active" {{old('status',$lang->status)=='Active'?'selected':''}}>@lang(Lang::locale().'.ACTIVE')</option>
                <option value="Inactive" {{old('status',$lang->status)=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
              </select><span class="text-danger">{{ $errors->first('status') }}</span>
            </div>


            <div class="col-md-12">
              <input type="submit" class="btn btn-primary" value="@lang(Lang::locale().'.SAVE')">
              <button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection