@if(count($logs)>0)
	@foreach($logs as $key=>$log)
	@php $data = json_decode($log->description); @endphp
	<tr>
		<td>{{($key+1)}}</td>
		<td>{{ucwords(str_replace("_"," ",$log->type))}}</td>
		<td>{{custom_date_format($log->created_at)}}</td>
		<td><strong>{{ ucwords(str_replace("_"," ",$log->type)) }}</strong> @lang(Lang::locale().'.PERFORMED') @lang(Lang::locale().'.BY') <strong>{{$log->user($data->userid)->name}} </strong></td>
	</tr>
	@endforeach
@else
	<tr>
		<td colspan="4" class="text-center"> @lang(Lang::locale().'.NOT_FOUND_TITLE')</td>
	</tr>
@endif
