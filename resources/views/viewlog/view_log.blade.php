<div class="modal fade mt-2" id="logModel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">@lang(Lang::locale().'.VIEW') @lang(Lang::locale().'.LOG_PLURAL')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
          <div class="col-md-12">
           
          </div>
        </div>
       <table class="table" >
        <thead>
          <tr>
            <th>#</th>
            <th>@lang(Lang::locale().'.LOG_SINGULAR') @lang(Lang::locale().'.ADDED_AT')</th>
            <th>@lang(Lang::locale().'.ACTION')</th>
            <th>@lang(Lang::locale().'.DESCRIPTION')</th>
          </tr>
        </thead>
        <tbody id="logbody">
        
        
        </tbody>
      </table>
      <div id="poploader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated"><div class="vue-content-placeholders-text"><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div></div></div>
      </div>

      </div>
    </div>
  </div>
</div>