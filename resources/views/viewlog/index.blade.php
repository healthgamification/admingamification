@extends('layouts.app')

@section('content')

<div class="container">
	<div class="card-header px-0 mt-2 bg-transparent clearfix">
		<h4 class="float-left pt-2">@lang(Lang::locale().'.LOG_PLURAL')</h4>		
	</div>
	<div class="card-body px-0">
		@if(session()->has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
			{{ session()->get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<div class="row">
			@include('viewlog.'.$modalname)
		</div>
	</div>
</div>
@endsection


