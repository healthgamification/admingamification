<div class="row col-md-12 mb-3">
        <div class="col-md-6">
          <div class="form-group ">
             <label for="@lang(Lang::locale().'.SEARCH')">@lang(Lang::locale().'.LOG', ["type"=>"Question"])</label>
			     <input type="text" placeholder="@lang(Lang::locale().'.LOG', ['type'=>'Question'])" id="serach"  onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}')" class="form-control">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LOG_TYPE')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LOG_TYPE')</label>
              <select class="form-control" name="setId" onchange="get_filtered_team_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">
                <option value="0">@lang(Lang::locale().'.ALL')</option>
                @foreach($logtype as $type)
                  <option value="{{ $type }}">{{ucwords(str_replace("_"," ",$type))}}</option>
                     @endforeach
               </select>
             
            </div>
         </div>
      </div>
<div class="col-md-12">
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>Log Type</th>
				<th>Log Message</th>
				<th>User Name</th>
				<th>Created Date</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody id="tbody">
			@include('viewlog.search.'.$modalname)
		</tbody>
	</table>
</div>