@foreach($logs as $key=>$log)
@php $data = json_decode($log->description); @endphp
<tr>
	<td>{{($key+1)}}</td>
	<td>{{ucwords(str_replace("_"," ",$log->type))}}</td>
	<td>{{$data->msg}}</td>
	<td>{{$log->user($data->userid)->name}}</td>
	<td>{{custom_date_format($log->created_at)}}</td>
	<td>
		@if($log->type=='update')
		@php 
		$newdata =(array) $data->data; 
		$olddata =(array) $data->olddata; 
		unset($newdata['updated_at']);
		unset($newdata['created_at']);
		$result = array_diff($newdata,$olddata);
		@endphp 
		@foreach($result as $key=>$val)
		<label>Old {{ucwords(str_replace("_"," ",$key))}} : </label>
		<span>{{$olddata[$key]}}</span>
		<br />
		<label>New {{ucwords(str_replace("_"," ",$key))}} :</label>
		<span>{{$val}}</span>
		<br />
		@endforeach
		@elseif($log->type=='create')
		@elseif($log->type=='quiz_set_deleted')
		@elseif($log->type=='bulkupload')
		<label>Total Row :</label>
		<span>{{$data->totalrow}}</span>
		<br />
		<label>Duplicate Row Found :</label>
		<span>{{$data->duplicate}}</span>
		<br />	
		<label>Wrong Category Name Row Found :</label>
		<span>{{$data->wrongcat}}</span>
		<br />
		<label>Required Field Mismatch Row Found :</label>
		<span>{{$data->validation}}</span>
		<br />
		<label>Total Uploaded Row :</label>
		<span>{{$data->uploaded}}</span>
		<br />
		@endif
	</td>
</tr>
@endforeach