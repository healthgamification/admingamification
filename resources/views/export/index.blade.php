@extends('layouts.app')
@section('content')
<div class="container">
	<div class="card-header px-0 bg-transparent clearfix">
		<h4 class="float-left">@lang(Lang::locale().'.EXPORT_PLURAL')</h4>
	</div>
	<div class="card-body px-0">
		@if(session()->has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
			{{ session()->get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<div class="row">

			<div class="col-md-6">
				<form method="POST" action="{{route('pexport')}}" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.EXPORT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.EXPORT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
					@csrf
					<div class="input-group">
						<div class="form-group">
							<select class="form-control" data-validation="required" name="type">
								<option value=""> @lang(Lang::locale().'.SELECT_EXPORT_TYPE')</option>
								<option value="1"> @lang(Lang::locale().'.EXPORT_EMPLOYEE_SAMPLE_FILE')</option>
								@if($userRole == 'super-admin')
								<option value="2"> @lang(Lang::locale().'.EXPORT_ORGANIZATION_LIST')</option>
								<option value="3"> @lang(Lang::locale().'.EXPORT_CHALLENGE_SAMPLE_FILE')</option>
								<!-- <option value="4"> @lang(Lang::locale().'.EXPORT_CHALLENGE_CATEGORY_LIST')</option> -->
								<option value="5"> @lang(Lang::locale().'.EXPORT_QUESTION_SAMPLE_FILE')</option>
								@endif
							</select>
						</div>
						<div class="form-group ml-2">
							<button class="btn btn-primary">@lang(Lang::locale().'.EXPORT_SINGULAR')</button>
						</div>
					</div>

				</form>

			</div>
		</div>

	</div>
</div>


@endsection
