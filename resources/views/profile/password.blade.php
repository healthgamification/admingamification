@extends('layouts.app')

@section('content')
     <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header px-0 mt-2 bg-transparent clearfix">
          <h4 class="float-left pt-2">@lang(Lang::locale().'.PASSWORD') @lang(Lang::locale().'.UPDATE')</h4>     
        </div>
        <div class="card-body px-0">
          @if(Session::has('message'))
         <div class="alert alert-success alert-dismissible" role="alert">
            {{ session()->get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif
          <form  method="post" action="/password" autocomplete="off" data-title="@lang(Lang::locale().'.PROFILE_EDIT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.PROFILE_EDIT_PASSWORD_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @csrf
          <div class="row">
            <div class="col-md-6 form-group {{ $errors->has('current') ? 'has-error' : '' }}">
              <label >@lang(Lang::locale().'.CURRENT') @lang(Lang::locale().'.PASSWORD'): <span class="text-danger">*</span></label>
                <input class="form-control"  type="password" name="current" value="{{old('current')}}" data-validation="required length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.PROFILE_PASSWORD_CURRENT_PLACEHOLDER')" data-validation-error-msg="@lang(Lang::locale().'.PROFILE_PASSWORD_CURRENT_ERROR_MSG')">
                <span class="text-danger">{{ $errors->first('current') }}</span>
            </div>
            <div class="col-md-6 form-group {{ $errors->has('password') ? 'has-error' : '' }}">
              <label>@lang(Lang::locale().'.PASSWORD'): <span class="text-danger">*</span></label>
              <input class="form-control"  type="password" name="password" value="{{old('password')}}" data-validation="required length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.PROFILE_PASSWORD_PLACEHOLDER')" data-validation-error-msg="@lang(Lang::locale().'.PROFILE_PASSWORD_ERROR_MSG')">
                <span class="text-danger">{{ $errors->first('password') }}</span>
            </div>
              <div class="col-md-6 form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
              <label>@lang(Lang::locale().'.CONFIRM') @lang(Lang::locale().'.PASSWORD'): <span class="text-danger">*</span></label>
              <input class="form-control"  type="password" name="password_confirmation" value="{{old('password_confirmation')}}" data-validation="required length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.PROFILE_CONFIRM_PASSWORD_PLACEHOLDER')" data-validation-error-msg="@lang(Lang::locale().'.PROFILE_CONFIRM_PASSWORD_ERROR_MSG')">
               <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
            </div>
            <div class="col-md-12 form-group text-right">
            <input type="submit" class="btn btn-primary" value="@lang(Lang::locale().'.SAVE')">
            <button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
          </form>          
        </div>
      </div>
    </div>
  </div>
@endsection
