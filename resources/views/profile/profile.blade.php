@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="card-header px-0 mt-2 bg-transparent clearfix">
          <h4 class="float-left pt-2">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.PROFILE_SINGULAR')</h4>     
        </div>
        <div class="card-body px-0">
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissible" role="alert">
            {{ session()->get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div> 
          @endif
          <form  method="post" enctype="multipart/form-data" action="/profile" autocomplete="off" data-title="@lang(Lang::locale().'.PROFILE_EDIT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.PROFILE_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @csrf
            <div class="col-md-12 form-group row justify-content-md-center text-center">
              <!-- <div class="col-md-3">@lang(Lang::locale().'.AVATAR'): </div> -->
                <div class="col-md-12 justify-content-md-center">
                <img class="img-responsive img-circle" id="imgpriview" src="{{$user->avatar_url}}" alt="Avatar" style="width:150px;">
              </div>
              <div class="col-md-12 mt-2">
                <small class="form-text text-muted">@lang(Lang::locale().'.PROFILE_IMAGE_MAX_SIZE') {{$max_file_size}}</small>
                <label for="changeimage" class="btn btn-primary">@lang(Lang::locale().'.PROFILE_IMAGE_CHANGE_BTN')</label>                
                <input type="file" accept="image/x-png,image/gif,image/jpeg" onchange="showimage(this,'imgpriview')" name="profilrimage" id="changeimage" style="display: none;">
                <label  onclick="reset_image(this,'imgpriview','{{$user->id}}','{{ csrf_token() }}');" class="btn btn-info" >@lang(Lang::locale().'.PROFILE_IMAGE_RESET_BTN')</label>
              </div>
                <!-- <small class="form-text text-muted">@lang(Lang::locale().'.PROFILE_AVATAR_TEXT')</small> -->
            </div>
            <hr class="my-3">
            <div class="row">
            <div class="col-md-6 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
              <label >@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
                <input class="form-control"  type="text" name="name" value="{{old('name',$user->name)}}" placeholder="@lang(Lang::locale().'.PROFILE_NAME_PLACEHOLDER')" data-validation="required custom length"  data-validation-length="max50">
              
                <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <div class="col-md-6 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
              <label>@lang(Lang::locale().'.EMAIL'): <span class="text-danger">*</span></label>
              <input class="form-control"  type="email" name="email" value="{{old('email',$user->email)}}"  placeholder="@lang(Lang::locale().'.PROFILE_EMAIL_PLACEHOLDER')" data-validation="required email length" data-validation-length="max50">
                
                <span class="text-danger">{{ $errors->first('email') }}</span>
            </div>
            <div class="col-md-12 form-group text-right">
            <input type="submit" class="btn btn-primary" value="@lang(Lang::locale().'.SAVE')">
            <button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
          </form>          
        </div>
      </div>
    </div>
  </div>
@endsection
