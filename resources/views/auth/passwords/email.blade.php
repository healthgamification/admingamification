@extends('layouts.auth')

@section('auth')
<div class="col-md-6">
    <div class="card mx-4">
        <div class="card-body p-4">
            <h1> @lang(Lang::locale().'.RESET_PASSWORD') </h1>
            <p class="text-muted">@lang(Lang::locale().'.RESET_YOUR_PASSWORD')</p>

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text">@</span>
                    </div>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder=" @lang(Lang::locale().'.EMAIL_ADDRESS')" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">
                     @lang(Lang::locale().'.SEND_PASSWORD_RESET_LINK')
                </button>
                <button type="button" onclick="window.location.href='/'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')
                </button>

                @if (session('status'))
                    <div class="alert alert-success mt-4 alert-dismissible" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </form>
        </div>
    </div>
</div>
@endsection
