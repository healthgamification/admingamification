@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($departments as $department)


<tr>
  <td>{{$ii}}</td>
  <td class="wordbreak">{{$department->name}}</td>
  <td class="wordbreak">{{$department->organization_name}}</td>
  <td>{{custom_date_format($department->created_at)}}</td>
  <td>{{$department->status}}</td>

  <td>
    @can('read-department')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
      <a href="/department/{{$department->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
    @endcan
    @can('update-department')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
      <a href="/department/edit/{{$department->id}}" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endcan
    @can('delete-department')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
      <a onclick="confirmationDelete('/department/delete/{{$department->id}}','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.DEPARTMENT_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/department/delete/{{$department->id}}" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
    @endcan
  </td>
</tr>
@php $ii++;@endphp
@endforeach
@if(count($departments)!=0)
@if($departments->hasPages())
<tr>
  <td colspan="8" id="mainpagenation">

    {!! $departments->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($departments)==0)
<tr>
  <td colspan="8">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-department')
      <!-- <a class="btn btn-primary" href="/member/create" role="button"> -->
      <a href="/department/create/{{$orgid}}" class="btn btn-primary createbtn">
        <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DEPARTMENT_SINGULAR')
      </a>
      @endcan
    </div>
</tr>
</td>
@endif
