@extends('layouts.app')

@section('content')

<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <div class="col-md-12">
      <h4>{{ $departmentData->name }}</h4>
    </div>
  </div>

  <table class="table table-hover mt-2">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.DEPARTMENT_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $departmentData->name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $organizationData->name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.DEPARTMENT_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{ $departmentData->description }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-35 wordbreak">{{ $organizationData->status }}</td>

      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($departmentData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-department')
      <a href="/department/edit/{{$departmentData->id}}" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.DEPARTMENT_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection
