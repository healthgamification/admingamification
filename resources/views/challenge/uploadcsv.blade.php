<!-- <div class="collapse" id="uploadcollapes"> -->
<div id="uploadcollapes">
	<div class="well">
	<div class="row container"><label for="@lang(Lang::locale().'.BULK_UPLOAD')"> @lang(Lang::locale().'.BULK_UPLOAD'):</label></div>
		<div class="row">
			<div class="col-md-4">
			
				<div class="custom-file">
				
					<input type="file" accept=".csv" onchange="csvchange(this);" class="custom-file-input" id="customFile">
					<label class="custom-file-label" for="customFile">@lang(Lang::locale().'.CHALLENGE_CHOOSE_FILE_TEXT')</label>
				</div>
				<div class="progress mt-2" style="display:none;width: 100%;" id="progressbar">
					<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
						0%
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<button class="btn btn-primary" data-memurl="{{request()->route()->getAction()['prefix']}}/{{$category_id??0}}/{{$lang}}"
								data-token="{{csrf_token()}}" data-url="/challenges/bulk_upload" onclick="uploadcsv(this)">
					@lang(Lang::locale().'.CHALLENGE_BULK_UPLOAD_BTN_TEXT_AFTER')</button>
				<span class="text-danger" id="uploadmsg"></span>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>