@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')</h4>
  </div>
  <form method="POST" action="/challenge-set/store" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.CHALLENGE_SET_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.CHALLENGE_SET_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
    <div class="card-body px-0">
      @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
         @endif
      @if($errors->has('challenge_ids'))
      <div class="alert alert-danger alert-dismissible" role="alert">
        {{ $errors->first('challenge_ids') }}
      </div>
      @endif

      @if(count($errors))

      @endif
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" value="0" id="set_id">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('challenge_set_name') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
            <input type="text" id="name" name="challenge_set_name" class="form-control" value="{{ old('challenge_set_name') }}" data-validation="required custom length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.CHALLENGE_SET_NAME_PLACEHOLDER')">
            <span class="text-danger">{{ $errors->first('challenge_set_name') }}</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
            <label for="lang">@lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
            <select name="lang" class="form-control" onchange="get_lang_filtered_set_data(this,'{{request()->route()->getAction()['prefix']}}','type','challenge_category')" name="lang" data-validation="required">
              @forelse(getLanguages() as $lang1)
              <option {{$lang1->code==$lang?'selected':''}} value="{{$lang1->code}}">{{$lang1->name}}</option>
              @empty
              @endforelse
            </select><span class="text-danger">{{ $errors->first('lang') }}</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
            <select name="status" class="form-control" data-validation="required">
              <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
              <option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
              <option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
            </select>
            <span class="text-danger">{{ $errors->first('status') }}</span>
          </div>
        </div>
        <div class="col-md-12 form-group {{ $errors->has('challenge_set_desc') ? 'has-error' : '' }}">
          <label for="name">@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.DESCRIPTION'):</label>
          <textarea class="form-control" placeholder="@lang(Lang::locale().'.CHALLENGE_SET_DESCRIPTION_PLACEHOLDER')" data-validation="length" data-validation-length="max150" name="challenge_set_desc">{{old('challenge_set_desc')}}</textarea>
          <span class="text-danger">{{ $errors->first('challenge_set_desc') }}</span>
        </div>
        <div class="col-md-12">
          <button class="btn btn-primary" onclick="$(this).closest('div').hide(); $('#secondrow').toggle('slide');" type="button">@lang(Lang::locale().'.ADD_CHALLENGES')</button>
          <button type="button" onclick="back('/challenge-sets/{{$lang}}')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
        </div>
      </div>

      <div class="row" style="display: none;" id="secondrow">
        <div class="col-md-12">
          <div class="card-sub-header px-0 bg-transparent clearfix">
            <strong class="float-left pt-3">@lang(Lang::locale().'.ADD_CHALLENGES')</strong>
          </div>
        </div>
        <div class="col-md-4 mt-3 form-group">
          {{ csrf_field() }}
          <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')</label>
          <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')" onkeyup="get_challenge_filtered_data($('#type').val(),$('#challenge_category').val(),'{{request()->route()->getAction()['prefix']}}',this.value)" id="serach" class="form-control">
        </div>
        <div class="col-md-4 mt-3 form-group">
          <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</label>
          <input type="hidden" id="type" value="2">
          <select class="form-control" name="setId" id="challenge_category" onchange="get_challenge_filtered_data($('#type').val(),this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">

            <option value="0">@lang(Lang::locale().'.ALL')</option>

            @foreach($category_data as $cat_data)
            <option value="{{ $cat_data->id }}">{{$cat_data->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-md-12 table-body">
          <table id="challenge_table" class="table">
            <thead>
              <th class="sorting " style="cursor: pointer; width:2%">#</th>
              <th class="sorting " style="cursor: pointer; width:2%">
                <input type="checkbox" id="selectall" onclick="challenge_set_select(true,this);">
              </th>
              <th class="sorting " style="width:10%">@lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</th>
              <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#language').val()+'/'+$('#type').val()+'/'+$('#challenge_category').val()+'/'+$('#set_id').val(),'challenge_name')" style="cursor: pointer; width:40%">@lang(Lang::locale().'.CHALLENGE_SINGULAR')<span class="icons challenge_name"></span></th>
            </thead>
            <tbody id="tbody">
              @include("challenge.search_challenge_data")
            </tbody>
          </table>
          <input type="hidden" name="challenge_ids" id="challenge_ids" value="[]">
          <div class="form-group">
            <button disabled="disabled" id="challengesubmitbtn" class="btn btn-primary">@lang(Lang::locale().'.SAVE')</button>
            <button type="button" onclick="back('/challenge-sets')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
          </div>
        </div>
  </form>
  <input type="hidden" id="cname" value="{{request()->route()->getAction()['prefix']}}">
  <input type="hidden" id="hidden_column_name" value="challenge_name">
  <input type="hidden" id="hidden_sort_type" value="asc">
  <input type="hidden" id="hidden_page" value="1">
  <input type="hidden" id="language" name="language" value="{{$lang}}">

</div>
@endsection
