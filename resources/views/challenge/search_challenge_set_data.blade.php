@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $set)

<tr>
  <td>{{$ii}}</td>
  <td>{{$set->title}}</td>
  <td>{{$set->challenges->count()}}</td>
  <td>{{date('d-m-Y', strtotime($set->created_at))}}</td>
  <td>{{$set->status}}</td>


  <td>
    @can('read-challenge-set')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
      <a href="/challenge-set/{{$set->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
    @endcan
    @can('update-challenge-set')
    @if ($userRole!='admin')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
      <a href="/challenge-set/{{ $set->id }}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endif
    @endcan
    @can('delete-challenge-set')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
      <a onclick="confirmationDelete('/challenge-set/{{ $set->id }}/delete','@lang(Lang::locale().'.CHALLENGE_SET_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.CHALLENGE_SET_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/challenge-set/{{ $set->id }}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
    @endcan
  </td>
</tr>
@php $ii++;@endphp
@endforeach
@if(count($data)!=0)
@if($data->hasPages())
<tr>
  <td colspan="6" id="gsetpagination">

    {!! $data->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($data)==0)
<tr>
  <td colspan="6">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-challenge-set')
      <!-- <a class="btn btn-primary" href="/questions/create" role="button"> -->
      <button data-href="/challenge-set/create','')}}" onclick="window.location.href='/challenge-set/create/{{$lang}}'" class="btn btn-primary createbtn">
        <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')
        </a>
        @endcan
    </div>
</tr>
</td>
@endif
