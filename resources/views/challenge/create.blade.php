@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
        @endif
        <form method="POST" action="/challenge/store" autocomplete="off" data-title="@lang(Lang::locale().'.CHALLENGE_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.CHALLENGE_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('lang_code') ? 'has-error' : '' }}">
              <label for="name"> @lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
              <select data-validation="required" name="lang_code" class="form-control">
                <!-- <option value=""> -- Select @lang(Lang::locale().'.LANGUAGE') -- </option> --> @forelse($languages as $language)
                <option {{$language->code==$lang?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
                @empty
                @endforelse
              </select>
              <span class="text-danger">{{ $errors->first('lang_code') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('challenge_name') ? 'has-error' : '' }}">
              <label for="name"> @lang(Lang::locale().'.TITLE'): <span class="text-danger">*</span></label>
              <input type="text" id="name" name="challenge_name" class="form-control" placeholder="@lang(Lang::locale().'.CHALLENGE_TITLE_PLACEHOLDER')" value="{{ old('challenge_name') }}" data-validation="required custom length" data-validation-length="max50">
              <span class="text-danger">{{ $errors->first('challenge_name') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('challenge_description') ? 'has-error' : '' }}">
              <label for="about"> @lang(Lang::locale().'.DESCRIPTION'): <span class="text-danger">*</span></label>
              <textarea type="text" id="about" name="challenge_description" data-validation="required length" data-validation-length="max150" rows="8" class="form-control">{{ old('challenge_description') }}</textarea>
              <span class="text-danger">{{ $errors->first('challenge_description') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('challenge_category_id') ? 'has-error' : '' }}">
              <label for="challenge_category_id"> @lang(Lang::locale().'.CHALLENGE_CATEGORY_PLURAL'): <span class="text-danger">*</span></label>
              <select name="challenge_category_id" class="form-control" data-validation="required">
                <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.CATEGORY')</option>
                @foreach($challenge_sets as $challenge_set)
                <option value="{{$challenge_set->id}}" {{$categoty_id ==$challenge_set->id?'selected':''}}>{{$challenge_set->name}}</option>
                @endforeach

              </select>
              <span class="text-danger">{{ $errors->first('challenge_category_id') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('challenge_complexity') ? 'has-error' : '' }}">
              <label for="challenge_complexity"> @lang(Lang::locale().'.CHALLENGE_COMPLEXITY'): <span class="text-danger">*</span></label>
              <input type="number" id="challenge_complexity" data-validation="required" name="challenge_complexity" class="form-control" placeholder="@lang(Lang::locale().'.CHALLENGE_COMPLEXITY_PLACEHOLDER')" value="{{ old('challenge_complexity',5) }}" min="1" max="7">
              <span class="text-danger">{{ $errors->first('challenge_complexity') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('CHALLENGE_COMPLETE_DAYS') ? 'has-error' : '' }}">
              <label for="CHALLENGE_COMPLETE_DAYS"> @lang(Lang::locale().'.CHALLENGE_COMPLETE_DAYS'): <span class="text-danger">*</span></label>
              <input type="number" id="no_of_days_to_complete" data-validation="required" name="no_of_days_to_complete" class="form-control" placeholder="@lang(Lang::locale().'.CHALLENGE_COMPLETE_DAYS_PLACEHOLDER')" value="{{ old('no_of_days_to_complete') }}" min="1" max="7">
              <span class="text-danger">{{ $errors->first('no_of_days_to_complete') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('challenge_status') ? 'has-error' : '' }}">
              <label for="challenge_status"> @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
              <select name="challenge_status" class="form-control" data-validation="required">
                <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
                <option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
                <option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
              </select>
              <span class="text-danger">{{ $errors->first('challenge_status') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-12 {{ $errors->has('challenge_options') ? 'has-error' : '' }}">
              <table class="table" id="dynamic_field">
                <thead>
                  <tr>
                    <th>
                      <label for="name"> @lang(Lang::locale().'.OPTIONS'):</label>
                    </th>
                    <th>
                      <label for="name"> @lang(Lang::locale().'.POINT'):</label>
                    </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <input type="text" name="challenge_options[]" placeholder="@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')" data-validation="length" data-validation-length="max50" class="form-control name_list" />
                    </td>
                    <td><input type="text" name="challenge_point[]" placeholder="@lang(Lang::locale().'.CHALLENGE_POINT_PLACEHOLDER')" data-validation="custom length" maxlength="3" data-validation-optional="true" data-validation-length="max3" data-validation-regexp="^\d+$" class="form-control name_list" /></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td colspan="3" class="text-right"><button type="button" name="add" onclick="addMore('1','@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')','@lang(Lang::locale().'.CHALLENGE_POINT_PLACEHOLDER')')" id="add" class="btn btn-sm btn-success">Add More</button></td>
                  </tr>
                </tbody>
              </table>

              <span class="text-danger">{{ $errors->first('challenge_options') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <input type="hidden" id="btnvalue" name="btnvalue" value="">
              <button onclick="$('#btnvalue').val('new');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_NEW')</button>
              <button onclick="$('#btnvalue').val('exit');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_EXIT')</button>
              <button type="button" onclick="window.location.href='/challenges/{{ $categoty_id??0 }}/{{$lang}}'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection
