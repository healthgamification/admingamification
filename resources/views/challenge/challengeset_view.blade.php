@extends('layouts.app')

@section('content')

<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
      <h4 class="float-left">{{ $challengeSetData->title }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeSetData->title }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeSetData->language->name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeSetData->description }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeSetData->status }}</td>

      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($challengeSetData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  @if(count($challengeData)>0)
  @php $i=1 @endphp
  <div class="row">
    <div class="col-md-12 mt-3">
      <div class="card-sub-header px-0 bg-transparent clearfix">
        <strong class="float-left pt-3">@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.DETAILS')</strong>
      </div>
    </div>
  </div>
  <table class="table table-hover mt15">
    @foreach($challengeData as $cd)
    @if($i%2 == 1)
    <tr>
      @endif
      <td class="width-20"><strong>
          @lang(Lang::locale().'.CHALLENGE_SINGULAR') {{ $i }}
        </strong>
      </td>
      <td class="width-30">
        {{ $cd->challenge->challenge_name }}
      </td>
      @if($i%2==0 || count($challengeData)==$i)
    </tr>
    @endif
    @php $i++ @endphp
    @endforeach
  </table>
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-challenge-set')
      <a href="/challenge-set/{{ $challengeSetData->id }}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection
