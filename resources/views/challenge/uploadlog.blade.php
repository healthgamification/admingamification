@extends('layouts.app')

@section('content')
<div class="container">  
   <div class="card-header px-0 mt-2 bg-transparent clearfix">
          <h4 class="float-left pt-2">Challenge Bulk Upload Log</h4>
         
        </div>
      
                <table class="table">                	
                <thead>                	
               <tr>
               	<th>Row No.</th>
               	<th>Language</th>
               	<th>Category Name</th>
               	<th>Title</th>
               	<th>Description</th>
               	<th>Options</th>
               </tr>
                </thead>
                <tbody>
                	<tr>
                		<th>File Name</th>
                		<td>{{$updata->filename}}</td>
                		<th>Uploaded Date</th>
                		<td>{{date('d-m-Y h:i:s a',strtotime($updata->created_at))}}</td>
                		<th>Uploaded By</th>
                		<td>{{$updata->user->name}}</td>
                	</tr>
                	@php
                	$datas = json_decode($updata->data);
                	@endphp
                	@forelse($datas as $key=>$data)
                	<tr class="{{$key}}">
                		<th colspan="6">{{ucfirst(str_replace('_',' ',$key))}}</th>
                	</tr>
                	@forelse($data as $k=>$d)
                	<tr class="{{$key}}">
                		<td>{{$k}}</td>
                		<td style="{{empty($d[0])?'background-color: red;':''}}">{{$d[0]}}</td>
                		<td style="{{empty($d[1])?'background-color: red;':''}}">{{$d[1]}}</td>
                		<td style="{{empty($d[2])?'background-color: red;':''}}">{{$d[2]}}</td>
                		<td style="{{empty($d[3])?'background-color: red;':''}}">{{$d[3]}}</td>
                		<td style="{{empty($d[4])?'background-color: red;':''}}">{{$d[4]}}</td>
                	</tr>
                	@empty
                	@endforelse
                	@empty
                	@endforelse
                </tbody>
               
                </table>
     

      <!----> <!---->
   </div>
</div>

@endsection