@extends('layouts.app')
@section('content')
@if(empty($setId))
@php
$setId='';
$style="display:none";
@endphp
@else
@php
$style="display:block";
@endphp
@endif
<div class="container">
   <span id="challengeData" style="">
      <div class="card-header px-0 bg-transparent clearfix">
         <h4 class="float-left mt-1">@lang(Lang::locale().'.CHALLENGE_PLURAL')</h4>
         @can('create-challenge')
         <div class="card-header-actions mr-1"><a href="/challenge-category" class="btn btn-sm btn-primary">@lang(Lang::locale().'.VIEW') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</a></div>
         @endcan
      </div>
      <div class="card-body px-0">
         <div class="alert alert-success alert-dismissible" style="display: none;" id="mainmsg"></div>
         @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
         @endif
         <div class="row justify-content-between">        
            <div class="col-md-12">
                @include('challenge/uploadcsv')
            </div>
         </div> 
         <div class="saperator-line">
         </div>
         <div class="row mb-3 mt-3">
            {{ csrf_field() }}
            <div class="col-md-4">
               <div class="form-group mb-3">
                  <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')</label>
                  <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_PLURAL')" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}/'+$('#hidden_category_id').val()+'/'+$('#language').val())" id="serach" name="q" value="" class="form-control">
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group mb-3">
                  <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE')</label>
                  <select class="form-control" name="lang" onchange="get_lang_filtered_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">
                     <!-- <option value="0">Select Language</option> -->
                     @foreach($languages as $language)
                     <option {{$language->code==$lang?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
                     @endforeach
                  </select>
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group mb-3">
                  <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</label>
                  <select class="form-control" id="challenge_category" name="setId" onchange="get_filtered_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">
                     <option value="0">@lang(Lang::locale().'.ALL')</option>
                     @foreach($challenge_sets as $challenge_set)
                     <option value="{{$challenge_set->id}}" {{$category_id==$challenge_set->id?'selected':''}}>{{$challenge_set->name}}</option>
                     @endforeach
                  </select>

               </div>
            </div>
            <!-- <div class="col-md-3 mt-4 mb-3 text-right">
               @can('create-challenge')
               <div class="card-header-actions mr-1">
                  <button data-href="/challenge/create" @if($category_id==null || $category_id==0) onclick="window.location.href='/challenge/create';" @else onclick="window.location.href='/challenge/create/{{$category_id??0}}/{{$lang}}';" @endif class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</button></div>
               @endcan
            </div> -->
            <div class="clearfix"></div>
         </div>
         <div class="table-body">
            <table class="table table-hover">
               <thead>
                  <tr>
                     <th class="sorting ">#</th>
                     <th class="sorting " onclick="challenge_sorting('{{request()->route()->getAction()['prefix']}}','challenge_name')" style="cursor: pointer">@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.TITLE') <span class="icons challenge_name"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
                     <th class="sorting">@lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR') </th>
                     <th class="sorting " onclick="challenge_sorting('{{request()->route()->getAction()['prefix']}}','options_count')" style="cursor: pointer">@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.OPTIONS') <span class="icons options_count"></span></th>
                     <th class="sorting " onclick="challenge_sorting('{{request()->route()->getAction()['prefix']}}','created_at')" style="cursor: pointer">@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
                   

                     <th class="sorting " onclick="challenge_sorting('{{request()->route()->getAction()['prefix']}}','challenge_status')" style="cursor: pointer">@lang(Lang::locale().'.STATUS') <span class="icons challenge_status"></span></th>

                     <th>@lang(Lang::locale().'.ACTION')</th>
                     
                     @can('read-log')
                       <th>@lang(Lang::locale().'.LOG_SINGULAR')</th> 
                     @endcan
                    

                  </tr>
               </thead>
               <tbody id="tbody">
                  @include('challenge/search_data')

               </tbody>
            </table>
         </div>
         <div id="loader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated">
               <div class="vue-content-placeholders-text">
                  <div class="vue-content-placeholders-text__line"></div>
                  <div class="vue-content-placeholders-text__line"></div>
                  <div class="vue-content-placeholders-text__line"></div>
                  <div class="vue-content-placeholders-text__line"></div>
               </div>
            </div>
         </div>
         <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
         <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="challenge_name" />
         <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
         <input type="hidden" name="hidden_category_id" id="hidden_category_id" value="{{$category_id??0}}" />
         <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$category_id??0}}/{{$lang}}" />
         <input type="hidden" name="language" id="language" value="{{$lang}}" />

         <!---->
         <!---->
      </div>
   </span>
</div>

@can('create-challenge')
<span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')" class="btn-plus">
   <a data-href="/challenge/create" class="createbtn" @if($category_id==null || $category_id==0) onclick="window.location.href='/challenge/create';" @else onclick="window.location.href='/challenge/create/{{$category_id??0}}/{{$lang}}';" @endif><i class="fa fa-plus"></i></a>
</span>
@endcan
@include('viewlog/view_log')
@endsection
