@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $challenge)

<tr>
  <td>{{$ii}}</td>
  <td>{{$challenge->challenge_name}}</td>
  <td>{{$challenge->category->name}}</td>
  <td>{{$challenge->options_count}}</td>
  <td>
    {{date('d-m-Y',strtotime($challenge->created_at))}}
  </td>
 
  <td>{{$challenge->challenge_status}}</td>

  <td>
    @can('read-challenge')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
      <a href="/challenge/{{$challenge->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
    @endcan
    @can('update-challenge')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
      <!-- <a onclick="confirmation('/challenge/{{$challenge->id}}/edit','@lang(Lang::locale().'.CHALLENGE_EDIT_CONFIRM_TITLE')',' @lang(Lang::locale().'.CHALLENGE_EDIT_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') "  href="/challenge/{{$challenge->id}}/edit" class="edit-button" title="Edit"><i class="fas fa-pencil-alt"></i></a> -->
      <a href="/challenge/{{$challenge->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endcan
    @can('delete-challenge')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
      <a onclick="confirmationDelete('/challenge/{{$challenge->id}}/delete','@lang(Lang::locale().'.CHALLENGE_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.CHALLENGE_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/challenge/{{$challenge->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>

               @endcan
                  </td>
                  @can('read-log')
                  <td>
                    <a data-target="#logModel" data-toggle="modal" data-id="{{ $challenge->id }}" data-url="{{ config('app.url') }}" data-model_id="{{ encrypedecrypt('ChallengeLog') }}" href="#"  class="bg-loglist view-button"><i class="fa fa-list"></i></a>
                  </td>
                  @endcan
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($data)!=0)
            @if($data->hasPages())
              <tr>
                <td colspan="7" id="mainpagenation">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-challenge')       
        <button  data-href="/challenge/create" onclick="window.location.href='/challenge/create/{{$category_id??0}}/{{$lang}}';" class="btn btn-primary createbtn"> <i class="fa fa-plus"></i>&nbsp;  @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</button>
        @endcan
      </div>
</tr></td>
@endif
            