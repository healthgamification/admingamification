@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <div class="col-md-12">
      <h4>{{ $challengeData->challenge_name }}</h4>
    </div>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeData->challenge_name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{$challengeData->challenges->name}}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeData->challenge_description }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.LANGUAGE_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $challengeData->language->name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_COMPLEXITY')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeData->complexity_of_challenge }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-35">{{ $challengeData->challenge_status }}</td>
        
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3" class="wordbreak">{{ custom_date_format($challengeData->created_at) }}</td>
      </tr>
    </tbody>

  </table>
  @if(count($challengeOptionData)>0)
  @php $i=1 @endphp
  <div class="row">
    <div class="col-md-12 mt-3">
    <div class="card-sub-header px-0 bg-transparent clearfix">
      <strong class="float-left pt-3">@lang(Lang::locale().'.CHALLENGE_SINGULAR') @lang(Lang::locale().'.OPTIONS')</strong>
    </div>
    </div>
  </div>
  <table class="table table-hover mt15">
    @foreach($challengeOptionData as $cod)
    @if($i%2 == 1)
    <tr>
      @endif
      <td class="width-10"><strong>
          @lang(Lang::locale().'.OPTION') {{ $i }}
        </strong>
      </td>
      <td class="width-15">
        {{ $cod->option_name }}
      </td>
      <td class="width-15"><strong>
          @lang(Lang::locale().'.OPTION') {{ $i }} @lang(Lang::locale().'.POINT')
        </strong>
      </td>
      <td class="width-10">
        {{ $cod->point }}
      </td>
      @if($i%2==0 || count($challengeOptionData)==$i)
    </tr>
    @endif
    @php $i++ @endphp
    @endforeach
  </table>
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-challenge')
      <a href="/challenge/{{$challengeData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection
