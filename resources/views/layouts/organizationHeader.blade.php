<div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{$ogData['name']}}</h4>
</div>
<div class="card-body px-0 py-2">
    <div class="navbar menus-light">
        <div class="navbar-inner ogmenu">
            <ul class="nav">
                <li class="nav-item {{ request()->is('organization/*/view') ? 'open' : '' }}"><a href="/organization/{{$ogData['id']}}/view" class="nav-link {{ request()->is('organization/*/view') ? 'active' : '' }}">@lang(Lang::locale().'.ORGANIZATION_SINGULAR') </a></li>
                <li class="nav-item {{ request()->is('organization/*/view/users') ? 'open' : '' }}"><a href="/organization/{{$ogData['id']}}/view/users" class="nav-link {{ request()->is('organization/*/view/users') ? 'active' : '' }}">@lang(Lang::locale().'.MEMBER_PLURAL')</a></li>
                <li class="nav-item {{ request()->is('organization/*/view/teams') ? 'active' : '' }}"><a href="/organization/{{$ogData['id']}}/view/teams" class="nav-link {{ request()->is('organization/*/view/teams') ? 'active' : '' }}">@lang(Lang::locale().'.TEAM_PLURAL')</a></li>
                <li class="nav-item"><a href="/organization/{{$ogData['id']}}/view/quizzes" class="nav-link {{ request()->is('organization/*/view/quizzes') ? 'active' : '' }}">@lang(Lang::locale().'.QUESTION_SET_PLURAL')</a></li>
                <li class="nav-item"><a href="/organization/{{$ogData['id']}}/view/challenges" class="nav-link {{ request()->is('organization/*/view/challenges') ? 'active' : '' }}">@lang(Lang::locale().'.CHALLENGE_SET_PLURAL')</a></li>
            </ul>
        </div>
    </div>