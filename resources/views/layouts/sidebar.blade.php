<div class="sidebar">

    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/dashboard">
                    <i class="nav-icon icon-speedometer"></i>@lang(Lang::locale().'.MY_DASHBOARD')
                </a>
            </li>
            @php
            $currentUserRole=json_decode(Auth::user()->load('roles')->roles);
            $userRole =  $currentUserRole[0]->name; 
            @endphp
            @if ($userRole=='admin')
            @php $org_url = "/organization/".Auth::user()->org_id."/view"; @endphp
            @else
            @php $org_url = "/organization"; @endphp
            @endif
            @can('read-organization')
            <li class="nav-item">
                <a class="nav-link {{ (request()->is('organization/*') || request()->is('assign/*')) ? 'active' : '' }}" href="{{ $org_url }}">
                    <i class="nav-icon icon-organization"></i>@lang(Lang::locale().'.MY_ORGANIZATION')
                </a>
            </li>
            @endcan
            @can('read-department')
            <li class="nav-item" style="margin-left: 1.8rem;">
                <a class="nav-link {{ request()->is('departments/*') || request()->is('department/*')  ? 'active' : '' }}" href="/departments">
                    <i class="nav-icon icon-home"></i> @lang(Lang::locale().'.DEPARTMENT_PLURAL')
                </a>
            </li>
            @endcan
            @can('read-designation')
            <li class="nav-item" style="margin-left: 1.8rem;">
                <a class="nav-link {{ request()->is('designations/*') || request()->is('designation/*')  ? 'active' : '' }}" href="/designations">
                    <i class="nav-icon icon-badge"></i> @lang(Lang::locale().'.DESIGNATION_PLURAL')
                </a>
            </li>
            @endcan

            @can('read-member')
            <li class="nav-item" style="margin-left: 1.8rem;">
                <a class="nav-link {{ request()->is('member/*') ? 'active' : '' }}" href="/members">
                    <i class="nav-icon icon-user"></i>@lang(Lang::locale().'.MEMBER_PLURAL')
                </a>
            </li>
            @endcan
            @can('read-team')
            <li class="nav-item" style="margin-left: 1.8rem;">
                <a class="nav-link {{ request()->is('team/*') || request()->is('players/*') ? 'active' : '' }}" href="/teams">
                    <i class="nav-icon icon-people"></i>@lang(Lang::locale().'.TEAM_PLURAL')
                </a>
            </li>
            @endcan
            @canany(['read-challenge', 'read-challenge-set'])
            <li class="nav-title">@lang(Lang::locale().'.CHALLENGE_SINGULAR')</li>
            @can('read-challenge-category')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('challenge-set/*') ? 'active' : '' }}" href="/challenge-category">
                    <i class="nav-icon icon-notebook"></i> @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')
                </a>
                
            </li>
            @endcan
            
            @can('read-challenge-set')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('challenge-set/*') ? 'active' : '' }}" href="/challenge-sets">
                    <i class="nav-icon icon-notebook"></i> @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')
                </a>
                
            </li>
            @endcan
            @can('read-challenge')
            <li class="nav-item">
                <a class="nav-link {{request()->is('challenge/*') ? 'active' : '' }}" href="/challenges">
                    <i class="nav-icon icon-question"></i> @lang(Lang::locale().'.CHALLENGE_BANK')
                </a>
            </li>
            @endcan
            @endcan
            @canany(['read-question', 'read-quiz-set'])
            <li class="nav-title">@lang(Lang::locale().'.QUIZ_SINGULAR')</li>
            @can('read-quiz-category')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('qcategory/*') ? 'active' : '' }}" href="/qcategory">
                    <i class="nav-icon icon-notebook"></i> @lang(Lang::locale().'.QUESTION_CATEGORY')
                </a>
                
            </li>
            @endcan

            @can('read-quiz-set')           
            <li class="nav-item">
                <a class="nav-link {{request()->is('question-sets/*') || request()->is('question-set/*')  ? 'active' : '' }}" href="/question-sets">
                    <i class="nav-icon icon-notebook"></i>  @lang(Lang::locale().'.QUESTION_SET_PLURAL')
                </a>
            </li>
            @endcan
            @can('read-question')
            <li class="nav-item">
                <a class="nav-link {{request()->is('questions/*') || request()->is('question/*') ? 'active' : '' }}" href="/questions">
                    <i class="nav-icon icon-question"></i>  @lang(Lang::locale().'.QUESTION_BANK')    
                </a>
            </li>
            @endcan
            @endcan
            @can('read-users','read-roles')
            <li class="nav-title">@lang(Lang::locale().'.SETTINGS')</li>
            @endcan
            @can('read-users')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('users/*') ? 'active' : '' }}" href="/users">
                    <i class="nav-icon icon-user-follow"></i> @lang(Lang::locale().'.USERS')
                </a>
            </li>
            @endcan
            @can('read-roles')
          <!--   <li class="nav-item">
                <a class="nav-link" href="/roles">
                    <i class="nav-icon icon-key"></i> Roles
                </a>
            </li> -->
            @endcan
            @can('read-export')
            <li class="nav-item">
                <a class="nav-link {{ request()->is('export/*') ? 'active' : '' }}" href="/export">
                    <i class="nav-icon icon-cloud-download"></i> @lang(Lang::locale().'.EXPORT')
                </a>
            </li>
            @endcan
             @can('read-language')
            <li class="nav-item">
                <a class="nav-link {{(request()->is('translations/*') || request()->is('translation/*')) ? 'active' : '' }}" href="/translations">
                    <i class="nav-icon icon-note"></i> @lang(Lang::locale().'.LANGUAGE_PLURAL')
                </a>
            </li>
            @endcan

             @can('update-mail-config')
            <li class="nav-item">
                <a class="nav-link {{(request()->is('email/*') || request()->is('email/*')) ? 'active' : '' }}" href="/email">
                    <i class="nav-icon icon-envelope-open"></i> @lang(Lang::locale().'.EMAIL_SETUP')
                </a>
            </li>
            @endcan

            @can('read-mail-template')
            <li class="nav-item">
                <a class="nav-link {{(request()->is('templates/*') || request()->is('template/*')) ? 'active' : '' }}" href="/templates">
                    <i class="nav-icon icon-envelope-letter"></i> @lang(Lang::locale().'.TEMPLATE_PLURAL')
                </a>
            </li>
            @endcan 
            @can('read-dynamicfield')
           <!--  <li class="nav-item">
                <a class="nav-link {{(request()->is('dynamicfields/*') || request()->is('dynamicfield/*')) ? 'active' : '' }}" href="/dynamicfields">
                    <i class="nav-icon icon-envelope-letter"></i> @lang(Lang::locale().'.DYNAMICFIELD_PLURAL')
                </a>
            </li> -->
            @endcan
           
        </ul>
    </nav>
</div>
