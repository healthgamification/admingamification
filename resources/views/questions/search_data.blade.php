@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $question)

<tr>
               <td>{{$ii}}</td>
               <td>
                <div class="media">
               
                <div class="media-body">
                  <div>
                    @foreach($question->categories as $cat)
                      <p style="margin:0px">{{ $cat->category->category_name }}</p>
                    @endforeach</div>
                  
              </div>
            </td>
               <td>{{ wordwrap(stripcslashes($question->question),80) }}</td>
                <td>
              {{date('d-m-Y',strtotime($question->created_at))}}
            </td>
             <td>{{$question->status}}</td>
               
               <td>
                @can('update-question')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
              <a href="/question/{{$question->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
              @endcan
               @can('update-question')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/question/{{$question->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-question')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/question/{{$question->id}}/delete','@lang(Lang::locale().'.QUESTION_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.QUESTION_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/questions/{{$question->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
               @endcan
                  </td>
                  @can('read-log')
                  <td>
                    <a data-target="#logModel" data-toggle="modal" data-id="{{ $question->id }}" data-url="{{ config('app.url') }}" data-model_id="{{ encrypedecrypt('QueLog') }}" href="#"  class="bg-loglist view-button"><i class="fa fa-list"></i></a>
                  </td>
                  @endcan
</tr>
@php $ii++;@endphp
@endforeach
@if(count($data)!=0)
@if($data->hasPages())
<tr>
  <td colspan="6" id="mainpagenation">

    {!! $data->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($data)==0)
<tr>
  <td colspan="6">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-question')
      <!-- <a class="btn btn-primary" href="/questions/create" role="button"> -->
      <button data-href="/questions/create" @if($category_id==null || $category_id==0) onclick="window.location.href='/question/create/{{$category_id??0}}/{{ $lang }}';" @else onclick="window.location.href='/question/create/{{$category_id??0}}/{{ $lang }}';" @endif class="btn btn-primary createbtn">
        <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION')
        </a>
        @endcan
      </div>
</tr></td>
@endif
            