@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{ $questionSetData->title }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $questionSetData->title }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</strong></td>
        <td class="width-30 wordbreak">{{ $questionSetData->language->name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{ $questionSetData->description }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30 wordbreak">{{ $questionSetData->status }}</td>

      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($questionSetData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  @if(count($questionData)>0)
  @php $i=1 @endphp
  <div class="row">
    <div class="col-md-12 mt-3">

      <div class="card-sub-header px-0 mt-3 bg-transparent clearfix">
        <strong class="float-left">@lang(Lang::locale().'.QUESTION_PLURAL') @lang(Lang::locale().'.DETAILS')</strong>
      </div>

    </div>
  </div>
  <table class="table table-hover mt15">
    @foreach($questionData as $qd)
    @if($i%2 == 1)
    <tr>
      @endif
      <td class="width-20"><strong>
          @lang(Lang::locale().'.QUESTION') {{ $i }}
        </strong>
      </td>
      <td class="width-30">
        {{ $qd->question->question }}
      </td>
      @if($i%2==0 || count($questionData)==$i)
    </tr>
    @endif
    @php $i++ @endphp
    @endforeach
  </table>
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-quiz-set')
      <a href="{{route('qset.edit',[$questionSetData->lang_code,$questionSetData->id])}}" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')</a>

      @endcan
    </div>
  </div>
</div>
</div>


@endsection
