@extends('layouts.app')
@section('content')
@if(empty($setId))
@php
$setId='';
$style="display:none";
@endphp
@else
@php
$style="display:block";
@endphp
@endif
<div class="container">
   <div class="card-header px-0 bg-transparent clearfix">
      <h4 class="float-left mt-1">@lang(Lang::locale().'.QUESTION_BANK')</h4>
      @can('read-quiz-category')
      <div class="card-header-actions mr-1"><a href="/qcategory" class="btn btn-sm btn-primary">@lang(Lang::locale().'.VIEW') @lang(Lang::locale().'.QUESTION_CATEGORY')</a></div>
      @endcan
   </div>
   <div class="card-body px-0">
      <div class="alert alert-success alert-dismissible" style="display: none;" id="mainmsg"></div>
      @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
      </div>
      @endif
      <div class="row justify-content-between">        
            <div class="col-md-12">
                @include('questions/uploadcsv')
            </div>
         </div> 
         <div class="saperator-line">
         </div>
      <div class="row">
         <div class="col-md-4">
            <div class="form-group mt-3">
               {{ csrf_field() }}
               <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL')</label>
               <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL')" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#hidden_category_id').val()+'/'+$('#language').val())" id="serach" class="form-control">
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group mt-3">
               <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</label>
               <select class="form-control" name="lang" onchange="get_lang_filtered_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}/{{$perpage}}')">

                  <!-- <option value="0">Select Language</option> -->
                  @foreach($languages as $language)
                  <option {{$language->code==$lang?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
                  @endforeach
               </select>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group mt-3">
               <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.QUESTION_CATEGORY')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.QUESTION_CATEGORY')</label>
               <select class="form-control" name="setId" id="challenge_category" onchange="get_filtered_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}/{{$perpage}}')">

                  <option value="0">@lang(Lang::locale().'.ALL')</option>

                  @foreach($category_data as $cat_data)
                  <option value="{{ $cat_data->id }}" {{$category_id==$cat_data->id?'selected':''}}>{{$cat_data->category_name}}</option>
                  @endforeach
               </select>

            </div>

         </div>


      </div>
   </div>
   <div class="table-body">
      <table class="table table-hover">
         <thead>
            <tr>
               <th class="sorting ">#</th>
               <th class="sorting " style="width:20%">@lang(Lang::locale().'.QUESTION_CATEGORY_NAME')</th>
               <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#hidden_category_id').val()+'/'+$('#language').val(),'question')" style="cursor: pointer; width:40%">@lang(Lang::locale().'.QUESTION')<span class="icons question"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>

               <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#hidden_category_id').val()+'/'+$('#language').val(),'created_at')" style="cursor: pointer; width:10%">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
               <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#hidden_category_id').val()+'/'+$('#language').val(),'status')" style="cursor: pointer; width:10%">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>

                <th>@lang(Lang::locale().'.ACTION')</th>
              
               @can('read-log')
                 <th>@lang(Lang::locale().'.LOG_SINGULAR')</th> 
               @endcan
              
              
            </tr>
         </thead>
         <tbody id="tbody">
            @include('questions/search_data')
         </tbody>
      </table>
   </div>
   <div id="loader" style="display:none;">
      <div class="vue-content-placeholders vue-content-placeholders-is-animated">
         <div class="vue-content-placeholders-text">
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
         </div>
      </div>
   </div>
   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
   <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="id" />
   <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
   <input type="hidden" name="hidden_category_id" id="hidden_category_id" value="{{$category_id??0}}" />
   <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$perpage}}/{{$category_id??0}}/{{$lang}}" />
   <input type="hidden" name="language" id="language" value="{{$lang}}" />
</div>

@can('create-question')

<span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION')" class="btn-plus">
	<a data-href="/question/create" @if($category_id==null || $category_id==0) onclick="window.location.href='/question/create';" @else onclick="window.location.href='/question/create/{{$category_id}}/{{ $lang }}';"  @endif class="createbtn" ><i class="fa fa-plus"></i></a>
</span>
@endcan

 @include('viewlog/view_log')
@endsection
