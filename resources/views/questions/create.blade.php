@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.NEW') @lang(Lang::locale().'.QUESTION')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <form method="POST" action="/question/store" autocomplete="off" data-title="@lang(Lang::locale().'.QUESTION_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.QUESTION_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="hidden_category" value="{{ $category_id }}">

          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
              <label for="lang">@lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
              <select name="lang" class="form-control" name="lang" data-validation="required">
                @forelse(getLanguages() as $lang)
                <option {{$lang->code==$language?'selected':''}} value="{{$lang->code}}">{{$lang->name}}</option>
                @empty
                @endforelse
              </select><span class="text-danger">{{ $errors->first('lang') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('category') ? 'has-error' : '' }}">
              <label for="category">@lang(Lang::locale().'.QUESTION_CATEGORY_NAME'): <span class="text-danger">*</span></label>
              <select id="category" multiple class="form-control myselect" style="width: 100%;" name="category[]" data-validation="required">
                @if(!empty($qcategorydata))
                @foreach($qcategorydata as $qcd)
                <option value="{{ $qcd->id }}" {{ $qcd->id==$category_id?'selected':'' }}>{{ $qcd->category_name }}</option>
                @endforeach
                @endif
              </select>
              <span class="text-danger">{{ $errors->first('category') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('status') ? 'has-error' : '' }}">
              <label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
              <select name="status" class="form-control" name="status" data-validation="required">
                <option value="">@lang(Lang::locale().'.STATUS_ERROR')</option>
                <option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
                <option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
              </select><span class="text-danger">{{ $errors->first('status') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('question') ? 'has-error' : '' }}">
              <label for="question">@lang(Lang::locale().'.QUESTION'): <span class="text-danger">*</span></label>
              <textarea type="text" id="question" name="question" class="form-control" data-validation="required" placeholder="@lang(Lang::locale().'.QUESTION_PLACEHOLDER')" rows="3">{{ old('question') }}</textarea>
              <span class="text-danger">{{ $errors->first('question') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('fact') ? 'has-error' : '' }}">
              <label for="fact">@lang(Lang::locale().'.QUESTION_FACT'): <span class="text-danger">*</span></label>
              <textarea type="text" id="fact" name="fact" class="form-control" data-validation="required" data-validation="required length" data-validation-length="max150" placeholder="@lang(Lang::locale().'.QUESTION_FACT_PLACEHOLDER')" rows="3">{{ old('fact') }}</textarea>
              <span class="text-danger">{{ $errors->first('fact') }}</span>
            </div>
            @if(1==0)
            <div class="col-md-4 form-group {{ $errors->has('weightage') ? 'has-error' : '' }}">
              <label for="weightage">@lang(Lang::locale().'.WEIGHTAGE'):</label>
              <input type="number" id="weightage" name="weightage" class="form-control" value="{{ old('weightage') }}" min="0" placeholder="@lang(Lang::locale().'.QUESTION_WEIGHTAGE')">
              <span class="text-danger">{{ $errors->first('weightage') }}</span>
            </div>
            @endif
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-12 {{ $errors->has('challenge_options[]') ? 'has-error' : '' }}">
              <table class="table" id="dynamic_field">
                <tr>
                  <td style="width: 75%;"><label for="name"> @lang(Lang::locale().'.OPTIONS'):</label></td>
                  <td style="width: 15%;"><label for="name"> @lang(Lang::locale().'.SELECT_ANSWER'):</label></td>
                  <td style="width: 10%;"></td>
                </tr>
                <tr>
                  <td><input type="text" name="challenge_options[]" placeholder="@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')" class="form-control name_list" data-validation="required length" data-validation-length="max50" /></td>
                  <td class="text-center"><input type="radio" name='challange_answer' data-validation="required" value='0' id='chanllange_answer0' checked /></td>
                  <td></td>
                </tr>

                <tr>
                  <td colspan="3" class="text-right"><button type="button" name="add" onclick="addMoreOption('0','@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')')" id="add" class="btn btn-sm btn-success">Add More</button></td>
                </tr>
              </table>

              <span class="text-danger">{{ $errors->first('challenge_options') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <!-- <button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button> -->
              <input type="hidden" id="btnvalue" name="btnvalue" value="">
              <button onclick="$('#btnvalue').val('new');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_NEW')</button>
              <button onclick="$('#btnvalue').val('exit');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_EXIT')</button>
              <button type="button" onclick="window.location.href='/questions/{{ $category_id??0 }}/{{ $language }}'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
            <div class="clearfix"></div>
          </div>
      </div>
      </form>

    </div>
  </div>
</div>


@endsection
