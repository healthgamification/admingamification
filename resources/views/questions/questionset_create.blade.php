@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')</h4>
  </div>
  <form method="POST" action="{{route('setsave')}}" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.QUESTION_SET_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.QUESTION_SET_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
    <div class="card-body px-0">
      @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
      @endif
      @if($errors->has('question_ids'))
      <div class="alert alert-danger">
        {{ $errors->first('question_ids') }}
      </div>
      @endif

      @if(count($errors))

      @endif
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" value="0" id="set_id">
      <div class="row" id="firstrow">
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('question_set_name') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
            <input type="text" id="name" name="question_set_name" class="form-control" value="{{ old('question_set_name') }}" data-validation="required custom length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.QUESTION_SET_NAME_PLACEHOLDER')">
            <span class="text-danger">{{ $errors->first('question_set_name') }}</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('question_set_name') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
            <select class="form-control" name="lang" onchange="get_lang_filtered_set_data(this,'{{request()->route()->getAction()['prefix']}}','type','question_category')">
              @foreach(getLanguages() as $language)
              <option {{$language->code==$lang?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
            <select name="status" class="form-control" data-validation="required">
              <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
              <option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
              <option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
            </select>
            <span class="text-danger">{{ $errors->first('status') }}</span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group {{ $errors->has('question_set_desc') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.DESCRIPTION'):</label>
            <textarea class="form-control" placeholder="@lang(Lang::locale().'.QUESTION_SET_DESCRIPTION_PLACEHOLDER')" data-validation="length" data-validation-length="max150" name="question_set_desc">{{old('question_set_desc')}}</textarea>
            <span class="text-danger">{{ $errors->first('question_set_name') }}</span>
          </div>
        </div>
        <div class="col-md-12">
          <button class="btn btn-primary" onclick="$(this).closest('div').hide(); $('#secondrow').toggle('slide');" type="button">@lang(Lang::locale().'.ADD_QUESTIONS')</button>
          <button type="button" onclick="back('/question-sets/{{$lang}}')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
        </div>
      </div>
      <div class="row" style="display: none;" id="secondrow">
        <div class="col-md-12">

          <div class="card-sub-header px-0 mt-3 mb-3 bg-transparent clearfix">
            <strong class="float-left">@lang(Lang::locale().'.ADD_QUESTIONS')</strong>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="name">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL') :</label>
            <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL') " onkeyup="get_question_filtered_data($('#type').val(),$('#question_category').val(),'{{request()->route()->getAction()['prefix']}}/'+$('#language').val(),this.value)" id="serach" class="form-control">
            <input type="hidden" id="type" value="2">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="name">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.QUESTION_CATEGORY') :</label>
            <select class="form-control" name="setId" id="question_category" onchange="get_question_filtered_data($('#type').val(),this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}/'+$('#language').val(),$('#serach').val())">

              <option value="0">@lang(Lang::locale().'.ALL')</option>

              @foreach($category_data as $cat_data)
              <option value="{{ $cat_data->id }}">{{$cat_data->category_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-12 table-body">
          <table id="question_table" class="table">
            <thead>
              <th class="sorting " style="cursor: pointer; width:2%">#</th>
              <th class="sorting " style="cursor: pointer; width:2%">
                <input type="checkbox" id="selectall_question_set" onclick="question_set_select(true,this);">
              </th>
              <th class="sorting" style="width:10%">@lang(Lang::locale().'.QUESTION_CATEGORY_NAME')</th>
              <th class="sorting" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#language').val()+'/'+$('#type').val()+'/'+$('#question_category').val()+'/0','question')" style="cursor: pointer; width:40%">@lang(Lang::locale().'.QUESTION') <span class="icons question">
                  <i class="mr-1 fas fa-long-arrow-alt-down"></i>
                </span></th>
              <!--  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_category_id').val()+'/'+$('#language').val(),'status')" style="cursor: pointer; width:10%">@lang(Lang::locale().'.STATUS') <span class="icons phone"></span></th> -->
            </thead>
            <tbody id="tbody">
              @include("questions.search_question_data")
            </tbody>
          </table>
        </div>
        <div class="col-md-12">
          <input type="hidden" name="question_ids" id="question_ids" value="[]">
          <div class="form-group">
            <button disabled="disabled" id="quizsubmitbtn" class="btn btn-primary">@lang(Lang::locale().'.SAVE')</button>
            <button type="button" onclick="back('/question-sets/{{$lang}}')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
          </div>
        </div>
  </form>

  <input type="hidden" id="language" value="{{$lang}}">
  <input type="hidden" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$lang}}">
  <input type="hidden" id="hidden_column_name" value="question">
  <input type="hidden" id="hidden_sort_type" value="asc">
  <input type="hidden" id="hidden_page" value="1">

</div>
@endsection
