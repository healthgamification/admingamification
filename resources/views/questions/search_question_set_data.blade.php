@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $set)

<tr>
               <td>{{$ii}}</td>
               <td>{{$set->title}}</td>
               <td>{{$set->questions->count()}}</td>               
             <td>{{$set->status}}</td>
               
               <td>
                @can('read-quiz-set')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
              <a href="{{route('qset.view',[$set->id])}}" class="view-button" title="@lang(Lang::locale().'.VIEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')"><i class="fas fa-eye"></i></a>
    </span>
              @endcan
               @can('update-quiz-set')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="{{route('qset.edit',[$lang,$set->id])}}" class="edit-button" title="@lang(Lang::locale().'.EDIT') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')"><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-quiz-set')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('{{route('qset.delete',$set->id)}}','@lang(Lang::locale().'.QUESTION_SET_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.QUESTION_SET_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="{{route('qset.delete',$set->id)}}" class="delete-button"><i class="fas fa-trash-alt"></i></a>
               @endcan
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($data)!=0)
            @if($data->hasPages())
              <tr>
                <td colspan="6" id="gsetpagination">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($data)==0)
            <tr><td colspan="6">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-quiz-set')
        <!-- <a class="btn btn-primary" href="/questions/create" role="button"> -->
           <button  data-href="{{route('question.create','')}}" onclick="window.location.href='{{route("question.create",$lang)}}';"  class="btn btn-primary createbtn">
           @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')
        </button>
        
        @endcan
    </div>
</tr>
</td>
@endif
            