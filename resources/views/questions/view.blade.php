@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4>@lang(Lang::locale().'.QUESTION') @lang(Lang::locale().'.DETAILS')</h4>
  </div>


  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $questionData->question }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.QUESTION_CATEGORY')</strong></td>
        <td class="width-35 wordbreak">
          @forelse($questionData->categories as $qd)
          {{ $qd->category->category_name }}
          @empty @endforelse</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.LANGUAGE_SINGULAR')</strong></td>
        <td class="width-30 wordbreak">{{ $questionData->language->name }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_FACT')</strong></td>
        <td class="width-30 wordbreak">{{ $questionData->fact }}</td>
        
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30">{{ $questionData->status }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($questionData->created_at) }}</td>

      </tr>
    </tbody>

  </table>

  @if(count($questionOptionData)>0)
  @php $i=1 @endphp
  <div class="row">
    <div class="col-md-12 mt-3">

      <div class="card-sub-header px-0 mt-3 bg-transparent clearfix">
        <strong class="float-left">@lang(Lang::locale().'.QUESTION') @lang(Lang::locale().'.OPTIONS')</strong>
      </div>

    </div>
  </div>
  <table class="table table-hover mt15">
    @foreach($questionOptionData as $qod)
    @php $active =''; $correct=''; @endphp
    @if($i%2 == 1)
    <tr>
      @endif
      @if($qod->is_correct == 1)
      @php $active='bg-green'; $correct = '(Correct Option)';
      @endphp
      @endif
      <td class="width-20 {{ $active}}"><strong>
          @lang(Lang::locale().'.OPTION') {{ $i }} {{$correct}}
        </strong>
      </td>
      <td class="width-30 {{ $active}}">
        {{ $qod->option }}
      </td>
      @if($i%2==0 || count($questionOptionData)==$i)
    </tr>
    @endif
    @php $i++ @endphp
    @endforeach
  </table>
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-question')
      <a href="/question/{{$questionData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.QUESTION')</a>

      @endcan
    </div>
  </div>
</div>
</div>
</div>

@endsection