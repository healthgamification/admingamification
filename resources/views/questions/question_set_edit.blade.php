@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')</h4>
  </div>
  <form method="POST" action="{{route('setupdate')}}" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.QUESTION_SET_EDIT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.QUESTION_SET_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
    <div class="card-body px-0">
      @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
        {{ session()->get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      @if($errors->has('question_ids'))
      <div class="alert alert-danger alert-dismissible" role="alert">
        {{ $errors->first('question_ids') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif

      @if(count($errors))

      @endif
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="set_id" id="set_id" value="{{ $question_set->id }}">
      <div class="row" id="firstrow">
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('question_set_name') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
            <input type="text" id="name" name="question_set_name" class="form-control" value="{{ old('question_set_name',$question_set->title) }}" data-validation="required custom length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.QUESTION_SET_NAME_PLACEHOLDER')">
            <span class="text-danger">{{ $errors->first('question_set_name') }}</span>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('question_set_name') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
            <select class="form-control" name="lang" onchange="get_lang_filtered_set_data(this,'{{request()->route()->getAction()['prefix']}}','question_category')">
              @foreach(getLanguages() as $language)
              <option {{$language->code==$question_set->lang_code?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
            <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
            <select name="status" class="form-control" data-validation="required">
              <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
              <option value="Active" {{$question_set->status=='Active'?'selected':''}}>@lang(Lang::locale().'.ACTIVE')</option>
              <option value="Inactive" {{$question_set->status=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
            </select>
            <span class="text-danger">{{ $errors->first('status') }}</span>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group {{ $errors->has('question_set_desc') ? 'has-error' : '' }}">
            <label for="name">@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.DESCRIPTION'):</label>
            <textarea class="form-control" data-validation="length" data-validation-length="max150" name="question_set_desc">{{old('question_set_desc',$question_set->description)}}</textarea>
            <span class="text-danger">{{ $errors->first('question_set_desc') }}</span>
          </div>
        </div>
        <div class="col-md-12">
          <button class="btn btn-primary" onclick="$(this).closest('div').hide(); $('#secondrow').toggle('slide');" type="button">@lang(Lang::locale().'.ADD_QUESTIONS')</button>
          <button type="button" onclick="back('/question-sets/{{$lang}}')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
        </div>
      </div>
      <div class="row" style="display: none;" id="secondrow">
        <div class="col-md-12">

          <div class="card-sub-header px-0 mt-3 mb-2 bg-transparent clearfix">
            <strong class="float-left">@lang(Lang::locale().'.ADD_QUESTIONS')</strong>
          </div>

        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="name">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL') :</label>
            <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_PLURAL') " onkeyup="get_question_filtered_data($('#type').val(),$('#question_category').val(),'{{request()->route()->getAction()['prefix']}}/'+$('#language').val(),this.value)" id="serach" class="form-control">
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="name">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.TYPE') :</label>
            <select class="form-control" id="type" onchange="get_question_filtered_data(this.value,$('#question_category').val(),'{{request()->route()->getAction()['prefix']}}/{{$lang}}')">
              <option selected value="1">@lang(Lang::locale().'.SELECTED')</option>
              <option value="2">@lang(Lang::locale().'.UNSELECTED')</option>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label for="name">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.QUESTION_CATEGORY') :</label>
            <select class="form-control" name="setId" id="question_category" onchange="get_question_filtered_data($('#type').val(),this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}/{{$lang}}')">

              <option value="0">@lang(Lang::locale().'.ALL')</option>

              @foreach($category_data as $cat_data)
              <option value="{{ $cat_data->id }}">{{$cat_data->category_name}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class=" col-md-12 table-body">
          <table id='question_table' class="table">
            <thead>
              <th class="sorting " style="cursor: pointer; width:2%">#</th>
              <th class="sorting " style="cursor: pointer; width:2%">
                <input type="checkbox" id="selectall_question_set" onclick="question_set_select(true,this);">
              </th>
              <th class="sorting" style="width:10%">@lang(Lang::locale().'.QUESTION_CATEGORY_NAME') </th>
              <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$lang}}/'+$('#type').val()+'/'+$('#question_category').val()+'/'+$('#set_id').val(),'question')" style="cursor: pointer; width:40%">@lang(Lang::locale().'.QUESTION')<span class="icons question"></span></th>
              <!--  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#question_category').val()+'/'+$('#language').val(),'status')" style="cursor: pointer; width:10%">@lang(Lang::locale().'.STATUS') <span class="icons phone"></span></th> -->
            </thead>
            <tbody id="tbody">
              @include("questions.search_question_data")
            </tbody>
          </table>
        </div>
        <div class="col-md-12">
          <input type="hidden" name="question_ids" id="question_ids" value="{{$question_set->questions->pluck('question_id')}}">
          <div class="form-group">
            <button disabled="disabled" id="quizsubmitbtn" class="btn btn-primary">@lang(Lang::locale().'.UPDATE')</button>
            <button type="button" onclick="back('/question-sets/{{$lang}}')" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
          </div>
        </div>
  </form>

  <input type="hidden" id="language" value="{{$lang}}">
  <input type="hidden" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$lang}}">
  <input type="hidden" id="hidden_column_name" value="question">
  <input type="hidden" id="hidden_sort_type" value="asc">
  <input type="hidden" id="hidden_page" value="1">

</div>
@endsection
