@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $question)
<tr>
  <td>{{$ii}}</td>
  <td><input type="checkbox" onclick="question_set_select(false,this)" name="questionid[]" value="{{$question->id}}"></td>
  <td>
    @foreach($question->categories as $cat)
    <p>{{$cat->category->category_name}}</p>
    @endforeach
  </td>
  <td>{{ wordwrap($question->question,80) }}</td>
  <!--  <td>{{$question->status}}</td>   -->
</tr>
@php $ii++;@endphp
@endforeach
@if(count($data)!=0)
@if($data->hasPages())
<tr>
  <td colspan="6" id="questionsetpagination">

    {!! $data->links() !!}

  </td>
</tr>
@endif
@endif