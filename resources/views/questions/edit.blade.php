@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.QUESTION')</h4>
      </div>
      <div class="card-body px-0">
        <form method="POST" action="/question/update/{{$question['id']}}" autocomplete="off" data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.QUESTION_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">

            <div class="col-md-4 form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
              <label for="lang">@lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
              <select name="lang" class="form-control" name="lang" data-validation="required">
                @forelse(getLanguages() as $lang)
                <option {{$lang->code==$question['lang']?'selected':''}} value="{{$lang->code}}">{{$lang->name}}</option>
                @empty
                @endforelse
              </select><span class="text-danger">{{ $errors->first('lang') }}</span>
            </div>

            <div class="col-md-4 form-group {{ $errors->has('category') ? 'has-error' : '' }}">
              <label for="category">@lang(Lang::locale().'.QUESTION_CATEGORY_NAME'): <span class="text-danger">*</span></label>
              <select id="category" multiple class="form-control myselect" name="category[]" data-validation="required">
                @if(!empty($allcategorydata))
                @foreach($allcategorydata as $qcd)
                <option value="{{ $qcd->id }}" {{ in_array($qcd->id,$qcategorydata)?'selected':''}}>{{ $qcd->category_name }}</option>
                @endforeach
                @endif
              </select>
              <span class="text-danger">{{ $errors->first('category') }}</span>
            </div>


            <div class="col-md-4 form-group {{ $errors->has('status') ? 'has-error' : '' }}">
              <label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
              <select name="status" class="form-control" name="status" data-validation="required">
                <option value="Active" {{ $question['status']=='Active'?'selected':'' }}>@lang(Lang::locale().'.ACTIVE')</option>
                <option value="Inactive" {{ $question['status']=='Inactive'?'selected':'' }}>@lang(Lang::locale().'.INACTIVE')</option>
              </select>
              <span class="text-danger">{{ $errors->first('status') }}</span>
            </div>


            <div class="clearfix"></div>
          </div>

          <div class="row">

            <div class="col-md-4 form-group {{ $errors->has('question') ? 'has-error' : '' }}">
              <label for="question">@lang(Lang::locale().'.QUESTION'): <span class="text-danger">*</span></label>
              <textarea type="text" id="question" name="question" class="form-control" data-validation="required" rows="3" placeholder="@lang(Lang::locale().'.QUESTION_PLACEHOLDER')">{{ $question['question'] }}</textarea>
              <span class="text-danger">{{ $errors->first('question') }}</span>
            </div>

            <div class="col-md-4 form-group {{ $errors->has('fact') ? 'has-error' : '' }}">
              <label for="fact">@lang(Lang::locale().'.QUESTION_FACT'): <span class="text-danger">*</span></label>
              <textarea type="text" id="fact" name="fact" class="form-control" data-validation="required" data-validation="required length" data-validation-length="max150" placeholder="@lang(Lang::locale().'.QUESTION_FACT_PLACEHOLDER')" rows="3">{{ $question['fact'] }}</textarea>
              <span class="text-danger">{{ $errors->first('fact') }}</span>
            </div>

          </div>
          <div class="row">
            @if(1==0)
            <div class="col-md-4 form-group {{ $errors->has('weightage') ? 'has-error' : '' }}">
              <label for="weightage">@lang(Lang::locale().'.WEIGHTAGE'): </label>
              <input type="number" id="weightage" name="weightage" class="form-control" value="{{ $question['weightage'] }}" min="0" placeholder="@lang(Lang::locale().'.QUESTION_WEIGHTAGE')">
              <span class="text-danger">{{ $errors->first('weightage') }}</span>
            </div>
            @endif
            <div class="col-md-12 {{ $errors->has('challenge_options') ? 'has-error' : '' }}">
              <table class="table" id="dynamic_field">
                <tr>
                  <td style="width:75%;"><label for="name"> @lang(Lang::locale().'.OPTIONS'):</label></td>
                  <td style="width:15%;"><label for="name"> @lang(Lang::locale().'.SELECT_ANSWER'):</label></td>
                  <td style="width:10%;"></td>
                </tr>

                @if(count($options) > 0)
                <?php $i = 0; ?>
                @foreach($options as $opt)
                <tr id="row{{ $i }}">
                  <td><input type="text" name="challenge_options[{{ $i }}]" placeholder="@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')" class="form-control name_list" data-validation="required length" data-validation-length="max50" value="{{ $opt->option }}" /></td>
                  <td class="text-center"><input type="radio" name='challange_answer' value='{{ $i }}' id='challenge_answer{{ $i }}' {{ $opt['is_correct']=='1'?'checked':''}} /></td>
                  <td class="text-right">
                    @if($i > 0)
                    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
                      <button type="button" name="remove" id="{{ $i }}" class="btn btn-sm btn-danger btn_remove"><i class="fa fa-times"></i></button>
                    </span>
                    @endif
                  </td>
                </tr>
                <?php $i++; ?>
                @endforeach
                <tr>
                  <td colspan="3" class="text-right"><button type="button" name="add" onclick="addMoreOption('{{ $i-1 }}','@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')')" id="add" class="btn btn-sm btn-success">Add More</button></td>
                </tr>
                @else
                <tr>
                  <td style="width: 100%;"><input type="text" name="challenge_options[]" placeholder="@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')" class="form-control name_list" data-validation="required" /></td>
                  <td><input type="radio" name='challange_answer' value='0' id='chanllange_answer0' data-validation="required" checked /></td>
                  <td></td>
                </tr>

                <tr>
                  <td colspan="3" class="text-center"><button type="button" name="add" onclick="addMoreOption('0','@lang(Lang::locale().'.CHALLENGE_OPTION_PLACEHOLDER')')" id="add" class="btn btn-success">Add More</button></td>
                </tr>
                @endif

              </table>
              <span class="text-danger">{{ $errors->first('challenge_options') }}</span>
            </div>

            <div class="col-md-12">
              <button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button>
              <button type="button" onclick="window.location.href='/questions'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
      </div>
      </form>

    </div>
  </div>
</div>
@endsection
