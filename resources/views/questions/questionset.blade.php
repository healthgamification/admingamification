@extends('layouts.app')
@section('content')
<div class="container">
   <div class="card-header px-0 bg-transparent clearfix">
      <h4 class="float-left">@lang(Lang::locale().'.QUESTION_SET_PLURAL')</h4>
   </div>
   <div class="card-body px-0">
      @if(session()->has('message'))
      <div class="alert alert-success alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
      </div>
      @endif
      <div class="row">
         <div class="col-md-4">
            <div class="form-group mb-3">
               {{ csrf_field() }}
               <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')</label>
               <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#language').val())" id="serach" class="form-control">
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group mb-3">
               <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.LANGUAGE_SINGULAR')</label>
               <select class="form-control" name="lang" onchange="get_question_set_lang_filtered_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}/{{$perpage}}')">

                  <!-- <option value="0">Select Language</option> -->
                  @foreach(getLanguages() as $language)
                  <option {{$language->code==$lang?'selected':''}} value="{{$language->code}}">{{$language->name}}</option>
                  @endforeach
               </select>
            </div>

         </div>
         <!-- <div class="col-md-4 mt-4">
            @can('create-quiz-set')
            <div class="card-header-actions mr-1">-->
         <!-- <a href="/questions/create" class="btn btn-primary">@lang(Lang::locale().'.NEW') @lang(Lang::locale().'.QUESTION')</a> -->
         <!--<button data-href="{{route('question.create','')}}" onclick="window.location.href='{{route('question.create',$lang)}}';" class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')</button>
            </div>
            @endcan
         </div> -->
         <div class="clearfix"></div>

      </div>
   </div>
   <div class="table-body">
      <table class="table table-hover">
         <thead>
            <tr>
               <th class="sorting ">#</th>
               <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#language').val(),'title')" style="cursor: pointer; width:60%">@lang(Lang::locale().'.QUESTION_SET_SINGULAR') @lang(Lang::locale().'.NAME') <span class="icons title"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
               <th class="sorting " style="width:10%">@lang(Lang::locale().'.QUESTION_PLURAL')</th>
               <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#language').val(),'status')" style="cursor: pointer; width:10%">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>

               <th>@lang(Lang::locale().'.ACTION')</th>

            </tr>
         </thead>
         <tbody id="tbody">
            @include('questions/search_question_set_data')
         </tbody>
      </table>
   </div>
   <div id="loader" style="display:none;">
      <div class="vue-content-placeholders vue-content-placeholders-is-animated">
         <div class="vue-content-placeholders-text">
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
            <div class="vue-content-placeholders-text__line"></div>
         </div>
      </div>
   </div>
   <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
   <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="title" />
   <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
   <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$perpage}}" />
   <input type="hidden" name="language" id="language" value="{{$lang}}" />
</div>

@can('create-quiz-set')
<span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')" class="btn-plus">
   <a data-href="{{route('question.create','')}}" class="createbtn" onclick="window.location.href='{{route('question.create',$lang)}}';"><i class="fa fa-plus"></i></a>
</span>
@endcan

@endsection
