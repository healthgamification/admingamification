@extends('layouts.app')
@section('content')
<div class="container">
   @include('layouts.organizationHeader')
   @if(session()->has('message'))
   <div class="alert alert-success alert-dismissible mt-2" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
      </button>
   </div>
   @endif
   <div class="card-sub-header px-0 bg-transparent clearfix">
      <strong class="float-left pt-3">@lang(Lang::locale().'.CHALLENGE_SET_PLURAL')</strong>
   </div>
   <div class="row mt-2">
      <div class="col-md-4">
         <input type="text" name="q" class="form-control" id="serach" onkeyup="popupsearch_data(this)" data-column_name="title" data-sort_type="asc" data-page="1" data-tbody="tbody" data-loader="poploader" data-controllername="{{request()->route()->getAction()['prefix']}}/search/view/challenges/{{$ogData->id}}" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_SET_PLURAL')">
      </div>
      <div class="col-md-12 mt-2">
         <table class="table table-hover">
            <thead>
               <tr>
                  <th class="sorting width-5">#</th>
                  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/search/view/challenges/{{$ogData->id}}','title')" style="cursor: pointer;">@lang(Lang::locale().'.CHALLENGE_SET_SINGULAR') @lang(Lang::locale().'.NAME') <span class="icons title"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
                  <th class="sorting width-5">@lang(Lang::locale().'.CHALLENGE_PLURAL')</th>
                  <th class="sorting width-25" style="cursor: pointer;">
                     @lang(Lang::locale().'.ASSIGN') @lang(Lang::locale().'.WEEK') <span class="icons status"></span>
                  </th>

                  <!--  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}/{{$perpage}}/'+$('#language').val(),'status')" style="cursor: pointer;">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>  -->
                  <!--         
               
                <th>@lang(Lang::locale().'.ACTION')</th> -->



               </tr>
            </thead>
            <tbody id="tbody">
               @include('organization/challenge_set_data')
            </tbody>
         </table>
         <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
         <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="title" />
         <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
      </div>
   </div>
</div>
</div>


@endsection
