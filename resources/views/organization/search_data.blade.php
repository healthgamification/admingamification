@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $organization)

<tr>
               <td >{{$ii}}</td>
               <td>{{$organization->name}}</td>
               <td style="word-break: break-all;">{{$organization->email}}</td>
               <td>
                <a title="@lang(Lang::locale().'.ASSIGN') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')" href="{{route('assign.quiz',$organization->id)}}">@lang(Lang::locale().'.ASSIGN_BTN_TXT') </a>
                
               </td>    
               <td>{{custom_date_format($organization->created_at)}}</td>           
               <td>{{$organization->status}}</td>
               
               <td>
               <!-- <a onclick="confirmation('/organization/{{$organization->id}}/view','@lang(Lang::locale().'.ORGANIZATION_VIEW_CONFIRM_TITLE')',' @lang(Lang::locale().'.ORGANIZATION_VIEW_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') "  href="/organization/{{$organization->id}}/view" class="view-button" title="view"><i class="fas fa-eye"></i></a> -->
               @can('read-organization')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
               <a href="/organization/{{$organization->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
               @endcan
               @can('update-organization')
    @if ($userRole!='admin')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/organization/{{$organization->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endif
               @endcan
               @can('delete-organization')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/organization/{{$organization->id}}/delete','@lang(Lang::locale().'.ORGANIZATION_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.ORGANIZATION_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/organization/{{$organization->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
               @endcan
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if($data->hasPages())
              <tr>
                <td colspan="7" id="pagination">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
            @if(count($data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-organization')
        <a class="btn btn-primary createbtn" href="/organization/create" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')
        </a>
        @endcan
      </div>
    </td>
</tr>
@endif
            