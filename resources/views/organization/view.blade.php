@extends('layouts.app')

@section('content')

<div class="container">
  @include('layouts.organizationHeader')
  @if(session()->has('message'))
  <div class="alert alert-success alert-dismissible mt-5" role="alert">
    {{ session()->get('message') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @endif

  <table class="table table-hover my-2">
    <tbody>
      <tr>
        <td class="width-15"><strong>@lang(Lang::locale().'.EMAIL')</strong></td>
        <td class="width-50">{{$ogData['email']}}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.PHONE')</strong></td>
        <td class="width-20">{{$ogData['phone']}}</td>
      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.MOBILE')</strong></td>
        <td>{{$ogData['mobile']}}</td>
        <td><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td>{{$ogData['status']}}</td>
      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDRESS')</strong></td>
        <td>{{$ogData['address']}}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td>
          <strong>@lang(Lang::locale().'.TOTAL') @lang(Lang::locale().'.MEMBER_PLURAL')</strong>
        </td>
        <td>{{$ogData->members->count()}}</td>
        <td><strong>@lang(Lang::locale().'.TOTAL') @lang(Lang::locale().'.ADMIN') @lang(Lang::locale().'.USER_PLURAL')</strong></td>
        <td>{{$ogData->users->count()}}</td>
      </tr>
      <tr>
        <td>
          <strong>@lang(Lang::locale().'.TOTAL') @lang(Lang::locale().'.TEAM_PLURAL')</strong>
        </td>
        <td>{{$ogData->teams->count()}}</td>
        <td></td>
        <td></td>
      </tr>

    </tbody>

  </table>


  <!---->
  <!---->
</div>
</div>


@endsection
