@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($users as $user)

   <tr>
                <td class="d-none d-sm-table-cell">{{$ii}}</td>
                <td class="wordbreak">{{$user->member_name}}</td>
                <td class="wordbreak">{{$user->member_email}}</td>
                <!-- <td>User</td> -->
                <td>{{$user->department?$user->department->name:''}}</td>
               <td>{{$user->designation?$user->designation->name:''}}</td>
                <td class="d-none d-sm-table-cell">                
                @can('delete-member')
                @if($userRole != 'admin')
		<span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE') @lang(Lang::locale().'.MEMBER_SINGULAR')">
                <a   onclick="confirmationDelete('/organization/{{$orgid}}/view/user/{{$user->id}}/delete','@lang(Lang::locale().'.USER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/organization/{{$orgid}}/view/user/{{$user->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
                </span>
		@else
                 @if(Auth::user()->email!=$user->member_email)
                 <a title="@lang(Lang::locale().'.DELETE') @lang(Lang::locale().'.MEMBER_SINGULAR')"  onclick="confirmationDelete('/organization/{{$orgid}}/view/user/{{$user->id}}/delete','@lang(Lang::locale().'.USER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/organization/{{$orgid}}/view/user/{{$user->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
                 @endif
                @endif
                @endcan
                
                @if( $user->is_admin == 0)
                @can('create-organization-admin')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.USER_MAKE_ADMIN')">
                <a title="@lang(Lang::locale().'.USER_MAKE_ADMIN')" onclick="confirmation('/organization/{{$orgid}}/view/admin/{{$user->id}}/add','@lang(Lang::locale().'.USER_MAKE_ADMIN_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_MAKE_ADMIN_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') " href="/organization/{{$orgid}}/view/admin/{{$user->id}}/add" class="edit-button">
                <i class="fas fa-check"></i></a>
    </span>
                @endcan
                @else
                @can('delete-organization-admin')
                @if($userRole != 'admin')
		            <span data-toggle="tooltip" title="@lang(Lang::locale().'.USER_REMOVE_ADMIN')">
                <a title="@lang(Lang::locale().'.USER_REMOVE_ADMIN')"   onclick="confirmation('/organization/{{$orgid}}/view/admin/{{$user->id}}/remove','@lang(Lang::locale().'.USER_REMOVE_ADMIN_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_REMOVE_ADMIN_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') " href="/organization/{{$orgid}}/view/admin/{{$user->id}}/remove" class="delete-button"><i class="fas fa-times"></i></a>
		            </span>
                @else
                 @if(Auth::user()->email!=$user->member_email)
		            <span data-toggle="tooltip" title="@lang(Lang::locale().'.USER_REMOVE_ADMIN')">
                <a onclick="confirmation('/organization/{{$orgid}}/view/admin/{{$user->id}}/remove','@lang(Lang::locale().'.USER_REMOVE_ADMIN_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_REMOVE_ADMIN_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')') " href="/organization/{{$orgid}}/view/admin/{{$user->id}}/remove" class="delete-button"><i class="fas fa-times"></i></a>
                  </span>
		            @endif
                @endif
                @endcan
                @endif
                    </td>
                </tr>


                @php $ii++;@endphp
@endforeach
@if($users->hasPages())
<tr>
  <td colspan="7">
    <div id="orguserpagenationdiv">
      {!! $users->links() !!}
    </div>
  </td>
  </tr>
  @endif
    @if(count($users)==0)
            <tr>
              <td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
               
      </div>
    </td>
</tr>
@endif
            