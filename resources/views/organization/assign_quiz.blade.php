@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.ADD') @lang(Lang::locale().'.ASSIGN_BTN_TXT')</h4>
      </div>
      <div class="card-body px-0">
        @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif

        <form method="POST" action="{{route('assign.quiz.save',$orgid)}}" autocomplete="off" data-title="@lang(Lang::locale().'.QUIZ_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.QUIZ_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @csrf

          <div class="row info-box">
            <div class="col-md-3">
              <div class="org-title">
                @lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.NAME')
              </div>
            </div>
            <div class="col-md-9">
              <div class="org-name">
                {{$org->name}}
              </div>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="row">
            <div class="row col-md-8">
              <div class="col-md-6">
                <div class="card-sub-header px-0 mt-3 bg-transparent clearfix">
                  <strong class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.WEEK_PLURAL')</strong>
                </div>
              </div>
              <!-- <div class="col-md-6">
            <span><p class="past-week" style="width: 20px; height: 20px; float: left;"></p> Past Week</span>
            <p class="current-week" style="width: 20px; height: 20px; float: left;"></p>
            <p class="up-comming" style="width: 20px; height: 20px; float: left;"></p>
          </div>  --> 
          </div>
          @php $quiz_permission=false; $challenge_permission=false; @endphp
          @can('assign-quiz')
            @php $quiz_permission=true; @endphp
          @endcan
          @can('assign-challenge')
            @php $challenge_permission=true; @endphp
          @endcan
          <div class="col-md-12">                       
            <table class="table" id="weektable">
              <thead>
                <tr>
                  <th class="width-10">@lang(Lang::locale().'.WEEK_SINGULAR') No.</th>
                  <th class="width-20">@lang(Lang::locale().'.WEEK_SINGULAR') @lang(Lang::locale().'.START_DATE')</th>
                  <th class="width-20">@lang(Lang::locale().'.END_DATE') @lang(Lang::locale().'.WEEK_SINGULAR')</th>
                  <th class="width-20">@lang(Lang::locale().'.QUIZ_SINGULAR')</th>
                  <th class="width-20">@lang(Lang::locale().'.CHALLENGE_SINGULAR')</th>
                  <th class="width-10"><button type="button" class="btn btn-success" onclick="addnewweek(this)" data-quiz_permission="{{ $quiz_permission }}" data-challenge_permission="{{ $challenge_permission }}"><i class="fa fa-plus"></i>
                  @lang(Lang::locale().'.ADD_MORE') @lang(Lang::locale().'.WEEK_SINGULAR')</button> </th>
                </tr>
              </thead>  
              <tbody id="addweek">
                @php
                $ldate='';
                @endphp
                @forelse($weeks as $week)
                @php
                  $current_date= strtotime(date('Y-m-d'));
                  $sdate = strtotime($week->startdate);
                  $edate = strtotime($week->enddate);
                  $is_current_week = ($current_date>=$sdate && $current_date<=$edate);
                  $is_past_week = ($current_date>$edate);
                  if(empty($ldate))
                  {
                    $ldate = date('Y-m-d',$sdate);
                  }
                  @endphp
              <tr class="{{$is_past_week?'past-week':($is_current_week?'current-week':'up-comming')}}">
                <td>
                  <span class="wkno">@lang(Lang::locale().'.WEEK_SINGULAR') {{$week->week_no}} </span> 
                        <p class="info-text">
                  ({{$is_past_week?'Past':($is_current_week?'Current':'Upcomming')}})
                        </p>
                  @if(!$is_past_week)
                  <input type="hidden" name="weekno[]" value="{{$week->week_no}}">
                  @endif
                </td>
                <td>
                  @if($is_past_week)
                  <span class="form-control">{{$week->startdate}}</span>
                  @else
                  <input type="text" data-mindate="{{$ldate}}"  onchange="makeweek(this);" value="{{$week->startdate}}" name="weekstartdate[{{$week->week_no}}]" class="form-control sdate">
                  @endif
                </td>
                <td>
                   @if($is_past_week)
                  <span class="form-control">{{$week->enddate}}</span>
                  <input type="hidden" value="{{$week->enddate}}" class="edate" >
                  @else
                  <input type="text" readonly name="weekenddate[{{$week->week_no}}]" value="{{$week->enddate}}" class="form-control edate">
                  @endif
                  @php
                $ldate=date('Y-m-d',strtotime($week->enddate.'+ 1 day'));
                @endphp
                </td>
                <td class="text-center">
                   <span class="btn btn-primary" id="quiz_count_{{$week->week_no}}"> {{$week->quiz->count()}} @lang(Lang::locale().'.ASSIGNED')</span>
                  @if(!$is_past_week)
                  @can('assign-quiz')
                  <button type="button" data-weekno="{{$week->week_no}}" class="btn btn-primary" onclick="openquizsetmodal(this);"><i class="fa fa-plus"></i></button>
                  @endcan
                   <input type="hidden" name="quiz_id[{{$week->week_no}}]" id="quiz_id_{{$week->week_no}}" value="{{implode(',',$week->quiz->pluck('quiz_set_id')->all())}}">
                  @endif
                 
                   
                  </td>
                <td class="text-center">
                   <span class="btn btn-primary" id="challenge_count_{{$week->week_no}}"> {{$week->challenge->count()}} @lang(Lang::locale().'.ASSIGNED')</span>
                  @if(!$is_past_week)
                  @can('assign-challenge')
                  <button type="button" data-weekno="{{$week->week_no}}" class="btn btn-primary" onclick="openchallengesetmodal(this);"><i class="fa fa-plus"></i></button>
                  @endcan
                   <input type="hidden" name="challenge_id[{{$week->week_no}}]" id="challenge_id_{{$week->week_no}}" value="{{implode(',',$week->challenge->pluck('challenge_set_id')->all())}}">
                  @endif
                </td>
                <td></td>
              </tr>
              @empty
              <tr>
                <td class="wkno">
                  @lang(Lang::locale().'.WEEK_SINGULAR') 1
                  <input type="hidden" name="weekno[]" value="1">
                </td>
                <td><input type="text" data-mindate="{{date('Y-m-d')}}" onchange="makeweek(this);" name="weekstartdate[1]" class="form-control sdate" value="{{date('Y-m-d')}}"></td>
                <td>
                  @php
                $firstweeklastdate=date('Y-m-d',strtotime(date('Y-m-d').'+ 6 day'));
                @endphp
                  <input type="text" readonly name="weekenddate[1]" class="form-control edate" value="{{$firstweeklastdate}}"></td>
                <td class="text-center">
                    <span class="btn btn-primary" id="quiz_count_1" > 0 @lang(Lang::locale().'.ASSIGNED')</span>
                     @can('assign-quiz')
                  <button type="button" data-weekno="1" class="btn btn-primary" onclick="openquizsetmodal(this);"><i class="fa fa-plus"></i></button>
                  @endcan
                    <input type="hidden" name="quiz_id[1]" id="quiz_id_1" >
                  </td>
                <td class="text-center">
                    <span class="btn btn-primary" id="challenge_count_1" > 0 @lang(Lang::locale().'.ASSIGNED')</span>
                     @can('assign-challenge')
                   <button type="button" data-weekno="1" class="btn btn-primary" onclick="openchallengesetmodal(this);"><i class="fa fa-plus"></i></button>
                   @endcan
                    <input type="hidden" name="challenge_id[1]" id="challenge_id_1" >
                </td>
                <td></td>
              </tr>
              @endforelse

            </tbody>
            </table>
            <div class="form-group text-right">
              <button id="mainsubmitbtn" class="btn btn-primary">@lang(Lang::locale().'.SAVE')</button>
              <button type="button" onclick="window.location.href='/organization'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
          </div>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
@include('organization/add_quiz_popup')
@include('organization/add_challenge_popup')
@endsection
