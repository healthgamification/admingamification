@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($adminUsers as $user)

   <tr>
                <td class="d-none d-sm-table-cell">{{$ii}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
              
                <td class="d-none d-sm-table-cell">                
                @can('delete-organization-admin')
                @if(Auth::user()->id!=$user->id)
                @if(count($adminUsers) > 1)
                <span data-toggle="tooltip" title="@lang(Lang::locale().'.USER_REMOVE_ADMIN')">
                  <a onclick="confirmationDelete('/organization/{{$orgid}}/view/admin/{{$user->id}}/delete','@lang(Lang::locale().'.USER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.USER_REMOVE_ADMIN_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/organization/{{$orgid}}/view/user/{{$user->id}}/delete" class="delete-button"><i class="fas fa-user-times"></i></a>
                </span>
                @endif
               @endif
                @endcan               
                    </td>
                </tr>

                @php $ii++;@endphp
@endforeach
@if($adminUsers->hasPages())
<tr>
  <td colspan="7">
    <div id="orgadminpagenationdiv">
      {!! $adminUsers->links() !!}
    </div>
  </td>
  </tr>
  @endif
  @if(count($adminUsers)==0)
            <tr>
              <td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
               
      </div>
    </td>
</tr>
@endif
            