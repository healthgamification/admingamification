@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
   $ii=1;
 }else{
 $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($quizdata as $quiz)

<tr>
 <td >{{$ii}}</td>
 <td ><input type="checkbox" name="quiz_id" onclick="quiz_set_select(false,this);" value="{{$quiz->id}}"></td>
 <td>{{$quiz->title}}</td>
 <td>
  @foreach($quiz->selected($org->id) as $sel)
  Week {{$sel->week->week_no}}
  @endforeach
</td>
</tr>
@php $ii++;@endphp
@endforeach
@if(count($quizdata)>0)
@if($quizdata->hasPages())
<tr>
  <td colspan="7" id="quizsetpopuppagination">

    {!! $quizdata->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($quizdata)==0)
<tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-quiz-set')
        <a class="btn btn-primary" href="/question-set/create/{{$org->lang_code}}" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')
        </a>
        @endcan
      </div>
    </td>
</tr>
@endif
