<div class="modal fade" id="quiz_assign_model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">@lang(Lang::locale().'.ASSIGN') @lang(Lang::locale().'.QUESTION_SET_PLURAL')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
          <div class="col-md-12">
            <table class="table">
              <tr>
                <th class="width-5">Week No.</th>
                <td class="width-5 weeknomodal"></td>
                <th class="width-10">Week Start Date</th>
                <td class="width-5 weeksdmodal"></td>
                <th class="width-10">Week End Date</th>
                <td class="width-5 weekedmodal"></td>
              </tr>
              
            </table>
          </div>
      <div class="col-md-6">
        <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')</label>
        <div class="form-group">
        <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.QUESTION_SET_PLURAL')" id="popserach" onkeyup="popupsearch_data(this)" 
        data-column_name="title" 
        data-sort_type="asc" 
        data-page="1"
        data-tbody="tbody"
        data-loader="poploader"
        data-controllername="{{request()->route()->getAction()['prefix']}}/{{$org->lang_code}}/{{$org->id}}" class="form-control">
              
             
    </div>
    </div>
    <div class="col-md-6 mt-4 text-right">
    <a class="btn btn-primary" href="/question-set/create/{{$org->lang_code}}" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_SET_SINGULAR')
        </a>
    </div>
  </div> 
      <table class="table" id="quiz_set_modal_table">
  <thead>
    <tr>
      <th style="width: 10%">#</th>
      <th style="width: 10%"> <input type="checkbox" onclick="quiz_set_select(true,this);" id="selectall" > </th>
      <th>Name</th>
      <th>Selected Week</th>
    </tr>
  </thead>
  <tbody id="tbody">
         @include('organization/search_quiz_set_data')
        
        </tbody>
</table>
<div id="poploader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated"><div class="vue-content-placeholders-text"><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div></div></div>
            </div>
             <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="title" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
    <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$org->lang_code}}/{{$org->id}}" />
<div id="popuppage">
</div>
  <div class="col-md-12 text-right">
    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
      @lang(Lang::locale().'.BTN_BACK')
          </button>
    <button class="btn btn-primary" type="button" id="quizsubmitbtn" onclick="" >@lang(Lang::locale().'.SAVE')</button>
  </div>
      </div>
    </div>
  </div>
</div>