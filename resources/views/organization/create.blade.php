@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row ">
      	<div class="col-md-12">
        	<div class="card-header px-0 bg-transparent clearfix">
          		<h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</h4>
        	</div>
        	<div class="card-body px-0">
        		<form method="POST" action="/organization/store" autocomplete="off" data-title="@lang(Lang::locale().'.ORGANIZATION_SAVE_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.ORGANIZATION_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
					@if(count($errors))
					@endif
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-md-4 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label for="name">@lang(Lang::locale().'.NAME'):<span class="text-danger">*</span></label>
							<input type="text" id="name" name="name" class="form-control" placeholder="@lang(Lang::locale().'.ORGANIZATION_NAME_PLACEHOLDER')" value="{{ old('name') }}"  data-validation="required custom length"  data-validation-length="max100">
							<span class="text-danger">{{ $errors->first('name') }}</span>
						</div>

						<div class="col-md-4 form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<label for="email">@lang(Lang::locale().'.EMAIL'): <span class="text-danger">*</span></label>
							<input type="text" id="email" name="email" class="form-control"  value="{{ old('email') }}" data-validation="required email length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.ORGANIZATION_EMAIL_PLACEHOLDER')" >
							<span class="text-danger">{{ $errors->first('email') }}</span>
						</div>

						<div class="col-md-4 form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
							<label for="phone">@lang(Lang::locale().'.PHONE'):</label>
							<div class="row">
								<div class="col-sm-4">
									<select name="countryCode" class="form-control">
										<option value="+45">+45</option>
									</select>
								</div>
								<div class="col-sm-8">
									<input type="text" id="phone" name="phone" data-validation="custom " data-validation-optional="true"  data-validation-regexp="^((\(?\+45\)?)?)(\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$"   class="form-control" data-validation-error-msg="@lang(Lang::locale().'.ORGANIZATION_PHONE_ERROR_MSG')"  placeholder="@lang(Lang::locale().'.ORGANIZATION_PHONE_PLACEHOLDER')" value="{{ old('phone') }}">
									<span class="text-danger">{{ $errors->first('phone') }}</span>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
							<label for="mobile">@lang(Lang::locale().'.MOBILE'):</label>
							<input type="text" id="mobile" name="mobile" data-validation="custom" data-validation-optional="true"  data-validation-regexp="^((\(?\+45\)?)?)(\s?\d{2}\s?\d{2}\s?\d{2}\s?\d{2})$"  class="form-control" data-validation-error-msg="@lang(Lang::locale().'.ORGANIZATION_MOBILE_ERROR_MSG')" placeholder="@lang(Lang::locale().'.ORGANIZATION_MOBILE_PLACEHOLDER')" value="{{ old('mobile') }}">
							<span class="text-danger">{{ $errors->first('mobile') }}</span>
						</div>
						
						<div class="col-md-4 form-group {{ $errors->has('address') ? 'has-error' : '' }}">
							<label for="address">@lang(Lang::locale().'.ADDRESS'): <span class="text-danger">*</span></label>
							<textarea type="text" id="address" name="address" class="form-control" data-validation="required length" rows="6" data-validation-length="max200" placeholder="@lang(Lang::locale().'.ORGANIZATION_ADDRESS_PLACEHOLDER')"  >{{ old('address') }}</textarea>
							<span class="text-danger">{{ $errors->first('address') }}</span>
						</div>

						<div class="col-md-4 form-group {{ $errors->has('about') ? 'has-error' : '' }}">
							<label for="about">@lang(Lang::locale().'.ABOUT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR'):</label>
							<textarea type="text" id="about" placeholder="@lang(Lang::locale().'.ORGANIZATION_ABOUT_PLACEHOLDER')" name="about" rows="6" class="form-control" >{{ old('about') }}</textarea>
							<span class="text-danger">{{ $errors->first('about') }}</span>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group {{ $errors->has('no_of_quizzes') ? 'has-error' : '' }}">
							<label for="no_of_quizzes">@lang(Lang::locale().'.ORGANIZATION_NO_OF_QIZZ'):</label>
							<input type="text" id="no_of_quizzes" name="no_of_quizzes" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.ORGANIZATION_NO_OF_QIZZ_PLACEHOLDER')" value="{{ old('no_of_quizzes') }}">
							<span class="text-danger">{{ $errors->first('no_of_quizzes') }}</span>
						</div>
		
						<div class="col-md-4 form-group {{ $errors->has('no_of_challenge') ? 'has-error' : '' }}">
							<label for="no_of_challenge">@lang(Lang::locale().'.ORGANIZATION_NO_OF_CHALLENGE'):</label>
							<input type="text" id="no_of_challenge" name="no_of_challenge" class="form-control" data-validation="number" data-validation-optional="true" data-validation-allowing="range[0;9]" placeholder="@lang(Lang::locale().'.ORGANIZATION_NO_OF_CHALLENGE_PLACEHOLDER')" value="{{ old('no_of_challenge') }}">
							<span class="text-danger">{{ $errors->first('no_of_challenge') }}</span>
						</div>
					
						<div class="col-md-4 form-group {{ $errors->has('no_of_license') ? 'has-error' : '' }}">
							<label for="no_of_license">@lang(Lang::locale().'.ORGANIZATION_TOTAL_LICENSE'):</label>
							<input type="text" id="no_of_license" name="no_of_license" class="form-control" data-validation="number length" data-validation-length="max4" data-validation-optional="true" placeholder="@lang(Lang::locale().'.ORGANIZATION_TOTAL_LICENSE_PLACEHOLDER')" value="{{ old('no_of_license') }}">
							<span class="text-danger">{{ $errors->first('no_of_license') }}</span>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group {{ $errors->has('lang') ? 'has-error' : '' }}">
							<label for="lang">@lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
							<select name="lang" class="form-control" name="lang" data-validation="required" >
								@foreach(getLanguages() as $language)
								<option {{$language->code=='en'?'selected':''}} value="{{$language->code}}" >{{$language->name}}</option>
								@endforeach
							</select>
							<span class="text-danger">{{ $errors->first('status') }}</span>
						</div>

						<div class="col-md-4 form-group {{ $errors->has('address') ? 'has-error' : '' }}">
							<label for="status">@lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
							<select name="status" class="form-control" name="status" data-validation="required" >
								<option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
								<option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
								<option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
							</select>
							<span class="text-danger">{{ $errors->first('status') }}</span>
						</div>
					
						<div class="clearfix"></div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button>
							<button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
						</div>
						<div class="clearfix"></div>
					</div>

         		</form>
        	</div>
		</div>
    </div>
</div>
@endsection
