@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $set)

<tr>
               <td>{{$ii}}</td>
               <td>{{$set->set->title}}</td>
               <td>{{$set->set->challenges->count()}}</td>               
             <td>
              @foreach($set->set->selected($set->org_id) as $week)
              @lang(Lang::locale().'.WEEK') {{$week->week->week_no}}
    <span class="info-text">
      ({{custom_date_format($week->week->startdate)}} to {{custom_date_format($week->week->enddate)}})
    </span>
              @endforeach
             </td>          
            <!--  <td>{{$set->set->status}}</td>       -->    
              
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($data)!=0)
            @if($data->hasPages())
              <tr>
                <td colspan="5" id="gsetpagination">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($data)==0)
            <tr>
              <td colspan="5">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>      
      </div>
    </td>
</tr>
@endif
            