@extends('layouts.app')

@section('content')

<div class="container">

@include('layouts.organizationHeader')
  
   @if(session()->has('message'))
   <div class="alert alert-success alert-dismissible mt-2" role="alert">
               {{ session()->get('message') }}
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
@endif
<div class="card-header px-0 mt-2 bg-transparent clearfix">
      <strong class="float-left pt-2">@lang(Lang::locale().'.TEAM_PLURAL')</strong>
   </div>
   <div class="row mt-2">
  <div class="col-md-4">
<input type="text" name="q" class="form-control" id="orgteamsearch" onkeyup="popupsearch_data(this)" 
        data-column_name="name" 
        data-sort_type="asc" 
        data-page="1"
        data-tbody="orgteambody"
        data-loader="poploader"
        data-controllername="{{request()->route()->getAction()['prefix']}}/view/team/{{$ogData->id}}/{{$perpage}}"
         placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.TEAM_PLURAL')">
</div>
<div class="col-md-12 mt-2">
      <table class="table table-hover">
         <thead>           
            <tr>
            <th class="sorting d-none d-sm-table-cell width-5">#<span class="icons id"></span></th>
              
                <th class="sorting d-none d-sm-table-cell width-30">@lang(Lang::locale().'.TEAM_SINGULAR') @lang(Lang::locale().'.TITLE')</th>
                
                <!-- <th class="sorting d-none d-sm-table-cell" onclick="sorting('{{request()->route()->getAction()['prefix']}}','organization_name')" style="cursor: pointer">@lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.NAME')<span class="icons organization_name"></span></th> -->

                <th class="sorting d-none d-sm-table-cell width-10" >@lang(Lang::locale().'.PLAYERS')</th>
                <th class="sorting d-none d-sm-table-cell width-10" >@lang(Lang::locale().'.STATUS')</th>
               
               <!--  <th>@lang(Lang::locale().'.ACTION')</th> -->
              
            </tr>
         </thead>
         <tbody id="orgteambody">
       @include('organization/search_data_team')
             </tbody>
           </table>            
 </div>
</div>
   </div>
</div>


@endsection

