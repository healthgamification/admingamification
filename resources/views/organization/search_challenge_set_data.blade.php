@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
   $ii=1;
 }else{
 $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($challengedata as $challenge)

<tr>
 <td >{{$ii}}</td>
 <td ><input type="checkbox" name="challenge_id" onclick="challenge_select_all(false,this);" value="{{$challenge->id}}"></td>
 <td>{{$challenge->title}}</td>
 <td>
  @foreach($challenge->selected($org->id) as $sel)
  Week {{$sel->week->week_no}}
  @endforeach
</td>
</tr>
@php $ii++;@endphp
@endforeach
@if(count($challengedata)>0)
@if($challengedata->hasPages())
<tr>
  <td colspan="7" id="challengesetpopuppagination">

    {!! $challengedata->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($challengedata)==0)
<tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-challenge-set')
        <a class="btn btn-primary" href="/challenge-set/create/{{$org->lang_code}}" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')
        </a>
        @endcan
      </div>
    </td>
</tr>
@endif
