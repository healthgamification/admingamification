@extends('layouts.app')

@section('content')

<div class="container">

@include('layouts.organizationHeader')
  
   @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible mt-2" role="alert">
               {{ session()->get('message') }}
               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
@endif
   <div class="card-sub-header px-0 bg-transparent clearfix">
      <strong class="float-left pt-3">@lang(Lang::locale().'.ADMINS')</strong>
   </div>
   <div class="row mt-2">
  <div class="col-md-4">
<input type="text" name="q" class="form-control" id="orgadminsearch" onkeyup="popupsearch_data(this)" 
        data-column_name="name" 
        data-sort_type="asc" 
        data-page="1"
        data-tbody="orgadminbody"
        data-loader="poploader"
        data-controllername="{{request()->route()->getAction()['prefix']}}/1/{{$ogData->id}}/{{$perpage}}"
         placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.ADMINS')">
</div>
<div class="col-md-12 mt-2">
      <table class="table table-hover">
         <thead>
            <tr>
                <th class="d-none d-sm-table-cell width-5">#</th>
                <th class="width-20">@lang(Lang::locale().'.NAME')</th>
               
                <th class="width-65">@lang(Lang::locale().'.EMAIL')</th>
              
                <th class="width-15">@lang(Lang::locale().'.ACTION')</th>
              
            </tr>
         </thead>
         <tbody id="orgadminbody">
       @include('organization/search_data_admin')
             </tbody>
           </table>            
 </div>
</div>

   <div class="card-sub-header px-0 mt-3 bg-transparent clearfix">
      <strong class="float-left">@lang(Lang::locale().'.MEMBER_PLURAL')</strong>
   </div>
   <div class="row mt-2">
  <div class="col-md-4">
<input type="text" name="q" class="form-control" id="orgusersearch" onkeyup="popupsearch_data(this)" 
        data-column_name="member_name" 
        data-sort_type="asc" 
        data-page="1"
        data-tbody="orguserbody"
        data-loader="poploader"
        data-controllername="{{request()->route()->getAction()['prefix']}}/0/{{$ogData->id}}/{{$perpage}}"
        placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.MEMBER_PLURAL')">
</div>
<div class="col-md-12 mt-2">
        <table class="table table-hover">
         <thead>
            <tr>
                <th class="d-none d-sm-table-cell width-5">#</th>
                 <th class="width-15">@lang(Lang::locale().'.NAME')</th>
               
                <th class="width-25">@lang(Lang::locale().'.EMAIL')</th>
                <!-- <th class="width-10">@lang(Lang::locale().'.ROLE')</th> -->
                <th class="width-15">@lang(Lang::locale().'.DEPATRMENT')</th>
                <th class="width-15">@lang(Lang::locale().'.DESIGNATION')</th>
                <th class="width-15">@lang(Lang::locale().'.ACTION')</th>
              
            </tr>
         </thead>
         <tbody id="orguserbody">
          @include('organization/search_data_user')
         </tbody>
      </table>
      
  </div>
  </div>
     

      <!----> <!---->
   </div>
</div>


@endsection

