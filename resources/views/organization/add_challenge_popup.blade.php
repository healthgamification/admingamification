<div class="modal fade" id="challenge_assign_model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">@lang(Lang::locale().'.ASSIGN') @lang(Lang::locale().'.CHALLENGE_SET_PLURAL')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
          <div class="col-md-12">
            <table class="table">
              <tr>
                <th class="width-5">Week No.</th>
                <td class="width-5 weeknomodal"></td>
                <th class="width-10">Week Start Date</th>
                <td class="width-5 weeksdmodal"></td>
                <th class="width-10">Week End Date</th>
                <td class="width-5 weekedmodal"></td>
              </tr>
              </tr>
            </table>
          </div>
  <div class="col-md-6">
   
    <div class="form-group">
       <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_SET_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_SET_PLURAL')</label>
        <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_SET_PLURAL')" id="cpopserach" onkeyup="popupsearch_data(this)" 
        data-column_name="title" 
        data-sort_type="asc" 
        data-page="1"
        data-tbody="challengetbody"
        data-loader="poploader"
        data-controllername="{{request()->route()->getAction()['prefix']}}/challenge/{{$org->lang_code}}/{{$org->id}}"  class="form-control">
              
             
            </div>
  </div>
  <div class="col-md-6 mt-4 text-right">
    <a class="btn btn-primary" href="/challenge-set/create/{{$org->lang_code}}" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_SET_SINGULAR')
        </a>
  </div>
  </div> 
      <table class="table" id="challenge_set_modal_table">
  <thead>
    <tr>
      <th style="width: 10%">#</th>
      <th style="width: 10%"> <input type="checkbox" onclick="challenge_select_all(true,this);" id="selectall_challenge" > </th>
      <th>Name</th>
      <th>Selected Week</th>
    </tr>
  </thead>
  <tbody id="challengetbody">
         @include('organization/search_challenge_set_data')
        
        </tbody>
</table>
<div id="cpoploader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated"><div class="vue-content-placeholders-text"><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div></div></div>
            </div>
             <input type="hidden" name="hidden_page" id="chidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="chidden_column_name" value="title" />
    <input type="hidden" name="hidden_sort_type" id="chidden_sort_type" value="asc" />
    <input type="hidden" name="cname" id="ccname" value="{{request()->route()->getAction()['prefix']}}/challenge/{{$org->lang_code}}/{{$org->id}}" />
<div id="cpopuppage">
</div>
  <div class="col-md-12 text-right">
    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
      @lang(Lang::locale().'.BTN_BACK')
          </button>
    <button class="btn btn-primary" type="button" id="challengesubmitbtn" disabled="disabled" onclick="" >@lang(Lang::locale().'.SAVE')</button>
  </div>
      </div>
    </div>
  </div>
</div>