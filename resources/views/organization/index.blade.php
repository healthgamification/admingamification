 @extends('layouts.app')
 @section('content')
 <div class="container">
    <div class="card-header px-0 bg-transparent clearfix">
       <h4 class="float-left">@lang(Lang::locale().'.ORGANIZATION_PLURAL')</h4>
    </div>
    <div class="card-body px-0">
       @if(session()->has('message'))
       <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">&times;</span>
          </button>
       </div>
       @endif
       <div class="row">
          <div class="col-md-4">
             {{ csrf_field() }}
             <label for="@lang(Lang::locale().'.SEARCH')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.ORGANIZATION_PLURAL')</label>
             <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.ORGANIZATION_PLURAL')" id="serach" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}')" class="form-control">
          </div>
          <div class="clearfix"></div>
       </div>
    </div>
    <div class="table-body">
       <table class="table table-hover">
          <thead>
             <tr>
                <th class="sorting width-5"># <span class="icons id"></span></th>
                <th class="sorting width-20" onclick="sorting('{{request()->route()->getAction()['prefix']}}','name')" style="cursor: pointer;">@lang(Lang::locale().'.NAME') <span class="icons name"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
                <th class="sorting width-20" onclick="sorting('{{request()->route()->getAction()['prefix']}}','email')" style="cursor: pointer;">@lang(Lang::locale().'.EMAIL') <span class="icons email"></span></th>
                <th class="sorting width-20" style="cursor: pointer;">@lang(Lang::locale().'.MANAGE') <span class="icons phone"></span></th>
                <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}','created_at')" style="cursor: pointer;">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
                <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}','status')" style="cursor: pointer;">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>
                <th class="width-15">Action</th>
             </tr>
          </thead>
          <tbody id="tbody">
             @include('organization/search_data')
          </tbody>
       </table>
    </div>
    <div id="loader" style="display:none;">
       <div class="vue-content-placeholders vue-content-placeholders-is-animated">
          <div class="vue-content-placeholders-text">
             <div class="vue-content-placeholders-text__line"></div>
             <div class="vue-content-placeholders-text__line"></div>
             <div class="vue-content-placeholders-text__line"></div>
             <div class="vue-content-placeholders-text__line"></div>
          </div>
       </div>
    </div>
    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="name" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
    <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}" />
    @can('create-organization')
    <!-- <a href="/organization/create">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</a> -->
    <span class="btn-plus" data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')">
       <a href="/organization/create" class="createbtn"><i class="fa fa-plus"></i></a>
    </span>
    @endcan
    <!---->
    <!---->
 </div>
 </div>
 @endsection