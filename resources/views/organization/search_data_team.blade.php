@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($team_data as $team)

            <tr>
               <td class="d-none d-sm-table-cell">{{$ii}}</td>
               <td>{{$team->name}}</td>
              <!--  <td>{{$team->organization_name}}</td> -->
               <td>{{$team->player->count()}}</td>
               <td>{{$team->status}}</td>
               
              <!-- <td class="d-none d-sm-table-cell">
             <a href="/menber/{{$team->id}}/view" class="view-button" title="View"><i class="fas fa-eye"></i></a>
               @can('update-member')
              
               <a  href="/team/{{$team->id}}/edit" class="edit-button" title="Edit"><i class="fas fa-pencil-alt"></i></a>
            
               @endcan
               @can('delete-member')
               <a title="delete" onclick="confirmationDelete('/team/{{$team->id}}/delete','@lang(Lang::locale().'.TEAM_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.TEAM_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/team/{{$team->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>

               @endcan
                  </td>-->
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($team_data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-team')
        <a class="btn btn-primary" href="/team/create/{{$ogData->id}}" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.TEAM_SINGULAR')
        </a>
        @endcan
      </div>
</tr></td>
@endif

@if($team_data->hasPages())
<tr>
  <td colspan="7">
    <div id="orgteampagenationdiv">
      {!! $team_data->links() !!}
    </div>
  </td>
  </tr>
  @endif
            