@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 mt-2 bg-transparent clearfix">
        <h4 class="float-left pt-2">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.TEMPLATE_SINGULAR')</h4>

      </div>
      <div class="card-body px-0">
       @if(session()->has('message'))
       <div class="alert alert-success alert-dismissible" role="alert">
        {{ session()->get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      <form method="POST" action="/template/update/{{$templete->id}}" id="from1" autocomplete="off"  data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.TEMPLATE_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
              <label for="organization">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.TEMPLATE_TYPE'): <span class="text-danger">*</span></label>
              <select class="form-control" name="type" onchange="getDynamicParams(this.value);" data-validation="required">
                <option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.TEMPLATE_TYPE')</option>
                @foreach(getTemplateType() as $key=>$tamp)
                {
                  <option value="{{$key}}" {{old('type',$templete->type)==$key?'selected':''}}>{{$tamp}}</option>
                }
                @endforeach
              </select>          
              <span class="text-danger">{{ $errors->first('type') }}</span>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
             <label for="name">@lang(Lang::locale().'.TEMPLATE_SUBJECT'): <span class="text-danger">*</span></label>
             <input type="text" id="subject" name="subject" class="form-control"  value="{{ old('subject',$templete->subject) }}" data-validation="required custom length"  data-validation-length="max60" placeholder="@lang(Lang::locale().'.TEMPLATE_NAME_PLACEHOLDER')">
             <span class="text-danger">{{ $errors->first('subject') }}</span>
           </div>
         </div>  

         <div class="col-md-6 form-group {{ $errors->has('lang_code') ? 'has-error' : '' }}">
          <label for="lang">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.LANGUAGE'): <span class="text-danger">*</span></label>
          <select class="form-control" name="lang_code" data-validation="required" >
            @foreach(getLanguages() as $language)
            <option {{old('lang_code',$templete->lang_code)==$language->code?'selected':''}} value="{{$language->code}}" >{{$language->name}}</option>
            @endforeach
          </select>
          <span class="text-danger">{{ $errors->first('lang_code') }}</span>
        </div>        

        <div class="col-md-6">
          <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
           <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
           <select name="status" class="form-control" name="status" data-validation="required">
            <option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
            <option value="Active" {{old('status',$templete->status)=='Active'?'selected':''}} >@lang(Lang::locale().'.ACTIVE')</option>
            <option value="Inactive"{{old('status',$templete->status)=='Inactive'?'selected':''}} >@lang(Lang::locale().'.INACTIVE')</option>
          </select>         
          <span class="text-danger">{{ $errors->first('status') }}</span>
        </div>
      </div>   
      <div class="col-md-12">
         <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
           <label for="status">@lang(Lang::locale().'.TEMPLATE_BODY'): <span class="text-danger">*</span></label>
           <p>@lang(Lang::locale().'.TEMPLATE_BODY_MSG')</p>
           <p id="variable">{{$variables}}</p>
        <textarea name="body" rows="10" id="editor" class="form-control" data-validation="required">{{old('body',$templete->body)}}</textarea>
        <span class="text-danger">{{ $errors->first('body') }}</span>
      </div>
      </div>      

      <div class="col-md-12">
        <div class="form-group text-right">

           <button  class="btn btn-primary">@lang(Lang::locale().'.UPDATE')</button>
          <button type="button" onclick="window.location.href='/templates'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
        </div>
      </div>
    </div>
  </form>

</div>
</div>
</div>
</div>
@endsection
