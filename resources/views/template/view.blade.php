@extends('layouts.app')

@section('content')

<div class="container"> 
    <div class="card-header px-0 mt-2 bg-transparent clearfix">
      <div class="col-md-12"><h2>{{ getTemplateType($template->type) }}</h2></div>
    </div>
    <div class="col-md-12 mt-2">
      <label>@lang(Lang::locale().'.TEMPLATE_SUBJECT'):</label>
      <span>{{$template->subject}}</span>
    </div> 
    <div class="col-md-12 mt-2">
      <label>@lang(Lang::locale().'.TEMPLATE_BODY'):</label>
      <p>{!! $template->body !!}</p>
    </div> 

       <div class="saperator-line"></div>
    <div class='row mt-2'>
      <div class="col-md-12 text-right">
        @can('update-mail-template')
               <a  href="/template/{{$template->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.TEMPLATE_SINGULAR')</a>
            
        @endcan</div>
    </div>
  </div>
</div>


@endsection

