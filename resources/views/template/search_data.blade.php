@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($templates as $template)


            <tr>
               <td>{{$ii}}</td>
               <td class="wordbreak">{{$template->subject}}</td>
               <td class="wordbreak">{{getTemplateType($template->type)}}</td>
               <td>{{custom_date_format($template->created_at)}}</td>
               <td>{{$template->status}}</td>
               
               <td>
                @can('read-mail-template')
                <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
               <a href="/template/{{$template->id}}/view" class="view-button" ><i class="fas fa-eye"></i></a>
             </span>
             
              @endcan
               @can('update-mail-template')
               <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/template/{{$template->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
            
               @endcan
               @can('delete-mail-template')
               <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/template/{{$template->id}}/delete','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.TEMPLATE_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/template/{{$template->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
             </span>

               @endcan
               </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
             @if(count($templates)!=0)
            @if($templates->hasPages())
              <tr>
                <td colspan="9" id="mainpagenation">
                  
                  {!! $templates->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($templates)==0)
            <tr><td colspan="9">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-mail-template')
         <a href="/template/create" class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.TEMPLATE_SINGULAR')
                             </a> 
        </a>
        @endcan
      </div>
</tr></td>
@endif
            