@extends('layouts.app')

@section('content')
<div class="container">
   <div class="card-header px-0 mt-2 bg-transparent clearfix">
      <h4 class="float-left pt-2">@lang(Lang::locale().'.TEMPLATE_PLURAL')</h4>
      
   </div>
   <div class="card-body px-0">
         <div class="alert alert-success alert-dismissible" style="display: none;" id="mainmsg"></div>
               @if(session()->has('message'))
         <div class="alert alert-success alert-dismissible" role="alert">
            {{ session()->get('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
               @endif
        
        <div class="row">
          <div class="col-md-3 mt-3">
            <div class="form-group">
              <label for="@lang(Lang::locale().'.SEARCH')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.TEMPLATE_PLURAL')</label>
              <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.TEMPLATE_PLURAL')" id="serach" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}')"  class="form-control">
            </div>
          </div>          
            
      </div>
<div class="table-body">
      <table class="table table-hover">
         <thead>
            <tr>
            <th class="sorting width-5"># <span class="icons id"></span></th>
              
                <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}','subject')" style="cursor: pointer">@lang(Lang::locale().'.TEMPLATE_SUBJECT') <span class="icons subject"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>
                 <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}','type')" style="cursor: pointer">@lang(Lang::locale().'.TEMPLATE_TYPE') <span class="icons type"></span></th>
                
                <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}','created_at')" style="cursor: pointer;">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
                <th class="sorting width-5" onclick="sorting('{{request()->route()->getAction()['prefix']}}','status')"  style="cursor: pointer">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>
               
                <th class="width-15">@lang(Lang::locale().'.ACTION')</th>
              
            </tr>
         </thead>
         <tbody id="tbody">
         @include('template/search_data')
               </tbody>
               </table>
               </div>
       
            <div id="loader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated"><div class="vue-content-placeholders-text"><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div><div class="vue-content-placeholders-text__line"></div></div></div>
            </div>
           
            <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="subject" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
    <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}" />
   </div>
</div>
 @can('create-mail-template')
<span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.TEMPLATE_SINGULAR')" class="btn-plus">
   <a href="/template/create" class="createbtn"><i class="fa fa-plus"></i></a>
</span>
@endcan
@endsection


