@extends('layouts.app')

@section('content')

<div class="container">
	<div class="card-header px-0 mt-2 bg-transparent clearfix">
		<h4 class="float-left pt-2">@lang(Lang::locale().'.EMAIL_PLURAL')</h4>		
	</div>
	<div class="card-body px-0">
		@if(session()->has('message'))
		<div class="alert alert-success alert-dismissible" role="alert">
			{{ session()->get('message') }}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif
		<form  method="POST" action="{{route('email.save')}}" id="from1"  autocomplete="off" data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.EMAIL_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
			@csrf

			<div class="row"> 
				<div class="col-md-4 form-group {{ $errors->has('driver') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_DRIVER')<span class="text-danger">*</span></label>
					<input type="text" name="driver" value="smtp" readonly data-required="true" class="form-control">
				<span class="text-danger">{{ $errors->first('driver') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('host') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_HOST')<span class="text-danger">*</span></label>
					<input type="text" name="host" value="{{old('host',($email?$email->host:''))}}" data-required="true" class="form-control">
					<span class="text-danger">{{ $errors->first('host') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('port') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_PORT')<span class="text-danger">*</span></label>
					<input type="text" name="port" value="{{old('port',$email?$email->port:'')}}" data-required="true" class="form-control">
					<span class="text-danger">{{ $errors->first('port') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('from') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_FROM')<span class="text-danger">*</span> <span>@lang(Lang::locale().'.EMAIL_FROM_DEMO')</span></label>
					<input type="text" name="from" value="{{old('from',$email?implode(':',json_decode($email->from,true)):'')}}" data-required="true" class="form-control">
					<span class="text-danger">{{ $errors->first('from') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('encryption') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_ENCRYPTION')<span class="text-danger">*</span> </label>
					<select class="form-control" data-required="true" name="encryption">
						<option value="ssl"  {{old('encryption',$email?$email->encryption:'')=='ssl'?'selected':''}}>SSL</option>
						<option value="tls" {{old('encryption',$email?$email->encryption:'')=='tls'?'selected':''}}>TLS</option>
					</select>
					<span class="text-danger">{{ $errors->first('encryption') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('username') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_USERNAME')<span class="text-danger">*</span></label>
					<input type="text"  value="{{old('username',$email?$email->username:'')}}" name="username" data-required="true" class="form-control">
					<span class="text-danger">{{ $errors->first('username') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('password') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_PASSWORD')<span class="text-danger">*</span></label>
					<input type="text" name="password" value="{{old('password',$email?$email->password:'')}}" data-required="true" class="form-control">
					<span class="text-danger">{{ $errors->first('password') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('sendmail') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_PATH')<span class="text-danger">*</span></label>
					<input type="text" name="sendmail" data-required="true" readonly value="/usr/sbin/sendmail -bs" class="form-control">
					<span class="text-danger">{{ $errors->first('sendmail') }}</span>
				</div>
				<div class="col-md-4 form-group {{ $errors->has('pretend') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_PRETEND')<span class="text-danger">*</span></label>
					<select class="form-control" data-required="true" name="pretend">
						<option value="1" {{old('pretend',$email?$email->pretend:'')=='1'?'selected':''}}>Yes</option>
						<option value="0" {{old('pretend',$email?$email->pretend:'')=='0'?'selected':''}}>No</option>
					</select>
					<span class="text-danger">{{ $errors->first('pretend') }}</span>
				</div>
				<div class="col-md-12 form-group text-right">
					<button  class="btn btn-primary">@lang(Lang::locale().'.EMAIL_SAVE_BTN_TXT')</button>
					<button type="button" data-toggle="modal" data-target="#sendtextmail" class="btn btn-info">@lang(Lang::locale().'.EMAIL_TEST_MAIL_BTN_TXT')</button>
				</div>

			</div> 
		</form>
		
	</div>
</div>

<!-- Modal -->
<div id="sendtextmail" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">@lang(Lang::locale().'.EMAIL_TEST_MAIL_BTN_TXT')</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      <form method="POST" action="{{route('email.testmail')}}" autocomplete="off" data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.EMAIL_TEST_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')" >
      	@csrf
      	<div class="form-group {{ $errors->has('testmail') ? 'has-error' : '' }}">
					<label>@lang(Lang::locale().'.EMAIL_TEST_MAIL')<span class="text-danger">*</span></label>
					<input type="text"  value="{{old('testmail')}}" name="testmail" data-required="true" required class="form-control" placeholder="demo@demo.com">
					<span class="text-danger">{{ $errors->first('testmail') }}</span>
				</div>
		<div class="col-md-12 form-group text-right">
					<button  class="btn btn-primary">@lang(Lang::locale().'.EMAIL_SEND_BTN_TXT')</button>
				</div>
      </form>
      </div>
    </div>

  </div>
</div>


@endsection


