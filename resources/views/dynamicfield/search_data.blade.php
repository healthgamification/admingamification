@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($dynamicfield as $dynamicfielddata)


            <tr data-sortable_id="{{$dynamicfielddata->id}}">
               <td>{{$ii}}</td>
              <!--  <td class="wordbreak">{{$dynamicfielddata->name}}</td>   -->           
               <td>{{$dynamicfielddata->label}}</td>
               <td>{{$dynamicfielddata->type}}</td>
               <td>{{$dynamicfielddata->status}}</td>
               
               <td>
                @can('read-dynamicfield')
               <a href="/dynamicfield/{{$dynamicfielddata->id}}/view" class="view-button" title="@lang(Lang::locale().'.VIEW')"><i class="fas fa-eye"></i></a>
               @endcan
               @can('update-dynamicfield')
               <a href="/dynamicfield/{{$dynamicfielddata->id}}/edit" class="edit-button" title="@lang(Lang::locale().'.EDIT')"><i class="fas fa-pencil-alt"></i></a>
            
               @endcan
               @can('delete-dynamicfield')
               <a title="@lang(Lang::locale().'.DELETE')" onclick="confirmationDelete('/dynamicfield/{{$dynamicfielddata->id}}/delete','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.DYNAMICFIELD_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/dynamicfield/{{$dynamicfielddata->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>

               @endcan
               </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
             @if(count($dynamicfield)!=0)
            @if($dynamicfield->hasPages())
              <tr>
                <td colspan="9" id="mainpagenation">
                  
                  {!! $dynamicfield->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($dynamicfield)==0)
            <tr><td colspan="9">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-dynamicfield')
         <a href="/dynamicfield/create" class="btn btn-primary">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DYNAMICFIELD_SINGULAR')
                             </a> 
        </a>
        @endcan
      </div>
</tr></td>
@endif
            