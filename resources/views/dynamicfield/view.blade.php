@extends('layouts.app')

@section('content')

<div class="container"> 
    <div class="card-header px-0 mt-2 bg-transparent clearfix">
      <div class="col-md-12"><h2>{{$dynamicfield->name}}</h2></div>
    </div>
    <div class="col-md-12 mt-2">
      <table class="table table-hover mt-2">
    <tbody>
      <tr>
        <td><label>@lang(Lang::locale().'.NAME'):</label></td>
        <td>{{$dynamicfield->name}}</td>
      </tr>
    </tbody>
  </table>
      
    </div>     

       <div class="saperator-line"></div>
    <div class='row mt-2'>
      <div class="col-md-12 text-right">
        @can('update-dynamicfield')
               <a  href="/dynamicfield/{{$dynamicfield->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.DYNAMICFIELD_SINGULAR')</a>
            
        @endcan</div>
    </div>
  </div>
</div>


@endsection

