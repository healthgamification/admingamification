@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 mt-2 bg-transparent clearfix">
        <h4 class="float-left pt-2">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.DYNAMICFIELD_SINGULAR')</h4>

      </div>
      <div class="card-body px-0">
       @if(session()->has('message'))
       <div class="alert alert-success alert-dismissible" role="alert">
        {{ session()->get('message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      <form method="POST" action="/dynamicfield/update/{{$dynamicfield->id}}" id="from1" autocomplete="off"  data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.DYNAMICFIELD_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
         <div class="col-md-3">
          <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
           <label for="name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
           <input type="text" id="name" name="name" class="form-control"  value="{{ old('name',$dynamicfield->name) }}" data-validation="required custom length"  data-validation-length="max60" placeholder="@lang(Lang::locale().'.DYNAMICFIELD_NAME_PLACEHOLDER')">
           <span class="text-danger">{{ $errors->first('name') }}</span>
         </div>
       </div>   
       <div class="col-md-3">
        <div class="form-group {{ $errors->has('label') ? 'has-error' : '' }}">
         <label for="label">@lang(Lang::locale().'.DYNAMICFIELD_LABEL'): <span class="text-danger">*</span></label>
         <input type="text" id="label" name="label" class="form-control"  value="{{ old('label',$dynamicfield->label) }}" data-validation="required custom length"  data-validation-length="max60" placeholder="@lang(Lang::locale().'.DYNAMICFIELD_LABEL_PLACEHOLDER')">
         <span class="text-danger">{{ $errors->first('label') }}</span>
       </div>
     </div>        
     <div class="col-md-6">
      <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
       <label for="type">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.DYNAMICFIELD_TYPE'): <span class="text-danger">*</span></label>
       <select name="type" class="form-control" id="type" data-validation="required" onchange="getOption(this)">
        @foreach($fields as $key=>$field)
        <option value="{{$key}}" {{old('type',$dynamicfield->type)==$key?"selected":""}}>{{$field}}</option>
        @endforeach
      </select>         
      <span class="text-danger">{{ $errors->first('type') }}</span>
    </div>
  </div>
  <div id="fieldoptions" style="width: 100%;">
    @php 
    $opdata = $dynamicfield->options ?? '[]';
    $options = json_decode($opdata);
    @endphp
    @if($dynamicfield->type=="select" || $dynamicfield->type=="checkbox" || $dynamicfield->type=="radio")
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th>Option Value</th>
            <th>Option Label</th>
            <th>Is Default</th>
            <th><button type="button" onclick="addMoreRow(this,'selectfrow');" class="btn btn-primary"><i class="fa fa-plus"></i></button></th>
          </tr>
        </thead>
        <tbody>
          @forelse($options->value as $key=>$value)
          <tr id="{{$key==0?'selectfrow':''}}">
            <td><input type="text" name="options[value][{{$key}}]" value="{{old('options.value.'.$key,$value)}}" class="form-control"></td>
            <td><input type="text" name="options[label][{{$key}}]" value="{{old('options.label.'.$key,$options->label[$key])}}" class="form-control"></td>
            <td>
              <label class="switch switch-pill switch-outline-success-alt">
                <input class="switch-input" type="radio" {{old('options.is_default.'.$key,isset($options->is_default->$key)?$options->is_default->$key:0)=='1'?'checked':''}} name="options[is_default][{{$key}}]" value="1">
                <span class="switch-slider"></span>
              </label>
            </td>
            <td><button type="button" onclick="removeRow(this);" class="btn btn-danger"><i class="fa fa-minus"></i></button></td>
          </tr>
          @empty
          @endforelse
        </tbody>
      </table>
    </div>
    @elseif($dynamicfield->type=="dbselect")
    <div class="col-md-12">
      <table class="table">
        <thead>
          <tr>
            <th>Table Name</th>
            <th>Option Value</th>
            <th>Option Label</th>
            <th><button type="button" onclick="addMoreRow(this,'dbselectfrow');" class="btn btn-primary"><i class="fa fa-plus"></i></button></th>
          </tr>
        </thead>
        <tbody>
          @forelse($options->tblname as $key=>$tblname)
          <tr id="{{$key==0?'dbselectfrow':''}}">
            <td><input type="text" name="options[tblname][{{$key}}]" value="{{old('options.tblname.'.$key,$tblname)}}" class="form-control"></td>

            <td><input type="text" name="options[value][{{$key}}]" value="{{old('options.value.'.$key,$options->value[$key])}}" class="form-control"></td>

            <td><input type="text" name="options[label][{{$key}}]" value="{{old('options.label.'.$key,$options->label[$key])}}" class="form-control"></td>           
            <td><button type="button" onclick="removeRow(this);" class="btn btn-danger"><i class="fa fa-minus"></i></button></td>
          </tr>
          @empty
          @endforelse
        </tbody>
      </table>
    </div>
    @endif
  </div>
  <div class="col-md-6">
    <div class="form-group {{ $errors->has('label_attr') ? 'has-error' : '' }}">
     <label for="label_attr">@lang(Lang::locale().'.DYNAMICFIELD_LABEL_ATTR'): </label>
     <textarea id="label_attr" name="label_attr" class="form-control" placeholder="@lang(Lang::locale().'.DYNAMICFIELD_LABEL_ATTR_PLACEHOLDER')">{{ old('label_attr',$dynamicfield->label_attr) }}</textarea>
     <span class="text-danger">{{ $errors->first('label_attr') }}</span>
   </div>
 </div>   
 <div class="col-md-6">
  <div class="form-group {{ $errors->has('attr') ? 'has-error' : '' }}">
   <label for="attr">@lang(Lang::locale().'.DYNAMICFIELD_ATTR'):</label>
   <textarea id="attr" name="attr" class="form-control" placeholder="@lang(Lang::locale().'.DYNAMICFIELD_ATTR_PLACEHOLDER')">{{ old('attr',$dynamicfield->attr) }}</textarea>
   <span class="text-danger">{{ $errors->first('attr') }}</span>
 </div>
</div> 
<div class="col-md-6">
  <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
   <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
   <select name="status" class="form-control" name="status" data-validation="required">
    <option value="" >@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
    <option value="Active" {{old('status',$dynamicfield->status)=='Active'?'selected':''}} >@lang(Lang::locale().'.ACTIVE')</option>
    <option value="Inactive" {{old('status',$dynamicfield->status)=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
  </select>         
  <span class="text-danger">{{ $errors->first('status') }}</span>
</div>
</div>   
<div class="col-md-6">
   <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
     <label for="priority">@lang(Lang::locale().'.DYNAMICFIELD_PRIORITY'): </label>
     <input type="text" id="priority" name="priority" class="form-control"  value="{{ old('priority',$dynamicfield->priority) }}" data-validation="required custom length"  data-validation-length="max60" placeholder="@lang(Lang::locale().'.DYNAMICFIELD_PRIORITY_PLACEHOLDER')">
         <span class="text-danger">{{ $errors->first('priority') }}</span>
   </div>  
</div> 

<div class="col-md-12">
  <div class="form-group text-right">

    
    <button  class="btn btn-primary">@lang(Lang::locale().'.UPDATE')</button>
    <button type="button" onclick="window.location.href='/dynamicfields'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
  </div>
</div>
</div>
</form>

</div>
</div>
</div>
</div>
<script type="text/javascript">
  var html={};
   html['select']=`
  <div class="col-md-12">
  <table class="table">
  <thead>
  <tr>
  <th>Option Value</th>
  <th>Option Label</th>
  <th>Is Default</th>
  <th><button type="button" onclick="addMoreRow(this,'selectfrow');" class="btn btn-primary"><i class="fa fa-plus"></i></button></th>
  </tr>
  </thead>
  <tbody>
  <tr id="selectfrow">
  <td><input type="text" name="options[value][rowid]" class="form-control"></td>
  <td><input type="text" name="options[label][rowid]" class="form-control"></td>
  <td>
              <label class="switch switch-pill switch-outline-success-alt">
                <input class="switch-input" type="radio" name="options[is_default][rowid]" value="1">
                <span class="switch-slider"></span>
                </label>
  </td>
  <td></td>
  </tr>
  </tbody>
  </table>
  </div>
  `;
  html['dbselect']=`
  <div class="col-md-12">
  <table class="table">
  <thead>
  <tr>
  <th>Table Name</th>
  <th>Option Value</th>
  <th>Option Label</th>
  <th><button type="button" onclick="addMoreRow(this,'dbselectfrow');" class="btn btn-primary"><i class="fa fa-plus"></i></button></th>
  </tr>
  </thead>
  <tbody>
  <tr id="dbselectfrow">
  <td><input type="text" name="options[tblname][]" class="form-control"></td>
  <td><input type="text" name="options[value][]" class="form-control"></td>
  <td><input type="text" name="options[label][]" class="form-control"></td>
  <td></td>
  </tr>
  </tbody>
  </table>
  </div>
  `;
  function getOption(ele) {
    var val = $(ele).val();
    if(val=="select" || val=="radio" || val=="checkbox")
    {
      var count=$("#fieldoptions").find("table").find("tbody").find("tr").length;
      $("#fieldoptions").html(html['select'].replace(/rowid/g,count));
    }
    else if(val=="dbselect")
    {
      $("#fieldoptions").html(html['dbselect']);
    }
    else{
       $("#fieldoptions").html('');
    }
  }

  function addMoreRow(ele,id) {    
    var table = $(ele).closest("table");
    var count="["+table.find("tbody").find("tr").length+"]";
    var tbodyhtml=`<tr>${$("#"+id).html().replace("<td></td>","").replace(/\[0\]/g,count)}
  <td><button type="button" onclick="removeRow(this);" class="btn btn-danger"><i class="fa fa-minus"></i></button></td>
  </tr>`;
    table.find("tbody").append(tbodyhtml);
  }

  function removeRow(ele) {
    var tr = $(ele).closest("tr");
    tr.remove();
  }
</script>
@endsection
