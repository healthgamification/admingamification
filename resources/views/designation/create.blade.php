@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DESIGNATION_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <form method="POST" action="/designation/store" id="from1" autocomplete="off" data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.DESIGNATION_SAVE_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="row">
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" data-validation="required custom length" data-validation-length="max50" placeholder="@lang(Lang::locale().'.DESIGNATION_NAME_PLACEHOLDER')">
                <span class="text-danger">{{ $errors->first('name') }}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('organization_id') ? 'has-error' : '' }}">
                <label for="organization">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR'): <span class="text-danger">*</span></label>
                <select class="form-control" name="organization_id" data-validation="required">
                  <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.ORGANIZATION_SINGULAR') </option>
                  @foreach($organizations as $organization)
                  {
                  <option value="{{$organization->id}}" {{$orgid==$organization->id?'selected':''}}>{{$organization->name}}</option>
                  }
                  @endforeach
                </select>
                <span class="text-danger">{{ $errors->first('organization_id') }}</span>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                <label for="status">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
                <select name="status" class="form-control" name="status" data-validation="required">
                  <option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
                  <option value="Active">@lang(Lang::locale().'.ACTIVE')</option>
                  <option value="Inactive">@lang(Lang::locale().'.INACTIVE')</option>
                </select>
                <span class="text-danger">{{ $errors->first('status') }}</span>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label for="description"> @lang(Lang::locale().'.DESCRIPTION'):</label>
                <textarea type="text" id="description" name="description" rows="4" class="form-control">{{ old('description') }}</textarea>
                <span class="text-danger">{{ $errors->first('description') }}</span>
              </div>
            </div>

            <div class="col-md-12">
              <div class="form-group">

                <input type="hidden" id="btnvalue" name="btnvalue" value="">
                <button onclick="$('#btnvalue').val('new');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_NEW')</button>
                <button onclick="$('#btnvalue').val('exit');" class="btn btn-primary">@lang(Lang::locale().'.SAVE_AND_EXIT')</button>
                <button type="button" onclick="window.location.href='/designations/{{ $orgid }}'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
  </div>
</div>
@endsection
