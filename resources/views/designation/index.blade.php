@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">@lang(Lang::locale().'.DESIGNATION_PLURAL')</h4>
  </div>
  <div class="card-body px-0">
    <div class="alert alert-success" style="display: none;" id="mainmsg"></div>
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible" role="alert">
      {{ session()->get('message') }}
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    @endif
    <!--  <div class="row justify-content-between">
        
         <div class="col-md-12 text-right">
            <button data-toggle="collapse" data-target="#uploadcollapes" class="btn btn-primary bulk-btn">@lang(Lang::locale().'.DESIGNATION_BULK_UPLOAD_BTN_TEXT')
            <i class="fa fa-chevron-down upload-down-icon"></i>

            </button>
            </div>
            <div class="col-md-12">
                include('member/uploadcsv')
            </div>
          </div>  -->

    {{ csrf_field() }}

    <div class="row">
      <div class="col-md-3">
        <div class="form-group mb-3">
          <label for="@lang(Lang::locale().'.SEARCH')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.DESIGNATION_PLURAL')</label>
          <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.DESIGNATION_PLURAL')" id="serach" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}/{{$orgid??0}}')" class="form-control">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group mb-3">
          <label for="@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')">@lang(Lang::locale().'.SORT_BY') @lang(Lang::locale().'.ORGANIZATION_SINGULAR')</label>
          <select class="form-control" name="setId" onchange="get_filtered_team_data(this,this.options[this.selectedIndex].value,'{{request()->route()->getAction()['prefix']}}')">
            <option value="0">@lang(Lang::locale().'.ALL')</option>
            @foreach($organization_data as $org_data)
            <option value="{{ $org_data->id }}" {{$orgid==$org_data->id?'selected':''}}>{{$org_data->name}}</option>
            @endforeach
          </select>

        </div>
      </div>
      <div class="col-md-6 mt-4 text-right">
        <!-- @can('create-designation')
        <div class="card-header-actions mr-1">
          <button data-href="/designation/create" onclick="window.location.href='/designation/create';" class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DESIGNATION_SINGULAR')</button>
        </div>
        @endcan -->
      </div>
    </div>
  </div>
  <div class="table-body">
    <table class="table table-hover">
      <thead>
        <tr>
          <th class="sorting width-5"># <span class="icons id"></span></th>

          <th class="sorting width-20" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'name')" style="cursor: pointer">@lang(Lang::locale().'.NAME') <span class="icons name"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>

          <th class="sorting width-30">@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</th>
          <th class="sorting width-15" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'created_at')" style="cursor: pointer;">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>
          <th class="sorting width-10" onclick="sorting('{{request()->route()->getAction()['prefix']}}/'+$('#hidden_organizationId').val(),'status')" style="cursor: pointer">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>

          <th class="width-20">@lang(Lang::locale().'.ACTION')</th>

        </tr>
      </thead>
      <tbody id="tbody">
        @include('designation/search_data')
      </tbody>
    </table>
  </div>

  <div id="loader" style="display:none;">
    <div class="vue-content-placeholders vue-content-placeholders-is-animated">
      <div class="vue-content-placeholders-text">
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
        <div class="vue-content-placeholders-text__line"></div>
      </div>
    </div>
  </div>

  <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
  <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="name" />
  <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
  <input type="hidden" name="hidden_organizationId" id="hidden_organizationId" value="{{$orgid??0}}" />
  <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/{{$orgid??0}}" />

  @can('create-designation')
  <!-- <div class="card-header-actions mr-1">
    <button data-href="/designation/create" onclick="window.location.href='/designation/create';" class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DESIGNATION_SINGULAR')</button>
  </div> -->

  <span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DESIGNATION_SINGULAR')" class="btn-plus">
    <a data-href="/designation/create" class="createbtn" onclick="window.location.href='/designation/create';"><i class="fa fa-plus"></i></a>
  </span>

  @endcan

</div>
</div>

@endsection
