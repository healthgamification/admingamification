@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <div class="col-md-12">
      <h4>{{ $designationData->name }}</h4>
    </div>
  </div>
  <table class="table table-hover mt-2">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.DESIGNATION_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $designationData->name }} </td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</strong></td>
        <td class="width-35">{{ $organizationData->name }}</td>
      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.DESIGNATION_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td>{{ $designationData->description }}</td>
        <td><strong>Status</strong></td>
        <td>{{ $designationData->status }}</td>
      </tr>
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan='3'>{{ $designationData->created_at }}</td>

      </tr>
    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-designation')
      <a href="/designation/edit/{{$designationData->id}}" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.DESIGNATION_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection

