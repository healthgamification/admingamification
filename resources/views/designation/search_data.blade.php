@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($designations as $designation)


<tr>
  <td>{{$ii}}</td>
  <td class="wordbreak">{{$designation->name}}</td>
  <td class="wordbreak">{{$designation->organization_name}}</td>
  <td>{{custom_date_format($designation->created_at)}}</td>
  <td>{{$designation->status}}</td>

  <td>
    @can('read-designation')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
      <a href="/designation/{{$designation->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
    @endcan
    @can('update-designation')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
      <a href="/designation/edit/{{$designation->id}}" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endcan
    @can('delete-designation')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
      <a onclick="confirmationDelete('/designation/delete/{{$designation->id}}','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.DESIGNATION_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/designation/delete/{{$designation->id}}" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
    @endcan
  </td>
</tr>
@php $ii++;@endphp
@endforeach
@if(count($designations)!=0)
@if($designations->hasPages())
<tr>
  <td colspan="8" id="mainpagenation">

    {!! $designations->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($designations)==0)
<tr>
  <td colspan="8">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-designation')
      <!-- <a class="btn btn-primary" href="/member/create" role="button"> -->
      <a href="/designation/create/{{$orgid}}" class="btn btn-primary createbtn">
        <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.DESIGNATION_SINGULAR')
      </a>
      @endcan
    </div>
</tr>
</td>
@endif
