@extends('layouts.app')

@section('content')

<div class="container">
   <div class="card-header px-0 mt-2 bg-transparent clearfix">
      <h4 class="float-left pt-2">Signatures</h4>
      <div class="card-header-actions mr-1"><a href="/signatures/create" class="btn btn-success">New signature</a></div>
   </div>
   <div class="card-body px-0">
      <!-- <div class="row justify-content-between">
         <div class="col-7 col-md-5">
            <div class="input-group mb-3">
               <div class="input-group-prepend"><span class="input-group-text"><i class="fas fa-search"></i></span></div>
               <input type="text" placeholder="Seach" class="form-control">
            </div>
         </div>
      </div> -->
      <table class="table table-hover" style="display: block;overflow-x: auto;white-space: nowrap;">
         <thead>
            <tr>
                <th class="d-none d-sm-table-cell"><a href="#" class="text-dark">ID</a> <i class="mr-1 fas fa-long-arrow-alt-down"></i></th>
                <th><a href="#" class="text-dark">Name</a> <i class="mr-1 fas"></i></th>
                <th>Designation</th>
                <th>Linkedin</th>
                <th>Website Name</th>
                <th>mobile</th>
                <th>Landline</th>
                <th>Email</th>
                <th>Skype</th>
                <th>Address</th>
                <th>image</th>
                <th>Type</th>
            </tr>
         </thead>
         <tbody>
         @foreach($signatures as $signature)
            <tr>
               <td class="d-none d-sm-table-cell">{{$signature->id}}</td>
               <td>{{$signature->name}}</td>
               <td>{{$signature->designation}}</td>
               <td>{{$signature->linkedinUrl}}</td>
               <td>{{$signature->websiteName}}</td>
               <td>{{$signature->mobileNo}}</td>
               <td>{{$signature->landlineNo}}</td>
               <td>{{$signature->emailAddress}}</td>
               <td>{{$signature->skypeId}}</td>
               <td>{{$signature->officeAddress}}</td>
               <td><img style="width: 70%;" src='{{$signature->image}}'></td>
               <td>{{$signature->signatureType}}</td>
               
              
            </tr>
            @endforeach
         </tbody>
      </table>
      <div class="row">
         <div class="col pt-2">
            1-1 of 1
         </div>
         <!---->
      </div>
      <!----> <!---->
   </div>
</div>



@endsection

