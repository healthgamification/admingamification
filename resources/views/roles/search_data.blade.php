@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $role)

<tr>
               <td>{{$ii}}</td>
               <td>
                <div class="media">
               
                <div class="media-body">
                  <div>
                   <p style="margin:0px">{{$role->name}}</p>
                  </div>
              </div>
            </td>
               <td>{{$role->display_name}}</td>
                <td>{{ count($role->permissions) }} out of {{$totalPermission}}</td>
                 <td>{{ count($role->users) }}</td>
                <td>
              {{date('d-m-Y',strtotime($role->created_at))}}
            </td>
            
               
               <td>
                @can('read-roles')
              <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
                        <a href="/role/{{$role->id}}/view" class="view-button" ><i class="fas fa-eye"></i></a>
              </span>
              @endcan
              @if($role->name !='super-admin')
               @can('update-roles')
            <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
                       <a href="/role/{{$role->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
            </span>
               @endcan
               @can('delete-roles')
              <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE') ">
               <a onclick="confirmationDelete('/role/{{$role->id}}/delete','@lang(Lang::locale().'.ARE_YOU_SURE')',' @lang(Lang::locale().'.ROLE_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/questions/{{$role->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a></span>
               @endcan
               @endif
                  </td>
                 
</tr>
@php $ii++;@endphp
@endforeach
@if(count($data)!=0)
@if($data->hasPages())
<tr>
  <td colspan="7" id="mainpagenation">

    {!! $data->links() !!}

  </td>
</tr>
@endif
@endif
@if(count($data)==0)
<tr>
  <td colspan="6">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-roles')
      <button data-href="/role/create" onclick="window.location.href='/role/create'" class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.ROLE_SINGULAR')
        </button>
        @endcan
      </div>
</tr></td>
@endif
            