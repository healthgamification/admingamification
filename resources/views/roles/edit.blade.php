@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.ROLE_SINGULAR')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
          {{ session()->get('message') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        @endif
        <form method="POST" action="/role/update" autocomplete="off" data-title="@lang(Lang::locale().'.ARE_YOU_SURE')" data-message="@lang(Lang::locale().'.ROLE_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="id" value="{{ $id }}">
          <div class="row">
            <div class="col-md-4 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
              <label for="name">@lang(Lang::locale().'.NAME'): <span class="text-danger">*</span></label>
              <input type="text" id="name" name="name" class="form-control" data-validation="required length" data-validation-length="max50" value="{{ old('name',$roleData->name) }}" placeholder="@lang(Lang::locale().'.ROLE_NAME_PLACEHOLDER')">
              <span class="text-danger">{{ $errors->first('name') }}</span>
            </div>
            <div class="col-md-4 form-group {{ $errors->has('display_name') ? 'has-error' : '' }}">
              <label for="display_name">@lang(Lang::locale().'.ROLE_SINGULAR') @lang(Lang::locale().'.TITLE'): <span class="text-danger">*</span></label>
              <input type="text" id="display_name" name="display_name" class="form-control" data-validation="required length" data-validation-length="max50" value="{{ old('display_name',$roleData->display_name) }}" placeholder="@lang(Lang::locale().'.ROLE_TITLE_PLACEHOLDER')">
              <span class="text-danger">{{ $errors->first('display_name') }}</span>
            </div>
            <div class="clearfix"></div>
          </div>
          @if(count($moduleData) > 0)
           <div class="row">
            <div class="col-md-12"><h5>@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.PERMISSION_PLURAL')</h5></div>
            <div class="clearfix"></div>
          </div>
         
          @foreach($moduleData as $md)
            @if(count($md->permissions)>0)
            <div class="row">
              <div class="col-md-12">
                <h6><label>{{ $md->display_name }}</label>
                </h6>
              </div>
              <div class="clearfix"></div>
            </div>
           
            @foreach($md->permissions as $pd)
            @php $checked = '';
              if($roleData->hasPermissionTo($pd->id))
              {
                $checked ='checked';
              }
            @endphp
            <div class="row">
              <div class="col-md-1">
              </div>
              <div class="col-md-4">
                <span for="permission" class="float-left">{{ $pd->display_name }}</span>
                <label class="switch switch-pill switch-outline-success-alt float-right">
                <input class="switch-input" type="checkbox" {{ $checked }} name="permission[]" value="{{ $pd->id }}">
                <span class="switch-slider"></span>
                </label>
              </div>
              <div class="clearfix"></div>
            </div>
            @endforeach
             
          @endif
          @endforeach
         
          @endif
          <div class="row">
            <div class="col-md-12">
              <!-- <button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button> -->
              <input type="hidden" id="btnvalue" name="btnvalue" value="">
              <button class="btn btn-primary">@lang(Lang::locale().'.UPDATE')</button>
              <button type="button" onclick="window.location.href='/roles'" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
            </div>
            <div class="clearfix"></div>
          </div>
      </div>
      </form>

    </div>
  </div>
</div>


@endsection
