@extends('layouts.app')

@section('content')

<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <div class="col-md-12">
      <h4>{{ $roleData->display_name }}</h4>
    </div>
  </div>

  <table class="table table-hover mt-2">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.ROLE_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $roleData->name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ROLE_SINGULAR') @lang(Lang::locale().'.TITLE')</strong></td>
        <td class="width-35 wordbreak">{{ $roleData->display_name }}</td>
      </tr>
      
      <tr>
        <td><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td colspan="3">{{ custom_date_format($roleData->created_at) }}</td>

      </tr>
    </tbody>

  </table>
  @if(count($moduleData) > 0)
   <div class='row'>
    <div class="col-md-12">
      <h5>@lang(Lang::locale().'.PERMISSION_SINGULAR')</h5>
    </div>
    <div class="clearfix"></div>
  </div>
   @foreach($moduleData as $md)
      @if(count($md->permissions)>0)
      <div class="row">
        <div class="col-md-12">
          <h6><label>{{ $md->display_name }}</label>
          </h6>
        </div>
        <div class="clearfix"></div>
      </div>
     
      @foreach($md->permissions as $pd)
      @php $checked = '';
        if($roleData->hasPermissionTo($pd->id))
        {
          $checked ='checked';
        }
      @endphp
      <div class="row">
        <div class="col-md-1">
        </div>
        <div class="col-md-4">
          <span for="permission" class="float-left">{{ $pd->display_name }}</span>
          <label class="switch switch-pill switch-outline-success-alt float-right">
          <input class="switch-input" disabled type="checkbox" {{ $checked }} name="permission[]" value="{{ $pd->id }}">
          <span class="switch-slider"></span>
          </label>
        </div>
        <div class="clearfix"></div>
      </div>
      @endforeach
       
    @endif
    @endforeach
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-roles')
      <a href="/role/{{$roleData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.ROLE_SINGULAR')</a>
      @endcan</div>
  </div>
</div>
</div>


@endsection
