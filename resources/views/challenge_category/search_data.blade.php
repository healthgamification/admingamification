@php
$ii=1;
if(isset($_GET['page']))
{
if($_GET['page']==1)
{
$ii=1;
}else{
$ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $challengeCategory)

<tr>
  <td>{{$ii}}</td>
  <td>{{$challengeCategory->name}}</td>
  <td>{{$challengeCategory->status}}</td>
  <td>{{ date('d-m-Y', strtotime($challengeCategory->created_at)) }}</td>
  <td>
    @can('read-challenge-category')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
      <a href="/challenge-category/{{$challengeCategory->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
    @endcan
    @can('update-challenge-category')

    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
      <a href="/challenge-category/{{$challengeCategory->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
    @endcan
    @can('delete-challenge-category')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
      <a onclick="confirmationDelete('/challenge-category/{{$challengeCategory->id}}/delete','@lang(Lang::locale().'.CHALLENGE_CATEGORY_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.CHALLENGE_CATEGORY_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>

    @endcan
  </td>
</tr>
@php $ii++;@endphp
@endforeach
@if($data->hasPages())
<tr>
  <td colspan="7" id="pagination">

    {!! $data->links() !!}

  </td>
</tr>
@endif
@if(count($data)==0)
<tr>
  <td colspan="7">
    <div class="no-items-found text-center mt-1">
      <i class="icon-magnifier fa-3x text-muted"></i>
      <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
      <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
      @can('create-challenge-category')
      <a class="btn btn-primary createbtn" href="/challenge-category/create" role="button">
        <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')
      </a>
      @endcan
    </div>
</tr>
</td>
@endif
