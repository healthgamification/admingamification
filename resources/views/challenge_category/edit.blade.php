@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card-header px-0 bg-transparent clearfix">
				<h4 class="float-left">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.CHALLENGE_SINGULAR')</h4>
			</div>
			<div class="card-body px-0">
				<form method="POST" action="/challenge-category/update/{{$data['id']}}" autocomplete="off" data-title="@lang(Lang::locale().'.CHALLENGE_CATEGORY_EDIT_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.CHALLENGE_CATEGORY_EDIT_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="row">
						<div class="col-md-4 form-group {{ $errors->has('name') ? 'has-error' : '' }}">
							<label for="name"> @lang(Lang::locale().'.TITLE'): <span class="text-danger">*</span></label>
							<input type="text" id="name" name="name" class="form-control" placeholder="@lang(Lang::locale().'.CHALLENGE_TITLE_PLACEHOLDER')" value="{{$data['name']}}" data-validation="required custom length" data-validation-length="max50">
							<span class="text-danger">{{ $errors->first('name') }}</span>
						</div>


						<div class="col-md-4 form-group {{ $errors->has('status') ? 'has-error' : '' }}">
							<label for="status"> @lang(Lang::locale().'.STATUS'): <span class="text-danger">*</span></label>
							<select name="status" class="form-control" data-validation="required">
								<option value="">@lang(Lang::locale().'.SELECT') @lang(Lang::locale().'.STATUS')</option>
								<option value="Active" {{$data['status']=='Active'?'selected':''}}>@lang(Lang::locale().'.ACTIVE')</option>
								<option value="Inactive" {{$data['status']=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
							</select>
							<span class="text-danger">{{ $errors->first('status') }}</span>
						</div>



						<div class="col-md-12 form-group {{ $errors->has('description') ? 'has-error' : '' }}">
							<label for="description"> @lang(Lang::locale().'.DESCRIPTION'):</label>
							<textarea type="text" id="about" name="description" rows="4" class="form-control">{{$data['description']}}</textarea>
							<span class="text-danger">{{ $errors->first('description') }}</span>
						</div>

						<div class="col-md-12">
							<button class="btn btn-primary">@lang(Lang::locale().'.SUBMIT')</button>
							<button type="button" onclick="back();" class="btn btn-primary">@lang(Lang::locale().'.BTN_BACK')</button>
						</div>
					</div>

				</form>

			</div>
		</div>
	</div>
</div>

@endsection
