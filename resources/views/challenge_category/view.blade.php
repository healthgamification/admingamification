@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{ $challengeCategoryData->name }}</h4>
  </div>
</div>
<div class="col-md-12">
  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $challengeCategoryData->name }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{$challengeCategoryData->description}}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30 wordbreak">{{$challengeCategoryData->status}}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td class="width-30 wordbreak">{{ custom_date_format($challengeCategoryData->created_at) }}</td>

      </tr>

    </tbody>

  </table>
</div>
<div class="col-md-12">
  @can('update-challenge-category')
  <a href="/challenge-category/{{$challengeCategoryData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</a>
  @endcan</div>
</div>
</div>


@endsection
