@extends('layouts.app')
@section('content')
<div class="container">
   <div class="card-header px-0 bg-transparent clearfix">
      <h4 class="float-left">@lang(Lang::locale().'.CHALLENGE_CATEGORY_PLURAL')</h4>
   </div>
   <div class="card-body px-0">
      @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
      @endif 
      <div class="row mb-3">
         <div class="col-md-4">
            {{ csrf_field() }}
            <div class="form-group mb-3">
               <label for="@lang(Lang::locale().'.SEARCH')  @lang(Lang::locale().'.CHALLENGE_CATEGORY_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_CATEGORY_PLURAL')</label>
               <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.CHALLENGE_CATEGORY_PLURAL')" id="serach" onkeyup="search_data(this.value,'{{request()->route()->getAction()['prefix']}}')" name="q" value="" class="form-control">
            </div>
         </div>
         <!-- <div class="col-md-8 mt-4 mb-3">
            @can('create-challenge-category')
            <div class="card-header-actions mr-1"><a href="/challenge-category/create" class="btn btn-primary">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')</a></div>
            @endcan
         </div> -->

      </div>
      <div class="table-body">
         <table class="table table-hover">
            <thead>
               <tr>
                  <th class="sorting ">#</th>
                  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}','name')" style="cursor: pointer">@lang(Lang::locale().'.TITLE') <span class="icons name"><i class="mr-1 fas fa-long-arrow-alt-down"></i></span></th>

                  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}','status')" style="cursor: pointer">@lang(Lang::locale().'.STATUS') <span class="icons status"></span></th>
                  <th class="sorting " onclick="sorting('{{request()->route()->getAction()['prefix']}}','created_at')" style="cursor: pointer">@lang(Lang::locale().'.ADDED_AT') <span class="icons created_at"></span></th>

                  <th>@lang(Lang::locale().'.ACTION')</th>

               </tr>
            </thead>
            <tbody id="tbody">
               @include('challenge_category/search_data')

            </tbody>
         </table>
      </div>
      <div id="loader" style="display:none;">
         <div class="vue-content-placeholders vue-content-placeholders-is-animated">
            <div class="vue-content-placeholders-text">
               <div class="vue-content-placeholders-text__line"></div>
               <div class="vue-content-placeholders-text__line"></div>
               <div class="vue-content-placeholders-text__line"></div>
               <div class="vue-content-placeholders-text__line"></div>
            </div>
         </div>
      </div>

      <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
      <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="name" />
      <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
      <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}" />



      <!---->
      <!---->

      @can('create-challenge-category')
      <span data-toggle="tooltip" title="@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.CHALLENGE_CATEGORY_SINGULAR')" class="btn-plus">
         <a href="/challenge-category/create" class="createbtn"><i class="fa fa-plus"></i></a>
      </span>

      @endcan

   </div>
</div>

@endsection
