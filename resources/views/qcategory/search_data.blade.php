@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($data as $category)
<tr>
               <td>{{$ii}}</td>
               <td>
                <div class="media">
                
                <div class="media-body">
                  <div>{{$category->category_name}}</div>
                  
                </div>
              </div>
            </td>
            
                <td>
             {{date('d-m-Y',strtotime($category->created_at))}}
            </td>
            <td>
              {{ $category->status }}
            </td>
               
               <td>
                @can('read-quiz-category')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
              <a href="/qcategory/{{$category->id}}/view" class="view-button"><i class="fas fa-eye"></i></a>
    </span>
              @endcan
               @can('update-quiz-category')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a href="/qcategory/{{$category->id}}/edit" class="edit-button"><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-quiz-category')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a onclick="confirmationDelete('/qcategory/{{$category->id}}/delete','@lang(Lang::locale().'.USER_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.QUESTION_CATEGORY_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/qcategory/{{$category->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
               @endcan
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
              @if(count($data)!=0)
            @if($data->hasPages())
              <tr>
                <td colspan="6" id="mainpagenation">
                  
                  {!! $data->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-quiz-category')
        <a class="btn btn-primary createbtn" href="/qcategory/create" role="button">
          <i class="fa fa-plus"></i>&nbsp; @lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.QUESTION_CATEGORY')
        </a>
        @endcan
      </div>
</tr></td>
@endif
            