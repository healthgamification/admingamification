@extends('layouts.app')
@section('content')
<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
    <h4 class="float-left">{{ $questionCategoryData->category_name }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_CATEGORY') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $questionCategoryData->category_name }}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.QUESTION_CATEGORY') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{$questionCategoryData->description}}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30 wordbreak">{{$questionCategoryData->status}}</td>
        <td class="width-20"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td class="width-30 wordbreak">{{ custom_date_format($questionCategoryData->created_at) }}</td>

      </tr>

    </tbody>

  </table>
  <div class='row'>
    <div class="col-md-12">
      @can('update-quiz-category')
      <a href="/qcategory/{{$questionCategoryData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.QUESTION_CATEGORY')</a>

      @endcan
    </div>
  </div>
</div>
</div>


@endsection