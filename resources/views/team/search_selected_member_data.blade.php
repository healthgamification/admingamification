@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($members as $member)

            <tr>
               <td class="d-none d-sm-table-cell">{{$ii}}</td>
               <td><input type="checkbox" name="member_id[]" onclick="addmember(this,'submitbtn');" value="{{$member->id}}"></td>
               <td>{{$member->member_name}}</td>
               <td>{{$member->member_email}}</td>
               <td>{{$member->department?$member->department->name:''}}</td>
               <td>
                
                    <select name="status[{{$member->id}}]" class="form-control" name="status" data-validation="required">
            <option value="Active" {{$member->status=='Active'?'selected':''}}>@lang(Lang::locale().'.ACTIVE')</option>
            <option value="Inactive" {{$member->status=='Inactive'?'selected':''}}>@lang(Lang::locale().'.INACTIVE')</option>
            @can('delete-player')
            <option value="Delete" {{$member->status=='Delete'?'selected':''}}>@lang(Lang::locale().'.DELETE')</option>
            @endcan
                   </select>
               </td>
                
            </tr>
            @php $ii++; @endphp
           
@endforeach
@if(count($members)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
         @can('add-player')
        <button class="btn btn-primary" type="button" data-toggle="modal" data-target=".bd-example-modal-lg">@lang(Lang::locale().'.ADD') @lang(Lang::locale().'.PLAYER_PLURAL')</button>
        @endcan 
      </div>
</tr></td>
@endif

            