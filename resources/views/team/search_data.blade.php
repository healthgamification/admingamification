@php
$ii=1;
if(isset($_GET['page']))
{
  if($_GET['page']==1)
  {
 $ii=1;
}else{
  $ii = (($_GET['page']*$perpage)-$perpage)+1;
}
}
@endphp
@foreach($team_data as $team)

            <tr>
               <td>{{$ii}}</td>
               <td style="word-break: break-all;">{{$team->name}}</td>
               <td style="word-break: break-all;">{{$team->organization_name}}</td>
               <td>
                @can('read-player')
                <span data-toggle="tooltip" title="@lang(Lang::locale().'.ADD') @lang(Lang::locale().'.PLAYER_PLURAL')">
               <a href="/players/{{$team->id}}/{{$team->organization_id}}"> @lang(Lang::locale().'.PLAYER_MANAGE')</a>
               </span>
               @endcan
              </td>
              <td>{{$team->player->count()}}</td>
               <td>{{$team->status}}</td>
               
               <td>
                @can('read-team')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.VIEW')">
                <a href="/team/{{$team->id}}/view" class="view-button" ><i class="fas fa-eye"></i></a>
    </span>
                @endcan
               @can('update-team')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.EDIT')">
               <a  href="/team/{{$team->id}}/edit" class="edit-button" ><i class="fas fa-pencil-alt"></i></a>
    </span>
               @endcan
               @can('delete-team')
    <span data-toggle="tooltip" title="@lang(Lang::locale().'.DELETE')">
               <a  onclick="confirmationDelete('/team/{{$team->id}}/delete','@lang(Lang::locale().'.TEAM_DELETE_CONFIRM_TITLE')',' @lang(Lang::locale().'.TEAM_DELETE_CONFIRM_TEXT')','@lang(Lang::locale().'.CANCEL_BTN')','@lang(Lang::locale().'.ACCEPT_BTN')')" href="/team/{{$team->id}}/delete" class="delete-button"><i class="fas fa-trash-alt"></i></a>
    </span>
               @endcan
                  </td>
            </tr>
            @php $ii++;@endphp
            @endforeach
            @if(count($team_data)!=0)
            @if($team_data->hasPages())
              <tr>
                <td colspan="7" id="mainpagenation">
                  {!! $team_data->links() !!}
                
                </td>
                </tr>
             @endif
             @endif
            @if(count($team_data)==0)
            <tr><td colspan="7">
<div class="no-items-found text-center mt-1">
        <i class="icon-magnifier fa-3x text-muted"></i>
        <p class="mb-0 mt-3"><strong>@lang(Lang::locale().'.NOT_FOUND_TITLE')</strong></p>
        <p class="text-muted">@lang(Lang::locale().'.NOT_FOUND_DESC')</p>
        @can('create-team')
       
          <button  data-href="/team/create" @if($organizationId==null || $organizationId==0) onclick="window.location.href='/team/create';" @else onclick="window.location.href='/team/create/{{$organizationId}}';" @endif   class="btn btn-primary createbtn">@lang(Lang::locale().'.CREATE_NEW') @lang(Lang::locale().'.TEAM_SINGULAR')
        </button>
        @endcan
      </div>
</tr></td>
@endif
            