<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myLargeModalLabel">@lang(Lang::locale().'.ADD') @lang(Lang::locale().'.PLAYER_PLURAL')</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <table class="table">
              <tr>
                <th>@lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.NAME')</th>
                <th>@lang(Lang::locale().'.ORGANIZATION_TOTAL_LICENSE')</th>
                <th>@lang(Lang::locale().'.ORGANIZATION_CONSUME_LICENSE')</th>
                <th>@lang(Lang::locale().'.ORGANIZATION_REMAIN_LICENSE')</th>
              </tr>
              <tr>
                <td>{{$org->name}}</td>
                <td>{{$org->no_of_license}}</td>
                <td>{{$org->consume_license}}</td>
                @php
                $remain=($org->no_of_license-$org->consume_license);
                @endphp
                <td>{{$remain}}</td>
              </tr>
            </table>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="form-group mb-3">
              <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')</label>
              <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')" id="popserach" onkeyup="popupsearch_data(this)" data-column_name="member_name" data-sort_type="asc" data-page="1" data-tbody="poptbody" data-loader="poploader" data-chkid="unSelectItem" data-submitbtnid="unsubmitbtn" data-controllername="{{request()->route()->getAction()['prefix']}}/0/{{$org->id}}/{{$team->id}}/{{$perpage}}" class="form-control">
            </div>

          </div>
          <div class="col-md-6 text-right mt-4">
            <div class="form-group">
              <p id=""> You have selected <span id="selected_member">0</span> out of <span id="total_lic">{{$remain}}</span></p>
              <p id="license_error" style="display: none;" class="alert alert-danger"></p>
            </div>
          </div>

        </div>
        <table class="table">
          <thead>
            <tr>
              <th>#</th>
              <th> <input type="checkbox" data-enable="1" id="unSelectItem" onclick="selectall(this,'unsubmitbtn');"> </th>
              <th>Member Name</th>
              <th>Email</th>
              <th>Department</th>
            </tr>
          </thead>
          <tbody id="poptbody">
            @include('team/search_unselected_member_data')

          </tbody>
        </table>
        <div id="poploader" style="display:none;">
          <div class="vue-content-placeholders vue-content-placeholders-is-animated">
            <div class="vue-content-placeholders-text">
              <div class="vue-content-placeholders-text__line"></div>
              <div class="vue-content-placeholders-text__line"></div>
              <div class="vue-content-placeholders-text__line"></div>
              <div class="vue-content-placeholders-text__line"></div>
            </div>
          </div>
        </div>
        <div id="popuppage">
          {!! $unselectedmembers->links() !!}
        </div>
        <div class="col-md-12 text-right">
          <button class="btn btn-primary" data-token="{{csrf_token()}}" data-orgid="{{$org->id}}" data-team_id="{{$team->id}}" data-posturl="players/save" type="button" id="unsubmitbtn" disabled="disabled" onclick="saveplayer(this)">@lang(Lang::locale().'.SAVE')</button>
        </div>
      </div>
    </div>
  </div>
</div>