@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card-header px-0 bg-transparent clearfix">
        <h4 class="float-left">@lang(Lang::locale().'.MANAGE') @lang(Lang::locale().'.PLAYER_PLURAL')</h4>
      </div>
      <div class="card-body px-0">
        @if(session()->has('message'))
         <div class="alert alert-{{ session()->get('message_type') }} alert-dismissible" role="alert">
         {{ session()->get('message') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
         </div>
      @endif
        <form method="POST" action="/players/update" autocomplete="off" data-title="@lang(Lang::locale().'.PLAYER_ADD_CONFIRM_TITLE')" data-message="@lang(Lang::locale().'.PLAYER_ADD_CONFIRM_TEXT')" data-cancelbtn="@lang(Lang::locale().'.CANCEL_BTN')" data-acceptbtn="@lang(Lang::locale().'.ACCEPT_BTN')">
          @if(count($errors))

          @endif
          <table class="table">
            <tr>
              <th>@lang(Lang::locale().'.ORGANIZATION_SINGULAR') @lang(Lang::locale().'.NAME')</th>
              <td colspan="2">{{$org->name}}</td>
              <th>@lang(Lang::locale().'.TEAM_SINGULAR') @lang(Lang::locale().'.NAME')</th>
              <td colspan="2">{{$team->name}}</td>
            </tr>
            <tr>
              <th class="width-15">@lang(Lang::locale().'.ORGANIZATION_TOTAL_LICENSE')</th>
              <td class="width-15">{{$org->no_of_license}}</td>
              <th class="width-15">@lang(Lang::locale().'.ORGANIZATION_CONSUME_LICENSE')</th>
              <td class="width-15">{{$org->consume_license}}</td>
              <th class="width-15">@lang(Lang::locale().'.ORGANIZATION_REMAIN_LICENSE')</th>
              <td class="width-15">{{($org->no_of_license-$org->consume_license)}}</td>
            </tr>
          </table>

          <div class="row mt-4 mt-3">

            <div class="col-md-4">
              <div class="form-group mb-3">
                <label for="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')">@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')</label>
                <input type="text" placeholder="@lang(Lang::locale().'.SEARCH') @lang(Lang::locale().'.PLAYER_PLURAL')" id="popserach" onkeyup="popupsearch_data(this)" data-column_name="member_name" data-sort_type="asc" data-page="1" data-tbody="tbody" data-loader="loader" data-chkid="selectallmemmain" data-submitbtnid="submitbtn" data-controllername="{{request()->route()->getAction()['prefix']}}/1/{{$org->id}}/{{$team->id}}/{{$perpage}}" class="form-control">
              </div>
            </div>
            <div class="col-md-4 text-right mt-4">
              @can('add_player')
              <button class="btn btn-primary" type="button" data-toggle="modal" data-target=".bd-example-modal-lg">@lang(Lang::locale().'.ADD') @lang(Lang::locale().'.PLAYER_PLURAL')</button>
              @endcan
            </div>
          </div>


          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th> <input type="checkbox" data-enable="1" id="selectallmemmain" onclick="selectall(this,'submitbtn');"> </th>
                <th>Member Name</th>
                <th>Email</th>
                <th>Department</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="tbody">
              @include('team/search_selected_member_data')

            </tbody>
          </table>
          <div id="loader" style="display:none;">
            <div class="vue-content-placeholders vue-content-placeholders-is-animated">
              <div class="vue-content-placeholders-text">
                <div class="vue-content-placeholders-text__line"></div>
                <div class="vue-content-placeholders-text__line"></div>
                <div class="vue-content-placeholders-text__line"></div>
                <div class="vue-content-placeholders-text__line"></div>
              </div>
            </div>
          </div>
          <input type="hidden" name="team_id" id="team_id" value="{{$team->id}}" />
          <input type="hidden" name="org_id" id="org_id" value="{{$org->id}}" />
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
          <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="member_name" />
          <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />
          <input type="hidden" name="cname" id="cname" value="{{request()->route()->getAction()['prefix']}}/1/{{$org->id}}/{{$perpage}}" />
          <div id="mainpagenation">
            {!! $members->links() !!}
          </div>
          <div class="col-md-12">
            <input type="submit" id="submitbtn" class="btn btn-primary" disabled="disabled" value="@lang(Lang::locale().'.SAVE')">
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
@include('team/add_player')
@endsection
