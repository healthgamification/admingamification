@extends('layouts.app')

@section('content')

<div class="container">
  <div class="card-header px-0 bg-transparent clearfix">
      <h4>{{ $teamData->name }}</h4>
  </div>

  <table class="table table-hover mt-3">
    <tbody>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.TEAM_SINGULAR') @lang(Lang::locale().'.NAME')</strong></td>
        <td class="width-30 wordbreak">{{ $teamData->name }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ORGANIZATION_SINGULAR')</strong></td>
        <td class="width-35 wordbreak">{{ $teamData->organization_name }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.TEAM_SINGULAR') @lang(Lang::locale().'.DESCRIPTION')</strong></td>
        <td class="width-30 wordbreak">{{ $teamData->description }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.TOTAL') @lang(Lang::locale().'.PLAYERS')</strong></td>
        <td class="width-35 wordbreak">{{ $teamData->total_player }}</td>
      </tr>
      <tr>
        <td class="width-20"><strong>@lang(Lang::locale().'.STATUS')</strong></td>
        <td class="width-30">{{ $teamData->status }}</td>
        <td class="width-15"><strong>@lang(Lang::locale().'.ADDED_AT')</strong></td>
        <td class="width-35 wordbreak">{{ custom_date_format($teamData->created_at) }}</td>
      </tr>
    </tbody>
  </table>
  @if(count($teamPlayerData) > 0)
  <div class="row">
    <div class="col-md-12">
      <div class="card-sub-header px-0 bg-transparent clearfix">
        <strong class="float-left pt-3">@lang(Lang::locale().'.PLAYER_SINGULAR') @lang(Lang::locale().'.DETAILS')</strong></div>
    </div>
  </div>
  <table class="table table-hover mt15">
    @php $i = 1 @endphp
    @foreach($teamPlayerData as $tpd)
    @if($i%2 == 1)
    <tr>
      @endif
      <td class="width-20"><strong>
          @lang(Lang::locale().'.PLAYER_SINGULAR') {{ $i }}
        </strong>
      </td>
      <td class="width-30">
        {{ $tpd->player->member_name }}
      </td>
      @if($i%2==0 || count($teamPlayerData)==$i)
    </tr>
    @endif
    @php $i++ @endphp
    @endforeach
  </table>
  @endif
  <div class='row'>
    <div class="col-md-12">
      @can('update-team')
      <a href="/team/{{$teamData->id}}/edit" class="btn btn-primary">@lang(Lang::locale().'.UPDATE') @lang(Lang::locale().'.TEAM_SINGULAR')</a>

      @endcan</div>
  </div>
</div>
</div>


@endsection
