<?php

return [

    /*
    |--------------------------------------------------------------------------
    | All The Application Label Text & Messages
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the application. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
     /*
    |--------------------------------------------------------------------------
    | All Aplication
    |--------------------------------------------------------------------------
    |
    */
    'NEW'=> 'New',
    'UPDATE'=> 'Update',
    'ABOUT'=> 'About',
    'SEARCH'=> 'Search',
    'ID'=> 'id',
    'NAME'=> 'Name',
    'EMAIL'=> 'Email Id',
    'PHONE'=> 'Phone',
    'MOBILE'=> 'Mobile',
    'ADDRESS'=> 'Address',
    'ACTION'=> 'Action',
    'STATUS'=> 'Status',
    'STATUS_ERROR'=> 'Please select status',
    'ACTIVE'=> 'Active',
    'INACTIVE'=> 'Inactive',
    'SUBMIT'=> 'Submit',
    'DEPATRMENT'=> 'Department',
    'DESIGNATION'=> 'Designation',
    'SELECT'=> 'Select',
    'TITLE'=> 'Title',
    'OPTIONS'=> 'Options',
    'OPTION'=> 'Option',
    'ADD_MORE'=> 'Add More',
    'SETTINGS'=> 'Settings',
    'USERS'=> 'Users',
    'PROFILE'=> 'Profile',
    'PASSWORD'=> 'Password',
    'LOGOUT'=> 'Logout',
    'MY_DASHBOARD'=> 'My Dashboard',
    'MY_ORGANIZATION'=> 'My Organization',
    'DESCRIPTION'=> 'Description',
    'USER_MANAGEMENT'=> 'User Management',
     'CREATE_NEW'=> 'Create New',
     'VIEW_EDIT'=> 'View / Edit',
    'CANCEL_BTN'=> 'No, cancel it!',
    'ACCEPT_BTN'=> 'Yes, I am sure!',
    'NOT_FOUND_TITLE'=> 'Could not find any items',
    'NOT_FOUND_DESC'=> 'Try changing the filters or add a new one',
    'REGISTERED_ON'=>"Registered On",
    "FULL"=>'Full',
    'ROLE'=>'Role',
    'PASSWORD_ERROR'=>'Please enter your password.',
    'ROLE_ERROR'=>'Please select user role.',
    'BTN_BACK'=>'Cancel',
    'AVATAR'=>'Avatar',
    'CURRENT'=>'Current',
    'CONFIRM'=>'Confirm',
    'ADD'=>'Add',
    'SAVE'=>'Save',
    'DELETE'=>'Delete',
    'ADMINS'=>'Admins',
    'MAKE'=>'Make',
    'REMOVE'=>'Remove',
    'ADMIN'=>'Admin',
    "EXPORT"=>"Exports",
    "SAVE_AND_NEW"=>"Save & New",
    "SAVE_AND_EXIT"=>"Save & Exit",
    "LANGUAGE"=>"Language",
    "ALL"=>"All",
    "CATEGORY"=>"Category",
    "DESCRIPTION"=>"Description",
    "ASSIGN"=>"Assign",    
    "ASSIGNED"=>"Assigned",    
    'MANAGE'=>"Manage",
    'WEEK'=>"Week",
    'DEFAULT'=>"Default",
    'ARE_YOU_SURE'=>"Are you sure ?",
    'FIRST'=>"First",
    'BACK'=>"Back",
    'BY'=>"by",
    'SORT_BY'=>"Sort by",
    'SELECTED' => 'Selected',
    'UNSELECTED' => 'Unselected',
    'TOTAL' => 'Total',
    'IS'=>'Is',
    'YES'=>'Yes',
    'NO'=>'No',
    'DETAILS'=>'Details',
    'EDIT'=>'Edit',
    'APP_NAME' => 'Health Gamification System',
    'POINT'=>'Points',
    'PERFORMED' => 'performed',
    'RESET_YOUR_PASSWORD' => 'Reset your password',
    'RESET_PASSWORD' => 'Reset password',
    'EMAIL_ADDRESS' => 'Email Address',
    'SEND_PASSWORD_RESET_LINK' => 'Send Password Reset Link',
    'LOGIN' => 'Login',
    'SIGN_IN_TO_YOUR_ACCOUNT' => 'Sign In to your account',
    'FORGET_YOUR_PASSWORD'=>'Forgot Your Password?',
    'REMEMBER_ME'=>'Remember Me',
    /*
    |--------------------------------------------------------------------------
    | Organization Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the application. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    
    'ORGANIZATION_PLURAL'=> 'Organizations',
    'ORGANIZATION_SINGULAR'=> 'Organization',
    'ORGANIZATION_NAME_PLACEHOLDER'=> 'Enter Organization Name',
    'ORGANIZATION_EMAIL_PLACEHOLDER'=> 'Enter Organization Email Id',
    'ORGANIZATION_PHONE_PLACEHOLDER'=> 'Enter Organization Phone',
    'ORGANIZATION_PHONE_ERROR_MSG'=> 'Format for phone number is xx xx xx xx or xxxxxxxx',
    'ORGANIZATION_MOBILE_PLACEHOLDER'=> 'Enter Organization mobile',
    'ORGANIZATION_MOBILE_ERROR_MSG'=> 'Format for mobile number is xx xx xx xx or xxxxxxxx',
    'ORGANIZATION_ADDRESS_PLACEHOLDER'=> 'Enter Organization Address',
    'ORGANIZATION_ABOUT_PLACEHOLDER'=> 'Enter Organization Details',
    'ORGANIZATION_NO_OF_QIZZ'=> 'Total quizzes for the week (max 9)',
    'ORGANIZATION_NO_OF_QIZZ_PLACEHOLDER'=> 'Total quizzes for the week',
    'ORGANIZATION_NO_OF_CHALLENGE'=> 'Total challenges for the Week (max 9)',
    'ORGANIZATION_NO_OF_CHALLENGE_PLACEHOLDER'=> 'Total challenges for the Week',
    'ORGANIZATION_TOTAL_LICENSE'=> 'Total License',
    'ORGANIZATION_TOTAL_LICENSE_PLACEHOLDER'=> 'Total License',
    'ORGANIZATION_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'ORGANIZATION_SAVE_CONFIRM_TEXT'=> 'want to save this organization.',
    'ORGANIZATION_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'ORGANIZATION_EDIT_CONFIRM_TEXT'=> 'want to edit this organization.',
    'ORGANIZATION_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'ORGANIZATION_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this organization!',
    'ORGANIZATION_CREATE_MSG'=> 'Organization created!',
    'ORGANIZATION_DELETE_MSG'=> 'Organization deleted!',
    'ORGANIZATION_EDIT_MSG'=> 'Organization updated!',
    'ORGANIZATION_CHALLENGE_LIMIT_MSG' => 'If you change this value, it will impact the employees challenge limit for the week',
    'ORGANIZATION_QUIZ_LIMIT_MSG' => 'If you change this value, it will impact the employees quiz limit for the week',
     'ORGANIZATION_TOTAL_LICENSE'=> 'Total License',
    'ORGANIZATION_CONSUME_LICENSE'=> 'Consumed License',
    'ORGANIZATION_REMAIN_LICENSE'=> 'Remaining License',
    
   //Ogranization User 

   'USER_MAKE_ADMIN_CONFIRM_TITLE'=> 'Are you sure?',
   'USER_MAKE_ADMIN_CONFIRM_TEXT'=> 'want to make this user to be admin?',
   'USER_REMOVE_ADMIN_CONFIRM_TITLE'=> 'Are you sure?',
   'USER_REMOVE_ADMIN_CONFIRM_TEXT'=> 'want to remove this user from admin role?',
   'USER_MAKE_ADMIN_MSG'=> 'Admin role has been assigned to this employee.',
   'USER_REMOVE_ADMIN_MSG'=> 'Employee has been removed from admin role.',
   'ADMIN_UASER_DELETE_MSG'=> 'Admin user deleted!',
   'USER_MAKE_ADMIN'=> 'Assign admin role',
    'USER_REMOVE_ADMIN'=> 'Remove admin role',

   /*
    |--------------------------------------------------------------------------
    | Employee Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Employee Management. 
    |
    */
    
    'MEMBER_PLURAL'=> 'Employees',
    'MEMBER_SINGULAR'=> 'Employee',
    'MEMBER_NAME_PLACEHOLDER'=> 'Enter employee name',
    'MEMBER_NAME_ERROR_MSG'=> 'Please enter employee name',
    'MEMBER_EMAIL_PLACEHOLDER'=> 'Enter employee email id',
    'MEMBER_EMAIL_ERROR_MSG'=> 'Please enter employee email id',
    'MEMBER_DEPATRMENT_PLACEHOLDER'=> 'Enter employee department',
    'MEMBER_DESIGNATION_PLACEHOLDER'=> 'Enter employee designation',
    'MEMBER_ORGANIZATION_ERROR_MSG'=> 'Please select organization',
    'MEMBER_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'MEMBER_SAVE_CONFIRM_TEXT'=> 'want to save this employee.',
    'MEMBER_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'MEMBER_EDIT_CONFIRM_TEXT'=> 'want to edit this employee.',
    'MEMBER_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'MEMBER_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this employee!',
    'MEMBER_CREATE_MSG'=> 'Employee created!',
    'MEMBER_DELETE_MSG'=> 'Employee deleted!',
    'MEMBER_EDIT_MSG'=> 'Employee updated!',
    'MEMBER_BULK_UPLOAD_BTN_TEXT'=> 'Bulk Employees Upload',
    'MEMBER_BULK_UPLOAD_BTN_TEXT_AFTER'=> 'Click Here To Upload',
    'MEMBER_BULK_UPLOAD_SUCCESS_MSG'=> 'Bulk Employees uploaded!',
    // 'MEMBER_BULK_UPLOAD_NO_OF_DUPLICATE'=> ' duplicate email found and ignore them and rest data have been uploaded!',
    'MEMBER_BULK_UPLOAD_NO_OF_DUPLICATE'=> ':wrongdesig wrong designation found, :wrongdepart wrong department found, :wrongmail wrong email found, :duplicate duplicate email found, :wrongorg wrong organization found and :validation required field mismatch found ignore them and :uploaded employess have been uploaded!',
    'MEMBER_BULK_UPLOAD_ERROR_MSG'=> 'Please upload only csv file.',
    'MEMBER_BULK_UPLOAD_CONFIRM_TEXT'=> 'want to upload this file.',
    'MEMBER_CHOOSE_FILE_TEXT'=>'Choose file',
    'MEMBER_NO_OF_QIZZ'=> 'Total quizzes for the week (max 9)',
    'MEMBER_NO_OF_QIZZ_PLACEHOLDER'=> 'Total quizzes for the week (max 9)',
    'MEMBER_NO_OF_CHALLENGE'=> 'Total challenges for the Week (max 9)',
    'MEMBER_NO_OF_CHALLENGE_PLACEHOLDER'=> 'Total challenges for the Week (max 9)',
   
  /*
    |--------------------------------------------------------------------------
    | Challenges Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Challenge Management. 
    |
    */
    
    'CHALLENGE_PLURAL'=> 'Challenges',
    'CHALLENGE_SINGULAR'=> 'Challenge',
    'CHALLENGE_BANK'=> 'Challenge Bank',
    'CHALLENGE_TITLE_PLACEHOLDER'=> 'Enter challenge name',
    'CHALLENGE_TITLE_ERROR_MSG'=> 'Please enter challenge name',
    'CHALLENGE_DESCRIPTION_ERROR_MSG'=> 'Please enter description',
    'CHALLENGE_OPTION_PLACEHOLDER'=> 'Enter option',
    'CHALLENGE_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_SAVE_CONFIRM_TEXT'=> 'want to save this challenge.',
    'CHALLENGE_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_EDIT_CONFIRM_TEXT'=> 'want to edit this challenge.',
    'CHALLENGE_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this challenge!',
    'CHALLENGE_CREATE_MSG'=> 'Challenge created!',
    'CHALLENGE_DELETE_MSG'=> 'Challenge deleted!',
    'CHALLENGE_EDIT_MSG'=> 'Challenge updated!', 
    'CHALLENGE_BULK_UPLOAD_BTN_TEXT'=> 'Bulk Challenge Upload',
    'CHALLENGE_BULK_UPLOAD_BTN_TEXT_AFTER'=> 'Click Here To Upload',
    'CHALLENGE_BULK_UPLOAD_SUCCESS_MSG'=> 'Bulk Challege uploaded!',
    'CHALLENGE_BULK_UPLOAD_NO_OF_DUPLICATE'=> ':wronglang wrong Language found, :duplicate duplicate challenge found, :wrongcat wrong category name found, :wrongcomplexity wrong complexity found and :validation required field mismatch found ignore them and :uploaded challenges have been uploaded!',
    'CHALLENGE_BULK_UPLOAD_ERROR_MSG'=> 'Please upload only csv file.',
    'CHALLENGE_BULK_UPLOAD_LENGTH_ERROR_MSG'=>'Maximum Challenge Limit is 500. Please upload challenge file with maximum 500 challenges.',
    'CHALLENGE_BULK_UPLOAD_CONFIRM_TEXT'=> 'want to upload this file.',
     'CHALLENGE_CHOOSE_FILE_TEXT'=>'Choose file',
    'CHALLENGE_COMPLEXITY' => 'Challenge Complexity',
    'CHALLENGE_COMPLEXITY_PLACEHOLDER' => 'Enter Challenge Complexity',
    'CHALLENGE_POINT_PLACEHOLDER' => 'Enter Point',
    'CHALLENGE_COMPLETE_DAYS' => 'No. of Days to Complete',
    'CHALLENGE_COMPLETE_DAYS_PLACEHOLDER' => 'Enter no. of days to complete',
    'BULK_UPLOAD' => 'Bulk upload',

    /*
    |--------------------------------------------------------------------------
    | Teams Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Teams Management. 
    |
    */
    
    'TEAM_PLURAL'=> 'Teams',
    'TEAM_SINGULAR'=> 'Team',
    'PLAYERS'=> 'Players',
    'LOGO'=> 'Logo',
    'TEAM_TITLE_PLACEHOLDER'=> 'Enter team name',
    'TEAM_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'TEAM_SAVE_CONFIRM_TEXT'=> 'want to save this team.',
    'TEAM_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'TEAM_EDIT_CONFIRM_TEXT'=> 'want to edit this team.',
    'TEAM_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'TEAM_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this team!',
    'TEAM_CREATE_MSG'=> 'Team created!',
    'TEAM_DELETE_MSG'=> 'Team deleted!',
    'TEAM_EDIT_MSG'=> 'Team updated!', 
    'TEAM_DESCRIPTION_PLACEHOLDER' => 'Enter the Team Description', 

     //Payer
     'PLAYER_PLURAL'=> 'Players',
     'PLAYER_SINGULAR'=>'Player',
     'PLAYER_MANAGE'=>'Manage Player',
     'PLAYER_ADD_CONFIRM_TITLE'=>'Are you sure?',
     'PLAYER_ADD_CONFIRM_TEXT'=>'want to edit this player.',
     'PLAYER_ADD_MSG'=> 'Players added!',
     'PLAYER_UPDATE_MSG'=> 'Players updated!',

    /*
    |--------------------------------------------------------------------------
    | User Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the User Management. 
    |
    */
    
    'USER_PLURAL'=> 'Users',
    'USER_SINGULAR'=> 'User',
    'USER_NAME_PLACEHOLDER'=> 'Enter user name',
    'USER_NAME_ERROR_MSG'=> 'Please enter user name',
    'USER_EMAIL_PLACEHOLDER'=> 'Enter user email id',
    'USER_EMAIL_ERROR_MSG'=> 'Please enter user email id',
    'USER_DEPATRMENT_PLACEHOLDER'=> 'Enter user department',
    'USER_DESIGNATION_PLACEHOLDER'=> 'Enter user department',
    'USER_ORGANIZATION_ERROR_MSG'=> 'Please select organization',
    'USER_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'USER_SAVE_CONFIRM_TEXT'=> 'want to save this user.',
    'USER_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'USER_EDIT_CONFIRM_TEXT'=> 'want to edit this user.',
    'USER_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'USER_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this user!',
    'USER_CREATE_MSG'=> 'User created!',
    'USER_DELETE_MSG'=> 'User deleted!',
    'USER_EDIT_MSG'=> 'User updated!',
    'PASSWORD_PLACEHOLDER' => 'Enter Password',
    'NEW_PASSWORD_PLACEHOLDER' => 'Enter New Password',

 
     /*
    |--------------------------------------------------------------------------
    | Profile Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Profile Management. 
    |
    */
    
    'PROFILE_PLURAL'=> 'Profiles',
    'PROFILE_SINGULAR'=> 'Profile',
    'PROFILE_NAME_PLACEHOLDER'=> 'Enter user name',
    'PROFILE_NAME_ERROR_MSG'=> 'Please enter user name',
    'PROFILE_EMAIL_PLACEHOLDER'=> 'Enter user email id',
    'PROFILE_EMAIL_ERROR_MSG'=> 'Please enter user email id',
    'PROFILE_DEPATRMENT_PLACEHOLDER'=> 'Enter user department',
    'PROFILE_DESIGNATION_PLACEHOLDER'=> 'Enter user department',
    'PROFILE_ORGANIZATION_ERROR_MSG'=> 'Please select organization',
    'PROFILE_VIEW_CONFIRM_TITLE'=> 'Are you sure?',
    'PROFILE_VIEW_CONFIRM_TEXT'=> 'want to view the profile.',
    'PROFILE_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'PROFILE_EDIT_CONFIRM_TEXT'=> 'want to edit the profile.',
    'PROFILE_EDIT_PASSWORD_CONFIRM_TEXT'=> 'want to update your password.',
    'PROFILE_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'PROFILE_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover the profile!',
    'PROFILE_CREATE_MSG'=> 'User created!',
    'PROFILE_DELETE_MSG'=> 'User deleted!',
    'PROFILE_EDIT_MSG'=> 'Profile updated!',
    'PROFILE_PASSWORD_EDIT_MSG'=> 'Password updated!',
    'PROFILE_IMAGE_CHANGE_BTN'=> 'Change Profile Image',
    'PROFILE_IMAGE_RESET_BTN'=> 'Reset Profile Image',
    'PROFILE_PASSWORD_CURRENT_PLACEHOLDER'=> 'Please enter your current password.',
    'PROFILE_PASSWORD_CURRENT_ERROR_MSG'=> 'Please enter your current password.',
    'PROFILE_PASSWORD_PLACEHOLDER'=> 'Please enter your new password.',
    'PROFILE_PASSWORD_ERROR_MSG'=> 'Please enter your new password.', 
    'PROFILE_CONFIRM_PASSWORD_PLACEHOLDER'=> 'Please re-enter your new password.',
    'PROFILE_CONFIRM_PASSWORD_ERROR_MSG'=> 'Please re-enter your new password.',
    'PROFILE_IMAGE_MAX_SIZE'=> 'The maximum file size allowed is',
    'PROFILE_AVATAR_TEXT'=>'You can change your avatar here or remove the current avatar',
    'PROFILE_NAME_HINT'=>'Enter your name, so people you know can recognize you.',
    'PROFILE_EMAIL_HINT'=>'This email will be displayed on your public profile.',

    /*
    |--------------------------------------------------------------------------
    | Challenges Category Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Challenge Category Management. 
    |
    */
    
    'CHALLENGE_CATEGORY_PLURAL'=> 'Challenge Categories',
    'CHALLENGE_CATEGORY_SINGULAR'=> 'Challenge Category',
    'CHALLENGE_CATEGORY_TITLE_PLACEHOLDER'=> 'Enter title',
    'CHALLENGE_CATEGORY_DESCRIPTION_ERROR_MSG'=> 'Please enter description',
    'CHALLENGE_CATEGORY_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_CATEGORY_SAVE_CONFIRM_TEXT'=> 'want to save this challenge category.',
    'CHALLENGE_CATEGORY_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_CATEGORY_EDIT_CONFIRM_TEXT'=> 'want to edit this challenge category.',
    'CHALLENGE_CATEGORY_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_CATEGORY_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this challenge category!',
    'CHALLENGE_CATEGORY_CREATE_MSG'=> 'Challenge category created!',
    'CHALLENGE_CATEGORY_DELETE_MSG'=> 'Challenge category deleted!',
    'CHALLENGE_CATEGORY_EDIT_MSG'=> 'Challenge category updated!',   
    'CHALLENGE_CATEGORY_DESCRIPTION_PLACEHOLDER'=> 'Enter Category Description',

    /*
    |--------------------------------------------------------------------------
    | Challenges Category Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Challenge Category Management. 
    |
    */
    
    'CHALLENGE_SET_PLURAL'=> 'Challenge Sets',
    'CHALLENGE_SET_SINGULAR'=> 'Challenge Set',
    'CHALLENGE_SET_NAME_PLACEHOLDER'=> 'Enter Challenge Set Name',
    'CHALLENGE_SET_TITLE_PLACEHOLDER'=> 'Enter title',
    'CHALLENGE_SET_DESCRIPTION_ERROR_MSG'=> 'Please enter description',
    'CHALLENGE_SET_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_SET_SAVE_CONFIRM_TEXT'=> 'want to save this challenge set.',
    'CHALLENGE_SET_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_SET_EDIT_CONFIRM_TEXT'=> 'want to edit this challenge set.',
    'CHALLENGE_SET_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'CHALLENGE_SET_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this challenge set!',
    'CHALLENGE_SET_CREATE_MSG'=> 'Challenge set created!',
    'CHALLENGE_SET_DELETE_MSG'=> 'Challenge set deleted!',
    'CHALLENGE_SET_EDIT_MSG'=> 'Challenge set updated!',
    'ADD_CHALLENGES'=> 'Add challenges to challenge set',
    'CHALLENGE_SET_DESCRIPTION_PLACEHOLDER' => 'Enter the Challenge Set Description',



    /*
    |--------------------------------------------------------------------------
    | Export Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Export Management. 
    |
    */
    
    'EXPORT_PLURAL'=> 'Exports',
    'EXPORT_SINGULAR'=> 'Export',
    'EXPORT_CONFIRM_TITLE'=> 'Are you sure?',
    'EXPORT_CONFIRM_TEXT'=> 'want to export this.',
    'EXPORT_MSG'=> 'File Downloded Successfully!',
    'EXPORT_ERROR_MSG'=> 'File Not Found!',
    'SELECT_EXPORT_TYPE'=> '-- Select your download type --',
    'EXPORT_EMPLOYEE_SAMPLE_FILE'=> 'Download Employee Sample File ',
    'EXPORT_ORGANIZATION_LIST'=> 'Download Organization List',
    'EXPORT_CHALLENGE_SAMPLE_FILE'=> 'Download Challenge Sample File',
    'EXPORT_CHALLENGE_CATEGORY_LIST'=> 'Download Challenge Category List',
    'EXPORT_QUESTION_SAMPLE_FILE'=> 'Download Question Sample File',



     /*
    |--------------------------------------------------------------------------
    | Question Bank Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Question Bank Management. 
    |
    */
    'ADDED_AT'=> 'Created on',
    'VIEW'=> 'View',
    'QUESTION_BANK'=> 'Question Bank',
    'QUESTION_PLURAL' => 'Questions', 
    'QUESTION' => 'Question', 
    'QUESTION_CATEGORY_NAME' => 'Category Name',
    'QUESTION_CATEGORIES' => 'Question Categories', 
    'QUESTION_CATEGORY' => 'Question Category',
    'QUESTION_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SAVE_CONFIRM_TEXT'=> 'want to save this question.',
    'QUESTION_CATEGORY_SAVE_CONFIRM_TEXT'=> 'want to save this Quiz category.',
    'QUESTION_CATEGORY_NAME_PLACEHOLDER' => 'Enter Category Name',
    'QUESTION_CATEGORY_CREATE_MSG'=> 'Quiz Category created!',
    'QUESTION_CATEGORY_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_CATEGORY_EDIT_CONFIRM_TEXT'=> 'want to edit this category.',
    'QUESTION_EDIT_CONFIRM_TEXT'=> 'want to edit this question.',
    'QUESTION_CATEGORY_DELETE_MSG'=> 'Category deleted!',
    'QUESTION_CATEGORY_EDIT_MSG'=> 'Category updated!',
    'QUESTION_PLACEHOLDER' => 'Enter Question',
    'WEIGHTAGE' => 'Weightage',
    'QUESTION_WEIGHTAGE' => 'Enter Weightage',
    'ANSWER_PLACEHOLDER' => 'Enter Option',
    'SELECT_ANSWER' => 'Select One Answer',
    'SELECT_LANGUAGE' => 'Select Language',
    'ENGLISH' => 'English',
    'DANISH' => 'Danish',
    'QUESTION_CATEGORY_PLACEHOLDER'=>'Enter Category',
    'QUESTION_CREATE_MSG' => 'Question created!',
    'QUESTION_EDIT_MSG' => 'Question Updated!',
    'QUESTION_DELETE_MSG'=> 'Question deleted!',
    'QUESTION_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this question!',
    'QUESTION_BULK_UPLOAD_NO_OF_DUPLICATE'=> ':wronglang wrong language found, :duplicate duplicate question found and :wrongcat wrong category name found :validation required field mismatch found ignore them and :uploaded question have been uploaded!',
    'QUESTION_BULK_UPLOAD_BTN_TEXT'=> 'Bulk Questions Upload',
    'QUESTION_BULK_UPLOAD_BTN_TEXT_AFTER'=> 'Click Here To Upload',
    'QUESTION_BULK_UPLOAD_SUCCESS_MSG'=> 'Bulk Questions uploaded!',
    'QUESTION_BULK_UPLOAD_ERROR_MSG'=> 'Please upload only csv file.',
    'QUESTION_BULK_UPLOAD_CONFIRM_TEXT'=> 'want to upload this file.',
    'QUESTION_CHOOSE_FILE_TEXT'=>'Choose file',
    'QUESTION_FACT' => 'Fact about Question',
    'QUESTION_FACT_PLACEHOLDER' => 'Enter fact about Question',
    'QUESTION_BULK_UPLOAD_LENGTH_ERROR_MSG'=>'Maximum question Limit is 500. Please upload question file with maximum 500 questions.',
    'QUESTION_CATEGORY_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this question category!',

 /*
    |--------------------------------------------------------------------------
    | Question Set Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Question Set Management. 
    |
    */
    'QUESTION_SET_PLURAL'=> 'Quiz Sets',
    'QUESTION_SET_SINGULAR'=> 'Quiz Set',
    'QUIZ_SINGULAR'=> 'Quiz',
    'QUESTION_SET_NAME_PLACEHOLDER'=> 'Enter Quiz Set Name',
    'QUESTION_SET_TITLE_PLACEHOLDER'=> 'Enter title',
    'QUESTION_SET_DESCRIPTION_ERROR_MSG'=> 'Please enter description',
    'QUESTION_SET_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SET_SAVE_CONFIRM_TEXT'=> 'want to save this Quiz set.',
    'QUESTION_SET_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SET_EDIT_CONFIRM_TEXT'=> 'want to edit this Quiz set.',
    'QUESTION_SET_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SET_DESCRIPTION_PLACEHOLDER'=> 'Enter Question Set Description',
    'QUESTION_SET_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this Quiz set!',
    'QUESTION_SET_CREATE_MSG'=> 'Quiz set created!',
    'QUESTION_SET_DELETE_MSG'=> 'Quiz set deleted!',
    'QUESTION_SET_EDIT_MSG'=> 'Quiz set updated!',
    'ADD_QUESTIONS'=> 'Add Question to quizz set',
    'TYPE'=> 'Type',
/*
    |--------------------------------------------------------------------------
    | Assign Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Assign Set Management. 
    |
    */
    'WEEK_PLURAL'=> 'Weeks',
    'WEEK_SINGULAR'=> 'Week',
    'QUIZ_SAVE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUIZ_SAVE_CONFIRM_TEXT'=> 'want to save these weeks, quiz and challenge sets.',
    'QUESTION_SET_EDIT_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SET_EDIT_CONFIRM_TEXT'=> 'want to edit this Quiz set.',
    'QUESTION_SET_DELETE_CONFIRM_TITLE'=> 'Are you sure?',
    'QUESTION_SET_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this Quiz set!',
    'QUESTION_SET_CREATE_MSG'=> 'Quiz set created!',
    'QUESTION_SET_DELETE_MSG'=> 'Quiz set deleted!',
    'QUESTION_SET_EDIT_MSG'=> 'Quiz set updated!',
    'ASSIGN_CREATE_MSG'=> 'Quiz and challenge sets assigned!',
    'ASSIGN_DELETE_MSG'=> 'Quiz and challenge sets deleted!',
    'ASSIGN_EDIT_MSG'=> 'Quiz and challenge set updated!',
    'ASSIGN_BTN_TXT'=>"Quizzes / Challenges Sets",
    "START_DATE"=>"Start Date",
    "END_DATE"=>"End Date",

    /*
    |--------------------------------------------------------------------------
    | Language Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Language Management. 
    |
    */
    'LANGUAGE_PLURAL'=> 'Languages',
    'LANGUAGE_SINGULAR'=> 'Language',
    'MANAGE_TRANSLATION'=> 'Manage Translations',
    'LANGUAGE_CODE'=> 'Language Code',
    'LANGUAGE_NAME'=> 'Language Name',
    'LANGUAGE_SAVE_CONFIRM_TEXT'=> 'want to save language data.',
    'TRANSLATION_SAVE_CONFIRM_TEXT'=> 'want to save translation data.',
    'TRANSLATION_CREATE_MSG'=> 'Language translation has been saved successfully.',
    'LANGUAGE_DELETE_CONFIRM_TEXT'=> 'want to delete this language.',
    'LANGUAGE_CREATE_MSG'=> 'Language has been saved successfully.',
    'LANGUAGE_UPDATE_MSG'=> 'Language has been updated successfully.',
    'LANGUAGE_DELETE_MSG'=> 'Language has been deleted successfully.',
    'LANGUAGE_MENU'=> 'Language',

 /*
    |--------------------------------------------------------------------------
    | Department Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Department Management. 
    |
    */

    'DEPARTMENT_PLURAL'=> 'Departments',
    'DEPARTMENT_SINGULAR'=> 'Department',
    'DEPARTMENT_NAME_PLACEHOLDER'=> 'Enter department name',
    'DEPARTMENT_SAVE_CONFIRM_TEXT'=> 'want to save this department.',
    'DEPARTMENT_EDIT_CONFIRM_TEXT'=> 'want to edit this department.',
    'DEPARTMENT_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this department!',
    'DEPARTMENT_CREATE_MSG'=> 'Department created!',
    'DEPARTMENT_DELETE_MSG'=> 'Department deleted!',
    'DEPARTMENT_UPDATE_MSG'=> 'Department updated!',
    'DEPARTMENT_BULK_UPLOAD_BTN_TEXT'=> 'Bulk Employees Upload',
    'DEPARTMENT_BULK_UPLOAD_BTN_TEXT_AFTER'=> 'Click Here To Upload',
    'DEPARTMENT_BULK_UPLOAD_SUCCESS_MSG'=> 'Bulk members uploaded!',
    'DEPARTMENT_NOT_FOUND_MSG'=> 'Selected department not found.',


    /*
    |--------------------------------------------------------------------------
    | Designation Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Designation Management. 
    |
    */

    'DESIGNATION_PLURAL'=> 'Designations',
    'DESIGNATION_SINGULAR'=> 'Designation',
    'DESIGNATION_NAME_PLACEHOLDER'=> 'Enter designation name',
    'DESIGNATION_SAVE_CONFIRM_TEXT'=> 'want to save this designation.',
    'DESIGNATION_EDIT_CONFIRM_TEXT'=> 'want to edit this designation.',
    'DESIGNATION_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this designation!',
    'DESIGNATION_CREATE_MSG'=> 'Designation created!',
    'DESIGNATION_DELETE_MSG'=> 'Designation deleted!',
    'DESIGNATION_UPDATE_MSG'=> 'Designation updated!',
    'DESIGNATION_BULK_UPLOAD_BTN_TEXT'=> 'Bulk Employees Upload',
    'DESIGNATION_BULK_UPLOAD_BTN_TEXT_AFTER'=> 'Click Here To Upload',
    'DESIGNATION_BULK_UPLOAD_SUCCESS_MSG'=> 'Bulk members uploaded!',    
    'DESIGNATION_NOT_FOUND_MSG'=> 'Selected designation not found.',


     /*
    |--------------------------------------------------------------------------
    | Email Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Email Management. 
    |
    */

    'EMAIL_PLURAL'=> 'Email SMTP Settings',
    'EMAIL_SINGULAR'=> 'Email SMTP Setting',
    'EMAIL_DRIVER'=> 'Driver',
    'EMAIL_HOST'=> 'Host',
    'EMAIL_PORT'=> 'Port',
    'EMAIL_FROM'=> 'From',
    'EMAIL_FROM_DEMO'=> 'Email:Name (demo@demo.com:Demo)',
    'EMAIL_ENCRYPTION'=> 'Encryption',
    'EMAIL_USERNAME'=> 'Username',
    'EMAIL_PASSWORD'=> 'Password',
    'EMAIL_PATH'=> 'Path',
    'EMAIL_PRETEND'=> 'Pretend',
    'EMAIL_SAVE_BTN_TXT'=> 'Save configuration',
    'EMAIL_TEST_MAIL_BTN_TXT'=> 'Send Test Mail',
    'EMAIL_CONFIRM_TEXT'=> 'want to save this configuration.',    
    'EMAIL_TEST_CONFIRM_TEXT'=> 'want to send test email.',    
    'EMAIL_CREATE_MSG'=> 'Email configuration has been saved.',
    'EMAIL_TEST_MAIL'=> 'Test Email',
    'EMAIL_SEND_BTN_TXT'=> 'Send Test Email',
    'EMAIL_TEST_SEND_MSG'=> 'Test Email has been send.',
    'EMAIL_SETUP'=> 'Email SMTP',



    /*
    |--------------------------------------------------------------------------
    | Email Template Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Email Template Management. 
    |
    */

    'TEMPLATE_PLURAL'=> 'Email Templates',
    'TEMPLATE_SINGULAR'=> 'Email Template',
    'TEMPLATE_TYPE'=> 'Template Type',
    'TEMPLATE_SUBJECT'=> 'Template Subject',
    'TEMPLATE_BODY'=> 'Template Body',
    'TEMPLATE_BODY_MSG'=> 'Please use below variables to put dynamic content to the template.',
    'TEMPLATE_NAME_PLACEHOLDER'=> 'Enter template name',
    'TEMPLATE_SAVE_CONFIRM_TEXT'=> 'want to save this template.',
    'TEMPLATE_EDIT_CONFIRM_TEXT'=> 'want to edit this template.',
    'TEMPLATE_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this template!',
    'TEMPLATE_CREATE_MSG'=> 'Template created!',
    'TEMPLATE_DELETE_MSG'=> 'Template deleted!',
    'TEMPLATE_UPDATE_MSG'=> 'Template updated!',   
    'TEMPLATE_NOT_FOUND_MSG'=> 'Selected template not found.',


     /*
    |--------------------------------------------------------------------------
    | Log Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Log Management. 
    |
    */

    'LOG_PLURAL'=> 'Logs',
    'LOG_SINGULAR'=> 'Log',
    'LOG_TYPE'=> 'Log Type',
    'LOG'=> 'Search :type Log',
    'TEMPLATE_SUBJECT'=> 'Template Subject',
    'TEMPLATE_BODY'=> 'Template Body',
    'TEMPLATE_BODY_MSG'=> 'Please use below variables to put dynamic content to the template.',
    'TEMPLATE_NAME_PLACEHOLDER'=> 'Enter template name',
    'TEMPLATE_SAVE_CONFIRM_TEXT'=> 'want to save this template.',
    'TEMPLATE_EDIT_CONFIRM_TEXT'=> 'want to edit this template.',
    'TEMPLATE_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this template!',
    'TEMPLATE_CREATE_MSG'=> 'Template created!',
    'TEMPLATE_DELETE_MSG'=> 'Template deleted!',
    'TEMPLATE_UPDATE_MSG'=> 'Template updated!',   
    'TEMPLATE_NOT_FOUND_MSG'=> 'Selected template not found.',

    /*
    |--------------------------------------------------------------------------
    | Role Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Role Management. 
    |
    */
    'ROLE_PLURAL'=> 'Roles',
    'ROLE_SINGULAR'=> 'Role',
    'TOTAL_PERMISSION' => 'Total Permission',
    'TOTAL_USER' => 'Total User',
    'ROLE_DELETE_CONFIRM_TEXT'=> 'Once deleted, you will not be able to recover this role!',
    'ROLE_NAME_PLACEHOLDER' => 'Enter Role Name',
    'ROLE_TITLE_PLACEHOLDER' => 'Enter Role Title',
    'PERMISSION_PLURAL' => 'Permissions',
    'PERMISSION_SINGULAR' => 'Permission',
    'ROLE_SAVE_CONFIRM_TEXT'=>'Want to save this Role.',
    'ROLE_EDIT_CONFIRM_TEXT'=> 'Want to edit this role.',
    'ROLE_CREATE_MSG' => 'Role Created!',
    'ROLE_DELETE_MSG'=> 'Role deleted!',
    'ROLE_UPDATE_MSG'=> 'Role updated!',
    

 /*


  /*
    |--------------------------------------------------------------------------
    | Dynamic Field Management
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default Label messages used by
    | the Dynamic Field Management. 
    |
    */
    'DYNAMICFIELD_PLURAL'=> 'Fields',
    'DYNAMICFIELD_SINGULAR'=> 'Field',
    'DYNAMICFIELD_NAME_PLACEHOLDER'=> 'Enter your field name',
    'DYNAMICFIELD_LABEL'=> 'Field Label',
    'DYNAMICFIELD_LABEL_PLACEHOLDER'=> 'Enter your field label name',
    'DYNAMICFIELD_LABEL_ATTR'=> 'Field Label Attributes',
    'DYNAMICFIELD_LABEL_ATTR_PLACEHOLDER'=> 'for=field_id',
    'DYNAMICFIELD_ATTR_PLACEHOLDER'=> 'clsss=field_id,',
    'DYNAMICFIELD_TYPE'=> 'Field Type',
    'DYNAMICFIELD_ATTR'=> 'Field Attributes',
    'DYNAMICFIELD_CREATE_MSG'=> 'Field Created!',
    'DYNAMICFIELD_UPDATE_MSG'=> 'Field Updated!',
    'DYNAMICFIELD_DELETE_MSG'=> 'Field Deleted!',
    'DYNAMICFIELD_PRIORITY'=> 'Priority',
    'DYNAMICFIELD_PRIORITY_PLACEHOLDER'=> 'Enter Priority',
    'DYNAMICFIELD_SAVE_CONFIRM_TEXT'=> 'want to save this field.',
    'DYNAMICFIELD_DELETE_CONFIRM_TEXT'=> 'want to delete this field.',
    'DYNAMICFIELD_EDIT_CONFIRM_TEXT'=> 'want to update this field.',
    

 /*


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
