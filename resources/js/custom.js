
$(document).ready(function () {
  $("input").attr("autocomplete", "off");
  $('.myselect').select2();
  window.search_data = function (searchText, controllerName) {

    var query = $.trim(searchText);

     // if(query.length>0){
      var column_name = $('#hidden_column_name').val();
      var sort_type = $('#hidden_sort_type').val();
      var page = 1;
      fetch_data(page, sort_type, column_name, query, controllerName);
      //}
    };
    function clear_icon() {
      $('#id_icon').html('');
      $('#member_name_icon').html('');
      $('#member_email_icon').html('');
      $('#member_department_icon').html('');
      $('#member_designation_icon').html('');
      $('#status_icon').html('');
    }

    function fetch_data(page, sort_type, sort_by, query, controllerName) {

      $("#loader").show();
      $("#tbody").hide();
      $.ajax({
        url: controllerName + "/ajax/fetch_data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query,
        success: function success(data) {
          $("#loader").hide();
          $("#tbody").show();

          $('#tbody').html('');
          $('#tbody').html(data);
          $("[data-toggle=tooltip]").tooltip();
        }
      });
    }
    window.show_next_element = function(ele,con,eleid) {
     if($(ele).val()==con)
     {
      $("#"+eleid).show();
    }else{
      $("#"+eleid).hide();
    }
  }
  window.back=function(url){
    if(url!= undefined)
    {
      window.location.href=url;
    }else{
    window.history.back();
  }
  }

  var confirmdilog= false;

  window.editconfirmation = function(ele,tilte,msg,cancelBtn,acceptBtn) {
    if(!confirmdilog)
    {
      event.preventDefault(); 
      editwarnBeforeAction(ele,tilte,msg,cancelBtn,acceptBtn);
    }
  };
  function editwarnBeforeAction(ele,tilte,msg,cancelBtn,acceptBtn) {
   swal({
     title: tilte, 
     text: msg,
     icon: "info",
     buttons: [
     cancelBtn,
     acceptBtn
     ],
     dangerMode: true,
   }).then(function(isConfirm) {
     if (isConfirm) {
      confirmdilog=true;
      $(ele).submit();
    } else {
                  //swal("Cancelled", "Your imaginary file is safe :)", "error");
                };
              });
 };
 window.sorting = function (controllerName, column_name) {
  $('.icons').html('');
  var column_name = column_name;
  var order_type =  $('#hidden_sort_type').val();
  var reverse_order = '';
  if (order_type == 'asc') {
    $(this).data('sorting_type', 'desc');
    var reverse_order = 'desc';
       // clear_icon();
       $('.' + column_name).html('<i class="mr-1 fas fa-long-arrow-alt-up"></i>');
     }
     if (order_type == 'desc') {

       $(this).data('sorting_type', 'asc');
       var reverse_order = 'asc';
       // clear_icon();
       $('.' + column_name).html('<i class="mr-1 fas fa-long-arrow-alt-down"></i>');
     }
     $('#hidden_column_name').val(column_name);
     $('#hidden_sort_type').val(reverse_order);
     var page = $('#hidden_page').val();
     var query = $('#serach').val();

     fetch_data(page, reverse_order, column_name, query, controllerName);
   };

  //  $(document).on('click', '.pagination a', function (event) {
  //   event.preventDefault();
  //   var page = $(this).attr('href').split('page=')[1];
  //   $('#hidden_page').val(page);
  //   var controllerName= $('#cname').val();
  //   var column_name = $('#hidden_column_name').val();
  //   var sort_type = $('#hidden_sort_type').val();

  //   var query = $('#serach').val();

  //   $('li').removeClass('active');
  //   $(this).parent().addClass('active');
  //   fetch_data(page, sort_type, column_name, query,controllerName);
  // });


   window.addMore = function(i,placeHolderText,pointPlaceHolderText){  
    i++;  
    $('#dynamic_field tr:last').before('<tr id="row'+i+'" class="dynamic-added"><td class="width-60"><input type="text" name="challenge_options[]" placeholder="'+placeHolderText+'" data-validation="length" data-validation-length="max50" class="form-control name_list" /></td><td class="width-30"><input type="text" name="challenge_point[]" placeholder="'+pointPlaceHolderText+'" data-validation="custom length" maxlength="3" data-validation-optional="true" data-validation-length="max3"  data-validation-regexp="^\\d+$"  class="form-control name_list" /></td><td class="width-10"><button type="button" name="remove" id="'+i+'" style="float: right;" class="btn btn-danger btn_remove">X</button></td></tr>');  
  };  

  $(document).on('click', '.btn_remove', function(){  
    var button_id = $(this).attr("id");   
    $('#row'+button_id+'').remove();  
  });  


  window.confirmationDelete=function(linkURL,tilte,msg,cancelBtn,acceptBtn) {
   event.preventDefault(); 
   warnBeforeDelete(linkURL,tilte,msg,cancelBtn,acceptBtn);
 };
 function warnBeforeDelete(linkURL,tilte,msg,cancelBtn,acceptBtn) {
  swal({
    title: tilte, 
    text: msg,
    icon: "warning",
    buttons: [
    cancelBtn,
    acceptBtn
    ],
    dangerMode: true,
  }).then(function(isConfirm) {
    if (isConfirm) {
      window.location.href = linkURL;
    } else {
               //swal("Cancelled", "Your imaginary file is safe :)", "error");
             };
           });
};

window.confirmation=function(linkURL,tilte,msg,cancelBtn,acceptBtn) {
  event.preventDefault(); 
  warnBeforeAction(linkURL,tilte,msg,cancelBtn,acceptBtn);
};
function warnBeforeAction(linkURL,tilte,msg,cancelBtn,acceptBtn) {
 swal({
   title: tilte, 
   text: msg,
   icon: "info",
   buttons: [
   cancelBtn,
   acceptBtn
   ],
   dangerMode: true,
 }).then(function(isConfirm) {
   if (isConfirm) {
     window.location.href = linkURL;
   } else {
                  //swal("Cancelled", "Your imaginary file is safe :)", "error");
                };
              });
};
var langa = $('html').attr('lang');
jQuery.validate({
  form: 'form',
  lang : langa,
  onSuccess:function(form){ 
    editconfirmation(form,$(form).data('title'),$(form).data('message'),$(form).data('cancelbtn'),$(form).data('acceptbtn'));
  }
});
window.showimage=function(input,show){
  if (input.files && input.files[0]) {
   var reader = new FileReader();
   
   reader.onload = function(e) {
     $('#'+show).attr('src', e.target.result);
   }
   
   reader.readAsDataURL(input.files[0]);
 }
}

window.reset_image = function(ele,imgid,uid,token){
 $.post('/resetimage','_token='+token+'&uid='+uid,function(data) {
  $('#'+imgid).attr('src', data);
});
}

window.csvchange = function (ele) {
  var fileName = $(ele).val().split("\\").pop();
  $(ele).siblings(".custom-file-label").addClass("selected").html(fileName);
  var lab = $(ele).prev('label').html($(ele).data('after_chage_text')).addClass('btn-success').attr('onclick', 'uploadcsv(this)').removeAttr('for');
}

window.uploadcsv = function (ele) {    
  var file = $("#customFile")[0].files;
  if( file.length >0){
    var url = $(ele).data('url');
    var token = $(ele).data('token');
    var formData = new FormData();
    formData.append("file", file[0], file[0].name);
    formData.append("_token", token);
    $.ajax({
      url: url,
      type: 'POST',
      data: formData,
      xhr: function() {
        var myXhr = $.ajaxSettings.xhr();
        if(myXhr.upload){
          $("#progressbar").show();
          myXhr.upload.addEventListener('progress',function(e){
            if(e.lengthComputable){
              var max = e.total;
              var current = e.loaded;

              var Percentage = (current * 100)/max;
              $("#progressbar .progress-bar-striped").css("width",Percentage+'%').html(Percentage+"%");     

              if(Percentage >= 100)
              {
                 // process completed  
               }
             } 
           }, false);
        }
        return myXhr;
      },
      cache:false,
      contentType: false,
      processData: false,
      success: function (res) {
        var msg = res;
        $("#mainmsg").show().html(msg);
        $("#uploadmsg").html('');
        $("#customFile").val("");
        $(".custom-file-label").text("");
        $('.bulk-btn').trigger('click');
        $("#progressbar").hide(); 
       // $(ele).html($(ele).data('btntext')).removeClass('btn-success').attr('for', 'uploadcsv').removeAttr('onclick');
        var cname = $("#cname").val();
        search_data('', cname);
      },
      error: function (res) {
        var msg = res.responseJSON.errors.file[0];
        $("#uploadmsg").html(msg);

        $(ele).html($(ele).data('btntext')).removeClass('btn-success').attr('for', 'uploadcsv').removeAttr('onclick');
      }
    })
    event.preventDefault();
  }else{
    $("#uploadmsg").html("Please choose csv file.");
  }
}
$('.bulk-btn').on('click', function(){
  $(this).children('i').toggleClass('rotate');
});

$(document).on('click', '#mainpagenation .pagination a', function (event) {
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  $('#hidden_page').val(page);
  var controllerName = $('#cname').val();
  var column_name = $('#hidden_column_name').val();
  var sort_type = $('#hidden_sort_type').val();

  var query = $('#serach').val();

  $('li').removeClass('active');
  $(this).parent().addClass('active');
  fetch_data(page, sort_type, column_name, query, controllerName);
});
window.popupsearch_data = function (ele) {

  var query = $(ele).val();
  var controllerName = $(ele).data('controllername');
  var column_name = $(ele).data('column_name');
  var sort_type = $(ele).data('sort_type');
  var page = $(ele).data('page');
  var loader = $(ele).data('loader');
  var tbody = $(ele).data('tbody');
  var checkboxId = $(ele).data('chkid');
  var submitBtn = $(ele).data('submitbtnid');
  selected_members.length=0;
  popupfetch_data(loader,tbody,page, sort_type, column_name, query, controllerName,checkboxId,submitBtn);
};
function popupfetch_data(loader,tbody,page, sort_type, sort_by, query, controllerName,checkboxId,submitBtn) {

  $("#"+checkboxId).prop('checked', false);
  $("#"+submitBtn).attr('disabled','disabled');
  $("#"+loader).show();
  $("#"+tbody).hide();
  $.ajax({
    url: controllerName + "/ajax/fetch_data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query,
    success: function success(data) {
      $("#"+checkboxId).prop('checked', false);
      $("#"+submitBtn).attr('disabled','disabled');
      $("#"+loader).hide();
      $("#"+tbody).show();

      $('#'+tbody).html('');
      $('#'+tbody).html(data);
    }
  });
}

$("#popuppage").on('click', '.pagination a', function (event) {
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var query = $("#popserach").val();
  var controllerName = $("#popserach").data('controllername');
  var column_name = $("#popserach").data('column_name');
  var sort_type = $("#popserach").data('sort_type');
  var loader = $("#popserach").data('loader');
  var tbody = $("#popserach").data('tbody');
  var checkboxId = $("#popserach").data('chkid');

  $('li').removeClass('active');
  $(this).parent().addClass('active');
  popupfetch_data(loader,tbody,page, sort_type, column_name, query, controllerName,checkboxId);
  event.preventDefault();
});

var selected_members=[];
window.selectall = function(ele,submitBtnId){
  var body = $(ele.closest('table')).find('tbody').attr('id');
  var enable_btn = $(ele.closest('table')).find('thead').find('input[type=checkbox]').data('enable') || 0;
//console.log(submitBtnId);
if($(ele).is(":checked")){
  $('#'+body+' input[type=checkbox]').each((k,v)=>{
    $(v).prop('checked', true);
    let val = $(v).val();
    if(selected_members.indexOf(val) == -1){
    selected_members.push(val);

    }
  });
 // console.log(selected_members.length);
 if(enable_btn==1  && selected_members.length > 0)
 {
  $("#"+submitBtnId).removeAttr('disabled');
}
}else{
 $('#'+body+' input[type=checkbox]').each((k,v)=>{
  $(v).prop('checked', false);
  let val = $(v).val();
  selected_members.length=0;
});

 if(enable_btn==1 && selected_members.length === 0 )
 {
  $("#"+submitBtnId).attr('disabled','disabled');
}
}
license_check(submitBtnId);
}
$(".bd-example-modal-lg").on('show.bs.modal',function(){
  var model = $(this);
  var table = model.find("table");
  $(table).find(' input[type=checkbox]').each((k,v)=>{
  $(v).prop('checked', false);
});
  selected_members.length=0;
  license_check("unsubmitbtn");
})
window.addmember = function(ele,submitBtnId){
  var body = $(ele.closest('table')).find('tbody').attr('id');
  var mainbox = $(ele.closest('table')).find('thead').find('input[type=checkbox]').attr('id');
  var enable_btn = $(ele.closest('table')).find('thead').find('input[type=checkbox]').data('enable') || 0;
  if($(ele).is(":checked")){
  //console.log('checked');

  let val = $(ele).val();
  selected_members.push(val);
    //console.log(selected_members.length);
    let all = $('#'+body+' input[type=checkbox]').length;
    let ckal = 0;
    $('#'+body+' input[type=checkbox]').each((k,v)=>{
     if($(v).is(":checked")){
      ckal=ckal+1;
    }
  });
    if(ckal==all){
     $("#"+mainbox).prop('checked', true);
   }
   if(enable_btn==1 && selected_members.length > 0)
   {
    $("#"+submitBtnId).removeAttr('disabled');
  }
}else{

  let val = $(ele).val();
  selected_members.splice(selected_members.indexOf(val),1);

  $("#"+mainbox).prop('checked', false);
  let ckalu = 0;
  $('#'+body+' input[type=checkbox]').each((k,v)=>{
   if($(v).is(":checked")){
    ckalu=ckalu+1;
  }
});
    //console.log(ckalu);
    //console.log(selected_members.length);
    if(selected_members.length === 0){ 
      //console.log('not checked');  
      $("#"+submitBtnId).attr('disabled','disabled');
    }
  }
  license_check(submitBtnId);
}

$("#poptbody").on('DOMSubtreeModified',function(){
 $("#selectallmem").prop('checked', false);
 let all = $('#poptbody input[type=checkbox]').length;
 let ckal = 0;
 $('#poptbody input[type=checkbox]').each((k,v)=>{
  let val = $(v).val();
  if(selected_members.indexOf(val) != -1){
   $(v).prop('checked', true);
 }
 if($(v).is(":checked")){
  ckal=ckal+1;
}
});
 if(ckal==all){
   $("#selectallmem").prop('checked', true);
 }
});


var is_license_valid=false;
window.saveplayer = function(ele){
  if(selected_members.length>0 && is_license_valid)
  {
    var org_id= $(ele).data('orgid');
    var team_id= $(ele).data('team_id');
    var token= $(ele).data('token');
    var members=selected_members.join(',');
    var url = '/'+$(ele).data('posturl');
    $(ele).attr("disabled", true);
    $.post(url,{_token:token,org_id:org_id,team_id:team_id,members:members},function(res){
      window.location.href = window.location.href;
    })
  }
}

function license_check(submitBtnId){
  if(submitBtnId =='unsubmitbtn')
  {
  var selected = parseInt(selected_members.length);
  var total = parseInt($("#total_lic").html());  
  $("#selected_member").html(selected);
  if(selected<=total)
  {
    if(selected >0){
    $("#"+submitBtnId).removeAttr("disabled");
  }
   $("#license_error").hide();
   is_license_valid=true;
}else{
  $("#license_error").html("You have exceeded max number of licenses.").show();
  $("#"+submitBtnId).attr("disabled","disabled");
  is_license_valid=false;
}
}
}

$(document).on('click', '#orguserpagenationdiv .pagination a', function (event) {
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var query = $("#orgusersearch").val();
  var controllerName = $("#orgusersearch").data('controllername');
  var column_name = $("#orgusersearch").data('column_name');
  var sort_type = $("#orgusersearch").data('sort_type');
  var loader = $("#orgusersearch").data('loader');
  var tbody = $("#orgusersearch").data('tbody');

  $('li').removeClass('active');
  $(this).parent().addClass('active');
  popupfetch_data(loader,tbody,page, sort_type, column_name, query, controllerName);
  event.preventDefault();
});

$(document).on('click', '#orgadminpagenationdiv .pagination a', function (event) {
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var query = $("#orgadminsearch").val();
  var controllerName = $("#orgadminsearch").data('controllername');
  var column_name = $("#orgadminsearch").data('column_name');
  var sort_type = $("#orgadminsearch").data('sort_type');
  var loader = $("#orgadminsearch").data('loader');
  var tbody = $("#orgadminsearch").data('tbody');

  $('li').removeClass('active');
  $(this).parent().addClass('active');
  popupfetch_data(loader,tbody,page, sort_type, column_name, query, controllerName);
  event.preventDefault();
}); 

$(document).on('click', '#orgteampagenationdiv .pagination a', function (event) {
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  var query = $("#orgteamsearch").val();
  var controllerName = $("#orgteamsearch").data('controllername');
  var column_name = $("#orgteamsearch").data('column_name');
  var sort_type = $("#orgteamsearch").data('sort_type');
  var loader = $("#orgteamsearch").data('loader');
  var tbody = $("#orgteamsearch").data('tbody');

  $('li').removeClass('active');
  $(this).parent().addClass('active');
  popupfetch_data(loader,tbody,page, sort_type, column_name, query, controllerName);
  event.preventDefault();
});

/************************************* Open member profile image choosing dialog *********************/
var imgpath='';
window.chooseimage = function(ele,path){
 imgpath =path;
 $("#selectProfile").find(".border-success").removeClass('border-success bg-success').addClass("border-primary");
 $(ele).find('div').removeClass('border-primary').addClass("border-success bg-success");
}
window.addimage = function(){
  if(imgpath!='')
  {
    $("#profile_image_ch").val(imgpath);
    $("#imgpriview").attr('src','/avatars/'+imgpath);
    $("#selectProfile").modal('hide');
  }else{

  }
}
/************************************* Open member profile image choosing dialog *********************/

/************************************* Search image by name in member profile image choosen dialog *********************/
window.search_images = function(ele){
  var input = $(ele);
  var filter = input.val().toLowerCase().trim();
  if(filter !='')
  {
    var nodes = $('.searchimage');
    $.each(nodes,(k,v)=>{
      if ($(v).find("h4").html().toLowerCase().includes(filter)) {
        $(v).show();
      } else {
        $(v).hide();
      }
    })
  }else{
    $('.searchimage').show();
  }
  
}
/************************************* Search image by name in member profile image choosen dialog *********************/

/*************************** Refresh after File Download ***************************************/
var inter = setInterval(function(){
  if ($.cookie("filedownloaded")) {
    window.location.reload();    
    $.removeCookie("filedownloaded"); 
  }
},2000);

/*************************** Refresh after File Download ***************************************/

/******************************************* Challenge conditional filter **************************/
window.get_filtered_data = function(ele,value,url)
{
  var lang = $("#language").val()||'en';
  var controllername = url+'/'+value+'/'+lang;
  var serach = $("#serach").val()||'';
  search_data(serach,controllername);
  $(".createbtn").each((k,v)=>{
    var url = $(v).data('href')+"/"+value+'/'+lang;
    $(v).removeAttr("disabled").attr("onclick",'window.location.href="'+url+'";');
    /* 
    if(value=='0')
    {
      //$(v).removeAttr("onclick").attr("disabled",'disabled');
     // $("#serach").attr('disabled','disabled');
    }else{
      
     // $("#serach").removeAttr('disabled').attr("onkeyup",'search_data(this.value,"'+controllername+'");');
    } */
  });
 // $("#cname").val(url+'/'+value);
  $("#hidden_category_id").val(value);
}
/******************************************* Challenge condistional filter **************************/

/******************************************* Challenge data sorting clicking of heading **************************/
window.challenge_sorting = function(controllername,column_name)
{
  var category = $("#hidden_category_id").val()||0;
   var lang = $("#language").val()||'en';
    var controllername = controllername+'/'+category+'/'+lang;
    sorting(controllername,column_name);
}
/******************************************* Challenge data sorting clicking of heading **************************/



/******************************************* Challenge language conditional filter **************************/
window.get_lang_filtered_data = function(ele,value,url)
{
  var category = $("#challenge_category").val()||'';
  if(category !='')
  {
  var controllername = url+'/'+category+'/'+value;
  var serach = $("#serach").val()||'';
  search_data(serach,controllername);
  $(".createbtn").each((k,v)=>{
      var url = $(v).data('href')+"/"+category+'/'+value;
      $(v).attr("onclick",'window.location.href="'+url+'";');
  });
}
  $("#language").val(value);  
}
/******************************************* Challenge language condistional filter **************************/

/******************************************* Challenge language conditional filter **************************/
window.get_lang_filtered_set_data = function(ele,url,type,category)
{
  var category = $("#"+category).val()||0;
  var controllername = url+'/'+$(ele).val()+'/'+$("#"+type).val()+'/'+category+'/'+$("#set_id").val();
  var serach = $("#serach").val()||'';
  search_data(serach,controllername);
  $("#language").val($(ele).val());  
}
/******************************************* Challenge language condistional filter **************************/


//Question add more options


    window.addMoreOption = function(i,placeHolderText){  
      i++;  
         $('#dynamic_field tr:last').before('<tr id="row'+i+'" class="dynamic-added"><td><input type="text" name="challenge_options[]" placeholder="'+placeHolderText+'" class="form-control name_list" data-validation="required length" data-validation-length="max50" /></td><td class="text-center"><input type="radio" name="challange_answer" value="'+i+'" id="chanllange_answer'+i+'" /></td><td class="text-right"><button type="button" name="remove" id="'+i+'" style="float: right;" class="btn btn-danger btn_remove">X</button></td></tr>');
         $("#add").attr("onclick","addMoreOption('"+i+"','"+placeHolderText+"')"); 
    };  


window.get_filtered_team_data = function(ele,value,url)
{
  var controllername = url+'/'+value; 
  var serach = $("#serach").val()||'';
  search_data(serach,controllername);
  $(".createbtn").each((k,v)=>{
     var url = $(v).data('href')+"/"+value;
      $(v).removeAttr("disabled").attr("onclick",'window.location.href="'+url+'";');
      $("#serach").attr("onkeyup",'search_data(this.value,"'+controllername+'");');
   /* if(value=='0')
    {
      //$(v).removeAttr("onclick").attr("disabled",'disabled');
      //$("#serach").attr('disabled','disabled');
    }else{
     
    } */
  });
  $("#cname").val(url+'/'+value);
  $("#hidden_organizationId").val(value);
}

/*********************** challenge Pagenation ********************/
$(document).on('click', '#pagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);
    var category=$('#hidden_category_id').val();
    var lang = $('#language').val();
    var controllerName= $('#cname').val()+'/'+category+'/'+lang;
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#serach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query,controllerName);
  });


/******************************************* Question set language conditional filter **************************/
window.get_question_set_lang_filtered_data = function(ele,value,url)
{
  var controllername = url+'/'+value;
  var serach = $("#serach").val()||'';
  search_data(serach,controllername);
  $(".createbtn").each((k,v)=>{
      var url = $(v).data('href')+"/"+value;
      $(v).attr("onclick",'window.location.href="'+url+'";');
  });
  $("#language").val(value);  
}
/******************************************* Question set language condistional filter **************************/

/********************** Question mapping filter data **********************/

window.get_question_filtered_data = function(type,cat,url,query){
  var setid = $("#set_id").val()||0;
  var controllername = url+'/'+type+'/'+cat+'/'+setid;
   search_data(query,controllername);
}

var question_set_selected= $.parseJSON(($("#question_ids").val()||'[]'));
checkOrUncheckAll();
/*********************** Question Set create and update Pagenation ********************/
$(document).on('click', '#questionsetpagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var setid = $("#set_id").val()||0;
    $('#hidden_page').val(page);
    var category=$('#question_category').val();
    var type = $('#type').val();
    var controllerName= $('#cname').val()+'/'+type+'/'+category+'/'+setid;
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#serach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query,controllerName);
  });

/************************ Question set pagenation **********************/
$(document).on('click', '#gsetpagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    $('#hidden_page').val(page);
    var lang = $('#language').val();
    var controllerName= $('#cname').val()+'/'+lang;
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#serach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query,controllerName);
  });


window.question_set_select=function(is_all,ele)
{
  
  if(is_all && $(ele).is(":checked")){
    $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = parseInt($(v).val());
      question_set_selected.push(val);
    });
  }
  else if(is_all && !$(ele).is(":checked"))
  {    
    $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = parseInt($(v).val());
      question_set_selected.splice(question_set_selected.indexOf(val),1);
    });

  }else if($(ele).is(":checked"))
  {
    var val = parseInt($(ele).val());
    question_set_selected.push(val);
  }else{
    var val = parseInt($(ele).val());
    question_set_selected.splice(question_set_selected.indexOf(val),1);
  }
  checkOrUncheckAll();
}

function checkOrUncheckAll(){
  var check=0;
  $("#question_table").find("tbody").find("input").map((k,v)=>{
    var val = parseInt($(v).val());
    if($.inArray(val,question_set_selected) != -1)
    {
      $(v).prop("checked",true);
      check = check+1;
    }else{
      $(v).prop("checked",false);
    }
    });
  if($("#question_table").find("tbody").find("input").length==check && check!=0){
    $("#selectall_question_set").prop("checked",true);
  }else{
    $("#selectall_question_set").prop("checked",false);
  }  
  $("#question_ids").val(question_set_selected.join());
  if($("#question_ids").val() !=''){
    $("#quizsubmitbtn").removeAttr("disabled");
  }else{
    $("#quizsubmitbtn").attr("disabled","disabled");
  }
}

$("#question_table").find("tbody").on('DOMSubtreeModified',function(){  
    checkOrUncheckAll();
});


/***************************** Assign quiz set make week in organization *************/
var lastdate ='';
var startdate ='';
window.makeweek = function(ele){
  var choosedate = new Date($(ele).val());
  var nextweek = format(new Date(choosedate.setDate(choosedate.getDate()+6)));
  $(ele).closest("td").next("td").find("input").val(nextweek);
 lastdate=nextweek;
 startdate = $(ele).val(); 
 var totalno=0;
 $.each($(ele).closest('tr').nextAll(),(k,v)=>{
  resetdate(lastdate,v);
 });
$(ele).closest("td").find(".msg").remove();
 setTimeout(function(){
createdatepicker();
},1000);
}

window.addnewweek =function(ele,date){
  var tr = $("#weektable").find("tr").last();
  var sd = tr.find(".sdate").val();
  var ed = tr.find(".edate").val();
  var quiz_permission = $(ele).data('quiz_permission');
  var challenge_permission = $(ele).data('challenge_permission');
  if(sd!='')
  {
    var eda = new Date(ed);
    var ceda = new Date();
    if(eda<ceda)
    {
      ed = format(new Date(ceda.setDate(ceda.getDate()-1)));
    }
    lastdate = ed;
    createhtml(lastdate,quiz_permission,challenge_permission); 
    setTimeout(function(){
      var tr = $("#weektable").find("tr").last();
      var lded = new Date(ed);
      var pldate = new Date(lded.setDate(lded.getDate()+1));
      var datepic = tr.find('.sdate');
      $(datepic).datepicker({
  dateFormat: "yy-mm-dd",
   minDate: new Date(pldate),
});
    },100);
  }else{
   addStartDateworningMsg(tr);
  }
}
createdatepicker();
function createdatepicker(){
  $('.sdate').each((k,v)=>{
    var mindate = $(v).attr('data-mindate');
    $(v).datepicker({
  dateFormat: "yy-mm-dd",
   minDate: new Date(mindate),
});
    $(v).datepicker("option", "minDate", mindate);
  })
}
window.removeweek =function(ele){
  $(ele).closest('tr').remove();
  var totalno=0;
  $.each($("#addweek tr"),(k,v)=>{
    $(v).find('.wkno').html("Week "+(k+1));
  });
}
function resetdate(ldate,ctr)
{
  var ld = new Date(ldate);
   var pldate = new Date(ld.setDate(ld.getDate()+1));
   var sdate = format(pldate);
  var edate = format(new Date(pldate.setDate(pldate.getDate()+6)));
  var wkno = $("#addweek").find("tr").length+1;  
  $(ctr).find(".sdate").val(sdate);
  $(ctr).find(".sdate").attr('data-mindate',sdate);
  $(ctr).find(".edate").val(edate);
lastdate=edate;
}

function createhtml(ldate,quiz_permission,challenge_permission)
{
  var ld = new Date(ldate);
   var pldate = new Date(ld.setDate(ld.getDate()+1));
   var sdate = format(pldate);
  var edate = format(new Date(pldate.setDate(pldate.getDate()+6)));
  var wkno = $("#addweek").find("tr").length+1;
  var html = `<tr>
                <td>
                 <span class="wkno">Week ${wkno}</span> 
                (Upcomming)
                 <input type="hidden" name="weekno[]" value="${wkno}">
                </td>
                <td><input type="text" data-mindate="${sdate}" onchange="makeweek(this);" name="weekstartdate[${wkno}]" class="form-control sdate" value="${sdate}"></td>
                <td><input type="text" readonly name="weekenddate[${wkno}]" class="form-control edate" value="${edate}"></td>
                <td class="text-center">
                <span class="btn btn-primary" id="quiz_count_${wkno}" > 0 Assigned</span> `;
                if(quiz_permission){
                html = html + `<button type="button" data-weekno="${wkno}" onclick="openquizsetmodal(this);" class="btn btn-primary"><i class="fa fa-plus"></i></button> `;
                }
                html = html + `<input type="hidden" name="quiz_id[${wkno}]" id="quiz_id_${wkno}">
                </td>
                <td class="text-center">
                    <span class="btn btn-primary" id="challenge_count_${wkno}" > 0 Assigned</span> `;
    if(challenge_permission){
                html= html+`<button type="button" data-weekno="${wkno}" class="btn btn-primary" onclick="openchallengesetmodal(this);"><i class="fa fa-plus"></i></button> `;
    }
                   html = html +`<input type="hidden" name="challenge_id[${wkno}]" id="challenge_id_${wkno}" >
                </td>
                <td><button class="btn btn-danger" onclick="removeweek(this)"><i class="fa fa-minus"></i></button></td>
              </tr>`;
lastdate=edate;
 $("#addweek").append(html);
}

function format(date){
var yyyy = date.getFullYear().toString();
       var mm = (date.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = date.getDate().toString();
       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    }

var quiz_set_selected= []; //quiz set selected 
var quiz_set_inpu_box=''; // Selected week inputbox id
var quiz_set_count_box=''; // Selected week quiz count box id
var current_open_model; //current open model reference instence

/*********************************** Assign quiz set modal open function **********************/
window.openquizsetmodal =function(ele){
  var tr = $(ele).closest("tr");
  var sd = tr.find(".sdate").val();
  if(sd!='')
  {
    $("#quiz_assign_model").modal("show");
  var target = $(ele);
  var wno = $(target).data('weekno');
  var sdate = $(target).closest("tr").find(".sdate").val();
  var edate = $(target).closest("tr").find(".edate").val();
  var modal = $("#quiz_assign_model");
  modal.find('.weeknomodal').html(wno);
  modal.find('.weeksdmodal').html(sdate);
  modal.find('.weekedmodal').html(edate);
  quiz_set_inpu_box = "#quiz_id_"+wno;
  quiz_set_count_box = "#quiz_count_"+wno;  
  quiz_set_selected=$(quiz_set_inpu_box).val()?$(quiz_set_inpu_box).val().split(','):[];
  current_open_model=modal;
  checkOrUncheckAllquiz();
  }else{
   addStartDateworningMsg(tr);
  }
}

function addStartDateworningMsg(tr){
  if(tr.find(".sdate").closest("td").find(".msg").length<=0)
    {
    tr.find(".sdate").closest("td").append("<p class='msg'>Week Start Date is required.</p>");
  }
}

/*********************** Quiz Set Popup Pagenation ********************/
$(document).on('click', '#quizsetpopuppagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];;
    $('#hidden_page').val(page);
    var category=$('#question_category').val();
    var controllerName= $('#cname').val();
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#popserach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query,controllerName);
  });

window.quiz_set_select=function(is_all,ele)
{
  
  if(is_all && $(ele).is(":checked")){
    $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = ($(v).val());
      quiz_set_selected.push(val);
    });
  }
  else if(is_all && !$(ele).is(":checked"))
  {    
    quiz_set_selected.length=0;
  }else if($(ele).is(":checked"))
  {
    var val = ($(ele).val());
    quiz_set_selected.push(val);
  }else{
    var val = ($(ele).val());
    quiz_set_selected.splice(quiz_set_selected.indexOf(val),1);
  }
  checkOrUncheckAllquiz();
}

function checkOrUncheckAllquiz(){
  var check=0;
  $("#quiz_set_modal_table").find("tbody").find("input").map((k,v)=>{
    var val = ($(v).val());
    if($.inArray(val,quiz_set_selected) != -1)
    {
      $(v).prop("checked",true);
      check = check+1;
    }else{
      $(v).prop("checked",false);
    }
    });
  if($("#quiz_set_modal_table").find("tbody").find("input").length==check && check!=0){
    $("#selectall").prop("checked",true);
  }else{
    $("#selectall").prop("checked",false);
  }  
  //
  // if(quiz_set_selected.length>0){
  //   $("#quizsubmitbtn").removeAttr("disabled");
  // }else{
  //   $("#quizsubmitbtn").attr("disabled","disabled");
  // }
}

$("#quiz_set_modal_table").find("tbody").on('DOMSubtreeModified',function(){  
    checkOrUncheckAllquiz();
});

$("#quizsubmitbtn").click(function(e){
  $(quiz_set_inpu_box).val(quiz_set_selected.join());
  $(quiz_set_count_box).html(quiz_set_selected.length+" Assigned");
  $(current_open_model).modal("hide");
 // $("#mainsubmitbtn").removeAttr("disabled");
})


/********************************* Start Assign challenge Set to Organization *****************************/

var challenge_set_selected= []; //challenge set selected 
var challenge_set_inpu_box=''; // Selected week inputbox id
var challenge_set_count_box=''; // Selected week challenge count box id
var current_open_challenge_model; //current open model reference instence

/*********************************** Assign challenge set modal open function **********************/
window.openchallengesetmodal =function(ele){
  var tr = $(ele).closest("tr");
  var sd = tr.find(".sdate").val();
  if(sd!='')
  {
    $("#challenge_assign_model").modal("show");
  var target = $(ele);
  var wno = $(target).data('weekno');
  var sdate = $(target).closest("tr").find(".sdate").val();
  var edate = $(target).closest("tr").find(".edate").val();
  var modal = $("#challenge_assign_model");
  modal.find('.weeknomodal').html(wno);
  modal.find('.weeksdmodal').html(sdate);
  modal.find('.weekedmodal').html(edate);
  challenge_set_inpu_box = "#challenge_id_"+wno;
  challenge_set_count_box = "#challenge_count_"+wno;  
  challenge_set_selected=$(challenge_set_inpu_box).val()?$(challenge_set_inpu_box).val().split(','):[];
  current_open_challenge_model=modal;
  checkOrUncheckAllchallenge();
  }else{
    addStartDateworningMsg(tr);
  }
}

/*********************** Challenge Set Popup Pagenation ********************/
$(document).on('click', '#challengesetpopuppagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];;
    $('#chidden_page').val(page);
    var category=$('#question_category').val();
    var controllerName= $('#ccname').val();
    var column_name = $('#chidden_column_name').val();
    var sort_type = $('#chidden_sort_type').val();

    var query = $('#cpopserach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
     popupfetch_data("cpoploader","challengetbody",page, sort_type, column_name, query, controllerName);
  });

window.challenge_select_all = function(is_all,ele)
{
  if(is_all && $(ele).is(":checked")){
    $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = ($(v).val());
      challenge_set_selected.push(val);
    });
  }
  else if(is_all && !$(ele).is(":checked"))
  {    
    challenge_set_selected.length=0;
  }else if($(ele).is(":checked"))
  {
    var val = ($(ele).val());
    challenge_set_selected.push(val);
  }else{
    var val = ($(ele).val());
    challenge_set_selected.splice(challenge_set_selected.indexOf(val),1);
  }
  checkOrUncheckAllchallenge();
}

function checkOrUncheckAllchallenge(){
  var check=0;
  $("#challenge_set_modal_table").find("tbody").find("input").map((k,v)=>{
    var val = ($(v).val());
    if($.inArray(val,challenge_set_selected) != -1)
    {
      $(v).prop("checked",true);
      check = check+1;
    }else{
      $(v).prop("checked",false);
    }
    });
  if($("#challenge_set_modal_table").find("tbody").find("input").length==check && check!=0){
    $("#selectall_challenge").prop("checked",true);
  }else{
    $("#selectall_challenge").prop("checked",false);
  }  
  //
  // if(challenge_set_selected.length>0){
  //   $("#challengesubmitbtn").removeAttr("disabled");
  // }else{
  //   $("#challengesubmitbtn").attr("disabled","disabled");
  // }
}

$("#challenge_set_modal_table").find("tbody").on('DOMSubtreeModified',function(){  
    checkOrUncheckAllchallenge();
});

$("#challengesubmitbtn").click(function(e){
  $(challenge_set_inpu_box).val(challenge_set_selected.join());
  $(challenge_set_count_box).html(challenge_set_selected.length+" Assigned");
  $(current_open_challenge_model).modal("hide");
  $("#mainsubmitbtn").removeAttr("disabled");
})



/********************************* End Assign challenge Set to Organization *****************************/


  /****************** Challenge Set ***************************/
window.get_challenge_filtered_data = function(type,cat,url,query){
  var setid = $("#set_id").val()||0;
  var lang = $("#language").val()||'en';
  var controllername = url+'/'+lang+'/'+type+'/'+cat+'/'+setid;
   search_data(query,controllername);
}

var challenge_set_selected= $.parseJSON(($("#challenge_ids").val()||'[]'));
challengecheckOrUncheckAll();
window.challenge_set_select=function(is_all,ele)
{
  
  if(is_all && $(ele).is(":checked")){
    $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = parseInt($(v).val());
      challenge_set_selected.push(val);
    });
  }
  else if(is_all && !$(ele).is(":checked"))
  {    
     $(ele).closest("table").find("tbody").find("input").map((k,v)=>{
      var val = parseInt($(v).val());
      challenge_set_selected.splice(challenge_set_selected.indexOf(val),1);
    });
  }else if($(ele).is(":checked"))
  {
    var val = parseInt($(ele).val());
    challenge_set_selected.push(val);
  }else{
    var val = parseInt($(ele).val());
    challenge_set_selected.splice(challenge_set_selected.indexOf(val),1);
  }
  challengecheckOrUncheckAll();
}

function challengecheckOrUncheckAll(){
  var check=0;
  $("#challenge_table").find("tbody").find("input").map((k,v)=>{
    var val = parseInt($(v).val());
    if($.inArray(val,challenge_set_selected) != -1)
    {
      $(v).prop("checked",true);
      check = check+1;
    }else{
      $(v).prop("checked",false);
    }
    });
  if($("#challenge_table").find("tbody").find("input").length==check && check!=0){
    $("#selectall").prop("checked",true);
  }else{
    $("#selectall").prop("checked",false);
  }  
  $("#challenge_ids").val(challenge_set_selected.join());
  if($("#challenge_ids").val() !=''){
    $("#challengesubmitbtn").removeAttr("disabled");
  }else{
    $("#challengesubmitbtn").attr("disabled","disabled");
  }
}

$("#challenge_table").find("tbody").on('DOMSubtreeModified',function(){  
    challengecheckOrUncheckAll();
});

$(document).on('click', '#challengesetpagination .pagination a', function (event) {
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    var setid = $("#set_id").val()||0;
    var lang = $("#language").val()||'en';
    $('#hidden_page').val(page);
    var category=$('#challenge_category').val();
    var type = $('#type').val();
    var controllerName= $('#cname').val()+'/'+lang+'/'+type+'/'+category+'/'+setid;
    var column_name = $('#hidden_column_name').val();
    var sort_type = $('#hidden_sort_type').val();

    var query = $('#serach').val();

    $('li').removeClass('active');
    $(this).parent().addClass('active');
    fetch_data(page, sort_type, column_name, query,controllerName);
  });

/***************************** Start Organization quizz and hacks change notification ***********/

  window.popupmessage = function(id,nextid,title,msg,old_val,cancelBtn,acceptBtn){
    swal({
     title: title, 
     text: msg,
     icon: "info",
     buttons: [
     cancelBtn,
     acceptBtn
     ],
     dangerMode: true,
   }).then(function(isConfirm) {
     if(isConfirm) {
      
      var next_val =$("#"+id).val();
      $("#"+id).attr('onchange','popupmessage("'+id+'","'+nextid+'","'+title+'","'+msg+'","'+next_val+'","'+cancelBtn+'","'+acceptBtn+'")');


    } 
    else{
      $("#"+id).val(old_val);
    }
    $("#"+nextid).focus();
  });
  }

/***************************** End Organization quizz and hacks change notification ***********/



window.getDepartmentDesignation = function(orgid){
  if(orgid!='')
  {
  $.get("/member/getdepartmentanddesignation/"+orgid,function(res){
    var department = res.department.join(' ');
    var designation = res.designation.join(' ');    
    $("#member_department").html(department);
    $("#member_designation").html(designation);
  })
}
}

window.getDynamicParams = function(val){
  $.get("/template/getdynamicdata/"+val,function(res){
    $("#variable").html(res.join(" "));
  });
}
/********************* CK Editor *********************************/
  if( $('#editor').length )
  {
     CKEDITOR.replace('editor');
  }
/********************* Log Pop Up *********************************/
  $("#logModel").on('show.bs.modal',function(e){
      
      var target=$(e.relatedTarget);
      var id= target.data('id');
      var model= target.data('model_id');
      var base_url = target.data('url');
      $.ajax({
        url: base_url + "/view-log/view/" + model + "/" + id,
        success: function success(data) {
          console.log(data);
          if(data.trim() !=''){
           $("#logbody").html(data);
          }
        }
      });
  })

    $("#logModel").on('hidden.bs.modal',function(e){
     $("#logbody").empty();
    })


});
/*-- tooltip start --*/
$('[data-toggle="tooltip"]').tooltip();
/*-- tooltip end --*/

if( $('#sortable').length )
  {
     $('#sortable tbody').sortable({
      placeholder: "portlet-placeholder ui-corner-all",
      update: function( event, ui ) {
        $(this).children().each(function(index) {
      $(this).find('td').first().html(index + 1)
    });
        var cindex = ui.item.index();
        var id= ui.item.data("sortable_id");
        $.get("/dynamicfield/"+cindex+"/"+id+"/updateindex",function(res){
          console.log(res);
        })

  }
     });
  }
 