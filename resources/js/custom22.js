$(document).ready(function () {
  
    window.search_data = function (searchText, controllerName) {
      
      var query = searchText;
      var column_name = $('#hidden_column_name').val();
      var sort_type = $('#hidden_sort_type').val();
      var page = $('#hidden_page').val();
      fetch_data(page, sort_type, column_name, query, controllerName);
    };
    function clear_icon() {
      $('#id_icon').html('');
      $('#member_name_icon').html('');
      $('#member_email_icon').html('');
      $('#member_department_icon').html('');
      $('#member_designation_icon').html('');
      $('#status_icon').html('');
    }
  
    function fetch_data(page, sort_type, sort_by, query, controllerName) {
     
      $("#loader").show();
      $("#tbody").hide();
      $.ajax({
        url: controllerName + "/ajax/fetch_data?page=" + page + "&sortby=" + sort_by + "&sorttype=" + sort_type + "&query=" + query,
        success: function success(data) {
          $("#loader").hide();
          $("#tbody").show();
         
          $('#tbody').html('');
          $('#tbody').html(data);
        }
      });
    }
    window.sorting = function (controllerName, column_name) {
      $('.icons').html('');
      var column_name = column_name;
      var order_type =  $('#hidden_sort_type').val();
      var reverse_order = '';
      if (order_type == 'asc') {
        $(this).data('sorting_type', 'desc');
        var reverse_order = 'desc';
       // clear_icon();
        $('.' + column_name).html('<i class="mr-1 fas fa-long-arrow-alt-up"></i>');
      }
      if (order_type == 'desc') {
          console.log(order_type)
         $(this).data('sorting_type', 'asc');
        var reverse_order = 'asc';
       // clear_icon();
        $('.' + column_name).html('<i class="mr-1 fas fa-long-arrow-alt-down"></i>');
      }
      $('#hidden_column_name').val(column_name);
      $('#hidden_sort_type').val(reverse_order);
      var page = $('#hidden_page').val();
      var query = $('#serach').val();
      
      fetch_data(page, reverse_order, column_name, query, controllerName);
    };
  
    $(document).on('click', '.pagination a', function (event) {
      event.preventDefault();
      var page = $(this).attr('href').split('page=')[1];
      $('#hidden_page').val(page);
     var controllerName= $('#cname').val();
      var column_name = $('#hidden_column_name').val();
      var sort_type = $('#hidden_sort_type').val();
  
      var query = $('#serach').val();
  
      $('li').removeClass('active');
      $(this).parent().addClass('active');
      fetch_data(page, sort_type, column_name, query,controllerName);
    });

    
    window.addMore = function(i,placeHolderText){  
      i++;  
         $('#dynamic_field tr:last').before('<tr id="row'+i+'" class="dynamic-added"><td style="width: 100%;"><input type="text" name="challenge_options[]" placeholder="'+placeHolderText+'" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" style="float: right;" class="btn btn-danger btn_remove">X</button></td></tr>');  
    };  

    $(document).on('click', '.btn_remove', function(){  
      var button_id = $(this).attr("id");   
      $('#row'+button_id+'').remove();  
 });  
 

window.confirmationDelete=function(linkURL,tilte,msg,cancelBtn,acceptBtn) {
 event.preventDefault(); 
 warnBeforeDelete(linkURL,tilte,msg,cancelBtn,acceptBtn);
  };
   function warnBeforeDelete(linkURL,tilte,msg,cancelBtn,acceptBtn) {
      swal({
        title: tilte, 
        text: msg,
        icon: "warning",
     buttons: [
      cancelBtn,
      acceptBtn
      ],
      dangerMode: true,
      }).then(function(isConfirm) {
              if (isConfirm) {
                window.location.href = linkURL;
            } else {
               //swal("Cancelled", "Your imaginary file is safe :)", "error");
            };
    });
  };

  window.confirmation=function(linkURL,tilte,msg,cancelBtn,acceptBtn) {
    event.preventDefault(); 
    warnBeforeAction(linkURL,tilte,msg,cancelBtn,acceptBtn);
     };
      function warnBeforeAction(linkURL,tilte,msg,cancelBtn,acceptBtn) {
         swal({
           title: tilte, 
           text: msg,
           icon: "info",
        buttons: [
          cancelBtn,
          acceptBtn
         ],
         dangerMode: true,
         }).then(function(isConfirm) {
                 if (isConfirm) {
                   window.location.href = linkURL;
               } else {
                  //swal("Cancelled", "Your imaginary file is safe :)", "error");
               };
       });
     };
     function resolveAfter2Seconds() {
      return new Promise(resolve => {
        setTimeout(() => {
          resolve('resolved');
        }, 5000);
      });
    }


    jQuery.validate({
      form: 'form', onSuccess : async function (form) {
        var swll=await swal({
          title: 'dfdsgdf', 
          text: 'dfgdgdfgdfgh',
          icon: "info",
       buttons: [
         'cancelBtn',
         'acceptBtn'
        ],
        dangerMode: true,
        }).then(function(isConfirm) {
                if (isConfirm) {
                  return 1;
              } else{
                 
                  return 0;
              }
      });
        if (swll==0) {
        event.preventDefault(); 
        console.log(swll);
          
      }
      var result = await resolveAfter2Seconds();
      //event.preventDefault(); 
    
             }
       });

    
  
    

// document.querySelector('#from1').addEventListener('submit', function(e) {
//   var form = this;
  
//   e.preventDefault();
  
//   swal({
//       title: "Are you sure?",
//       text: "Once deleted, you will not be able to recover this user!",
//       icon: "warning",
//       buttons: [
//         'No, cancel it!',
//         'Yes, I am sure!'
//       ],
//       dangerMode: true,
//     }).then(function(isConfirm) {
//       if (isConfirm) {
//         form.submit();
//       } else {
//         //swal("Cancelled", "Your imaginary file is safe :)", "error");
//       }
//     });
// });
  });